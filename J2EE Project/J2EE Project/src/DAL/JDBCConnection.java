package DAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;

import BL.ReportData;

public class JDBCConnection implements GasStationDB {
	private static Connection con;
	// JDBC driver name and database URL
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost/gasstation";
	// Database credentials
	private static final String USER = "root";
	private static final String PASS = "";

	public void connectToDB() {
	    con = null;
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected to DB thru JDBC");
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			System.out.println("Error: unable to load driver class!!!");
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void insertFuelTransactionIntoDB(int pumpNumber, double profit) {
		CallableStatement stmt = null;
		try {
			String sql = "{call add_fuel_transaction (?, ?)}";
			// calling the DB - defined stored procedure
			stmt = con.prepareCall(sql);
			// Setting the input parameters	
			stmt.setInt(1, pumpNumber); 
			stmt.setDouble(2, profit);
			// Executing the stored procedure
			stmt.execute();
		    stmt.close();		
		} catch (SQLException se) {
			try {
				stmt.close();
			} catch (SQLException e) {}
		} 
	}  // insertFuelTransactionIntoDB
	
	public synchronized void insertCleanServiceTransactionIntoDB(int teamNumber, double profit) {
		CallableStatement stmt = null;
		try {
			String sql = "{call add_cleanservice_transaction (?, ?)}";
			// calling the DB - defined stored procedure
			stmt = con.prepareCall(sql);
			// Setting the input parameters
			stmt.setInt(1, teamNumber); 
			stmt.setDouble(2, profit);
			// Executing the stored procedure
			stmt.execute();
		    stmt.close();		
		} catch (SQLException se) {
			try {
				stmt.close();
			} catch (SQLException e) {}
		} 
	}  // insertCleanServiceTransactionIntoDB
	
	// this function returns the earliest/last year of reports from the DB
	// for the earliest year the type will be MIN, for the last it will be MAX
	@SuppressWarnings("deprecation")
	public int getRelevantYear(String type) {
		Date year_as_date = null;
		int year_as_int = 1900;
		ResultSet rs = null;
		Statement st = null;
		String sql = null;
		try {
			st = con.createStatement();
			if (type.equals("min")) {
				sql = "SELECT MIN(Date_Fueled)" +
					 " FROM fuel_transaction ";
				}
			else if (type.equals("max")) {
				sql = "SELECT MAX(Date_Fueled)" +
					 " FROM fuel_transaction ";
			}				
			rs = st.executeQuery(sql);
			rs.next();
			year_as_date = rs.getDate(1);
			year_as_int = year_as_int + year_as_date.getYear();
			st.close();
			rs.close();
		} catch (SQLException se) {
			try {
				st.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}		
		}
		return year_as_int;
	} // getRelevantYear
	
	// type        - pump report / clean service report
	// accordingTo - show all / according to # (pump# or team#)
	// number      - pump# / clean# itself
	public ArrayList<ReportData> getReport(String numberType, String accordingTo, int number) {
		ArrayList<ReportData> tableRows = new ArrayList<ReportData>();
		ResultSet rs = null;
		Statement st = null;
		String sql = null;
		try {
			st = con.createStatement();
			if (numberType.equals("Pump #")) {
				if (accordingTo.equals("showAll")) {
					sql = "SELECT * FROM fuel_transaction ORDER BY 'desc'";
				}
				else if (accordingTo.equals("accordingTo#")) {
					sql = "SELECT * FROM fuel_transaction " + 
						 " WHERE Pump_Number = " + number + " ORDER BY 'desc'";
				}
			}
				
			else if (numberType.equals("Team #")) {
				if (accordingTo.equals("showAll")) {
					sql = "SELECT * FROM cleanservice_transaction ORDER BY 'desc'";
				}
				else if (accordingTo.equals("accordingTo#")) {
					sql = "SELECT * FROM cleanservice_transaction " + 
						 " WHERE Team_Number = " + number + " ORDER BY 'desc'";
				}
			}		
			rs = st.executeQuery(sql);
			Integer num = 0;
			Double profit = 0.0;
			Date date = null;
			Time time = null;
			while (rs.next()) {
				if (numberType.equals("Pump #")) {
					 num = rs.getInt("Pump_Number");
					 profit = rs.getDouble("Pump_profit");
					 date = rs.getDate("Date_fueled");
					 time = rs.getTime("Time_fueled");
				}
				else if (numberType.equals("Team #")){
					num = rs.getInt("Team_Number");
					profit = rs.getDouble("CleanService_Profit");
					date = rs.getDate("Date_Cleaned");
					time = rs.getTime("Time_Cleaned");
				}			
				ReportData rd = new ReportData(num, profit, date, time);
				tableRows.add(rd);
			}
			rs.close();
			st.close();
		} catch (SQLException se) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}		
		}
		return tableRows;
	} // getReport
	
	
	public ArrayList<ReportData> getReportAccordingToDates(String numberType, String time1, String time2, String date1,String date2) {
		ArrayList<ReportData> tableRows = new ArrayList<ReportData>();
		ResultSet rs = null;
		Statement st = null;
		String sql = null;
		try {
			st = con.createStatement();
			if (numberType.equals("Pump #")) {
				sql = "SELECT * FROM fuel_transaction " + " WHERE Time_Fueled between " + time1 + " and " + time2
						+ " and Date_Fueled between " + date1 + " and " + date2;
			}

			else if (numberType.equals("Team #")) {
				sql = "SELECT * FROM cleanservice_transaction " + " WHERE Time_Cleaned  between " + time1 + " and " + time2
						+ " and Date_Cleaned between " + date1 + " and " + date2;
			}		
			rs = st.executeQuery(sql);
			Integer num = 0;
			Double profit = 0.0;
			Date date = null;
			Time time = null;
			while (rs.next()) {
				if (numberType.equals("Pump #")) {
					 num = rs.getInt("Pump_Number");
					 profit = rs.getDouble("Pump_profit");
					 date = rs.getDate("Date_fueled");
					 time = rs.getTime("Time_fueled");
				}
				else if (numberType.equals("Team #")){
					num = rs.getInt("Team_Number");
					profit = rs.getDouble("CleanService_Profit");
					date = rs.getDate("Date_Cleaned");
					time = rs.getTime("Time_Cleaned");
				}			
				ReportData rd = new ReportData(num, profit, date, time);
				tableRows.add(rd);
			}
			rs.close();
			st.close();
		} catch (SQLException se) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}		
		}
		return tableRows;
	} // getReportAccordingToDates
	
	// this method returns the number of pumps/teams
	public ArrayList<Integer> getNumberOfPumpsOrTeams(String numberType) {
		ArrayList<Integer> numArr = new ArrayList<Integer>();
		ResultSet rs = null;
		Statement st = null;
		String sql = null;
		try {
			st = con.createStatement();
			if (numberType.equals("Pump #")) {
				sql = "SELECT pump_number FROM pump";		
				rs = st.executeQuery(sql);
				while (rs.next()) {
					numArr.add(rs.getInt("Pump_Number"));
				}
			}			
			else if (numberType.equals("Team #")) {
				sql = "SELECT team_number FROM cleanservice";
				rs = st.executeQuery(sql);
				while (rs.next()) {
					numArr.add(rs.getInt("Team_Number"));
				}
			}		
			rs.close();
			st.close();
		} catch (SQLException se) {
			try {
				st.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}		
		}
		return numArr;
	} // getReport
	
	public void closeConnection() {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isConnected() {
		return con != null;
	}
}