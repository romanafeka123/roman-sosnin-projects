package DAL;

import java.util.ArrayList;

import BL.ReportData;

public interface GasStationDB {
	
	public void connectToDB();
	
	public void insertFuelTransactionIntoDB(int pumpNumber, double profit);
	
	public void insertCleanServiceTransactionIntoDB(int teamNumber, double profit);
	
	public int getRelevantYear(String type);
	
	public ArrayList<ReportData> getReport(String numberType, String accordingTo, int number);
	
	public ArrayList<ReportData> getReportAccordingToDates(String numberType, String time1, String time2, String date1, String date2);
	
	public ArrayList<Integer> getNumberOfPumpsOrTeams(String numberType);
	
	public void closeConnection();
	
	public boolean isConnected();
}
