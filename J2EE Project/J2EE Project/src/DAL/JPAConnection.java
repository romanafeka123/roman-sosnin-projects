package DAL;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import BL.ReportData;

public class JPAConnection implements GasStationDB{
	
	private static final String	PERSISTENCE_UNIT_NAME = "gasStationPU";

	@Override
	public void connectToDB() {
		System.out.println("Connected to DB thru JPA");
	}

	@Override
	public synchronized void insertFuelTransactionIntoDB(int pumpNumber, double profit) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();	
		StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("add_fuel_transaction");
		storedProcedure.registerStoredProcedureParameter("Pump_Number", Integer.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("Pump_Profit", Double.class, ParameterMode.IN);
		storedProcedure.setParameter("Pump_Number", pumpNumber);
		storedProcedure.setParameter("Pump_Profit", profit);
		storedProcedure.execute();
		em.getTransaction().commit();
        em.close();
	}

	@Override
	public synchronized void insertCleanServiceTransactionIntoDB(int teamNumber, double profit) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();	
		StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("add_cleanservice_transaction");
		storedProcedure.registerStoredProcedureParameter("Team_Number", Integer.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("CleanService_Profit", Double.class, ParameterMode.IN);
		storedProcedure.setParameter("Team_Number", teamNumber);
		storedProcedure.setParameter("CleanService_Profit", profit);
		storedProcedure.execute();
		em.getTransaction().commit();
        em.close();
	}

	@Override
	public int getRelevantYear(String type) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		
		Date year_as_date = null;
		int year_as_int = 1900;
		String sql = null;

		if (type.equals("min")) {
			sql = "SELECT MIN(Date_Fueled) FROM fuel_transaction ";
		} else if (type.equals("max")) {
			sql = "SELECT MAX(Date_Fueled) FROM fuel_transaction ";
		}

		Query query = em.createNativeQuery(sql);
		if (query.getSingleResult() != null)
			year_as_date = (Date) query.getSingleResult();

		year_as_int = year_as_int + year_as_date.getYear();
		em.close();
		return year_as_int;
	}

	@Override
	public ArrayList<ReportData> getReport(String numberType, String accordingTo, int number) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		String sql = null;
		if (numberType.equals("Pump #")) {
			if (accordingTo.equals("showAll")) {
				sql = "SELECT * FROM fuel_transaction ORDER BY 'desc'";
			} else if (accordingTo.equals("accordingTo#")) {
				sql = "SELECT * FROM fuel_transaction " + " WHERE Pump_Number = " + number + " ORDER BY 'desc'";
			}
		}

		else if (numberType.equals("Team #")) {
			if (accordingTo.equals("showAll")) {
				sql = "SELECT * FROM cleanservice_transaction ORDER BY 'desc'";
			} else if (accordingTo.equals("accordingTo#")) {
				sql = "SELECT * FROM cleanservice_transaction " + " WHERE Team_Number = " + number + " ORDER BY 'desc'";
			}
		}
		List<Object[]> results = em.createNativeQuery(sql).getResultList();
		ArrayList<ReportData> tableRows = new ArrayList<ReportData>();
		Integer num = 0;
		Double profit = 0.0;
		Date date = null;
		Time time = null;
		for (int i = 0; i < results.size(); i++) {
			num = (Integer) results.get(i)[0];
	        profit = (Double) results.get(i)[1];
	        date = (Date) results.get(i)[2];
	        time = (Time) results.get(i)[3];
	        ReportData rd = new ReportData(num, profit, date, time);
			tableRows.add(rd);
		}
		em.close();
		return tableRows;
	}

	
	@Override
	public ArrayList<ReportData> getReportAccordingToDates(String numberType, String time1, String time2, String date1,String date2) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		String sql = null;
		if (numberType.equals("Pump #")) {
			sql = "SELECT * FROM fuel_transaction " + " WHERE Time_Fueled between " + time1 + " and " + time2
					+ " and Date_Fueled between " + date1 + " and " + date2;
		}

		else if (numberType.equals("Team #")) {
			sql = "SELECT * FROM cleanservice_transaction " + " WHERE Time_Cleaned  between " + time1 + " and " + time2
					+ " and Date_Cleaned between " + date1 + " and " + date2;
		}	
		List<Object[]> results = em.createNativeQuery(sql).getResultList();
		ArrayList<ReportData> tableRows = new ArrayList<ReportData>();
		Integer num = 0;
		Double profit = 0.0;
		Date date = null;
		Time time = null;
		for (int i = 0; i < results.size(); i++) {
			num = (Integer) results.get(i)[0];
	        profit = (Double) results.get(i)[1];
	        date = (Date) results.get(i)[2];
	        time = (Time) results.get(i)[3];
	        ReportData rd = new ReportData(num, profit, date, time);
			tableRows.add(rd);
		}
		em.close();
		return tableRows;
	}

	@Override
	public ArrayList<Integer> getNumberOfPumpsOrTeams(String numberType) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		Query query = null;
		String sql = null;
		if (numberType.equals("Pump #")) {
			sql = "SELECT pump_number FROM pump";
			query = em.createNativeQuery(sql);
		} 
		else if (numberType.equals("Team #")) {
			sql = "SELECT team_number FROM cleanservice";
			query = em.createNativeQuery(sql);			
		}
		List<Integer> numList = query.getResultList();
		ArrayList<Integer> numArrList = new ArrayList<Integer>();
		numArrList.addAll(numList);
		em.close();
		return numArrList;
	}

	@Override
	public void closeConnection() {		
	}

	@Override
	public boolean isConnected() {
		return true;
	}
}
