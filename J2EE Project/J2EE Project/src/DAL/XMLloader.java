package DAL;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import BL.Car;
import BL.GasStation;

public class XMLloader {

	private String fileURI;
	private Document document;

	public XMLloader(String fileURI) {
		this.fileURI = fileURI;
		String dir = System.getProperty("user.dir"); // get absolute path
		try {
			File xmlFile = new File(dir + fileURI);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(xmlFile);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	public String getFileURI() {
		return fileURI;
	}

	public ArrayList<Car> loadCars() throws SecurityException, IOException {ArrayList<Car> cars = new ArrayList<Car>();
		int id = -1;
		int numOfLiters = -1;
		int pumpNum = -1;
		boolean wantCleaning = false;
		boolean wantFuel = false;

		NodeList nList = document.getElementsByTagName("Car");
		Car car = null;
		for (int i = 0; i < nList.getLength(); i++) {
			Node node = nList.item(i);
			if (node.hasChildNodes()) {
				wantFuel = true;
				Element wantsFuel = (Element) node.getFirstChild()
						.getNextSibling();
				numOfLiters = Integer.parseInt(wantsFuel
						.getAttribute("numOfLiters"));
				pumpNum = Integer.parseInt(wantsFuel.getAttribute("pumpNum"));
			}
			
			Element carEl = (Element) node;
			id = Integer.parseInt(carEl.getAttribute("id"));
			if (carEl.hasAttribute("wantCleaning")) {
				wantCleaning = Boolean.parseBoolean(carEl
						.getAttribute("wantCleaning"));
			}		
			////first we set the gas station null
			// later when we call that function we set the gas station for each car
			car = new Car(null, id, wantCleaning, wantFuel, pumpNum, numOfLiters + 0.0, false);																					
			cars.add(car);
			// delete values
			id = -1;
			numOfLiters = -1;
			pumpNum = -1;
			wantCleaning = false;
			wantFuel = false;
		}
		return cars;
	}
	
	/*this method creates a new Gas Station from the file*/
	public GasStation loadGasStation() throws SecurityException, IOException{
		double pricePerLiter;
		double washingPrice;
		int numberOfCleaningTeam;
		int numOfPumps;
		int maxMainFuelCapacity;
		int currentMainFuelCapacity;
		int stationId;
		String stationName;

		NodeList nList = document.getElementsByTagName("GasStation");
		Element stationEl = (Element) nList.item(0);

		numOfPumps = Integer.parseInt(stationEl.getAttribute("numOfPumps"));
		pricePerLiter = Double.parseDouble(stationEl.getAttribute("pricePerLiter"));
		stationName = stationEl.getAttribute("name");
		stationId = Integer.parseInt(stationEl.getAttribute("id"));
		
		nList = document.getElementsByTagName("CleaningService");
		Element clteamEl = (Element) nList.item(0);
		numberOfCleaningTeam = Integer.parseInt(clteamEl.getAttribute("numOfTeams"));
		washingPrice = Double.parseDouble(clteamEl.getAttribute("price"));

		nList = document.getElementsByTagName("MainFuelPool");
		Element poolEl = (Element) nList.item(0);
		maxMainFuelCapacity = Integer.parseInt(poolEl.getAttribute("maxCapacity"));
		currentMainFuelCapacity = Integer.parseInt(poolEl.getAttribute("currentCapacity"));

		return new GasStation(stationId, stationName, maxMainFuelCapacity,currentMainFuelCapacity,
				numOfPumps, pricePerLiter,numberOfCleaningTeam, washingPrice);
	}
}
