package DAL;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import BL.ReportData;


public class DBController {
	private static GasStationDB connection;

	public DBController() {
		
		// create the variable from the configuration
		@SuppressWarnings("resource")
		ApplicationContext theContext = new ClassPathXmlApplicationContext("DBconnection.xml");

		// load the variable
		connection = (GasStationDB) theContext.getBean("JPAconnectionType");
		connection.connectToDB();
	} 

	public GasStationDB getConnection() {
		return connection;
	}
	
	public void connectToDB() {
		connection.connectToDB();
	}
	
	public void insertFuelTransactionIntoDB(int pumpNumber, double profit) {
		connection.insertFuelTransactionIntoDB(pumpNumber, profit);
	}
	
	public void insertCleanServiceTransactionIntoDB(int teamNumber, double profit) {
		connection.insertCleanServiceTransactionIntoDB(teamNumber, profit);
	}
	
	public int getRelevantYear(String type) {
		return connection.getRelevantYear(type);
	}
	
	public ArrayList<ReportData> getReport(String numberType, String accordingTo, int number) {
		return connection.getReport(numberType, accordingTo, number);
	}
	
	public ArrayList<ReportData> getReportAccordingToDates(String numberType, String time1, String time2, String date1, String date2) {
		return connection.getReportAccordingToDates(numberType, time1, time2, date1, date2);
	}
	
	public ArrayList<Integer> getNumberOfPumpsOrTeams(String numberType) {
		return connection.getNumberOfPumpsOrTeams(numberType);
	}
	
	public void closeConnection() {
		connection.closeConnection();
	}
	
	public boolean isConnected() {
		return connection.isConnected();
	}
}
