package DAL.Tables;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Pump
 *
 */
@Entity

public class Pump implements Serializable {

	   
	@Id
	private int Pump_Number;
	private static final long serialVersionUID = 1L;

	public Pump() {
		super();
	}   
	public int getPump_Number() {
		return this.Pump_Number;
	}

	public void setPump_Number(int Pump_Number) {
		this.Pump_Number = Pump_Number;
	}
   
}
