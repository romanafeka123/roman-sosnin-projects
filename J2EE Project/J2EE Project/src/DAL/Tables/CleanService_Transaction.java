package DAL.Tables;

import java.io.Serializable;
import java.sql.Time;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CleanService_Transaction
 *
 */
@Entity

public class CleanService_Transaction implements Serializable {

	
	private int Team_Number;
	private double CleanService_Profit;
	private Time Time_Cleaned;   
	@Id
	private int CS_TRANSACTION_ID;
	private static final long serialVersionUID = 1L;

	public CleanService_Transaction() {
		super();
	}   
	public int getTeam_Number() {
		return this.Team_Number;
	}

	public void setTeam_Number(int Team_Number) {
		this.Team_Number = Team_Number;
	}   
	public double getCleanService_Profit() {
		return this.CleanService_Profit;
	}

	public void setCleanService_Profit(double CleanService_Profit) {
		this.CleanService_Profit = CleanService_Profit;
	}   
	public Time getTime_Cleaned() {
		return this.Time_Cleaned;
	}

	public void setTime_Cleaned(Time Time_Cleaned) {
		this.Time_Cleaned = Time_Cleaned;
	}   
	public int getCS_TRANSACTION_ID() {
		return this.CS_TRANSACTION_ID;
	}

	public void setCS_TRANSACTION_ID(int CS_TRANSACTION_ID) {
		this.CS_TRANSACTION_ID = CS_TRANSACTION_ID;
	}
   
}
