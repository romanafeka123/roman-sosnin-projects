package DAL.Tables;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Fuel_Transaction
 *
 */
@Entity

public class Fuel_Transaction implements Serializable {

	
	private int Pump_Number;
	private double Pump_Profit;
	private Date Date_Fueled;
	private Time Time_Fueled;   
	@Id
	private int PUMP_TRANSACTION_ID;
	private static final long serialVersionUID = 1L;

	public Fuel_Transaction() {
		super();
	}   
	public int getPump_Number() {
		return this.Pump_Number;
	}

	public void setPump_Number(int Pump_Number) {
		this.Pump_Number = Pump_Number;
	}   
	public double getPump_Profit() {
		return this.Pump_Profit;
	}

	public void setPump_Profit(double Pump_Profit) {
		this.Pump_Profit = Pump_Profit;
	}   
	public Date getDate_Fueled() {
		return this.Date_Fueled;
	}

	public void setDate_Fueled(Date Date_Fueled) {
		this.Date_Fueled = Date_Fueled;
	}   
	public Time getTime_Fueled() {
		return this.Time_Fueled;
	}

	public void setTime_Fueled(Time Time_Fueled) {
		this.Time_Fueled = Time_Fueled;
	}   
	public int getPUMP_TRANSACTION_ID() {
		return this.PUMP_TRANSACTION_ID;
	}

	public void setPUMP_TRANSACTION_ID(int PUMP_TRANSACTION_ID) {
		this.PUMP_TRANSACTION_ID = PUMP_TRANSACTION_ID;
	}
   
}
