package DAL.Tables;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CleanService
 *
 */
@Entity

public class CleanService implements Serializable {

	   
	@Id
	private int Team_Number;
	private static final long serialVersionUID = 1L;

	public CleanService() {
		super();
	}   
	public int getTeam_Number() {
		return this.Team_Number;
	}

	public void setTeam_Number(int Team_Number) {
		this.Team_Number = Team_Number;
	}
   
}
