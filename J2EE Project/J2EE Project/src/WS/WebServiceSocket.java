package WS;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JOptionPane;

import ServerRequests.AmountOfMoneyPaidRequest;
import ServerRequests.AmountOfMoneyPaidResponse;
import ServerRequests.CloseConnectionRequest;

public class WebServiceSocket {

	private final int SERVER_PORT;
	private final String SERVER_ADDRESS;
	private Socket socket;
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;
	private boolean connectedToServer;

	public WebServiceSocket(String addr, int port) {
		SERVER_ADDRESS = addr;
		SERVER_PORT = port;
	}

	public boolean connect() {
		try {
			socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
			if (!socket.isConnected()) {
				return false;
			}
			outStream = new ObjectOutputStream(socket.getOutputStream());
			inStream = new ObjectInputStream(socket.getInputStream());
			connectedToServer = true;
		} catch (IOException e) {
			connectedToServer = false;
		}
		return connectedToServer;
	}

	public boolean disconnect() {
		try {
			outStream.writeObject(new CloseConnectionRequest());
			connectedToServer = false;
		} catch (IOException e) {
		}
		return connectedToServer;
	}

	public void inquireAmountOfMoneyPaidRequest(int carID) {
		if (!isConnected())
			return;
		AmountOfMoneyPaidRequest req = new AmountOfMoneyPaidRequest(carID);
		try {
			outStream.writeObject(req);
		} catch (IOException e) {
		}
	}

	public double listenToReplyFromServer() {
		if (!isConnected()) {
			return -1.0;
		}
		try {
			boolean connected = true;
			do {
				Object obj = inStream.readObject();
				if (obj instanceof AmountOfMoneyPaidResponse) {
					AmountOfMoneyPaidResponse msg = (AmountOfMoneyPaidResponse) obj;
					return msg.getMoneyPaid();
				}
				else if (obj instanceof CloseConnectionRequest) {
					connectedToServer = false;
					return -1.0;
				}
			} while (connected);
		} catch (EOFException e) {
			connectedToServer = false;
			JOptionPane.showMessageDialog(null, "Server Closed");
		} catch (ClassNotFoundException e) {
		} catch (IOException e) {
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return -1.0;
	}

	public boolean isConnected() {
		return connectedToServer;
	}
}
