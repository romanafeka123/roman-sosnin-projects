package WS;

public class WebServicesServer {
	
	public double inquireAmountOfMoneyPaid(int carID) {
		WebServiceSocket wsSocket = new WebServiceSocket("localhost", 7000);
		if (!wsSocket.isConnected()) {
			wsSocket.connect();
		}
		if (wsSocket.isConnected()) {
			wsSocket.inquireAmountOfMoneyPaidRequest(carID);
			double moneyPaid = wsSocket.listenToReplyFromServer();
			wsSocket.disconnect();
			return moneyPaid;
		}
		return -1.0;
	}
}	