package WS;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;

public class WebServiceClient {
	
	public WebServiceClient() {
	}
	
	public double callWebService(int carID) {
		
		WebServicesServerStub service;
		try {
			service = new WebServicesServerStub();
			WebServicesServerStub.InquireAmountOfMoneyPaid params = new WebServicesServerStub.InquireAmountOfMoneyPaid();
			params.setCarID(carID);
			double res = service.inquireAmountOfMoneyPaid(params).get_return();
			return res;
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return -1.0;
	}

}
