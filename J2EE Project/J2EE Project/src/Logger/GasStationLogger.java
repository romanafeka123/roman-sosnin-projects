package Logger;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

// Design pattern used: Singleton
public class GasStationLogger {
     
	public static final String LOGS_FOLDER	= "logs/";
	private static GasStationLogger	gsLogger;	
	private Logger theLogger;

	private GasStationLogger() {
		theLogger = Logger.getLogger("myLogger");
		theLogger.setUseParentHandlers(false);
		try {
			createHandler(new FileHandler(LOGS_FOLDER + "Gas Station.txt"), null, null);
		} catch (SecurityException e) {			
		} catch (IOException e) {}
	    }

	public static GasStationLogger getInstance() {
		if (gsLogger == null) {
			gsLogger = new GasStationLogger();
		}
		return gsLogger;
	}

	public void writeLog(String msg, Object objectToWrite) {
		theLogger.log(Level.INFO, msg, objectToWrite);
	}

	public Logger getLogger() {
		return theLogger;
	}

	public void createHandler(FileHandler fileHandler, MyObjectFilter myFilter,
	Formatter newFormatter) {
		if (myFilter != null){
			fileHandler.setFilter(myFilter);
		}
		if (newFormatter != null){
			fileHandler.setFormatter(newFormatter);
		}
		if (newFormatter == null){
			fileHandler.setFormatter(new MyFormat());
		}
		if (fileHandler != null) {
			theLogger.addHandler(fileHandler);
		}
	}

	public void closeHandlers() {
		for (Handler h : theLogger.getHandlers()) {
			h.close();
		}
	}

	public void cleanLogFolder() {
		File dir = new File(LOGS_FOLDER);
		deleteDir(dir);
		dir.mkdir();
	}

	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0 ; i < children.length ; i++) {
				boolean success = deleteDir(new File(dir, children [i]));
				if (!success) {
					return false;
				}
			}
		}
		// The directory is now empty, so delete it
		return dir.delete();
	}
}
