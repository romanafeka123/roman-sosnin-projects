package GUI;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class Column3 extends VBox {

	private ScrollPane scrPane;
	private VBox vboxInScrPane;
	private Label headerLbl;
	private Label dashLbl;

	public Column3() {
		scrPane = new ScrollPane();
		scrPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrPane.setMinSize(140, 200);
		scrPane.setMaxSize(140, 200);
		scrPane.setId("pane");
		
		headerLbl = new Label("      Fueling Up");
		headerLbl.setId("header-label");
		dashLbl = new Label("------------------------");
		dashLbl.setId("dash-label");
	
		this.getChildren().addAll(scrPane);	
		this.setAlignment(Pos.TOP_CENTER);
		this.setMinWidth(140);

		vboxInScrPane = new VBox(0);
		vboxInScrPane.getChildren().addAll(headerLbl, dashLbl);
		scrPane.setContent(vboxInScrPane);
	}
	
	public void carIsFuelingUpEvent(int carID, int pumpID) {
		final Label lbl = new Label("Car #" + carID + " is at pump #" + pumpID);
		lbl.setId("event-label-small-size");
		// it is impossible to update the UI, from a thread, that is not the Application thread
		Platform.runLater(new Runnable() {
		    @Override
		    public void run() {
		    	vboxInScrPane.getChildren().add(lbl);
		    	addImage();
		    }
		});	
	}
	
	// this function deletes the relevant labels from the panel
	public void carFinishedFuelingUpEvent(final int carID) {
		// it is impossible to update the UI, from a thread, that is not the Application thread
		Platform.runLater(new Runnable() {
		    @Override
		    public void run() {    	
				int numOfChildren = vboxInScrPane.getChildren().size();
				int indexToDelete = 0;
				for (int i = 2; i < numOfChildren; i++) {
					if (Integer.parseInt(((Label)vboxInScrPane.getChildren().get(i)).getText().substring(5, 8)) == carID) {
					    indexToDelete = i; 
					}
				}	
				if (indexToDelete != 0) {
					vboxInScrPane.getChildren().remove(indexToDelete);
					removeImage();   
				}
		    }
		});	
	}
	
	private void addImage() {
		ImageView imv = new ImageView();
		Image image = new Image(Column3.class.getResourceAsStream("images/gsPumping.gif"));
		imv.setImage(image);
		this.getChildren().addAll(imv);	
	}
	
	private void removeImage() {
		this.getChildren().remove(this.getChildren().get(this.getChildren().size()-1));
	}
}
