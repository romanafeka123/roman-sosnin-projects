package GUI;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class Column1 extends VBox {

	private ScrollPane scrPane;
	private VBox mainPanel;
	private VBox vboxInScrPane;
	private Label headerLbl;
	private Label dashLbl;
	private boolean fillingTankImageSet = false;

	public Column1() {
		scrPane = new ScrollPane();
		scrPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrPane.setMinSize(318, 300);
		scrPane.setMaxSize(318, 300);
		scrPane.setId("pane");
		
		headerLbl = new Label("                      Cars entered");
		headerLbl.setId("header-label");
		dashLbl = new Label("-------------------------------------------------------");
		dashLbl.setId("dash-label");

		mainPanel = new VBox(0);
		mainPanel.getChildren().addAll(scrPane);
		setImage("images/welcome.jpg", "n");
		
		this.getChildren().add(mainPanel);
		
		this.setMinWidth(200);

		vboxInScrPane = new VBox(0);
		vboxInScrPane.getChildren().addAll(headerLbl, dashLbl);
		scrPane.setContent(vboxInScrPane);
	}
	
	public void carArrivedEvent(int id) {
		final Label lbl = new Label("Car #" + id + " entered");
		lbl.setId("event-label-big-size");
		// it is impossible to update the UI, from a thread, that is not the Application thread
		Platform.runLater(new Runnable() {
		    @Override
		    public void run() {
		    	vboxInScrPane.getChildren().add(lbl);
		    }
		});	
	}
	
	public void setImage(final String src, final String flag) {
		if (src.equals("images/fillGasTank.JPG"))
			fillingTankImageSet = true;
		else
			fillingTankImageSet = false;
		Platform.runLater(new Runnable() {
		    @Override
		    public void run() {
				if (flag.equals("r")) {
					mainPanel.getChildren().remove(mainPanel.getChildren().get(mainPanel.getChildren().size() - 1));
				}
				ImageView imv = new ImageView();
				Image image = new Image(Column1.class.getResourceAsStream(src));
				imv.setImage(image);
				mainPanel.getChildren().addAll(imv);
		    }
		});
	}

	public boolean isFillingTankImageSet() {
		return fillingTankImageSet;
	}
}
