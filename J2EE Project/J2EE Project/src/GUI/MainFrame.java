package GUI;

import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JFrame;

import BL.GasStation;
import BL.Statistics;
import Listeners.GUIEventsListener;
import MVCController.Controller;
import Server.TheServer;
import ServerMessages.DataInitializeMsg;
import ServerRequests.AddCarRequest;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class MainFrame extends Application {
	private Group root;
	private Scene scene;
	private Stage stage;
	private HBox hbox;
	private Vector<GUIEventsListener> listeners;
	private JFrame statsFrame, closeFrame;
	private DBInfoTable dbTableFrame;
	private AddCarFrame addCarFrame;
	private ProgressBar progBar;
	private final double MAX_PROGRESS = 1.0;
	
	private Column1 col1;
	private Column2 col2;
	private Column3 col3;
	private Column4 col4;
	private Column5 col5;
	private Column6 col6;
	private Button fillUpTankBtn, dbInfoBtn, statsBtn, closeBtn, addCarBtn;
	private Statistics stats;
	private int numOfCarsInActionCurrently;
	private int idSize, maxLiters, numOfPumps;
	
	private static TheServer server;
	
	/** For synchronizing purposes **/
	private Lock lock;
	private CountDownLatch latch;
	private static CountDownLatch servletLatch;
	
	public MainFrame() {
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		stage.setTitle("Gas Station");
		root = new Group();
		scene = new Scene(root, 1350, 540);
		scene.getStylesheets().add(MainFrame.class.getResource("TheStyle.css").toExternalForm());
		hbox = new HBox(0);
		listeners = new Vector<GUIEventsListener>();
		stats = new Statistics();
		numOfCarsInActionCurrently = 0;
		
		progBar = new ProgressBar();
        progBar.setMinSize(200, 30);
		
		col1 = new Column1();
		col2 = new Column2();
		col3 = new Column3();
		col4 = new Column4();
		col5 = new Column5();
		col6 = new Column6();
		
		addButtons();
	
		hbox.getChildren().addAll(col1, col2, col3, col4, col5, col6);
		root.getChildren().add(hbox);

		stage.setScene(scene);
		stage.show();
		
		setListeners();
		
		lock = new ReentrantLock();
		latch = new CountDownLatch(1);
			
		server = TheServer.runServer(latch);	
				
		lock.lock();
		latch.await();
		lock.unlock();
		servletLatch.countDown();
		
		@SuppressWarnings("unused")
		Controller cont = new Controller(this, server);							
		fireDataInitializeEvent();
	}
	
	public static void runProgram(CountDownLatch latch) {
		servletLatch = latch;
		launch();	
	}
	
	public static GasStation getGasStation() {
		return server.getGasStation();
	}
	
	public TheServer getServer() {
		return server;
	}

	public Column1 getCol1() {
		return col1;
	}
	public Column2 getCol2() {
		return col2;
	}
	public Column3 getCol3() {
		return col3;
	}
	public Column4 getCol4() {
		return col4;
	}
	public Column5 getCol5() {
		return col5;
	}
	public Column6 getCol6() {
		return col6;
	}
	public void registerListener(GUIEventsListener listener) {
		listeners.add(listener);
	}
	
	private void setListeners() {
		fillUpTankBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				boolean canFillTankNow = fireFillTankEventFromGUI();		
				if (!canFillTankNow) {
					PopUpDialog.openPopUpDialog("Some cars are fueling now, wait till they finish!");
					return;
				}
				if (!col1.isFillingTankImageSet() && progBar.getProgress() < MAX_PROGRESS) {
					col1.setImage("images/fillGasTank.JPG", "r");     					
				}
				else if (progBar.getProgress() == MAX_PROGRESS) {
					PopUpDialog.openPopUpDialog("          The main fuel tank is already full!          ");
				}
				else {
					PopUpDialog.openPopUpDialog("      The main fuel tank is already being filled!      ");
				}
			}
		});
		
		statsBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (statsFrame != null) {
					statsFrame.dispose();
        		}
		        statsFrame = new JFrame("Statistics for current run");
		        final JFXPanel fxPanel = new JFXPanel();
		        statsFrame.setContentPane(fxPanel);
		        statsFrame.setSize(350, 250);
		        statsFrame.setVisible(true);
		        statsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		        Platform.runLater(new Runnable() {
		            @Override
		            public void run() {     	          	
		                initStatsFX(fxPanel);
		            }
		       });
			}
		});
		
		closeBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {		        
		        Platform.runLater(new Runnable() {
		            @Override
		            public void run() {		            	
		            	boolean canClose = fireCloseGasStationEvent();
		            	if (canClose) {
			            	if (dbTableFrame != null) {
			            		dbTableFrame.close();
			            	}
			            	if (addCarFrame != null) {
			            		addCarFrame.close();
			            	}
			            	if (statsFrame != null) {
			            		statsFrame.dispose();
			            	}
			            	
		            		if (numOfCarsInActionCurrently != 0) {
		            			closeFrame = new JFrame("Closing Gas Station");
		            			final JFXPanel fxPanel = new JFXPanel();
		            			closeFrame.add(fxPanel);
		            			closeFrame.setSize(300, 250);
		            			closeFrame.setLocation(550, 500);
		            			closeFrame.setVisible(true);
		            			closeFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		            			initFX(fxPanel, "images/closed.jpg");     
		            		}
		            		else if (numOfCarsInActionCurrently == 0) {
		            			if (statsFrame != null)
		            				statsFrame.dispose();
			            		Platform.runLater(new Runnable() {
			            			@Override
			            			public void run() {
			            				stage.close();
			            			}
			            		});	
		            		}
		            	}
		            	else {
		            		PopUpDialog.openPopUpDialog("Can't close gas station while filling the gas tank!");
		            	}
		            }
		       });
			}
		});
		MainFrame mainFrame = this;
		dbInfoBtn.setOnAction(new EventHandler<ActionEvent>() {			
			public void handle(ActionEvent event) {			
		        Platform.runLater(new Runnable() {
		            @Override
		            public void run() {     
		            	if (server.getDbController().isConnected()) {
		            		if (dbTableFrame != null) {
		            			dbTableFrame.close();
		            		}
		            		dbTableFrame = new DBInfoTable(mainFrame);
		            	}
		            	else {
		            		PopUpDialog.openPopUpDialog("Haven't connected to the DB server yet!");
		            	}
		            }
		       });
			}
		});

		addCarBtn.setOnAction(new EventHandler<ActionEvent>() {			
			public void handle(ActionEvent event) {			
		        Platform.runLater(new Runnable() {
		            @Override
		            public void run() {     
	            		if (addCarFrame != null) {
	            			addCarFrame.close();
	            		}
	            		addCarFrame = new AddCarFrame(mainFrame, idSize, maxLiters, numOfPumps);
		            }
		       });
			}
		});
		
	}  // setListeners
	
	private void addButtons() {
		addCarBtn = new Button("Enter car");
		dbInfoBtn = new Button("DB Info");
		fillUpTankBtn = new Button("Fill Up Tank");
        statsBtn = new Button("Show statistics");
        closeBtn = new Button("Close the Gas Station");
        
        addCarBtn.setId("enterCar-btn");
        dbInfoBtn.setId("dbInfo-btn");
        fillUpTankBtn.setId("fillUpTank-btn");
        statsBtn.setId("stats-btn");
        closeBtn.setId("close-btn");
        col6.getChildren().addAll(dbInfoBtn, addCarBtn, fillUpTankBtn, statsBtn, closeBtn);
	}
	
	private boolean fireCloseGasStationEvent() {
		for (GUIEventsListener l : listeners) {
			return l.closeGasStationEvent();
		}
		return false;
	}
	
	public void currentGasTankCapacityEventFired(final double currentCapacity) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				progBar.setProgress(currentCapacity);
			}
		});
	}
	
	private boolean fireFillTankEventFromGUI() {
		for (GUIEventsListener l : listeners) {
			return l.fillTankEvent();
		}
		return false;
	}
	
	private double fireFuelCapacityPercentageInquiryEvent() {
		for (GUIEventsListener l : listeners) {
			return l.fuelCapacityPercentageInquiryEvent();
		}
		return 0;
	}
	
	public void carArrivedEventFired(int carID) {
		col1.carArrivedEvent(carID);
		numOfCarsInActionCurrently++;
	}
	
    public void carLeftEventFired(int carID) {
    	col6.carLeftEvent(carID);
    	numOfCarsInActionCurrently--;
	}
	
	public void setStats(Statistics s) {
		stats = s;
		if (statsFrame != null) {
			if (statsFrame.isActive()) {
				updateStatsFrame();
			}
		}
	}
	
	private void fireDataInitializeEvent() {
		DataInitializeMsg dataMsg = null;
		for (GUIEventsListener l : listeners) {
			dataMsg =  l.fireDataInitializeEvent();
		}
		idSize = dataMsg.getMaxIdSize();
		maxLiters = dataMsg.getMaxLitersToRequest();
		numOfPumps = dataMsg.getNumOfPumps();
	}
	
	public void finishedFillingTankEvent() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (progBar != null)
				progBar.setProgress(MAX_PROGRESS);
			}
		});
		col1.setImage("images/welcome.jpg", "r");	
	}
	
	public boolean fireAddCarFromGUIEvent(AddCarRequest request) {
		for (GUIEventsListener l : listeners) {
			return l.fireAddCarFromGUIEvent(request);
		}
		return false;
	}
	
	public void stationIsEmptyEventFired() {
		final JFrame frame = new JFrame("Program terminating");
        final JFXPanel fxPanel = new JFXPanel();
        frame.add(fxPanel);
        frame.setSize(230, 230);
        frame.setLocation(580, 270);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				initFX(fxPanel, "images/terminating.gif");
			}
		});
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        frame.dispose();
		closeFrame.dispose();
		
        if (statsFrame != null)
		    statsFrame.dispose();
   
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				stage.close();
			}
		});	
	}

	private void updateStatsFrame() {       
		final Label lbl1 = new Label("Cars fueled up:");
		final Label lbl2 = new Label("Cars cleaned:");
		final Label lbl3 = new Label("Fuel Profit:");
		final Label lbl4 = new Label("Clean Profit:");
        final Label lbl5 = new Label(stats.getNumOfCarsFueledUp() + "");
        final Label lbl6 = new Label(stats.getNumOfCarsCleaned() + "");
        final Label lbl7 = new Label(stats.getFuelProfitFormated());
        final Label lbl8 = new Label(stats.getCleanProfit() + "");
        final Label lbl9 = new Label("Fuel Tank Capacity:");
        
        progBar = new ProgressBar();
        progBar.setMinSize(200, 30);
        double currentFuelCapacityPercentage = fireFuelCapacityPercentageInquiryEvent();
        progBar.setProgress(currentFuelCapacityPercentage);
        
        lbl1.setId("header-label");
        lbl2.setId("header-label");
        lbl3.setId("header-label");
        lbl4.setId("header-label");
        lbl5.setId("header-label");
        lbl6.setId("header-label");
        lbl7.setId("header-label");
        lbl8.setId("header-label");
        lbl9.setId("header-label");
        
		Platform.runLater(new Runnable() {
			@Override
			public void run() {	
				GridPane grid = new GridPane();
		    	grid.setAlignment(Pos.CENTER);
		    	grid.setHgap(10);
		    	grid.setVgap(5);
		    	grid.setPadding(new Insets(25, 25, 25, 25));
		    	grid.setId("pane");
		    	
		    	((JFXPanel) statsFrame.getContentPane()).getScene().setRoot(grid);
		    	  				
				((GridPane) ((JFXPanel) statsFrame.getContentPane()).getScene().getRoot()).add(lbl1, 0, 1);
				((GridPane) ((JFXPanel) statsFrame.getContentPane()).getScene().getRoot()).add(lbl2, 0, 2);
				((GridPane) ((JFXPanel) statsFrame.getContentPane()).getScene().getRoot()).add(lbl3, 0, 3);
				((GridPane) ((JFXPanel) statsFrame.getContentPane()).getScene().getRoot()).add(lbl4, 0, 4);	
				((GridPane) ((JFXPanel) statsFrame.getContentPane()).getScene().getRoot()).add(lbl9, 0, 5);
				((GridPane) ((JFXPanel) statsFrame.getContentPane()).getScene().getRoot()).add(progBar, 0, 6);
				((GridPane) ((JFXPanel) statsFrame.getContentPane()).getScene().getRoot()).add(lbl5, 1, 1);
				((GridPane) ((JFXPanel) statsFrame.getContentPane()).getScene().getRoot()).add(lbl6, 1, 2);
				((GridPane) ((JFXPanel) statsFrame.getContentPane()).getScene().getRoot()).add(lbl7, 1, 3);
				((GridPane) ((JFXPanel) statsFrame.getContentPane()).getScene().getRoot()).add(lbl8, 1, 4);          
			}
		});
	}
	
    private void initStatsFX(JFXPanel fxPanel) {
        // This method is invoked on the JavaFX thread   	
    	GridPane grid = new GridPane();
    	grid.setAlignment(Pos.CENTER);
    	grid.setHgap(10);
    	grid.setVgap(5);
    	grid.setPadding(new Insets(25, 25, 25, 25));
    	Scene scene = new Scene(grid, 300, 275);
    	scene.getStylesheets().add(MainFrame.class.getResource("TheStyle.css").toExternalForm());
    	grid.setId("pane");

        Label lbl1 = new Label("Cars fueled up:");
        Label lbl2 = new Label("Cars cleaned:");
        Label lbl3 = new Label("Fuel Profit:");
        Label lbl4 = new Label("Clean Profit:");
        Label lbl5 = new Label(stats.getNumOfCarsFueledUp() + "");
        Label lbl6 = new Label(stats.getNumOfCarsCleaned() + "");
        Label lbl7 = new Label(stats.getFuelProfitFormated());
        Label lbl8 = new Label(stats.getCleanProfit() + "");
        Label lbl9 = new Label("Fuel Tank Capacity:");
        
        lbl1.setId("header-label");
        lbl2.setId("header-label");
        lbl3.setId("header-label");
        lbl4.setId("header-label");
        lbl5.setId("header-label");
        lbl6.setId("header-label");
        lbl7.setId("header-label");
        lbl8.setId("header-label");
        lbl9.setId("header-label");
        
        double currentFuelCapacityPercentage = fireFuelCapacityPercentageInquiryEvent();
        progBar.setProgress(currentFuelCapacityPercentage);
        
    	grid.add(lbl1, 0, 1);
    	grid.add(lbl2, 0, 2);
    	grid.add(lbl3, 0, 3);
    	grid.add(lbl4, 0, 4);
    	grid.add(lbl9, 0, 5);
    	grid.add(progBar, 0, 6);
    	grid.add(lbl5, 1, 1);
    	grid.add(lbl6, 1, 2);
    	grid.add(lbl7, 1, 3);
    	grid.add(lbl8, 1, 4);
    	fxPanel.setScene(scene);
    }
    
    private void initFX(JFXPanel fxPanel, String src) {
        // This method is invoked on the JavaFX thread
    	FlowPane flowPane  = new FlowPane(); 
    	flowPane.setAlignment(Pos.CENTER);   	
    	ImageView imv = new ImageView();
		Image image = new Image(MainFrame.class.getResourceAsStream(src));
		imv.setImage(image);	
    	flowPane.getChildren().add(imv); // child nodes  to add.	
    	Scene scene = new Scene(flowPane, 405, 320);
    	scene.getStylesheets().add(MainFrame.class.getResource("TheStyle.css").toExternalForm());
    	fxPanel.setScene(scene);
    }

	public AddCarFrame getAddCarFrame() {
		return addCarFrame;
	}
	
	
}

