package GUI;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class PopUpDialog {

	public static void openPopUpDialog(String str) {
		final Stage dialogStage = new Stage();
		dialogStage.setTitle("Error");
		dialogStage.setResizable(false);	
		Text text1 = new Text("-----------------------------------------------------------------------------------");
		Text text2 = new Text(str);
		Text text3 = new Text("                                                                                                              ");
		Button btn = new Button("   OK   ");
		Text text4 = new Text("-----------------------------------------------------------------------------------");
		text2.setId("pop-up-dialog-text");
		btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
        });
		FlowPane pane = new FlowPane();
		pane.setId("pane");
		pane.setAlignment(Pos.CENTER);
		pane.getChildren().addAll(text1, text2, text3, btn, text4);
		Scene scene = new Scene(pane, 390, 85);
		scene.getStylesheets().add(MainFrame.class.getResource("TheStyle.css").toExternalForm());
		dialogStage.setScene(scene);
		dialogStage.show();
	}
}
