package GUI;

import javax.swing.JFrame;

import ServerRequests.AddCarRequest;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class AddCarFrame {
 
	private MainFrame mainFrame;  // this field is needed only for AddCarEvent fire
	private JFrame frame;
	private JFXPanel fxPanel;
	private VBox centralPanel;
	private GridPane grid;
	private Label carIdLbl, pumpIdLbl, carLiterLbl, carFuelingLbl, carCleanLbl;
	private TextField carIdTxtFld, carLiterTxtFld, pumpIdTxtFld;
	private RadioButton yesFuelRadBtn, noFuelRadBtn, yesCleanRadBtn, noCleanRadBtn;
	private Button addBtn, cancelBtn;
	private HBox hbox1ForRadBtn, hbox2ForRadBtn, hbox3ForButtons;
	
	private int idSize, maxLiters, numOfPumps;
	private AddCarRequest request;
    
    public AddCarFrame(MainFrame mainFrame, int idSize, int maxLiters, int numOfPumps) {
    	this.mainFrame = mainFrame;
    	this.idSize = idSize;
    	this.maxLiters = maxLiters;
    	this.numOfPumps = numOfPumps;
    	
    	frame = new JFrame("Enter car to gas station");
        fxPanel = new JFXPanel();
        frame.setContentPane(fxPanel);
        frame.setSize(430, 250);
        frame.setLocation(200, 50);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));
        
        createElements();
        addElementsToAreas();
        addListeners();
    	
        centralPanel = new VBox();
        centralPanel.getChildren().addAll(grid, hbox3ForButtons);
    	
    	Scene scene = new Scene(centralPanel, 430, 250);
    	scene.getStylesheets().add(MainFrame.class.getResource("TheStyle.css").toExternalForm());
    	centralPanel.setId("pane");   
        fxPanel.setScene(scene);
    }
    
    private void createElements() {
    	carIdLbl = new Label("Insert car ID:");
    	pumpIdLbl = new Label("Insert pump number:");
    	carLiterLbl = new Label("Insert gas liters for fueling:");
    	carFuelingLbl = new Label("Would you like to fuel up?");
    	carCleanLbl = new Label("Would you like to clean the car?");
    	
    	carIdLbl.setId("dash-label");
    	pumpIdLbl.setId("dash-label");
    	carLiterLbl.setId("dash-label");
    	carFuelingLbl.setId("dash-label");
    	carCleanLbl.setId("dash-label");	
    	carIdTxtFld = new TextField();
    	carLiterTxtFld = new TextField();
    	pumpIdTxtFld = new TextField();
    	
        ToggleGroup group1 = new ToggleGroup();
        ToggleGroup group2 = new ToggleGroup();
        yesFuelRadBtn = new RadioButton("Yes");
    	noFuelRadBtn = new RadioButton("No");
    	yesCleanRadBtn = new RadioButton("Yes");
    	noCleanRadBtn = new RadioButton("No");  	
    	yesFuelRadBtn.setToggleGroup(group1);
    	noFuelRadBtn.setToggleGroup(group1);
    	yesCleanRadBtn.setToggleGroup(group2);
    	noCleanRadBtn.setToggleGroup(group2);	
    	
    	hbox3ForButtons = new HBox();
    	addBtn = new Button("Add");
        cancelBtn = new Button("Cancel");
        addBtn.setId("fillUpTank-btn");
        cancelBtn.setId("fillUpTank-btn");
        hbox3ForButtons.setAlignment(Pos.CENTER);
        hbox3ForButtons.setSpacing(60);
        
        hbox1ForRadBtn = new HBox(); 
        hbox2ForRadBtn = new HBox();       
        hbox1ForRadBtn.setAlignment(Pos.CENTER);
        hbox2ForRadBtn.setAlignment(Pos.CENTER);
        hbox1ForRadBtn.setSpacing(60);
        hbox2ForRadBtn.setSpacing(60);
    }
    
	private void addElementsToAreas() {	
		hbox1ForRadBtn.getChildren().addAll(yesFuelRadBtn, noFuelRadBtn);
		hbox2ForRadBtn.getChildren().addAll(yesCleanRadBtn, noCleanRadBtn);
		
		grid.add(carIdLbl, 0, 0);
		grid.add(carFuelingLbl, 0, 1);
		grid.add(carLiterLbl, 0, 2);
		grid.add(pumpIdLbl, 0, 3);
		grid.add(carCleanLbl, 0, 4);
		
		grid.add(carIdTxtFld, 1, 0);
		grid.add(hbox1ForRadBtn, 1, 1);
		grid.add(carLiterTxtFld, 1, 2);
		grid.add(pumpIdTxtFld, 1, 3);
		grid.add(hbox2ForRadBtn, 1, 4);
		
		hbox3ForButtons.getChildren().addAll(addBtn, cancelBtn);
	}  // addElementsToAreas
	
	private void addListeners() {
		addBtn.setOnAction(new EventHandler<ActionEvent>() {		
			public void handle(ActionEvent event) {
				getCarInfo();
			}
		});
		
		cancelBtn.setOnAction(new EventHandler<ActionEvent>() {		
			public void handle(ActionEvent event) {
				close();
			}
		});
	}  // addListeners

	public void getCarInfo() {
		boolean wantsFuel, wantsClean;
		int carId = -1, amountToFuel = -1, pumpNum = -1;
		String carIDStr = carIdTxtFld.getText();
		String gasAmountStr, pumpStr;		
		
		if ((!yesFuelRadBtn.isSelected() && !noFuelRadBtn.isSelected()
		  && !yesCleanRadBtn.isSelected() && !noCleanRadBtn.isSelected())
		  || (noFuelRadBtn.isSelected() && noCleanRadBtn.isSelected())) {
			PopUpDialog.openPopUpDialog("You must choose an action!");
			return;
		}
		
		if (!yesFuelRadBtn.isSelected() && !noFuelRadBtn.isSelected()) {
			PopUpDialog.openPopUpDialog("You must select either you want fuel or not!");
			return;
		}	
		
		if (carIDStr.length() != idSize) {
			PopUpDialog.openPopUpDialog("Car ID must be " + idSize + " digits!");
			return;
		}
		try {
			carId = Integer.parseInt(carIDStr);
			if (carId <= 0) {
				PopUpDialog.openPopUpDialog("Car's ID must contain only positive integers!");
				return;
			}
			if (carId < 100) {
				PopUpDialog.openPopUpDialog("Car's ID must contain 3 digits!");
				return;
			}
		} catch (IllegalArgumentException ex) {
			PopUpDialog.openPopUpDialog("Car's ID must contain only integers!");
			return;
		}

		if (yesFuelRadBtn.isSelected()) {
			gasAmountStr = carLiterTxtFld.getText();
			try {
				amountToFuel = Integer.parseInt(gasAmountStr);
				if (amountToFuel > maxLiters || amountToFuel < 1) {
					PopUpDialog.openPopUpDialog("Enter a number between " + (maxLiters-maxLiters+1) + " and " + maxLiters);
					return;
				}
			} catch (IllegalArgumentException exp) {
				PopUpDialog.openPopUpDialog("Liters can be only integers!");
				return;
			}

			pumpStr = pumpIdTxtFld.getText();
			try {
				pumpNum = Integer.parseInt(pumpStr);
				if (pumpNum <= 0) {
					PopUpDialog.openPopUpDialog("Pump number must be a positive integer!");
					return;
				}
			} catch (IllegalArgumentException exp) {
				PopUpDialog.openPopUpDialog("Pump number must be a positive integer!");
				return;
			}
			wantsFuel = true;
		} else {
			wantsFuel = false;
		}
		
		if (pumpNum > numOfPumps) {
			PopUpDialog.openPopUpDialog("The number of pumps in the station is: " + numOfPumps + "!");
			return;
		}

		if (!yesCleanRadBtn.isSelected() && !noCleanRadBtn.isSelected()) {
			PopUpDialog.openPopUpDialog("Please choose the option about Clean Service!");
			return;
		}
		
		if (yesCleanRadBtn.isSelected()) {
			wantsClean = true;
		}
		else {
			wantsClean = false;
		}

		request = new AddCarRequest(carId, pumpNum, amountToFuel, wantsFuel, wantsClean);
		boolean isCarAlreadyInStation = mainFrame.fireAddCarFromGUIEvent(request);
		if (isCarAlreadyInStation) {
			PopUpDialog.openPopUpDialog("The car is already in the station! Enter another ID");
			return;
		}
		close();
	}  // gettingCarInfo

	public AddCarRequest getRequest() {
		return request;
	}

	public void close() {
    	frame.dispose();
    }
} 