package GUI;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class Column4 extends VBox {

	private ScrollPane scrPane;
	private VBox vboxInScrPane;
	private Label headerLbl;
	private Label dashLbl;

	public Column4() {
		scrPane = new ScrollPane();
		scrPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrPane.setMinSize(100, 200);
		scrPane.setMaxSize(320, 200);
		scrPane.setId("pane");
		
		headerLbl = new Label("      Washing Machine");
		headerLbl.setId("header-label");
		dashLbl = new Label("-----------------------------------");
		dashLbl.setId("dash-label");

		this.getChildren().addAll(scrPane);
		this.setAlignment(Pos.TOP_CENTER);

		this.setMinWidth(200);

		vboxInScrPane = new VBox(0);
		vboxInScrPane.getChildren().addAll(headerLbl, dashLbl);
		scrPane.setContent(vboxInScrPane);

	}
	
	public void carIsInWashingMachineEvent(int carID) {
		final Label lbl = new Label("Car #" + carID + " is at washing machine");
		lbl.setId("event-label-medium-size");
		// it is impossible to update the UI, from a thread, that is not the Application thread
		Platform.runLater(new Runnable() {
		    @Override
		    public void run() {
		    	vboxInScrPane.getChildren().add(lbl);
		    	showWashingImage();
		    }
		});	
	}
	
	// this function deletes the relevant labels from the panel
	public void carLeftWashingMachineEvent(final int carID) {
		// it is impossible to update the UI, from a thread, that is not the Application thread
		Platform.runLater(new Runnable() {
		    @Override
		    public void run() {    	
				int numOfChildren = vboxInScrPane.getChildren().size();
				int indexToDelete = 0;
				for (int i = 2; i < numOfChildren; i++) {
					if (Integer.parseInt(((Label)vboxInScrPane.getChildren().get(i)).getText().substring(5, 8)) == carID) {
					    indexToDelete = i;
					}
				}
		    	vboxInScrPane.getChildren().remove(indexToDelete);
		    	removeWashingImage();    	
		    }
		});	
	}
	
	private void showWashingImage() {
		ImageView imv = new ImageView();
		Image image = new Image(Column4.class.getResourceAsStream("images/washCar.jpg"));
		imv.setImage(image);
		this.getChildren().addAll(imv);	
	}
	
	private void removeWashingImage() {
		this.getChildren().remove(this.getChildren().get(this.getChildren().size()-1));
	}
}
