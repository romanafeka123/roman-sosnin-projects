package GUI;


import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class Column6 extends VBox {

	private ScrollPane scrPane;
	private VBox vboxInScrPane;
	private Label headerLbl;
	private Label dashLbl;

	public Column6() {
		scrPane = new ScrollPane();
		scrPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrPane.setMinSize(250, 200);
		scrPane.setMaxSize(320, 200);
		scrPane.setId("pane");
		
		headerLbl = new Label("                   Cars left");
		headerLbl.setId("header-label");
		dashLbl = new Label("----------------------------------------------");
		dashLbl.setId("dash-label");
		
		final ImageView imv = new ImageView();
		final Image image = new Image(Column6.class.getResourceAsStream("images/byebye.jpg"));
		imv.setImage(image);
		

		this.getChildren().addAll(scrPane, imv);

		this.setMinWidth(200);
		this.setAlignment(Pos.TOP_CENTER);

		vboxInScrPane = new VBox(0);
		vboxInScrPane.getChildren().addAll(headerLbl, dashLbl);

		scrPane.setContent(vboxInScrPane);
	}
	
	
	public void carLeftEvent(int id) {
		final Label lbl = new Label("Car #" + id + " left");
		lbl.setId("event-label-big-size");
		// it is impossible to update the UI, from a thread, that is not the Application thread
		Platform.runLater(new Runnable() {
		    @Override
		    public void run() {
		    	vboxInScrPane.getChildren().add(lbl);
		    }
		});	
	}
}
