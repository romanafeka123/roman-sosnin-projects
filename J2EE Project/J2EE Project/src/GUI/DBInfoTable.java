package GUI;

import javax.swing.JFrame;

import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;

public class DBInfoTable {
 
	private JFrame frame;
	private JFXPanel fxPanel;
	private HBox hbox;
    private TableViewVBox table1;
    private TableViewVBox table2;
    private MainFrame mainFrame;
    
    public DBInfoTable(MainFrame mainFrame) {	 	
    	this.mainFrame = mainFrame;
    	frame = new JFrame("DB Info");
        fxPanel = new JFXPanel();
        frame.setContentPane(fxPanel);
        frame.setSize(760, 700);
        frame.setLocation(200, 50);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        table1 = new TableViewVBox("Pump #", "Fuel Profit", this.mainFrame);
        table2 = new TableViewVBox("Team #", "Clean Service Profit", this.mainFrame);
        hbox = new HBox();
        hbox.getChildren().addAll(table1, table2);
        
    	Scene scene = new Scene(hbox, 760, 700);
    	scene.getStylesheets().add(MainFrame.class.getResource("TheStyle.css").toExternalForm());
    	hbox.setId("pane");   
        fxPanel.setScene(scene);
    }
    
    public void close() {
    	frame.dispose();
    }
} 