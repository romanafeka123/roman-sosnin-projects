package GUI;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.util.ArrayList;

import BL.ReportData;
import DAL.JDBCConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class TableViewVBox extends VBox {
	
	private Label titleLbl;
	private GridPane grid1;
	private GridPane grid2;
	private TableView<ReportData> table;
    private ObservableList<ReportData> data;
    private TableColumn numberCol, profitCol, dateCol, timeCol;
    private Label incomeTitleLbl, incomeLbl;
    private double income;

    private Label fromLbl, toLbl, timeLbl, dayLbl, monthLbl, yearLbl;
    private ComboBox<Integer> dayCmbBox1, yearCmbBox1; 
    private ComboBox<Integer> dayCmbBox2, yearCmbBox2;
    private ComboBox<String> monthCmbBox1, monthCmbBox2, timeCmbBox1, timeCmbBox2;
    
    private RadioButton showAllRadBtn, numTypeRadBtn, datesRadBtn;
    private ComboBox<Integer> numberTypeCmbBox;
    private Button searchBtn;
    
    private final int NUM_OF_HOURS = 25;
	private final int NUM_OF_DAYS = 31;
	private final int NUM_OF_MONTHS = 12;
	private static ArrayList<String> months = new ArrayList<String>();
	private static ArrayList<String> hours = new ArrayList<String>();
	private ArrayList<ReportData> tableRows;
	private MainFrame mainFrame;
     
    // this numberType can be either pump number or team number
    public TableViewVBox(String numberType, String title, MainFrame mainFrame) {
    	this.mainFrame = mainFrame;
    	titleLbl = new Label(title);
    	titleLbl.setId("dash-label");
    	grid1 = new GridPane();
    	grid1.setAlignment(Pos.CENTER);
    	grid1.setHgap(10);
    	grid1.setVgap(10);
    	grid1.setPadding(new Insets(10, 10, 10, 10));
    	
    	grid2 = new GridPane();
    	grid2.setAlignment(Pos.CENTER);
    	grid2.setHgap(10);
    	grid2.setVgap(10);
    	grid2.setPadding(new Insets(10, 10, 10, 10));
    	table = new TableView<>();
    	data = FXCollections.observableArrayList();
    	incomeTitleLbl = new Label("Total income for the specified dates:");
    	incomeLbl = new Label("");
    	incomeTitleLbl.setId("dash-label");
    	incomeLbl.setId("dash-label");
    	income = 0.0;
    	
    	setMonths();
    	setHours();
    	
    	createUpperArea();
    	createCentralArea(numberType);  	
    	createColumns(numberType);      
    	insertDataIntoComboBoxes(numberType);
    	setInitialValuesInCmbBoxes();
    	addListeners(numberType);
    	addElementsToAreas();
    	disableAllCmbBoxes();
    		
    	setAlignment(Pos.CENTER);
        setSpacing(5);
        setPadding(new Insets(10, 0, 0, 10));
        getChildren().addAll(titleLbl, grid1, grid2, table,incomeTitleLbl, incomeLbl);
    }  // TableViewVBox constructor
    
	private void createColumns(String numberType) {
	    numberCol = new TableColumn<>(numberType);
		numberCol.setMinWidth(30);
		numberCol.setCellValueFactory(new PropertyValueFactory("number"));

		profitCol = new TableColumn<>("Profit");
		profitCol.setMinWidth(50);
		profitCol.setCellValueFactory(new PropertyValueFactory("profit"));

		dateCol = new TableColumn<>("Date");
		dateCol.setMinWidth(100);
		dateCol.setCellValueFactory(new PropertyValueFactory("date"));

		timeCol = new TableColumn<>("Time");
		timeCol.setMinWidth(100);
		timeCol.setCellValueFactory(new PropertyValueFactory("time"));		
	}  // createColumns
	
	private void createUpperArea() {
	    fromLbl = new Label("From");
        toLbl = new Label("To");
        timeLbl = new Label("Time");
        dayLbl = new Label("Day");
        monthLbl = new Label("Month");
        yearLbl = new Label("Year");
	    timeCmbBox1 = new ComboBox<String>();
	    dayCmbBox1 = new ComboBox<Integer>();
	    monthCmbBox1 = new ComboBox<String>();
	    yearCmbBox1 = new ComboBox<Integer>();
	    timeCmbBox2 = new ComboBox<String>();
	    dayCmbBox2 = new ComboBox<Integer>();
	    monthCmbBox2 = new ComboBox<String>();
	    yearCmbBox2	= new ComboBox<Integer>();
	}  // createUpperArea
	
	private void createCentralArea(String numberType) {
		ToggleGroup group = new ToggleGroup();
		showAllRadBtn = new RadioButton("Show all");
		numTypeRadBtn = new RadioButton("According to " + numberType);
		datesRadBtn = new RadioButton("According to dates");
		showAllRadBtn.setToggleGroup(group);
		numTypeRadBtn.setToggleGroup(group);
		datesRadBtn.setToggleGroup(group);
		showAllRadBtn.setSelected(true);
		numberTypeCmbBox = new ComboBox<Integer>();
		searchBtn = new Button("Search");
	}  // createCentralArea
	
	private void insertDataIntoComboBoxes(String numberType) {
		for (int i = 0; i < NUM_OF_HOURS; i++) {
			timeCmbBox1.getItems().add(hours.get(i));
			timeCmbBox2.getItems().add(hours.get(i));
		}
		
		for (int i = 0; i < NUM_OF_DAYS; i++) {
			dayCmbBox1.getItems().add(i+1);
			dayCmbBox2.getItems().add(i+1);
		}
		
		for (int i = 0; i < NUM_OF_MONTHS; i++) {
			monthCmbBox1.getItems().add(months.get(i));
			monthCmbBox2.getItems().add(months.get(i));
		}
		
		// drawing the relevant years of reports from the DB and inserting them into the ComboBoxes
		int year1 = mainFrame.getServer().getDbController().getRelevantYear("min");
		int year2 = mainFrame.getServer().getDbController().getRelevantYear("max");
		for (int i = year1; i <= year2; i++) {
			yearCmbBox1.getItems().add(i);
			yearCmbBox2.getItems().add(i);
		}
		
		ArrayList<Integer> numArr = mainFrame.getServer().getDbController().getNumberOfPumpsOrTeams(numberType);
		for (int i = 0; i < numArr.size(); i++) {
			numberTypeCmbBox.getItems().add(numArr.get(i));
		}
	}  // insertDataIntoComboBoxes
	
	private void addListeners(final String numberType) {
		searchBtn.setOnAction(new EventHandler<ActionEvent>() {		
			public void handle(ActionEvent event) {
				if (numberType.equals("Pump #")) {
					if (showAllRadBtn.isSelected()) {
						tableRows = mainFrame.getServer().getDbController().getReport("Pump #", "showAll", -1);
						showReportOnTable("Pump #");
					} else if (numTypeRadBtn.isSelected()) {
						tableRows = mainFrame.getServer().getDbController().getReport("Pump #", "accordingTo#", numberTypeCmbBox.getValue());
						showReportOnTable("Pump #");
					} else if (datesRadBtn.isSelected()) {
						requestResultsAccordingToDates("Pump #");
					}			
				}
				
				else if (numberType.equals("Team #")) {
					if (showAllRadBtn.isSelected()) {
						tableRows = mainFrame.getServer().getDbController().getReport("Team #", "showAll", -1);
						showReportOnTable("Team #");
					} else if (numTypeRadBtn.isSelected()) {
						tableRows = mainFrame.getServer().getDbController().getReport("Team #", "accordingTo#", numberTypeCmbBox.getValue());
						showReportOnTable("Team #");
					} else if (datesRadBtn.isSelected()) {
						requestResultsAccordingToDates("Team #");
					}				
				}
			}
		});
		
		showAllRadBtn.setOnAction(new EventHandler<ActionEvent>() {	
			public void handle(ActionEvent event) {
				disableAllCmbBoxes();
			}
		});
		
		numTypeRadBtn.setOnAction(new EventHandler<ActionEvent>() {	
			public void handle(ActionEvent event) {
			    timeCmbBox1.setDisable(true);
			    dayCmbBox1.setDisable(true);
			    monthCmbBox1.setDisable(true);
			    yearCmbBox1.setDisable(true);
			    timeCmbBox2.setDisable(true);
			    dayCmbBox2.setDisable(true);
			    monthCmbBox2.setDisable(true);
			    yearCmbBox2.setDisable(true);
			    numberTypeCmbBox.setDisable(false);
			}
		});
		
		datesRadBtn.setOnAction(new EventHandler<ActionEvent>() {	
			public void handle(ActionEvent event) {
			    timeCmbBox1.setDisable(false);
			    dayCmbBox1.setDisable(false);
			    monthCmbBox1.setDisable(false);
			    yearCmbBox1.setDisable(false);
			    timeCmbBox2.setDisable(false);
			    dayCmbBox2.setDisable(false);
			    monthCmbBox2.setDisable(false);
			    yearCmbBox2.setDisable(false);
			    numberTypeCmbBox.setDisable(true);
			}
		});

		// When a month or a year is changed, the number of days should be changed also
		monthCmbBox1.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				int year1 = yearCmbBox1.getValue();
				String month1 = monthCmbBox1.getValue();
				int numOfDays = getNumOfDays(year1, month1);
				dayCmbBox1.getItems().clear();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox1.getItems().add(i + 1);
				}
				dayCmbBox1.setValue(dayCmbBox1.getItems().get(0));
			}
		});
		
		yearCmbBox1.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				int year1 = yearCmbBox1.getValue();
				String month1 = monthCmbBox1.getValue();
				int numOfDays = getNumOfDays(year1, month1);
				dayCmbBox1.getItems().clear();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox1.getItems().add(i + 1);
				}
				dayCmbBox1.setValue(dayCmbBox1.getItems().get(0));
			}
		});
		
		monthCmbBox2.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				int year2 = yearCmbBox2.getValue();
				String month2 = monthCmbBox2.getValue();
				int numOfDays = getNumOfDays(year2, month2);
				dayCmbBox2.getItems().clear();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox2.getItems().add(i + 1);
				}
				dayCmbBox2.setValue(dayCmbBox2.getItems().get(0));
			}
		});
		
		yearCmbBox2.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				int year2 = yearCmbBox2.getValue();
				String month2 = monthCmbBox2.getValue();
				int numOfDays = getNumOfDays(year2, month2);
				dayCmbBox2.getItems().clear();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox2.getItems().add(i + 1);
				}
				dayCmbBox2.setValue(dayCmbBox2.getItems().get(0));
			}
		});
	}  // addListeners
	
	private void showReportOnTable(String numberType) {
		if (tableRows != null) {
			data.clear();
			income = 0.0;
			for (int i = 0; i < tableRows.size(); i++) {
				data.add(tableRows.get(i));
				income = income + tableRows.get(i).getProfit();
			}
			incomeLbl.setText(String.format("%.2f", income) + "$");
		}
	} // showReportOnTable
	
	private void requestResultsAccordingToDates(String numberType) {
		int year1 = yearCmbBox1.getValue();
		int year2 = yearCmbBox2.getValue();
		int month1 = Integer.parseInt(monthCmbBox1.getValue().toString());
		int month2 = Integer.parseInt(monthCmbBox2.getValue().toString());
		int day1 = dayCmbBox1.getValue();
		int day2 = dayCmbBox2.getValue();		
		String tmpTime1 = timeCmbBox1.getValue();
		String tmpTime2 = timeCmbBox2.getValue();
		
		LocalDate fromDate = LocalDate.of(year1, month1, day1);
		LocalDate toDate = LocalDate.of(year2, month2, day2);
		if (fromDate.isAfter(toDate) || (fromDate.isEqual(toDate) &&
				hours.indexOf(tmpTime1) > hours.indexOf(tmpTime2))) {
			PopUpDialog.openPopUpDialog("Dates entered incorrectly!");
		}
		else {
			String date1 = "'" + year1 + "-" + month1 + "-" + day1 + "'";
			String date2 = "'" + year2 + "-" + month2 + "-" + day2 + "'";
			String time1 = "'" + tmpTime1 + ":00'";
			String time2 = "'" + tmpTime2 + ":00'";
			if (numberType.equals("Pump #")) {
				tableRows = mainFrame.getServer().getDbController().getReportAccordingToDates("Pump #", time1, time2, date1, date2);
				showReportOnTable("Pump #");
			}
			else if (numberType.equals("Team #")) {
				tableRows = mainFrame.getServer().getDbController().getReportAccordingToDates("Team #", time1, time2, date1, date2);
				showReportOnTable("Team #");
			}
		}
	}  // requestResultsAccordingToDates
	
	private void setInitialValuesInCmbBoxes() {
	    timeCmbBox1.setValue(timeCmbBox1.getItems().get(0));
	    dayCmbBox1.setValue(dayCmbBox1.getItems().get(0));
	    monthCmbBox1.setValue(monthCmbBox1.getItems().get(0));
	    yearCmbBox1.setValue(yearCmbBox1.getItems().get(0));
	    
	    timeCmbBox2.setValue(timeCmbBox2.getItems().get(0));
	    dayCmbBox2.setValue(dayCmbBox2.getItems().get(0));
	    monthCmbBox2.setValue(monthCmbBox2.getItems().get(0));
	    yearCmbBox2.setValue(yearCmbBox2.getItems().get(0));
	}  // setInitialValuesInCmbBoxes
	
	private int getNumOfDays(int year, String month) {
		int numOfDays = 0;
		LocalDate date = LocalDate.of(year, Integer.parseInt(month), 1);
		LocalDate end = date.withDayOfMonth(date.lengthOfMonth());
		numOfDays = end.getDayOfMonth();
		return numOfDays;
	}  // getNumOfDays
	
	private void disableAllCmbBoxes() {
	    timeCmbBox1.setDisable(true);
	    dayCmbBox1.setDisable(true);
	    monthCmbBox1.setDisable(true);
	    yearCmbBox1.setDisable(true);
	    timeCmbBox2.setDisable(true);
	    dayCmbBox2.setDisable(true);
	    monthCmbBox2.setDisable(true);
	    yearCmbBox2.setDisable(true);
	    numberTypeCmbBox.setDisable(true);
	}  // disableAllCmbBoxes
	
	private void addElementsToAreas() {
		grid1.add(timeLbl, 1, 0);
		grid1.add(dayLbl, 2, 0);
		grid1.add(monthLbl, 3, 0);
		grid1.add(yearLbl, 4, 0);
		grid1.add(fromLbl, 0, 1);
		grid1.add(timeCmbBox1, 1, 1);
		grid1.add(dayCmbBox1, 2, 1);
		grid1.add(monthCmbBox1, 3, 1);
		grid1.add(yearCmbBox1, 4, 1);		
		grid1.add(toLbl, 0, 2);
		grid1.add(timeCmbBox2, 1, 2);
		grid1.add(dayCmbBox2, 2, 2);
		grid1.add(monthCmbBox2, 3, 2);
		grid1.add(yearCmbBox2, 4, 2);	
		
		grid2.add(showAllRadBtn, 0, 0);
		grid2.add(numTypeRadBtn, 0, 1);
		grid2.add(datesRadBtn, 0, 2);
		grid2.add(searchBtn, 0, 3);
		grid2.add(numberTypeCmbBox, 1, 1);

		table.setItems(data);
        table.getColumns().addAll(numberCol, profitCol, dateCol, timeCol);
	}  // addElementsToAreas
	
	private void setMonths() {
		months.add("01"); months.add("02"); months.add("03"); months.add("04");
		months.add("05"); months.add("06"); months.add("07"); months.add("08");
		months.add("09"); months.add("10"); months.add("11"); months.add("12");	
	} // setMonths
	
	private void setHours() {
		hours.add("00:00"); hours.add("01:00"); hours.add("02:00"); hours.add("03:00"); hours.add("04:00");
		hours.add("05:00"); hours.add("06:00"); hours.add("07:00"); hours.add("08:00");
		hours.add("09:00"); hours.add("10:00"); hours.add("11:00"); hours.add("12:00");
		hours.add("13:00"); hours.add("14:00"); hours.add("15:00"); hours.add("16:00");
		hours.add("17:00"); hours.add("18:00"); hours.add("19:00"); hours.add("20:00");
		hours.add("21:00"); hours.add("22:00"); hours.add("23:00"); hours.add("24:00");
	} // setHours
}
