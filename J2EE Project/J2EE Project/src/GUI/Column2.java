package GUI;

import java.util.ArrayList;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.VBox;

public class Column2 extends VBox {
	
	private ScrollPane queue1ScrPane;
	private ScrollPane queue2ScrPane;
	private ScrollPane queue3ScrPane;
	private ScrollPane southernScrPane;
	private VBox vboxQueue1ScrPane, vboxQueue2ScrPane, vboxQueue3ScrPane, vboxSouthernScrPane;
	private Label headerLbl1, headerLbl2, headerLbl3, headerLbl4, dashLbl1, dashLbl2, dashLbl3, dashLbl4;
	private ArrayList<Integer> currentIDsInGUI = new ArrayList<Integer>();
	
	public Column2() {
		queue1ScrPane = new ScrollPane();
		queue3ScrPane = new ScrollPane();
		queue2ScrPane = new ScrollPane();
		southernScrPane = new ScrollPane();
		queue1ScrPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		queue2ScrPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		queue3ScrPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		southernScrPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		
		queue1ScrPane.setId("pane");
		queue2ScrPane.setId("pane");
		queue3ScrPane.setId("pane");
		southernScrPane.setId("pane");
		
		queue1ScrPane.setMinSize(220, 127);
		queue1ScrPane.setMaxSize(150, 127);		
		queue2ScrPane.setMinSize(220, 127);
		queue2ScrPane.setMaxSize(150, 127);
		queue3ScrPane.setMinSize(220, 127);
		queue3ScrPane.setMaxSize(150, 127);	
		southernScrPane.setMinSize(220, 160);
		southernScrPane.setMaxSize(150, 160);

		
		headerLbl1 = new Label("            Pumps Queue");
		headerLbl1.setId("header-label");
		dashLbl1 = new Label("--------------------------------------");
		dashLbl1.setId("dash-label");
		
		headerLbl2 = new Label(" Washing Machine Queue");
		headerLbl2.setId("header-label");
		dashLbl2 = new Label("--------------------------------------");
		dashLbl2.setId("dash-label");
		
		headerLbl3 = new Label("  Cleaning Teams Queue");
		headerLbl3.setId("header-label");
		dashLbl3 = new Label("--------------------------------------");
		dashLbl3.setId("dash-label");
		
		headerLbl4 = new Label("       On wait activities");
		headerLbl4.setId("header-label");
		dashLbl4 = new Label("--------------------------------------");
		dashLbl4.setId("dash-label");
		
		this.getChildren().addAll(queue1ScrPane, queue3ScrPane, queue2ScrPane, southernScrPane);
		this.setAlignment(Pos.TOP_LEFT);
		
		vboxQueue1ScrPane = new VBox(0);
		vboxQueue1ScrPane.getChildren().addAll(headerLbl1, dashLbl1);
		
		vboxQueue2ScrPane = new VBox(0);
		vboxQueue2ScrPane.getChildren().addAll(headerLbl2, dashLbl2);
		
		vboxQueue3ScrPane = new VBox(0);
		vboxQueue3ScrPane.getChildren().addAll(headerLbl3, dashLbl3);
		
		vboxSouthernScrPane = new VBox(0);
		vboxSouthernScrPane.getChildren().addAll(headerLbl4, dashLbl4);
		
		queue1ScrPane.setContent(vboxQueue1ScrPane);
		queue3ScrPane.setContent(vboxQueue2ScrPane);
		queue2ScrPane.setContent(vboxQueue3ScrPane);
		southernScrPane.setContent(vboxSouthernScrPane);
	}
	
	public void carIsAtQueueEvent(int carID, final String queue) {
		if (!currentIDsInGUI.contains(carID)) {
			final Label lbl = new Label("Car #" + carID + " is queued");
			lbl.setId("event-label-big-size");
			// it is impossible to update the UI, from a thread, that is not the
			// Application thread
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					if (queue.equals("PumpQueue"))
						vboxQueue1ScrPane.getChildren().add(lbl);
					else if (queue.equals("WashingMachineQueue"))
						vboxQueue2ScrPane.getChildren().add(lbl);
					else if (queue.equals("CleaningTeamQueue"))
						vboxQueue3ScrPane.getChildren().add(lbl);
				}
			});
			currentIDsInGUI.add(carID);
		}
	}
	
	// this function deletes the relevant labels from the panel
	public void carLeftQueueEvent(final int carID, final String queue) {
		if (currentIDsInGUI.contains(carID)) {
			// it is impossible to update the UI, from a thread, that is not the
			// Application thread
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					if (queue.equals("PumpQueue")) {
						int numOfChildren = vboxQueue1ScrPane.getChildren()
								.size();
						int indexToDelete = 0;
						for (int i = 2; i < numOfChildren; i++) {
							if (Integer.parseInt(((Label) vboxQueue1ScrPane
									.getChildren().get(i)).getText().substring(
									5, 8)) == carID) {
								indexToDelete = i;
							}
						}
						vboxQueue1ScrPane.getChildren().remove(indexToDelete);
					} else if (queue.equals("WashingMachineQueue")) {
						int numOfChildren = vboxQueue2ScrPane.getChildren()
								.size();
						int indexToDelete = 0;
						for (int i = 2; i < numOfChildren; i++) {
							if (Integer.parseInt(((Label) vboxQueue2ScrPane
									.getChildren().get(i)).getText().substring(
									5, 8)) == carID) {
								indexToDelete = i;
							}
						}
						vboxQueue2ScrPane.getChildren().remove(indexToDelete);
					} else if (queue.equals("CleaningTeamQueue")) {
						int numOfChildren = vboxQueue3ScrPane.getChildren()
								.size();
						int indexToDelete = 0;
						for (int i = 2; i < numOfChildren; i++) {
							if (Integer.parseInt(((Label) vboxQueue3ScrPane
									.getChildren().get(i)).getText().substring(
									5, 8)) == carID) {
								indexToDelete = i;
							}
						}
						vboxQueue3ScrPane.getChildren().remove(indexToDelete);
					}
				}
			});
			currentIDsInGUI.remove((Integer) carID);
		}
	}
	
	public void carIsOnWaitActivityEvent(final int carID, final String activity) {
		final Label lbl = new Label();
    	if (activity.equals("Newspaper")) {
    		lbl.setText("Car #" + carID + " is reading newspaper");
    		lbl.setId("event-label-big-size");
    	}
    	else if (activity.equals("PlayCellphone")) {
    		lbl.setText("Car #" + carID + " is playing on mobile");
    		lbl.setId("event-label-big-size");
    	}
    	else if (activity.equals("TalkCellphone")) {
    		lbl.setText("Car #" + carID + " is talking on mobile");
    		lbl.setId("event-label-big-size");
    	}		
		// it is impossible to update the UI, from a thread, that is not the Application thread
		Platform.runLater(new Runnable() {
		    @Override
		    public void run() {
		    	vboxSouthernScrPane.getChildren().add(lbl);  		
		    }
		});	
	}
	
	// this function deletes the relevant labels from the panel
	public void carFinishedOnWaitActivityEvent(final int carID) {
		// it is impossible to update the UI, from a thread, that is not the Application thread
		Platform.runLater(new Runnable() {
		    @Override
		    public void run() {    	
				int numOfChildren = vboxSouthernScrPane.getChildren().size();
				int indexToDelete = 0;
				for (int i = 2; i < numOfChildren; i++) {
					if (Integer.parseInt(((Label)vboxSouthernScrPane.getChildren().get(i)).getText().substring(5, 8)) == carID) {
					    indexToDelete = i; 
					}
				}	
				if (indexToDelete != 0) {
					vboxSouthernScrPane.getChildren().remove(indexToDelete);
				}
		    }
		});	
	}
}
