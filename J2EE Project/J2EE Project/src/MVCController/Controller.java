package MVCController;


import BL.Car;
import BL.Statistics;
import GUI.MainFrame;
import Listeners.GUIEventsListener;
import Listeners.ServerEventsListener;
import Server.MessageSending;
import Server.TheServer;
import ServerMessages.DataInitializeMsg;
import ServerRequests.AddCarRequest;

public class Controller implements GUIEventsListener, ServerEventsListener {
	// This is controller of MVC architecture, implement the Listener for BL also !!!
	private TheServer server;
	private MainFrame frame;
	
    public Controller(MainFrame frame, TheServer server) {
    	this.server = server;
    	this.frame = frame;
    	frame.registerListener(this);
    	server.getGasStation().registerListener(this);
    }

    /** GUI -----> BL events    **/
    
	@Override
	public boolean closeGasStationEvent() {
		boolean canClose = server.getGasStation().closeGasStation();
		if (server.getGasStation().getNumOfCarsInTheGasStationCurrently() == 0) {
			server.closeServer();   
		}
		return canClose;
	}
	
	@Override
	public boolean fillTankEvent() {
		for (int i = 0; i < server.getGasStation().getFuelServiceManager().getPumps().length; i++) {
			if (server.getGasStation().getFuelServiceManager().getPumps()[i].getCurrentlyFuelingCars().size() > 0) {
				return false;
			}
		}
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (server.getGasStation().getFuelServiceManager().getMainFuelPool().getCurrentCapacity() < 
					server.getGasStation().getFuelServiceManager().getMainFuelPool().getMaxCapacity())
					server.getGasStation().fillMainFuelTank();
			}
		}).start();
		return true;
	}
	
	@Override
	public double fuelCapacityPercentageInquiryEvent() {
		return server.getGasStation().fuelCapacityPercentageInquiryEventFired();
	}
	
	/**                                 **/
	

	/** BL -----> GUI/Clients events    **/
	
	@Override
	public void statisticsEvent(Statistics stats) {
		frame.setStats(stats);
	}

	@Override
	public void carArrivedEvent(Car car) {
		frame.carArrivedEventFired(car.getCarId());
		if (car.getOutStream() != null) {
			MessageSending.carArrivedEvent(car.getOutStream(), car.getCarId());
		}
	}

	@Override
	public void carIsFuelingUpEvent(Car car) {
		frame.getCol3().carIsFuelingUpEvent(car.getCarId(), car.getNumOfPump());
		if (car.getOutStream() != null) {
			MessageSending.carIsFuelingUpEvent(car.getOutStream(), car.getCarId(), car.getNumOfPump());
		}
	}

	@Override
	public void carFinishedFuelingUpEvent(Car car) {
		frame.getCol3().carFinishedFuelingUpEvent(car.getCarId());
		if (car.getOutStream() != null) {
			MessageSending.carFinishedFuelingUpEvent(car.getOutStream(), car.getCarId());
		}
	}

	@Override
	public void carIsInWashingMachineEvent(Car car) {
		frame.getCol4().carIsInWashingMachineEvent(car.getCarId());
		if (car.getOutStream() != null) {
			MessageSending.carIsInWashingMachineEvent(car.getOutStream(), car.getCarId());
		}
	}

	@Override
	public void carLeftWashingMachineEvent(Car car) {
		frame.getCol4().carLeftWashingMachineEvent(car.getCarId());
		if (car.getOutStream() != null) {
			MessageSending.carLeftWashingMachineEvent(car.getOutStream(), car.getCarId());
		}
	}

	@Override
	public void teamCleaningEvent(Car car, int teamID) {
		frame.getCol5().teamCleaningEvent(car.getCarId(), teamID);
		if (car.getOutStream() != null) {
			MessageSending.teamCleaningEvent(car.getOutStream(), car.getCarId(), teamID);
		}
	}

	@Override
	public void teamCleaningFinishedEvent(Car car) {
		frame.getCol5().teamCleaningFinishedEvent(car.getCarId());
		if (car.getOutStream() != null) {
			MessageSending.teamCleaningFinishedEvent(car.getOutStream(), car.getCarId());
		}
	}

	@Override
	public void carLeftEvent(Car car) {
		frame.carLeftEventFired(car.getCarId());
		if (car.getOutStream() != null) {
			MessageSending.carLeftEvent(car.getOutStream(), car.getCarId());
		}
	}

	@Override
	public void carIsAtQueueEvent(Car car, String queue) {
		frame.getCol2().carIsAtQueueEvent(car.getCarId(), queue);
		if (car.getOutStream() != null) {
			MessageSending.carIsAtQueueEvent(car.getOutStream(), car.getCarId(), queue);
		}
	}

	@Override
	public void carLeftQueueEvent(Car car, String queue) {
		frame.getCol2().carLeftQueueEvent(car.getCarId(), queue);
		if (car.getOutStream() != null) {
			MessageSending.carLeftQueueEvent(car.getOutStream(), car.getCarId(), queue);
		}
	}

	@Override
	public void finishedFillingTankEvent() {
		frame.finishedFillingTankEvent();
	}

	@Override
	public void fillTankEventFromBL() {
		frame.getCol1().setImage("images/fillGasTank.JPG", "r");
	}

	@Override
	public void stationIsEmptyEvent() {
		frame.stationIsEmptyEventFired();
		server.closeServer();		
	}

	@Override
	public boolean fireAddCarFromGUIEvent(AddCarRequest request) {
		return server.getGasStation().addCarFromGUIEventFired(request);
	}

	@Override
	public DataInitializeMsg fireDataInitializeEvent() {
		return server.getGasStation().fireDataInitializeEvent();
	}

	@Override
	public void onWaitActivityEventFired(int carID, String activity) {
		frame.getCol2().carIsOnWaitActivityEvent(carID, activity);
	}

	@Override
	public void fireFinishedOnWaitActivityEvent(int carID) {
		frame.getCol2().carFinishedOnWaitActivityEvent(carID);
	}

	@Override
	public void currentGasTankCapacityEvent(double currentCapacity) {
		frame.currentGasTankCapacityEventFired(currentCapacity);
	}
}
