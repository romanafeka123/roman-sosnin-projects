package ServerRequests;

import java.io.Serializable;

public class AmountOfMoneyPaidRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int carID;

	public AmountOfMoneyPaidRequest(int carID) {
		this.carID = carID;
	}

	public int getCarID() {
		return carID;
	}

	public void setCarID(int carID) {
		this.carID = carID;
	}
}
