package ServerRequests;

import java.io.Serializable;

public class AmountOfMoneyPaidResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private double moneyPaid;

	public AmountOfMoneyPaidResponse(double moneyPaid) {
		this.moneyPaid = moneyPaid;
	}

	public double getMoneyPaid() {
		return moneyPaid;
	}

	public void setMoneyPaid(double moneyPaid) {
		this.moneyPaid = moneyPaid;
	}
}
