package ServerRequests;

import java.io.Serializable;

public class AddCarRequest implements Serializable {

	private static final long serialVersionUID	= 2L;	
	
	private int carID;
	private int numOfPump;
	private double amountToFuel;
	private boolean needsFuel;
	private boolean needsCleaning;

	public AddCarRequest(int carID, int numOfPump, double amountToFuel, boolean needsFuel, boolean needsCleaning) {
		this.carID = carID;
		this.numOfPump = numOfPump;
		this.amountToFuel = amountToFuel;
		this.needsFuel = needsFuel;
		this.needsCleaning = needsCleaning;
	}

	public int getCarID() {
		return carID;
	}

	public void setCarID(int carID) {
		this.carID = carID;
	}

	public boolean isNeedsFuel() {
		return needsFuel;
	}

	public void setNeedsFuel(boolean needsFuel) {
		this.needsFuel = needsFuel;
	}

	public boolean isNeedsCleaning() {
		return needsCleaning;
	}

	public void setNeedsCleaning(boolean needsCleaning) {
		this.needsCleaning = needsCleaning;
	}

	public int getNumOfPump() {
		return numOfPump;
	}

	public void setNumOfPump(int numOfPump) {
		this.numOfPump = numOfPump;
	}

	public double getAmountToFuel() {
		return amountToFuel;
	}

	public void setAmountToFuel(double amountToFuel) {
		this.amountToFuel = amountToFuel;
	}
}
