package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import BL.Car;
import BL.GasStation;
import GUI.MainFrame;

/**
 * Servlet implementation class TheSerlet
 */
@WebServlet("/MainProgramServlet")
public class MainProgramServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private GasStation gasStation;
	
	/** For synchronizing purposes **/
	private Lock lock;
	private CountDownLatch latch;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainProgramServlet() {  	
    }
    
    @Override
    public void init() throws ServletException {
    	lock = new ReentrantLock();
		latch = new CountDownLatch(1);
    	new Thread(new Runnable() {
			@Override
			public void run() {
				MainFrame.runProgram(latch);			
			} 		
    	}).start();
    	 	
		try {
			lock.lock();
			latch.await();
			lock.unlock();
			setGasStation();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
    }
    
    public void setGasStation() {
    	this.gasStation = MainFrame.getGasStation();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		doGetAndPost(request, response);
	}

	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		doGetAndPost(request, response);
	}
	
	private void doGetAndPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException  {
		response.setContentType("text/html;charset=UTF-8");		
		PrintWriter out = response.getWriter();
		try {
			// for WebServiceServlet use
			this.getServletContext().setAttribute("TheGasStation", gasStation);
			
			int carID = Integer.parseInt(request.getParameter("carID"));			
			int liters = Integer.parseInt(request.getParameter("liters"));
			int pumpID = Integer.parseInt(request.getParameter("pumpID"));			
			String wantsFuelStr = request.getParameter("r1");
			String wantsCleaningStr = request.getParameter("r2");
			
			boolean wantsFuel = false;
			boolean wantsCleaning = false;
			
			if (wantsFuelStr.equals("fuelYes"))
				wantsFuel = true;
			if (wantsCleaningStr.equals("cleanYes"))
				wantsCleaning = true;
		
			Car car = new Car(gasStation, carID, wantsCleaning, wantsFuel, pumpID, liters, false);
			
			if (gasStation.isCarAlreadyEntered(carID)) {
				// static redirect
				String encodedURL = response.encodeRedirectURL("already_inside.html");
				response.sendRedirect(encodedURL);
			}
			else if (gasStation.isOpen()) {
				// dynamic redirect
				carEnteredGSHTMLResponse(out, car);
				gasStation.addCar(car);
			}
			else {
				// static redirect
				String encodedURL = response.encodeRedirectURL("closed.html");
				response.sendRedirect(encodedURL);
			}			
		} finally {
			out.close();
		}
	}
	
	private void carEnteredGSHTMLResponse(PrintWriter out, Car car) {
		String title = "";
		if (car.needsBothServices()) {
			title = "<h1 align=\"center\"> Services: Fueling, Cleaning. Car ID: " + car.getCarId() + ", liters: " + car.getRequestedAmountOfFuel() + ", pump #" + car.getNumOfPump() +"</h1>";
		}
		else if (car.getWantsFuel() && !car.getWantsCleaning()) {
			title = "<h1 align=\"center\"> Services: Fueling. Car ID: " + car.getCarId() + ", liters: " + car.getRequestedAmountOfFuel() + ", pump #" + car.getNumOfPump() +"</h1>";
		}
		else if (!car.getWantsFuel() && car.getWantsCleaning()) {
			title = "<h1 align=\"center\"> Services: Cleaning. Car ID: " + car.getCarId() + "</h1>";
		}
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Car entered gs</title>");
		out.println("</head>");
		out.println("<body align=\"center\" bgcolor=\"cyan\">");
		out.println("<p align=\"center\">");
		out.println(title);
		out.println("</p>");
		out.println("<img width=1350 height=550 src=\"gas.jpg\"/>");	
		out.println("</body>");
		out.println("</html>");
	}
}
