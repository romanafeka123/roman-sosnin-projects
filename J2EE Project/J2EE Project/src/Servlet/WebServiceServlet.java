package Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import BL.GasStation;
import WS.WebServiceClient;

/**
 * Servlet implementation class WebServiceServlet
 */
@WebServlet("/WebServiceServlet")
public class WebServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WebServiceServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetAndPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetAndPost(request, response);
	}
	
	private void doGetAndPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException  {
		response.setContentType("text/html;charset=UTF-8");

		PrintWriter out = response.getWriter();
		try {
			GasStation gs = (GasStation) this.getServletContext().getAttribute("TheGasStation");
			int carID = Integer.parseInt(request.getParameter("carID"));
			if (gs != null) {
				if (gs.isOpen()) {
					if (gs.wasCarServed(carID)) {
						webServiceRequest(out, carID);
					} else {
						returnNoSuchCarHTMLPage(out, carID);
					}
				}
				else {
					String encodedURL = response.encodeRedirectURL("closed.html");
					response.sendRedirect(encodedURL);
				}
			} else {
				String encodedURL = response.encodeRedirectURL("closed.html");
				response.sendRedirect(encodedURL);
			}
		} finally {
			out.close();
		}
	}
	
	private void webServiceRequest(PrintWriter out, int carID) {
		WebServiceClient wsClient = new WebServiceClient();
		double paidMoney = wsClient.callWebService(carID);	
		if (paidMoney != 1.0) {
			returnSuccessfulHTMLPage(out, carID, paidMoney);
		}
	}
	
	private void returnSuccessfulHTMLPage(PrintWriter out, int carID, double paidMoney) {
		out.println("<html>");
		out.println("<head>");		
		out.println("<title> Amount of money paid response </title>");
		out.println("</head>");
		out.println("<body align=\"center\" bgcolor=\"cyan\">");
		out.println("<p align=\"center\">");
		out.println("<h1 align=\"center\"> The car with ID: " + carID + " paid: " + String.format("%.2f", paidMoney) + "$</h1>");
		out.println("<img width=1330 height=630 src=\"money_car.jpg\"/>");
		out.println("</p>");
		out.println("</body>");
		out.println("</html>");
	}
	
	private void returnNoSuchCarHTMLPage(PrintWriter out, int carID) {
		out.println("<html>");
		out.println("<head>");		
		out.println("<title> Car with such ID wasn't served </title>");
		out.println("</head>");
		out.println("<body align=\"center\" bgcolor=\"cyan\">");
		out.println("<p align=\"center\">");
		out.println("<h1 align=\"center\"> The car with ID: " + carID + " wasn't served </h1>");
		out.println("<img width=1330 height=550 src=\"no_car.png\"/>");
		out.println("</p>");
		out.println("</body>");
		out.println("</html>");
	}
}
