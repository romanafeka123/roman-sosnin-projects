package BL;

import java.sql.Date;
import java.sql.Time;

public class ReportData {
	private int number; // this number can be either pump number or cleaning team number
	private double profit;
	private Date date;
	private Time time;
	
	public ReportData(Integer number, Double profit, Date date, Time time) {
		this.number = number;
		this.profit = profit;
		this.date = date;
		this.time = time;
	}

	public int getNumber() {
		return number;
	}
	public double getProfit() {
		return profit;
	}
	public Date getDate() {
		return date;
	}
	public Time getTime() {
		return time;
	}
}
