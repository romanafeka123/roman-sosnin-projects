package BL;
import java.io.IOException;

/*
 * That class manages everything that relates to the fuel service
 * Pumps,Main pool's events, fill main pool etc..
 */
public class FuelServiceManager implements MainFuelPoolEvents {

	private GasStation gasStation;
	private int numOfPumps;											   // number of pumps in station
	
	private MainFuelPool mainFuelPool;						 		  //the main fuel pool object 
	private Pump[] pumps; 											 //hashMap of pumps, we get the corresponded pump with its id
	private double amountOfSuppliedFuel;
	private int numberOfServedCars;									//keeps the numer of cars that have veen served
	private boolean active;											//indicates whether the service is active or not
	
	public FuelServiceManager(GasStation gasStation, int maxMainFuelCapacity,int currentMainFuelCapacity,int numOfPumps) throws SecurityException, IOException{
	    active = true;
	    this.gasStation = gasStation;
	    this.numOfPumps = numOfPumps;
	    mainFuelPool = new MainFuelPool(gasStation,maxMainFuelCapacity, currentMainFuelCapacity);
	    mainFuelPool.registerToMainFuelEvent(this);
	    setPumps();
	}
	
	public void setPumps() throws SecurityException, IOException {
		pumps = new Pump[numOfPumps];
		for(int i = 0; i < numOfPumps; i++)
			pumps[i] = new Pump(gasStation,mainFuelPool, i + 1, true);
	}
	
	public int getNumOfPumps() {
		return numOfPumps;
	}

	public MainFuelPool getMainFuelPool() {
		return mainFuelPool;
	}

	@Override
	public void mainPoolFuelIsRunningOut(double currentAmount) {
		getMainFuelPool().fillMainPool();
		activatePumps();
	}

	public void activatePumps() {
		for (Pump pump : pumps) {
			pump.activatePump();
		}
	}
	
	public void fsGiveFuel(Car car, double amountOfFuel ,int numOfPump){
		double amount = 0;
		amount = pumps[numOfPump-1].getFuelFromPump(car);
		if (amount > 0){
			numberOfServedCars++;
			amountOfSuppliedFuel += amount;
		}
	}
	
	public double getAmountOfSuppliedFuel() {
		return amountOfSuppliedFuel;
	}

	public int getNumberOfServedCars() {
		return numberOfServedCars;
	}
	
	public Pump[] getPumps() {
		return pumps;
	}

	public boolean isActive() {
		return active;
	}
	
	public void closeService(){
		active = false;
	}
	
	public double getAvgTimeToGetFuel(int pumpNum){
		double timeToGetFuel = 0;
		for (Car car : pumps[pumpNum-1].getWaitingCarsQueue()) {
			timeToGetFuel += car.getRequestedAmountOfFuel() * pumps[pumpNum-1].avgTimeForLitter;
		}
		return timeToGetFuel;
	}
}
