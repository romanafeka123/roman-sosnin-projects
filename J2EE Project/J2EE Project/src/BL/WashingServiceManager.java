package BL;

import java.io.IOException;

/*
 * That class manages everything that relates to the washing service
 * cleaning team,washing machine etc..
 */
public class WashingServiceManager {

	public static final int SECONDS_PER_AUTO_CLEAN = 15;

	private GasStation gasStation; // gas station ref
	private int numOfTeams; // number of cleaning teams in the washing service
	private CleaningTeam[] cleaningTeams; // array of cleaning team
	private WashingMachine washingMachine; // washing machine
	private int numberOfServedCars; // keep the number of cars which have been served
	private boolean active; // flag indicates whether the service is active or not
	
	public WashingServiceManager(GasStation gasStation, int numOfTeams) throws SecurityException, IOException {
		active = true;
		this.gasStation = gasStation;
		this.numOfTeams = numOfTeams;
		washingMachine = new WashingMachine(gasStation, SECONDS_PER_AUTO_CLEAN);
		setCleaningTeams();
	}

	// function creates array of cleaning Team
	public void setCleaningTeams() throws SecurityException, IOException {
		cleaningTeams = new CleaningTeam[numOfTeams];
		for (int i = 0; i < numOfTeams; i++) {
			cleaningTeams[i] = new CleaningTeam(gasStation, i + 1);
		}
	}

	// function returns array of cleaning Team
	public int getNumOfTeams() {
		return numOfTeams;
	}

	// function gets a car and first sends it to the washing machine then to the cleaning team
	public boolean wsCleanCar(Car car) {
		boolean washed = washingMachine.washCar(car);
		// if the station is closed and the car wasn't in the washing machine, don't go to ManualCleaning
		if (!gasStation.isOpen() && !washed) {
			return false;
		}
		int availableTeam = getAvailableTeam();
		if (availableTeam >= 0) {
			cleaningTeams[availableTeam].cleanCar(car);
		}
		else {
			// get random team if there is no available team
			availableTeam = gasStation.getRand().nextInt(cleaningTeams.length);
			cleaningTeams[availableTeam].cleanCar(car);
		}
		car.setTotalMoneyPaid(car.getTotalMoneyPaid() + gasStation.getWashingPrice());
		numberOfServedCars++;
		gasStation.getServer().getDbController().insertCleanServiceTransactionIntoDB(availableTeam+1, gasStation.getWashingPrice());
		return true;
	}

	// function returns number of cars that have been served
	public int getNumberOfServedCar() {
		return numberOfServedCars;
	}

	public boolean isActive() {
		return active;
	}
	
	public int getAvailableTeam() {
		for (int i = 0; i < cleaningTeams.length; i++) {
			if (!cleaningTeams[i].isBusy())
				return i;
		}
		return -1; // no one is available at the moment
	}

	public CleaningTeam[] getCleaningTeams() {
		return cleaningTeams;
	}

	public void closeService() {
		active = false;
	}

	public double getAvgTimeForCleaning() {
		double avgTimeForCleaning = 0;
		double minimum = -1, temp = 0;
		avgTimeForCleaning += washingMachine.getWaitingCarsCounter() * washingMachine.getSecondsPerAutoClean();
		for (CleaningTeam ct : cleaningTeams) {
			if ((temp = ct.getWaitingCarsCounter() * ct.getAvgTimeToCleanCar()) > minimum)
				minimum = temp;
		}
		avgTimeForCleaning += minimum;
		return avgTimeForCleaning;
	}
}
