package BL;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import Annotations.OnWaitActivity;
import Server.MessageSending;

public class Car extends Thread{
	
	private int carID;
	private boolean wantsCleaning;					//flag indicates whether the car wants cleaning service
	private boolean wantsFuel;						//flag indicates whether the car wants fuel  service 
	private boolean wantsBothServices;				// true if car both services are requested
	
	private int numOfPump;							//number of pump
	private double amountToFuel;					//amount of requested fuel
	private boolean arrivedFromClient;

	private boolean gotAnyService;					//flag to indicate if car received any service
	private double receivedFuelAmount;				//keep the amount of fuel that the car received
	private GasStation gasStation;					//ref to the gas station
	private double totalMoneyPaid;

	private ObjectOutputStream outStream;
	private final int NUM_OF_ACTIVITIES_TO_DO_ON_WAIT = 3;
	

	public Car(GasStation gasStation , int id, boolean wantCleaning, boolean wantsFuel, int numOfPump, double amountToFuel, boolean arrivedFromClientCar) throws SecurityException, IOException{	
		this.carID = id;
		this.wantsCleaning = wantCleaning;
		this.wantsFuel = wantsFuel;
		if (wantsFuel && wantsCleaning) {
			wantsBothServices = true;
		}
		else {
			wantsBothServices = false;
		}
		this.gasStation = gasStation;	
		this.numOfPump = numOfPump;
		this.amountToFuel = amountToFuel;
		this.arrivedFromClient = arrivedFromClientCar;
		outStream = null;
		setGasStation(gasStation);
		totalMoneyPaid = 0;
	}

	@Override
	public void run(){
		while(!gotAnyService) {
			if (wantsBothServices){
				double minAvgTimeToGetFuel = gasStation.getMinAvgTimeToGetFuel(numOfPump);
				double minAvgTimeForCleaning = gasStation.getMinAvgTimeForCleaning();
				if(minAvgTimeToGetFuel > minAvgTimeForCleaning){
					goForCleaning();
				}
				else if(minAvgTimeToGetFuel < minAvgTimeForCleaning){
					goForFuel();					
					wantsBothServices = false;
				}
				// waiting time is equal so we random one of the 2 services 
				else { 
					if(gasStation.getRand().nextInt() % 2 == 0) {
						goForFuel();
					}
					else {
						goForCleaning();
					}
				}
				if (!wantsFuel || !wantsCleaning)
					wantsBothServices = false;					
			}
			else if(wantsFuel){
				goForFuel();
			}
			else if(wantsCleaning){
				goForCleaning();		
			}
			else if(!wantsFuel && !wantsCleaning) {
				gotAnyService = true;
			}
		}	
		gasStation.getCurrentCarIDsInStation().remove((Integer)carID);
		gasStation.fireCarLeftEvent(this);
		gasStation.decreaseNumOfCarsInStation();
		gasStation.addServedCar(this);
		if (!gasStation.isOpen() && gasStation.getNumOfCarsInTheGasStationCurrently() == 0)
			gasStation.fireStationIsEmptyEvent();	
	}
	
	public void goForCleaning() {
		gasStation.cleanCar(this);
		setWantCleaning(false);
	}
	
	public void goForFuel(){
		gasStation.getFuel(this, amountToFuel, numOfPump);
	}
	
	@OnWaitActivity
	public void readNewsPaper(int carID) {
		gasStation.fireOnWaitActivityEvent(carID, "Newspaper");
		if (outStream != null) {
			MessageSending.onWaitActivityEvent(outStream, "readNewsPaper",carID);
		}
	}

	@OnWaitActivity
	public void smartphonePlay(int carID) {
		gasStation.fireOnWaitActivityEvent(carID, "PlayCellphone");
		if (outStream != null) {
			MessageSending.onWaitActivityEvent(outStream, "smartphonePlay",carID);
		}
	}

	@OnWaitActivity
	public void smartphoneSpeak(int carID) {
		gasStation.fireOnWaitActivityEvent(carID, "TalkCellphone");
		if (outStream != null) {
			MessageSending.onWaitActivityEvent(outStream, "smartphoneSpeak",carID);
		}
	}

	// this method is for reflection-annotation implementation
	public void doSomethingWhileAtWashingMachine() {
		Method[] allMethods = Car.class.getMethods();
		ArrayList<Method> annotatedMethods = new ArrayList<Method>();
		for (Method m : allMethods) {
			if (m.isAnnotationPresent(OnWaitActivity.class)) {	
				annotatedMethods.add(m);
			}
		}
		int randomMethodToActivate = (int) (Math.random() * NUM_OF_ACTIVITIES_TO_DO_ON_WAIT);
		try {
			annotatedMethods.get(randomMethodToActivate).invoke(this, carID);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	public double getTotalMoneyPaid() {
		return totalMoneyPaid;
	}

	public void setTotalMoneyPaid(double totalMoneyPaid) {
		this.totalMoneyPaid = totalMoneyPaid;
	}

	public double getAmountToFuel() {
		return amountToFuel;
	}

	public int getCarId() {
		return carID;
	}

	public int getNumOfPump() {
		return numOfPump;
	}

	public boolean isWantCleaning() {
		return wantsCleaning;
	}

	public void setWantCleaning(boolean wantsCleaning) {
		this.wantsCleaning = wantsCleaning;
	}

	public boolean needsBothServices() {
		return wantsBothServices;
	}

	public boolean gotService() {
		return gotAnyService;
	}
	
	public boolean isArrivedFromClientCar() {
		return arrivedFromClient;
	}

	public void setGotService(boolean gotAnyService) {
		this.gotAnyService = gotAnyService;
	}

	public void setGotFuel(double receivedFuel) {
		this.receivedFuelAmount = receivedFuel;
	}
	
	public boolean getWantsFuel() {
		return wantsFuel;
	}
	
	public void setWantsFuel(boolean wantFuel) {
		this.wantsFuel = wantFuel;
	}

	public double getRequestedAmountOfFuel() {
		return amountToFuel;
	}

	public void setAmountOfFuelRecieved(double amountOfFuel) {
		this.amountToFuel = amountOfFuel;
	}

	public boolean getWantsCleaning() {
		return wantsCleaning;
	}
	
	@Override
	public String toString() {
		return "Car [id=" + carID + "]";
	}
		
	public void setGasStation(GasStation station) {
		if (station != null) {
			gasStation = station; 
		}
	}
	public double getReceivedFuelAmount() {
		return receivedFuelAmount;
	}

	public void setReceivedFuelAmount(double receivedFuelAmount) {
		this.receivedFuelAmount = receivedFuelAmount;
	}

	public ObjectOutputStream getOutStream() {
		return outStream;
	}

	public void setOutStream(ObjectOutputStream outStream) {
		this.outStream = outStream;
	}
}
