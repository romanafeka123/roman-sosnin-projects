package BL;
import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CleaningTeam {
	
	public static final int MIN_WORKERS_PER_TEAM = 1; // arbitrary min number of workers
	public static final int MAX_WORKERS_PER_TEAM = 5;  // arbitrary max number of workers	
	private Lock lock;
	private boolean busy;						//a flag that indicates whether that team is busy already or not

	private int teamId;
	private int numberOfWorkers;
	private GasStation gasStation;
	
	private int avgTimeToCleanCar;
	private int waitingForInternalCleaning;			//counter for cars which wait for internal Cleaning from that team 
	
	
	public CleaningTeam(GasStation gasStation, int teamId) throws SecurityException, IOException{
		this.teamId = teamId;
		this.gasStation = gasStation;
		
		//we generate random number for workers in a cleaning team.(1 to 5)
		numberOfWorkers = gasStation.getRand().nextInt((MAX_WORKERS_PER_TEAM - MIN_WORKERS_PER_TEAM) + 1) + MIN_WORKERS_PER_TEAM;
		avgTimeToCleanCar = gasStation.getRand().nextInt() % numberOfWorkers + 1;		//my formula to get random cleaning time per team; -->can be changes any time
		lock = new ReentrantLock();
	}
	
	public void cleanCar(Car car) {
		gasStation.fireCarIsAtQueueEvent(car, "CleaningTeamQueue");
		waitingForInternalCleaning++;
		lock.lock();
		busy = true;
		gasStation.fireCarLeftQueueEvent(car, "CleaningTeamQueue");
		gasStation.fireTeamCleaningEvent(car, teamId);
		drawRandomCleanTime(car);
		gasStation.fireTeamCleaningFinishedEvent(car);
		busy = false;
		lock.unlock();
		waitingForInternalCleaning--;	
	}
	
	private void drawRandomCleanTime(Car car) {
		int cleanTime = (int) (Math.random() * 10000);
		try {
			Thread.sleep(cleanTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean isBusy() {
		return busy;
	}
	
	public int getAvgTimeToCleanCar(){
		return avgTimeToCleanCar;
	}
	
	public int getWaitingCarsCounter(){
		return waitingForInternalCleaning;
	}
	
	public int getTeamId() {
		return teamId;
	}

	public int getNumberOfWorkers() {
		return numberOfWorkers;
	}
}
