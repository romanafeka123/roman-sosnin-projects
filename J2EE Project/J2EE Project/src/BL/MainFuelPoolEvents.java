package BL;
public interface MainFuelPoolEvents {
	void mainPoolFuelIsRunningOut(double currentAmount); // event that the MainFuel
													// Class will throw
													// when Fuel tank Reaches
													// 20%

}
