package BL;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.Vector;

import Listeners.ServerEventsListener;
import Server.TheServer;
import ServerMessages.DataInitializeMsg;
import ServerRequests.AddCarRequest;

public class GasStation extends Thread {
	
	private int stationId;
	private String stationName;
	private double pricePerLiter;									  // pricer per liter
	private double washingPrice;									 // pricer per wash
	private Queue<Car> carsForFuel;									// Queue of cars for fuel
	private Queue<Car> carsForWashing;							   // Queue of cars for Washing
	private WashingServiceManager washingServiceManager;		  // washing service Manager
	private FuelServiceManager fuelServiceManager;				 // fuel service Manager
	private int numOfCarsInTheGasStationCurrently;
	private boolean stationIsOpen;	
	
	private Vector<ServerEventsListener> listeners;
	private Vector<Integer> currentCarIDsInStation;
	private HashMap<Integer, Double> servedCars;
	
	private Random rand = new Random();
	private Statistics stats;
	private final static int ID_MAX_SIZE = 3;
	private TheServer server;
	
	public enum Service {
		none,fuel,washing,both
	};
	
	public GasStation(int id, String name, int maxMainFuelCapacity,int currentMainFuelCapacity,int numOfPumps,double pricePerLiter,int numberOfCleaningTeam,double washingPrice) throws SecurityException, IOException{
		stationId = id;
		stationName = name;
		this.pricePerLiter = pricePerLiter;
		this.washingPrice = washingPrice;
		
		carsForWashing = new LinkedList<Car>();
		carsForFuel = new LinkedList<Car>();		
		fuelServiceManager = new FuelServiceManager(this, maxMainFuelCapacity, currentMainFuelCapacity, numOfPumps);
		washingServiceManager = new WashingServiceManager(this, numberOfCleaningTeam);	
		stats = new Statistics();
		listeners = new Vector<ServerEventsListener>();
		currentCarIDsInStation = new Vector<Integer>();
		servedCars = new HashMap<Integer, Double>();
		numOfCarsInTheGasStationCurrently = 0;
		openGasStation();
	}
	
	@Override
	public void run() {		
		while (isOpen()){
			if(carsForFuel.size() > 0)
				carsForFuel.poll().start(); 			//poll cars for fuel
			if(carsForWashing.size() > 0)
				carsForWashing.poll().start();			//poll cars for washing
		}
		fuelServiceManager.closeService();
		washingServiceManager.closeService();		
	}
	
	public void registerListener(ServerEventsListener listener) {
		listeners.add(listener);
	}
	
	public void openGasStation() {
		stationIsOpen = true;
	}
	
	// this function returns true, only if succeeded to close the gas station
	public boolean closeGasStation(){
		if (fuelServiceManager.getMainFuelPool().isBeingFilled() == false) {
			setStationIsOpen(false);
			return true;
		}
		return false;
	}
	
	public boolean isOpen(){
		return stationIsOpen;
	}
	
	public void setStationIsOpen(boolean stationIsOpen) {
		this.stationIsOpen = stationIsOpen;
	}

	public void getFuel(Car car, double amountOfFuel, int numOfPump) {
		fuelServiceManager.fsGiveFuel(car,amountOfFuel, numOfPump);
	}
	
	public void cleanCar(Car car) {
		boolean passedCleaningService = washingServiceManager.wsCleanCar(car);
		if (passedCleaningService) {
			stats.setNumOfCarsCleaned(stats.getNumOfCarsCleaned() + 1);
			stats.setCleanProfit(stats.getCleanProfit() + washingPrice);
			fireStatisticsEvent();
			fireFinishedOnWaitActivityEvent(car.getCarId());
		}
	}

	public void addCar(Car car) {
		if (stationIsOpen) {
			currentCarIDsInStation.add(car.getCarId());
			fireCarArrivedEvent(car);
			if (car.needsBothServices() || car.getWantsFuel())
				addToFuelingUpQueue(car);
			else
				addToCleaningQueue(car);
		}
	}
	
	public void printStatistic(){
		System.out.println("### Statistics ###");
		System.out.println("Number of cars which received fuel is : " + fuelServiceManager.getNumberOfServedCars());
		System.out.println("Total Amount Of Fuel  : " + fuelServiceManager.getAmountOfSuppliedFuel() +"\nPrice Per Liter is :" + pricePerLiter +
				"\nTotal Profit from Fuel service : " + fuelServiceManager.getAmountOfSuppliedFuel() * pricePerLiter);
		System.out.println("Total cars which have been to washing server : " + washingServiceManager.getNumberOfServedCar() +
				" price for car washing is : " + washingPrice +"\nTotal Profit from washing service is : " + washingServiceManager.getNumberOfServedCar() * washingPrice);
	}
	
	public void fillMainFuelTank() {
		if (fuelServiceManager.getMainFuelPool().isBeingFilled() == false) {
			fuelServiceManager.getMainFuelPool().notifyAllRegistered();
		}
	}
	
	public boolean isCarAlreadyEntered(int carID) {
		if (currentCarIDsInStation.contains(carID))
			return true;
		return false;
	}
	
	public FuelServiceManager getFuelServiceManager() {
		return fuelServiceManager;
	}
	
	public double getMinAvgTimeToGetFuel(int PumpId){
		return fuelServiceManager.getAvgTimeToGetFuel(PumpId);
	}
	
	public double getMinAvgTimeForCleaning(){
		return washingServiceManager.getAvgTimeForCleaning();
	}
	
	public void increaseNumOfCarsInStation() {
		numOfCarsInTheGasStationCurrently++;
	}
	
	public WashingServiceManager getWashingServiceManager() {
		return washingServiceManager;
	}
	
	public double getWashingPrice() {
		return washingPrice;
	}

	public synchronized void decreaseNumOfCarsInStation() {
		numOfCarsInTheGasStationCurrently--;
	}
	
	public synchronized int getNumOfCarsInTheGasStationCurrently() {
		return numOfCarsInTheGasStationCurrently;
	}
	
	public Statistics getStats() {
		return stats;
	}	
	
	public double getPricePerLiter() {
		return pricePerLiter;
	}

	public void setStats(Statistics stats) {
		this.stats = stats;
	}
	
	public int getStationId() {
		return stationId;
	}

	public void addToFuelingUpQueue(Car car) {
		increaseNumOfCarsInStation();
        carsForFuel.add(car);
	}
    public void addToCleaningQueue(Car car) {
    	increaseNumOfCarsInStation();
    	carsForWashing.add(car);
	}
      
    public Vector<Integer> getCurrentCarIDsInStation() {
		return currentCarIDsInStation;
	}
    
	public String getStationName() {
		return stationName;
	}
	
	public Random getRand() {
		return rand;
	}
	
	public void addServedCar(Car car) {
		Integer carID = car.getCarId();
		Double totalMoneyPaid = car.getTotalMoneyPaid();
		servedCars.put(carID, totalMoneyPaid);
	}
	
	public boolean wasCarServed(Integer carID) {
		return servedCars.containsKey(carID);
	}
	
	// returns amount of money the served car paid if it was served and -1.0 otherwise
	public double getServedCarTotalMoneyPaidByCarID(Integer carID) {
		if (wasCarServed(carID)) {
			return servedCars.get(carID);
		}
		return -1.0;
	}

	public static int getIdMaxSize() {
		return ID_MAX_SIZE;
	}

	public TheServer getServer() {
		return server;
	}

	public void setServer(TheServer server) {
		this.server = server;
	}

	public double fuelCapacityPercentageInquiryEventFired() {
    	double res = getFuelServiceManager().getMainFuelPool().getCurrentCapacity() /
    			     getFuelServiceManager().getMainFuelPool().maxCapacity();
    	return res;
    }
	
	// this function returns true if the car is already in the station (duplicate id tried to be entered)
	public boolean addCarFromGUIEventFired(AddCarRequest req) {
		Car car = null;
		if (currentCarIDsInStation.contains((Integer)req.getCarID())) {
			return true;
		}
		try {
			car = new Car(this, req.getCarID(), req.isNeedsCleaning(), req.isNeedsFuel(), req.getNumOfPump(), req.getAmountToFuel(), false);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		addCar(car);
		return false;
	}
	
	public DataInitializeMsg fireDataInitializeEvent() {
		DataInitializeMsg dataMsg = new DataInitializeMsg(getIdMaxSize(),
				(int) fuelServiceManager.getMainFuelPool().getMaxCapacity(),
				fuelServiceManager.getNumOfPumps());
		return dataMsg;
	}
    
	public void fireStatisticsEvent() {
		for (ServerEventsListener l : listeners) {
			l.statisticsEvent(stats);
		}
	}
	
	public void fireStationIsEmptyEvent() {
		for (ServerEventsListener l : listeners) {
			l.stationIsEmptyEvent();
		}	
	}
	
	public void fireCarArrivedEvent(Car car) {
		for (ServerEventsListener l : listeners) {
			l.carArrivedEvent(car);
		}	
	}
	
	public void fireCarIsFuelingUpEvent(Car car, Pump pump) {
		for (ServerEventsListener l : listeners) {
			l.carIsFuelingUpEvent(car);
    	}
	}
	
	public void fireCarFinishedFuelingUpEvent(Car car) {
		for (ServerEventsListener l : listeners) {
			l.carFinishedFuelingUpEvent(car);
		}
	}
	
	public void fireCarIsInWashingMachineEvent(Car car) {
		for (ServerEventsListener l : listeners) {
			l.carIsInWashingMachineEvent(car);
		}
	}
	
	public void fireCarLeftWashingMachineEvent(Car car) {
		for (ServerEventsListener l : listeners) {
			l.carLeftWashingMachineEvent(car);
		}
	}
	
	public void fireTeamCleaningEvent(Car car, int teamID) {
		for (ServerEventsListener l : listeners) {
			l.teamCleaningEvent(car, teamID);
		}
	}
	
	public void fireTeamCleaningFinishedEvent(Car car) {
		for (ServerEventsListener l : listeners) {
			l.teamCleaningFinishedEvent(car);
		}
	}
	
	public void fireCarLeftEvent(Car car) {
		for (ServerEventsListener l : listeners) {
			l.carLeftEvent(car);
		}
	}	
	
	public void fireCarIsAtQueueEvent(Car car, String queue) {
		for (ServerEventsListener l : listeners) {
			l.carIsAtQueueEvent(car, queue);
		}
	}
	
	public void fireCarLeftQueueEvent(Car car, String queue) {
		for (ServerEventsListener l : listeners) {
			l.carLeftQueueEvent(car, queue);
		}
	}
	
	public void fireFinishedFillingTankEvent() {
		for (ServerEventsListener l : listeners) {
			l.finishedFillingTankEvent();
		}	
	}
	
	public void fireFillTankEventFromBL() {
		for (ServerEventsListener l : listeners) {
			l.fillTankEventFromBL();
		}	
	}
	
	public void fireOnWaitActivityEvent(int carID, String activity) {
		for (ServerEventsListener l : listeners) {
			l.onWaitActivityEventFired(carID, activity);
		}	
	}
	
	public void fireFinishedOnWaitActivityEvent(int carID) {
		for (ServerEventsListener l : listeners) {
			l.fireFinishedOnWaitActivityEvent(carID);
		}	
	}
	
	public void fireCurrentGasTankCapacityEvent() {
    	double res = getFuelServiceManager().getMainFuelPool().getCurrentCapacity() /
			     getFuelServiceManager().getMainFuelPool().maxCapacity();
		for (ServerEventsListener l : listeners) {
			l.currentGasTankCapacityEvent(res);
		}
	}
}
