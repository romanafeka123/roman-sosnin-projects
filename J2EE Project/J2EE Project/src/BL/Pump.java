package BL;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Pump {
	
	private int pumpId;
	private MainFuelPool mainFuelPool;
	private boolean active;
	private GasStation gasStation;
	private Lock lockPump;
	private CountDownLatch notFull;
	public double avgTimeForLitter = 1.5;								//the average time it takes for a pump to give one litter(in seconds)
	
	private Queue<Car> waitingForPumps;
	private ArrayList<Car> currentlyFuelingCars;

	public Pump(GasStation gasStation, MainFuelPool mainFuelPool, int id, boolean isActive) throws SecurityException, IOException{
		this.gasStation = gasStation;
		this.mainFuelPool = mainFuelPool;
		pumpId = id;
		active = isActive;
		waitingForPumps = new LinkedList<Car>();
		currentlyFuelingCars = new ArrayList<Car>();
		lockPump = new ReentrantLock(true);
		notFull = new CountDownLatch(1);
	}
	
	public void setPumpStatus(boolean status){
		active = status;
	}
	
	public boolean isPumpActive(){
		return active;
	}
	
	public int getPumpId(){
		return pumpId;
	}
	
	public ArrayList<Car> getCurrentlyFuelingCars() {
		return currentlyFuelingCars;
	}

	public double getFuelFromPump(Car car) {
		try {
			//we use queue here to decide how much time it will take to use the pump
			gasStation.fireCarIsAtQueueEvent(car, "PumpQueue");
			waitingForPumps.add(car);				 
			lockPump.lock();
			
			while (mainFuelPool.isbeingFilled()) {
				notFull.await();
			}
			gasStation.fireCarLeftQueueEvent(car, "PumpQueue");
			gasStation.fireCarIsFuelingUpEvent(car, this);
			currentlyFuelingCars.add(car);
			car.setReceivedFuelAmount(mainFuelPool.getFuel(car));	
			car.setTotalMoneyPaid(car.getTotalMoneyPaid() + (car.getReceivedFuelAmount() * gasStation.getPricePerLiter()));
			lockPump.unlock();	
			currentlyFuelingCars.remove(car);
			car.setWantsFuel(false);
			gasStation.fireCurrentGasTankCapacityEvent();
			// update statistics
			double currentProfit = gasStation.getPricePerLiter() * car.getReceivedFuelAmount();
			gasStation.getStats().setNumOfCarsFueledUp(gasStation.getStats().getNumOfCarsFueledUp() + 1);
			gasStation.getStats().setFuelProfit(gasStation.getStats().getFuelProfit() + currentProfit);
			gasStation.fireStatisticsEvent();		
			gasStation.getServer().getDbController().insertFuelTransactionIntoDB(pumpId, currentProfit);		
			waitingForPumps.poll();					//we poll the first car that used the pump
		} catch (Exception e) {
			car.setReceivedFuelAmount(0);
			e.printStackTrace();
		}
		return car.getReceivedFuelAmount();
	}
	
	public CountDownLatch getCondition(){
		return notFull;
	}
	
	public void activatePump(){
		try{
			notFull.countDown();
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public CountDownLatch getNotFull() {
		return notFull;
	}

	public Lock getLockPump() {
		return lockPump;
	}

	@Override
	public String toString() {
		return "Pump [pumpId=" + pumpId + "]";
	}
	
	public Queue<Car> getWaitingCarsQueue(){
		return waitingForPumps;
	}
}
