package BL;
import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class WashingMachine  {
	
	private int secondsPerAutoClean;
	private GasStation gasStation;
	
	private Lock machine;
	private int waitingForMachine;			//counter for cars which wait for the machine
	
	public WashingMachine(GasStation gasStation, int secondsPerAutoClean) throws SecurityException, IOException {
		this.gasStation = gasStation;
		this.secondsPerAutoClean = secondsPerAutoClean;
		machine = new ReentrantLock();
	}

	public int getSecondsPerAutoClean() {
		return secondsPerAutoClean;	
	}
	
	public boolean washCar(Car car){
		gasStation.fireCarIsAtQueueEvent(car, "WashingMachineQueue");
		waitingForMachine++;
		machine.lock();
		gasStation.fireCarLeftQueueEvent(car, "WashingMachineQueue");
		if (!gasStation.isOpen()) {
			machine.unlock();
			return false;
		}
		gasStation.fireCarIsInWashingMachineEvent(car);
		car.doSomethingWhileAtWashingMachine();
		sleepWashTime(car);
		gasStation.fireCarLeftWashingMachineEvent(car);
		machine.unlock();
		waitingForMachine--;
		return true;		
	}
	
	private void sleepWashTime(Car car) {
		// it takes random time to get cleaned
		try {
			Thread.sleep(WashingServiceManager.SECONDS_PER_AUTO_CLEAN * 200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void setSecondsPerAutoClean(int secondsPerAutoClean) {
		this.secondsPerAutoClean = secondsPerAutoClean;
	}

	public int getWaitingCarsCounter(){
		return waitingForMachine;
	}
}
