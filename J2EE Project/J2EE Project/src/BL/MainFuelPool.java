package BL;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MainFuelPool {
	private GasStation gasStation;
	public double maxCapacity;
	private double currentCapacity;
	private Vector<MainFuelPoolEvents> mainFuelListeners;
	private boolean beingFilled;
	private Lock lock;
	private Condition latch;
	
	private ArrayList<Car> waitingOnFillFuelTankCars;
	private int numOfCarsFuelingUpNow;
	
	public MainFuelPool(GasStation gasStation,double maxCapacity, double currentCapacity){
		setMaxCapacity(maxCapacity);
		setCurrentCapacity(currentCapacity);
		mainFuelListeners = new Vector<MainFuelPoolEvents>();
		this.gasStation = gasStation; 
		beingFilled = false;
		waitingOnFillFuelTankCars = new ArrayList<Car>();
		lock = new ReentrantLock();
		latch = lock.newCondition();
	}
	
	// function returns the amount of fuel that has been supplied
	public double getFuel(Car car) {
		setNumOfCarsFuelingUpNow(getNumOfCarsFuelingUpNow() + 1);
		double amountWillBeReceived;
		if (car.getAmountToFuel() <= getCurrentCapacity()) {		
			amountWillBeReceived = car.getAmountToFuel();
		} 
		else if (getCurrentCapacity() > 0) {
			amountWillBeReceived = getCurrentCapacity();
		} 
		else {
			amountWillBeReceived = 0;
		}	
		try {
			Thread.sleep((long)(amountWillBeReceived * 100));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		car.setGotFuel(amountWillBeReceived);
		if (car.getAmountToFuel() <= getCurrentCapacity()) {		
			setCurrentCapacity(getCurrentCapacity() - amountWillBeReceived);
		} 
		else if (getCurrentCapacity() > 0) {
			setCurrentCapacity(0);
		} 
		checkIfLessThanTwentyPercent();
		gasStation.fireCarFinishedFuelingUpEvent(car);	
		gasStation.fireCurrentGasTankCapacityEvent();
		setNumOfCarsFuelingUpNow(getNumOfCarsFuelingUpNow() - 1);
		signalTankToFillUp();
		return amountWillBeReceived;
	}
	
	public boolean isbeingFilled() {
		return beingFilled;
	}
	
	public void fillMainPool() {	
		new Thread(new Runnable() {
			@Override
			public void run() {
				beingFilled = true;
				checkIfThereAreCarsFuelingUpNow();
				gasStation.fireFillTankEventFromBL();
				try {
					// draw a random number between 3000 and 13000
					Thread.sleep(3000 + gasStation.getRand().nextInt(10000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				setCurrentCapacity(getMaxCapacity());
				beingFilled = false;
				gasStation.fireFinishedFillingTankEvent();
				signalAllCarsToWakeUp();
			}
		}).start();
	}
	
	public void registerToMainFuelEvent(MainFuelPoolEvents observer){
		mainFuelListeners.add(observer);
	}
		
	public void notifyAllRegistered() {
		for (MainFuelPoolEvents event : mainFuelListeners) {
			event.mainPoolFuelIsRunningOut(getCurrentCapacity());
		}
	}
	
	public double getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(double maxCapacity2) {
		this.maxCapacity = maxCapacity2;
	}

	public double getCurrentCapacity() {
		return currentCapacity;
	}

	public synchronized void setCurrentCapacity(double newCapacity) {
		currentCapacity = newCapacity;		
	}
	
	private void checkIfLessThanTwentyPercent() {
		if (currentCapacity < 0.2 * getMaxCapacity()) {
			if (beingFilled == false) {
			    beingFilled = true;			    
			    notifyAllRegistered();
			}
		}
	}

	public double maxCapacity(){
		return maxCapacity;
	}
	
	public ArrayList<Car> getWaitingOnFillFuelTankCars() {
		return waitingOnFillFuelTankCars;
	}
	
	public boolean isEmpty(){
		return currentCapacity <= 0;
	}

	public boolean isBeingFilled() {
		return beingFilled;
	}
	
	public int getNumOfCarsFuelingUpNow() {
		return numOfCarsFuelingUpNow;
	}

	public synchronized void setNumOfCarsFuelingUpNow(int numOfCarsFuelingUpNow) {
		this.numOfCarsFuelingUpNow = numOfCarsFuelingUpNow;
	}	

	private void signalAllCarsToWakeUp() {
		for (int i = 0; i < gasStation.getFuelServiceManager().getNumOfPumps(); i++) {
			gasStation.getFuelServiceManager().getPumps()[i].getNotFull().countDown();
		}
	}
	
	private void checkIfThereAreCarsFuelingUpNow() {
		if (getNumOfCarsFuelingUpNow() > 0) {
			lock.lock();
			try {
				latch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			lock.unlock();
		}
	}
	private void signalTankToFillUp() {
		if (getNumOfCarsFuelingUpNow() == 0) {
			lock.lock();
			latch.signalAll();
			lock.unlock();
		}
	}
}
