package Listeners;

import ServerMessages.DataInitializeMsg;
import ServerRequests.AddCarRequest;

public interface GUIEventsListener {
	public boolean closeGasStationEvent();
	public boolean fillTankEvent();
	public double fuelCapacityPercentageInquiryEvent();
	public boolean fireAddCarFromGUIEvent(AddCarRequest request);
	public DataInitializeMsg fireDataInitializeEvent();
}
