package Listeners;

import BL.Car;
import BL.Statistics;

public interface ServerEventsListener {
	public void carArrivedEvent(Car car);
	public void statisticsEvent(Statistics stats);
	public void stationIsEmptyEvent();
	public void carIsFuelingUpEvent(Car car);
	public void carFinishedFuelingUpEvent(Car car);
	public void carIsInWashingMachineEvent(Car car);
	public void carLeftWashingMachineEvent(Car car);
	public void teamCleaningEvent(Car car, int teamID);
	public void teamCleaningFinishedEvent(Car car);
	public void carLeftEvent(Car car);
	public void carIsAtQueueEvent(Car car, String queue);
	public void carLeftQueueEvent(Car car, String queue);
	public void finishedFillingTankEvent();
	public void fillTankEventFromBL();
	public void onWaitActivityEventFired(int carID, String activity);
	public void fireFinishedOnWaitActivityEvent(int carID);
	public void currentGasTankCapacityEvent(double currentCapacity);
}