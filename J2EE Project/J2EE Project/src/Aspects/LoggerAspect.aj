package Aspects;

import java.io.IOException;
import java.util.logging.FileHandler;

import BL.Car;
import BL.CleaningTeam;
import BL.FuelServiceManager;
import BL.GasStation;
import BL.Pump;
import BL.WashingMachine;
import BL.WashingServiceManager;
import Logger.GasStationLogger;
import Logger.MyObjectFilter;

public aspect LoggerAspect {

	private GasStationLogger logger = GasStationLogger.getInstance();
	
	/** Gas station logging */
	
	after() returning(Object obj): call((Car).new(..)) ||
	 							   call((CleaningTeam).new(..)) || 
	 							   call((FuelServiceManager).new(..)) ||
	 							   call((Pump).new(..)) ||
	 							   call((WashingMachine).new(..)) ||
	 							   call((WashingServiceManager).new(..)) {
		try {
			if (obj instanceof Car) {
				Car car = (Car)obj;
				logger.createHandler(new FileHandler(GasStationLogger.LOGS_FOLDER + "Car " + car.getCarId() + ".txt"),new MyObjectFilter(car), null);
			} 
			else if (obj instanceof CleaningTeam) {
				CleaningTeam team = (CleaningTeam)obj;
				logger.createHandler(new FileHandler(GasStationLogger.LOGS_FOLDER + "Team " + team.getTeamId() + ".txt"),new MyObjectFilter(team), null);
			} 
			else if (obj instanceof FuelServiceManager) {
				FuelServiceManager fsManager = (FuelServiceManager)obj;
				logger.createHandler(new FileHandler(GasStationLogger.LOGS_FOLDER + "Fuel Service Manager.txt"),new MyObjectFilter(fsManager), null);
			} 
			else if (obj instanceof Pump) {
				Pump pump = (Pump)obj;
				logger.createHandler(new FileHandler(GasStationLogger.LOGS_FOLDER + "Pump " + pump.getPumpId() +".txt"),new MyObjectFilter(pump), null);
			} 
			else if (obj instanceof WashingMachine) {
				WashingMachine wMachine = (WashingMachine)obj;
				logger.createHandler(new FileHandler(GasStationLogger.LOGS_FOLDER + "WashingMachine.txt"),new MyObjectFilter(wMachine), null);
			} 
			else if (obj instanceof WashingServiceManager) {
				WashingServiceManager wsManager = (WashingServiceManager)obj;
				logger.createHandler(new FileHandler(GasStationLogger.LOGS_FOLDER + "Washing Service Manager.txt"),new MyObjectFilter(wsManager), null);
			}
		} catch (SecurityException e) {
		} catch (IOException e) {
		}
	}
	
	pointcut gsOpened(GasStation gs) : execution (public void openGasStation()) && args() && target(gs);	
	
	before(GasStation gs) : gsOpened(gs) {		
		logger.cleanLogFolder();
		logger.writeLog("Gas Station " + gs.getStationId() + " is open", gs);
	}
	
	pointcut gsClosed(boolean stationIsOpen, GasStation gs) : execution (public void setStationIsOpen(boolean)) && args(stationIsOpen) && target(gs);
	
	before(boolean stationIsOpen, GasStation gs) : gsClosed(stationIsOpen, gs) {
		if (!stationIsOpen) {
			logger.writeLog("Gas Station " + gs.getStationId() + " is closed", gs);
			logger.closeHandlers();	
		}
	}
	
	pointcut carAskedForCleaning(Car car, GasStation gs) : execution (public void cleanCar(Car)) && args(car) && target(gs);

	before(Car car, GasStation gs) : carAskedForCleaning(car, gs){
		logger.writeLog("Gas Station " + gs.getStationId() + " " + car +" asked for Cleaning", gs);
	}
	
	pointcut carEnteredGasStation(Car car, GasStation gs) : execution (public void addCar(Car)) && args(car) && target(gs);

	before(Car car, GasStation gs) : carEnteredGasStation(car, gs){
		logger.writeLog("Gas Station " + gs.getStationId() + " " + car + " was added", gs);
	}
	
	pointcut mainFuelPoolNeedsToBeFilled(GasStation gs) : execution (public void fillMainFuelTank()) && args() && target(gs);

	before(GasStation gs) : mainFuelPoolNeedsToBeFilled(gs){
		logger.writeLog("Gas Station " + gs.getStationId() + " Main Fuel Pool needs to be filled", gs);
	}
	
	pointcut carAskedForFuel(Car car, double amountOfFuel, int numOfPump, GasStation gs) : execution (public void getFuel(Car, double, int)) && args(car, amountOfFuel, numOfPump) && target(gs);

	before(Car car, double amountOfFuel, int numOfPump, GasStation gs) : carAskedForFuel(car, amountOfFuel, numOfPump, gs){
		logger.writeLog("Gas Station " + gs.getStationId() + " " + car +" asked for:" + amountOfFuel + " liters, pump:" + numOfPump, gs);
	}
	
	/** Car logging */
	
	pointcut carLeftGasStation(Car car) : execution (public void fireCarLeftEvent(Car)) && args(car);

	before(Car car) : carLeftGasStation(car){
		logger.writeLog("Car " + car.getCarId() + " has finished", car);
	}
	
	pointcut carGoToCleaning(Car car) : execution (public void Car.goForCleaning()) && target(car);

	before(Car car) : carGoToCleaning(car){
		logger.writeLog("Car " + car.getCarId() + " asked for washing", car);
	}
	
	pointcut carFinishedCleaning(boolean wantsCleaning, Car car) : execution (public void Car.setWantCleaning(boolean)) && args(wantsCleaning) && target(car);

	before(boolean wantsCleaning, Car car) : carFinishedCleaning(wantsCleaning, car){
		if (!wantsCleaning) {
			logger.writeLog("Car " + car.getCarId() + " was Cleaned", car);
		}
	}
	
	pointcut carGoToFueling(Car car) : execution (public void Car.goForFuel()) && target(car);

	before(Car car) : carGoToFueling(car){
		logger.writeLog("Car " + car.getCarId() + " asked for " + car.getAmountToFuel() + " liters", car);
	}
	
	after(Car car) : carGoToFueling(car){
		logger.writeLog("Car " + car.getCarId() + " received " + car.getReceivedFuelAmount() + " liters", car);
	}
	
	pointcut carReadingNewsPaper(int carID) : execution (public void Car.readNewsPaper(int)) && args(carID);

	before(int carID) : carReadingNewsPaper(carID){
		logger.writeLog("Car " + carID + " is reading newspaper while in clean service", (Car) thisJoinPoint.getThis());
	}
	
	pointcut carSmartphonePlay(int carID) : execution (public void Car.smartphonePlay(int)) && args(carID);

	before(int carID) : carSmartphonePlay(carID){
		logger.writeLog("Car " + carID + " is playing on smartphone while in clean service", (Car) thisJoinPoint.getThis());
	}
	
	pointcut carSmartphoneSpeak(int carID) : execution (public void Car.smartphoneSpeak(int)) && args(carID);

	before(int carID) : carSmartphoneSpeak(carID){
		logger.writeLog("Car " + carID + " is speaking on smartphone while in clean service", (Car) thisJoinPoint.getThis());
	}
	
	/** FuelServiceManager logging */
	
	pointcut mainPoolFuelIsRunningOut(double currentAmount, FuelServiceManager fsManager) : execution (public void FuelServiceManager.mainPoolFuelIsRunningOut(double)) && args(currentAmount) && target(fsManager);

	before(double currentAmount, FuelServiceManager fsManager) : mainPoolFuelIsRunningOut(currentAmount, fsManager){
		logger.writeLog("Main Fuel Pool needs to be filled up", fsManager);
		logger.writeLog("Main Fuel Pool is being filled up right now all pumps are disabled", fsManager);
	}
	
	after(double currentAmount, FuelServiceManager fsManager) : mainPoolFuelIsRunningOut(currentAmount, fsManager){
		logger.writeLog("Main Fuel Pool is full all pumps have been activated", fsManager);
	}
	
	pointcut activatePumps(FuelServiceManager fsManager) : execution (public void FuelServiceManager.activatePumps()) && target(fsManager);

	before(FuelServiceManager fsManager) : activatePumps(fsManager){
		for (Pump pump : fsManager.getPumps()) {
			logger.writeLog("Activating " + pump, fsManager);
		}
	}
	
	pointcut fsGiveFuel(Car car, double amountOfFuel, int numOfPump, FuelServiceManager fsManager) : execution (public void FuelServiceManager.fsGiveFuel(Car, double ,int)) && args(car, amountOfFuel, numOfPump) && target(fsManager);

	before(Car car, double amountOfFuel ,int numOfPump, FuelServiceManager fsManager) : fsGiveFuel(car, amountOfFuel , numOfPump, fsManager){
		logger.writeLog(car + " asked for :" + amountOfFuel + " from pump:" + numOfPump, fsManager);
	}
	
	after(Car car, double amountOfFuel ,int numOfPump, FuelServiceManager fsManager) : fsGiveFuel(car, amountOfFuel , numOfPump, fsManager){
		if (car.getReceivedFuelAmount() > 0) {
			logger.writeLog(car + " received: " + car.getReceivedFuelAmount()/amountOfFuel * 100 +"% of fuel, amount is:" + car.getReceivedFuelAmount(), fsManager);
		}
	}
	
	pointcut closeFuelSService(FuelServiceManager fsManager) : execution (public void FuelServiceManager.closeService()) && target(fsManager);

	before(FuelServiceManager fsManager) : closeFuelSService(fsManager){
		logger.writeLog("Fuel service has been closed", fsManager);
	}
	
	/** Pump logging */
	
	pointcut getFuelFromPump(Car car, Pump pump) : execution (public double Pump.getFuelFromPump(Car)) && args(car) && target(pump);

	before(Car car, Pump pump) : getFuelFromPump(car, pump){
		logger.writeLog(car + " is waiting for pump:" + pump.getPumpId(), pump);
	}
	
	after(Car car, Pump pump) : getFuelFromPump(car, pump){
		logger.writeLog(car + " finished at pump:" + pump.getPumpId() + " received:" + car.getReceivedFuelAmount(), pump);
	}
	
	pointcut carIsFuelingUpEventFired(Car car, Pump pump) : execution (public void GasStation.fireCarIsFuelingUpEvent(Car, Pump)) && args(car, pump);

	before(Car car, Pump pump) : carIsFuelingUpEventFired(car, pump){
		logger.writeLog(car + " is now connected to pump:" + pump.getPumpId(), pump);
	}
	
	pointcut activatePump(Pump pump) : execution (public void Pump.activatePump()) && target(pump);

	before(Pump pump) : activatePump(pump){
		logger.writeLog("Pump " + pump.getPumpId() + " is activated", pump);
	}
	
	/** WashingServiceManager logging */
	
	pointcut wsCleanCar(Car car, WashingServiceManager wsManager) : execution (public boolean WashingServiceManager.wsCleanCar(Car)) && args(car) && target(wsManager);

	before(Car car, WashingServiceManager wsManager) : wsCleanCar(car, wsManager){
		logger.writeLog(car + " has arrived to washing service, proceeding to external cleaning in the washing machine...", wsManager);
	}
		
	after(Car car, WashingServiceManager wsManager) : wsCleanCar(car, wsManager){
		logger.writeLog(car + " has finished the cleaning process", wsManager);
	}
	
	pointcut closeWashingService(WashingServiceManager wsManager) : execution (public void WashingServiceManager.closeService()) && target(wsManager);

	before(WashingServiceManager wsManager) : closeWashingService(wsManager){
		logger.writeLog("Washing service has been closed", wsManager);
	}
	
	/** WashingMachine logging */
	
	pointcut washCarInWashingMachine(Car car, WashingMachine washingMachine) : execution (public boolean WashingMachine.washCar(Car)) && args(car) && target(washingMachine);

	before(Car car, WashingMachine washingMachine) : washCarInWashingMachine(car, washingMachine){
		logger.writeLog(car + " is now waiting for external cleaning at machine", washingMachine);
	}
	
	after(Car car, WashingMachine washingMachine) : washCarInWashingMachine(car, washingMachine){
		logger.writeLog(car + " has finished external cleaning", washingMachine);
	}
	
	pointcut sleepWashTime(Car car, WashingMachine washingMachine) : execution (private void WashingMachine.sleepWashTime(Car)) && args(car) && target(washingMachine);

	before(Car car, WashingMachine washingMachine) : sleepWashTime(car, washingMachine){
		logger.writeLog(car + " is being cleaned by the machine", washingMachine);
	}
	
	/** CleaningTeam logging */
	
	pointcut manualCleanTeam(Car car, CleaningTeam cleanTeam) : execution (public void CleaningTeam.cleanCar(Car)) && args(car) && target(cleanTeam);

	before(Car car, CleaningTeam cleanTeam) : manualCleanTeam(car, cleanTeam){
		logger.writeLog(car + " is now waiting for internal cleaning", cleanTeam);
	}
	
	after(Car car, CleaningTeam cleanTeam) : manualCleanTeam(car, cleanTeam){
		logger.writeLog("Team " + cleanTeam.getTeamId() + " finished cleaning Car : " + car, cleanTeam);
	}
	
	pointcut drawRandomCleanTime(Car car, CleaningTeam cleanTeam) : execution (private void CleaningTeam.drawRandomCleanTime(Car)) && args(car) && target(cleanTeam);

	before(Car car, CleaningTeam cleanTeam) : drawRandomCleanTime(car, cleanTeam){
		logger.writeLog("Team " + cleanTeam.getTeamId() + " including : " + cleanTeam.getNumberOfWorkers() + " workers are now cleaning Car : " + car, cleanTeam);
	}
}
