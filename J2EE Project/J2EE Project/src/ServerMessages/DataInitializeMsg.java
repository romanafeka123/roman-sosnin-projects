package ServerMessages;

import java.io.Serializable;

public class DataInitializeMsg implements Serializable{

	private static final long serialVersionUID = 3593217844629099111L;	
	private int maxIdSize;
	private int maxLitersToRequest;
	private int numOfPumps;

	public DataInitializeMsg(int maxIdSize, int maxLitersToRequest,int numOfPumps) {
		this.maxIdSize = maxIdSize;
		this.maxLitersToRequest = maxLitersToRequest;
		this.numOfPumps = numOfPumps;
	}
	public int getMaxIdSize() {
		return maxIdSize;
	}
	public int getMaxLitersToRequest() {
		return maxLitersToRequest;
	}
	public int getNumOfPumps() {
		return numOfPumps;
	}
}
