package ServerMessages;

import java.io.Serializable;

public class ServerMsg implements Serializable{
	private static final long serialVersionUID = 1559884820397543544L;
	
	private Message msg;
	private int carID;
	private int pumpID;
	private int teamID;
	
	public ServerMsg(Message msg, int carID, int pumpID, int teamID) {
		this.msg = msg;
		this.carID = carID;
		this.pumpID = pumpID;
		this.teamID = teamID;
	}

	public int getCarID() {
		return carID;
	}

	public int getPumpID() {
		return pumpID;
	}

	public int getTeamID() {
		return teamID;
	}

	public Message getMsg() {
		return msg;
	}
}
