package ServerMessages;

import java.io.Serializable;

public class OnWaitActivityMsg implements Serializable{
	private static final long serialVersionUID = -5653857496026345770L;
	
	private String methodName;
	private int carID;

	public OnWaitActivityMsg(String methodName, int carID) {
		this.methodName = methodName;
		this.carID = carID;
	}

	public String getMethodName() {
		return methodName;
	}

	public int getCarID() {
		return carID;
	}
}
