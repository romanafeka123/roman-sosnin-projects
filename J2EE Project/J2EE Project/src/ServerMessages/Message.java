package ServerMessages;

public enum Message  {
	
	carArrivedMsg, 
	carLeftMsg,
	// Fueling up
	carIsFuelingUpMsg,
	carLeftFuelingUpMsg,
	carIsAtFuelQueueMsg,
	carLeftFuelQueueMsg,
	// Washing Machine
	carIsAtWashingMachineMsg,
	carLeftWashingMachineMsg,
	carIsAtWashingMachineQueueMsg,
	carLeftWashingMachineQueueMsg,
	// Cleaning Team
	carIsAtCleaningTeamMsg,
	carLeftCleaningTeamMsg,
	carIsAtCleaningTeamQueueMsg,
	carLeftCleaningTeamQueueMsg
}
