package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

import BL.Car;
import BL.GasStation;
import DAL.DBController;
import DAL.XMLloader;
import ServerMessages.DataInitializeMsg;
import ServerRequests.AddCarRequest;
import ServerRequests.AmountOfMoneyPaidRequest;
import ServerRequests.AmountOfMoneyPaidResponse;
import ServerRequests.CloseConnectionRequest;

public class TheServer implements Runnable {
	
	private static ArrayList<ClientSocket> connections = new ArrayList<ClientSocket>();
	
	private final int PORT;
	private ServerSocket server;
	private boolean serverUp;
	private Socket socket;
	private GasStation gasStation;
	private int XML_CARS_ARRIVE_RHYTHM = 400; // in milliseconds
	private static final String XML_FILE_LOCATION = "\\files\\GasStationData.txt";
	private DBController dbController;
	
	/** For synchronizing purposes **/
	private CountDownLatch latch;

	@Override
	public void run() {
		dbController = new DBController();
		loadObjectsFromXML();		
		listenToClients();	
	}
	
	public TheServer(int portNum, CountDownLatch latch) {
		this.latch = latch;
		PORT = portNum;
		try {
			server = new ServerSocket(PORT);
			serverUp = true;
		} catch (IOException e) {}
	}

	public void listenToClients() {
		while (serverUp) {
			try {
				socket = server.accept();
			} catch (IOException e) {
				// could not open socket or server was closed
				return;
			}
			// for every new incoming client, open a new thread
			new Thread(new Runnable() {
				@Override
				public void run() {
					ClientSocket currentSocketData = null;
					try {
						currentSocketData = new ClientSocket(socket);
						connections.add(currentSocketData);
						boolean connected = true;
						DataInitializeMsg dataMsg = new DataInitializeMsg(GasStation.getIdMaxSize(),
								(int) gasStation.getFuelServiceManager().getMainFuelPool().getMaxCapacity(),
								gasStation.getFuelServiceManager().getNumOfPumps());
						currentSocketData.getOutputStream().writeObject(dataMsg);
						do {
							Object obj = currentSocketData.getInputStream().readObject();
							if (obj instanceof CloseConnectionRequest) {
								connected = false;
								currentSocketData.closeConnection();
								connections.remove(currentSocketData);						
							}	
							else if (obj instanceof AddCarRequest) {
								AddCarRequest req = (AddCarRequest) obj;
								Car car = new Car(gasStation, req.getCarID(), req.isNeedsCleaning(), req.isNeedsFuel(), req.getNumOfPump(), req.getAmountToFuel(), true);
								// Car that arrived from client, has an output stream field.
								car.setOutStream(currentSocketData.getOutputStream());
								if (gasStation.getCurrentCarIDsInStation().contains((Integer)car.getCarId())) {
									MessageSending.duplicateCarIdMessageEvent(currentSocketData.getOutputStream());
								}
								else {
									gasStation.addCar(car);
								}
							}
							else if (obj instanceof AmountOfMoneyPaidRequest) {
								AmountOfMoneyPaidRequest req = (AmountOfMoneyPaidRequest)obj;
								AmountOfMoneyPaidResponse msg = new AmountOfMoneyPaidResponse(-1.0);
								int carID = req.getCarID();
								double totalMoneyPaid = gasStation.getServedCarTotalMoneyPaidByCarID(carID);
								msg.setMoneyPaid(totalMoneyPaid);				
								currentSocketData.getOutputStream().writeObject(msg);
							}
						} while (connected);
					} catch (ClassNotFoundException e) {} catch (IOException e) {} finally {
						try {
							if (currentSocketData != null) {
								currentSocketData.getSocket().close();
							}
						} catch (IOException e) {}
					}
				}
			}).start();
		}
	}

	public boolean isServerUp() {
		return serverUp;
	}

	public void closeServer() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Iterator<ClientSocket> it = connections.iterator();
				while (it.hasNext()) {
					try {
						ClientSocket cs = it.next();
						cs.getOutputStream().writeObject(new CloseConnectionRequest());
						cs.closeConnection();
						it.remove();
					} catch (IOException e) {
					}
				}
			}
		}).start();
		try {
			server.close();
			serverUp = false;
			dbController.closeConnection();
		} catch (IOException e) {
		}
	}
	
	private void loadObjectsFromXML() {
		XMLloader loader = new XMLloader(XML_FILE_LOCATION);
		try {
			gasStation = loader.loadGasStation(); // load gas station from file	
			gasStation.setServer(this);
			latch.countDown();
			ArrayList<Car> cars = loader.loadCars(); // load cars from file
			Car currentCar;
			for (int i = 0; i < cars.size(); i++) {
				Thread.sleep(XML_CARS_ARRIVE_RHYTHM);
				currentCar = cars.get(i);
				currentCar.setGasStation(gasStation);
				gasStation.addCar(currentCar);
			}
			gasStation.start();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static TheServer runServer(CountDownLatch latch) {
		TheServer server = new TheServer(7000, latch);
		Thread t = new Thread(server);
		t.start();
		return server;
	}

	public GasStation getGasStation() {
		return gasStation;
	}

	public DBController getDbController() {
		return dbController;
	}
	
}
