package Server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientSocket {

	private Socket socket;
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;
	private String clientIP;

	public ClientSocket(Socket socket) {
		this.socket = socket;
		try {
			inStream = new ObjectInputStream(socket.getInputStream());
			outStream = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException e) {
		}
		clientIP = socket.getInetAddress() + ":" + socket.getPort();
	}

	public Socket getSocket() {
		return socket;
	}

	public ObjectInputStream getInputStream() {
		return inStream;
	}

	public ObjectOutputStream getOutputStream() {
		return outStream;
	}

	public String getClientIP() {
		return clientIP;
	}
	
	public void closeConnection() {
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
