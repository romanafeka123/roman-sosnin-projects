package Server;

import java.io.IOException;
import java.io.ObjectOutputStream;

import ServerMessages.DuplicateCarIDMessage;
import ServerMessages.Message;
import ServerMessages.OnWaitActivityMsg;
import ServerMessages.ServerMsg;

public class MessageSending {
	
	public static synchronized void carArrivedEvent(ObjectOutputStream outStream, int carID) {
		try {
			outStream.writeObject(new ServerMsg(Message.carArrivedMsg, carID,-1, -1));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void carIsFuelingUpEvent(ObjectOutputStream outStream, int carID, int pumpID) {
		try {
			outStream.writeObject(new ServerMsg(Message.carIsFuelingUpMsg,carID, pumpID, -1));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void carFinishedFuelingUpEvent(ObjectOutputStream outStream, int carID) {
		try {
			outStream.writeObject(new ServerMsg(Message.carLeftFuelingUpMsg, carID, -1, -1));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static synchronized void carIsInWashingMachineEvent(ObjectOutputStream outStream, int carID) {
		try {
			outStream.writeObject(new ServerMsg(Message.carIsAtWashingMachineMsg, carID, -1, -1));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static synchronized void carLeftWashingMachineEvent(ObjectOutputStream outStream, int carID) {
		try {
			outStream.writeObject(new ServerMsg(Message.carLeftWashingMachineMsg, carID, -1, -1));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void teamCleaningEvent(ObjectOutputStream outStream, int carID, int teamID) {
		try {
			outStream.writeObject(new ServerMsg(Message.carIsAtCleaningTeamMsg, carID, -1, teamID));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static synchronized void teamCleaningFinishedEvent(ObjectOutputStream outStream, int carID) {
		try {
			outStream.writeObject(new ServerMsg(Message.carLeftCleaningTeamMsg, carID, -1, -1));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static synchronized void carLeftEvent(ObjectOutputStream outStream, int carID) {
		try {
			outStream.writeObject(new ServerMsg(Message.carLeftMsg, carID, -1,-1));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void carIsAtQueueEvent(ObjectOutputStream outStream, int carID, String queue) {
		try {
			if (queue.equals("PumpQueue")) {
				outStream.writeObject(new ServerMsg(Message.carIsAtFuelQueueMsg, carID, -1, -1));
			} else if (queue.equals("WashingMachineQueue")) {
				outStream.writeObject(new ServerMsg(Message.carIsAtWashingMachineQueueMsg, carID, -1, -1));
			} else if (queue.equals("CleaningTeamQueue")) {
				outStream.writeObject(new ServerMsg(Message.carIsAtCleaningTeamQueueMsg, carID, -1, -1));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void carLeftQueueEvent(ObjectOutputStream outStream, int carID, String queue) {
		try {
			if (queue.equals("PumpQueue")) {
				outStream.writeObject(new ServerMsg(Message.carLeftFuelQueueMsg, carID, -1, -1));
			} else if (queue.equals("WashingMachineQueue")) {
				outStream.writeObject(new ServerMsg(Message.carLeftWashingMachineQueueMsg, carID, -1, -1));
			} else if (queue.equals("CleaningTeamQueue")) {
				outStream.writeObject(new ServerMsg(Message.carLeftCleaningTeamQueueMsg, carID, -1, -1));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// reflection-annotation event
	public static synchronized void onWaitActivityEvent(ObjectOutputStream outStream, String methodName, int carID) {
		OnWaitActivityMsg msg = new OnWaitActivityMsg(methodName, carID);
		try {
			outStream.writeObject(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void duplicateCarIdMessageEvent(ObjectOutputStream outStream) {
		DuplicateCarIDMessage msg = new DuplicateCarIDMessage();
		try {
			outStream.writeObject(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
