package com.afeka.afekaeggs.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.afeka.afekaeggs.AfekaEggsApplication;
import com.afeka.afekaeggs.BL.Player;
import com.afeka.afekaeggs.GameCommunicationUtils.ClientConnection;
import com.afeka.afekaeggs.GameCommunicationUtils.ServerThread;
import com.afeka.afekaeggs.Levels.Level;
import com.afeka.afekaeggs.Levels.Level1;
import com.afeka.afekaeggs.Levels.Level2;
import com.afeka.afekaeggs.Levels.Level3;
import com.afeka.afekaeggs.Properties;
import com.afeka.afekaeggs.R;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Roman on 12/19/2015.
 */
public class PlayGameActivity extends Activity {
    // BL elements
    private Activity thisActivity = this;
    private Level level;
    private String currentPlayerName;
    private List<Player> playersInGameList;
    private boolean isGroupInitializer;

    // UI
    private RelativeLayout playGameLayout;

    // Communication elements
    private ServerThread serverThread;          // will be null if game client
    private ClientConnection clientConnection;  // will be null if game server

    //*******************************************************************************************\\
    //********************************** Callback methods section *******************************\\
    //*******************************************************************************************\\
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_game_layout);
        initializeFields();
        setCommunication();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseResources();
    }

    @Override
    public void onBackPressed() {
        backToMainMenu();
    }

    public void backToMainMenu() {
        setResult(RESULT_OK, new Intent());
        finish();
    }

    //********************************** BL & GUI Section ***************************************\\
    private void initializeFields() {
        Properties.GAME_STATUS = Properties.GAME_IS_IN_ACTION_STATUS;
        playGameLayout = (RelativeLayout)findViewById(R.id.play_game_layout);
        playersInGameList = new LinkedList<Player>();
        // get all the players info
        String initializerPlayerName = getIntent().getStringExtra(Properties.GROUP_INITIALIZER_NAME);
        Player initializerPlayer = new Player(initializerPlayerName, 0);
        playersInGameList.add(initializerPlayer);

        String[] joinedPlayersArray = getIntent().getStringArrayExtra(Properties.JOINED_PLAYERS_ARRAY);
        for (int i = 0; i < joinedPlayersArray.length; i++) {
            Player player = new Player(joinedPlayersArray[i], 0);
            playersInGameList.add(player);
        }

        isGroupInitializer = getIntent().getBooleanExtra(Properties.IS_GROUP_INITIALIZER, false);
        currentPlayerName = getIntent().getStringExtra(Properties.CURRENT_PLAYER_NAME);
        goToLevelNumber(1);
    }

    private void releaseResources() {
        playersInGameList = null;
        level.releaseResources();
        level = null;
    }

    public void setPlaygroundImage(int playBackgroundSource) {
        final RelativeLayout playGameLayout = (RelativeLayout)findViewById(R.id.play_game_layout);
        final BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(playBackgroundSource);
        playGameLayout.post(new Runnable() {
            @Override
            public void run() {
                playGameLayout.setBackgroundDrawable(bg);
            }
        });
    }

    public List<Player> getPlayersList() {
        return playersInGameList;
    }

    private void makeStartGameAnimation() {
        level.playAnimationOnCenterOfScreen(AfekaEggsApplication.THREE_TWO_ONE_SRC);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                playGameLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        level.playAnimationOnCenterOfScreen(AfekaEggsApplication.FIREWORK_SRC);
                    }
                });
            }
        }).start();
    }

    public void goToLevelNumber(int levelNumber) {
        if (levelNumber == 1) {
            level = new Level1(this, isGroupInitializer);
        }
        else if (levelNumber == 2) {
            level = new Level2(this, isGroupInitializer);
        }
        else if (levelNumber == 3) {
            level = new Level3(this, isGroupInitializer);
        }
        level.setEggsOnField();
        makeStartGameAnimation();
        if (isGroupInitializer && levelNumber != 1) {
            level.startGame();
        }
    }

    public void removeJoinedPlayerFromListView(String playerNameToRemove) {
        if (playersInGameList.size() == 2 && isGroupInitializer) {
            playGameLayout.post(new Runnable() {
                @Override
                public void run() {
                    AlertDialog alertDialog = new AlertDialog.Builder(thisActivity).create();
                    alertDialog.setMessage(Properties.LAST_PLAYER_LEFT_GAME_MSG);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    backToMainMenu();
                                }
                            });
                    alertDialog.show();
                }
            });
        }
        else {
            for (int i = 0; i < playersInGameList.size(); i++) {
                Player player = playersInGameList.get(i);
                if (player.getName().equals(playerNameToRemove)) {
                    playersInGameList.remove(i);
                    i--;
                }
            }
            level.updateListView();
        }
    }

    //********************************** Communication  section **********************************\\
    private void setCommunication() {
        if (isGroupInitializer) {
            serverThread = ServerThread.getInstance(false);
            serverThread.setPlayGameActivity((PlayGameActivity)this);
            System.out.println("STARTING GAME!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            level.startGame();
        }
        else {
            clientConnection = ClientConnection.getInstance(false);
            clientConnection.setPlayGameActivity((PlayGameActivity) this);
        }
    }

    public void sendMessageToClients(Object message) {
        serverThread.sendMessageToClients(message);
    }

    public void sendClickedOnEggMsgToGameServer(long currentPlayerEggBreakDelay, int brokenEggNumber) {
        clientConnection.sendClickedOnEggMsgToGameServer(currentPlayerName, currentPlayerEggBreakDelay, brokenEggNumber);
    }

    //******************************** Getters & Setters *****************************************\\
    public Level getLevel() {return level;}
    public String getCurrentPlayerName() { return currentPlayerName; }
    public void setPlayersInGameList(List<Player> playersInGameList) {this.playersInGameList = playersInGameList;}
    public InputStream getRawResource(int rawResourceID) {return getResources().openRawResource(rawResourceID);}

}