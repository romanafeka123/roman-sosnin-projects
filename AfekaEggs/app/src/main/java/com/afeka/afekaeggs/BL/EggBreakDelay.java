package com.afeka.afekaeggs.BL;

/**
 * Created by Roman on 1/25/2016.
 */
// this class represents the time that was taken to break an egg, the number of the broken egg and the player
public class EggBreakDelay {

    private String playerName;
    private long eggBreakDelay;
    private int brokenEggNumber;

    public EggBreakDelay(String playerName, long eggBreakDelay, int brokenEggNumber) {
        this.playerName = playerName;
        this.eggBreakDelay = eggBreakDelay;
        this.brokenEggNumber = brokenEggNumber;
    }

    public String getPlayerName() {
        return playerName;
    }
    public long getEggBreakDelay() {
        return eggBreakDelay;
    }
    public int getBrokenEggNumber() {
        return brokenEggNumber;
    }
}
