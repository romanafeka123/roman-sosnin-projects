package com.afeka.afekaeggs.BL;

/**
 * Created by Roman on 12/25/2015.
 */
public class Player {
    private String name;
    private int score;

    public Player(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }
    public int getScore() {
        return score;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setScore(int score) {
        this.score = score;
    }

    public void increaseScoreBy(int increaseBy) {
        score += increaseBy;
    }
}
