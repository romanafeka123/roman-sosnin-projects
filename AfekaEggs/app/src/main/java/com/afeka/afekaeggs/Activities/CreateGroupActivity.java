package com.afeka.afekaeggs.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.afeka.afekaeggs.CustomGUIElements.AlertDialogHelper;
import com.afeka.afekaeggs.GameCommunicationUtils.IPUtils;
import com.afeka.afekaeggs.GameCommunicationUtils.NetworkConnectionUtils;
import com.afeka.afekaeggs.CustomGUIElements.ProgressBarHelper;
import com.afeka.afekaeggs.GameCommunicationUtils.ParseCloudCommunication;
import com.afeka.afekaeggs.Properties;
import com.afeka.afekaeggs.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Roman on 12/20/2015.
 */
public class CreateGroupActivity extends Activity {
    // UI elements
    private RelativeLayout createGroupLayout;
    private Spinner playersAmountSpinner;
    private ImageButton doneBtn;
    private EditText groupNameEditTxt, yourNameEditTxt;
    private ParseCloudCommunication groupCreateCloudComminucation;
    private ProgressBarHelper progressBarHelper;
    private AlertDialogHelper alertDialogHelper;

    // For synchronizing purposes - to avoid Racing Condition
    private ReentrantLock lock;
    private CountDownLatch latch;

    // Connection & network components
    private NetworkConnectionUtils networkConnectionUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_group_layout);
        initializeFields();
        setValuesToSpinner();
        setListeners();
    }

    // finish this activity if left the game while playing
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK){
                finish();
            }
        }
    }

    private void initializeFields() {
        createGroupLayout = (RelativeLayout) findViewById(R.id.createGroupLayout);
        playersAmountSpinner = (Spinner) findViewById(R.id.spinner);
        doneBtn = (ImageButton) findViewById(R.id.done_btn);
        groupNameEditTxt = (EditText) findViewById(R.id.group_name_edit_txt);
        yourNameEditTxt = (EditText) findViewById(R.id.your_name_edit_txt);
        groupCreateCloudComminucation = new ParseCloudCommunication(this, -1);
        progressBarHelper = new ProgressBarHelper(createGroupLayout, doneBtn.getContext());
        alertDialogHelper = new AlertDialogHelper(this);
        networkConnectionUtils = new NetworkConnectionUtils(this);
    }

    private void setValuesToSpinner() {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = Properties.MIN_PLAYERS_AMOUNT; i <= Properties.MAX_PLAYERS_AMOUNT; i++)
            list.add(i);
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(getApplicationContext(), R.layout.spinner_text_view_layout, list);
        adapter.setDropDownViewResource(R.layout.spinner_text_view_layout);
        playersAmountSpinner.setAdapter(adapter);
    }

    private void setListeners() {
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (networkConnectionUtils.isInternetConnectionUP()) {
                    if (isInputCorrect()) {
                        lock = new ReentrantLock();
                        latch = new CountDownLatch(1);
                        openGroupInCloud();
                        progressBarHelper.showProgressbarOnWait(Properties.CREATING_GROUP_MSG_ON_WAIT);
                        openAwaitingActivityOnGroupCreatedEvent();
                    } else {
                        alertDialogHelper.showAlert(Properties.WRONG_NAME_LENGTH);
                    }
                } else {
                    alertDialogHelper.showAlert(Properties.INTERNET_CONNECTION_DOWN_MSG);
                }
            }
        });
    }

    private boolean isInputCorrect() {
        if (groupNameEditTxt.getText().length() < Properties.MIN_GROUP_NAME_LEN || groupNameEditTxt.getText().length() > Properties.MAX_GROUP_NAME_LEN)
            return false;
        if (yourNameEditTxt.getText().length() < Properties.MIN_PLAYER_NAME_LEN || yourNameEditTxt.getText().length() > Properties.MAX_PLAYER_NAME_LEN)
            return false;
        return true;
    }

    private void openGroupInCloud() {
        String groupName = groupNameEditTxt.getText().toString();
        String initializerName = yourNameEditTxt.getText().toString();
        String initializerIP = IPUtils.getIPAddress(true); // get IPv4 of the current device
        int playersAmount = (Integer) playersAmountSpinner.getSelectedItem();
        groupCreateCloudComminucation.openGroupInCloud(groupName, initializerName, initializerIP, playersAmount, lock, latch);
    }

    private void openAwaitingActivity() {
        Intent intent = new Intent(getApplicationContext(), AwaitingActivity.class);
        intent.putExtra(Properties.GROUP_NAME, groupNameEditTxt.getText().toString());
        intent.putExtra(Properties.PLAYER_NAME, yourNameEditTxt.getText().toString());
        intent.putExtra(Properties.NUMBER_OF_PLAYERS_IN_GAME, (Integer) playersAmountSpinner.getSelectedItem());
        intent.putExtra(Properties.IS_GROUP_INITIALIZER, true);
        startActivityForResult(intent, 1);
    }

    private void openAwaitingActivityOnGroupCreatedEvent() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                progressBarHelper.stopShowingProgressbarOnWait();
                if (groupCreateCloudComminucation.isSucceededToCreateGroup()) {
                    openAwaitingActivity();
                }
                else {
                    createGroupLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            alertDialogHelper.showAlert(Properties.INTERNET_CONNECTION_DOWN_MSG);
                        }
                    });
                }
            }
        }).start();
    }
}