package com.afeka.afekaeggs.BL;

/**
 * Created by Roman on 12/25/2015.
 */
public class Group {
    private String groupName;
    private int numOfPlayersInGroup;
    private String initializerName;
    private String initializerIP;

    public Group(String groupName, int numOfPlayersInGroup, String initializerName, String initializerIP) {
        this.groupName = groupName;
        this.numOfPlayersInGroup = numOfPlayersInGroup;
        this.initializerName = initializerName;
        this.initializerIP = initializerIP;
    }

    public String getGroupName() { return groupName; }
    public int getNumOfPlayersInGroup() { return numOfPlayersInGroup; }
    public String getInitializerIP() { return initializerIP; }
    public String getInitializerName() { return initializerName; }
}
