package com.afeka.afekaeggs.Activities;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Movie;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.RelativeLayout;

import com.afeka.afekaeggs.AfekaEggsApplication;
import com.afeka.afekaeggs.CustomGUIElements.GifView;
import com.afeka.afekaeggs.R;

import java.io.InputStream;
import java.util.Random;

/**
 * Created by Roman on 12/20/2015.
 */
public class ReadmeActivity extends Activity {
    private RelativeLayout readmeLayout;
    private int gifWidth;
    private int gifHeight;
    private int gifDuration;
    private Activity thisActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.readme);
        thisActivity = this;
        readmeLayout = (RelativeLayout)findViewById(R.id.readme_layout);
        initializeGIF();
        startAnimations();
    }

    private void startAnimations() {
        startAnimatingGIF();
    }

    private int[] randomizeAnimationCoordinates() {
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int dpWidth = displayMetrics.widthPixels;
        int dpHeight = displayMetrics.heightPixels;
        Random random = new Random();
        int randomizedWidth = random.nextInt(dpWidth);
        int randomizedHeight = random.nextInt(dpHeight);
        return new int[] {randomizedWidth, randomizedHeight};
    }

    private void initializeGIF() {
        InputStream gifInputStream = getResources().openRawResource(AfekaEggsApplication.MEDIUM_EGG_SRC);
        Movie gifMovie = Movie.decodeStream(gifInputStream);
        gifWidth = gifMovie.width();
        gifHeight = gifMovie.height();
        gifDuration = gifMovie.duration();
    }

    private void startAnimatingGIF() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!thisActivity.isFinishing()) {
                    int[] randomizedScreenLocation = randomizeAnimationCoordinates();
                    final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(gifWidth, gifHeight);
                    params.leftMargin = randomizedScreenLocation[0] - gifWidth;
                    params.topMargin = randomizedScreenLocation[1] - gifHeight;
                    readmeLayout.post(new Runnable() {
                        public void run() {
                            GifView gifView = new GifView(thisActivity, AfekaEggsApplication.MEDIUM_EGG_SRC, readmeLayout, true, true, false);
                            gifView.setBackgroundColor(Color.TRANSPARENT);
                            readmeLayout.addView(gifView, params);
                        }
                    });
                    try {
                        Thread.sleep(gifDuration);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
