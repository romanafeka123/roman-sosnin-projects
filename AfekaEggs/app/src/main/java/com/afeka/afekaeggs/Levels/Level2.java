package com.afeka.afekaeggs.Levels;

import android.graphics.Movie;

import com.afeka.afekaeggs.Activities.PlayGameActivity;
import com.afeka.afekaeggs.AfekaEggsApplication;
import com.afeka.afekaeggs.Properties;

import java.io.InputStream;

/**
 * Created by Roman on 1/28/2016.
 */
public class Level2 extends Level {

    public Level2(PlayGameActivity playGameActivity, boolean isServerPlayer) {
        super(playGameActivity, isServerPlayer);
        playGameActivity.setPlaygroundImage(Properties.PLAY_GAME_BACKGROUND_2);
        playBackgroundMelody(Properties.LEVEL_2_MELODY_SRC);
    }

    @Override
    public void setEggsOnField() {
        setMediumEggs();
        setSmallEggs();
        setListenersOnEggs();
    }

    private void setMediumEggs() {
        int screenWidth = AfekaEggsApplication.getScreenWidth() / 8;
        int screenHeight = AfekaEggsApplication.getScreenHeight();
        InputStream gifInputStream = playGameActivity.getRawResource(AfekaEggsApplication.MEDIUM_EGG_SRC);
        Movie gifMovie = Movie.decodeStream(gifInputStream);
        int movieWidth = gifMovie.width();
        int movieHeight = gifMovie.height();

        addEggToField(screenWidth*0, screenHeight, movieWidth, movieHeight,  AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth*1, screenHeight - movieHeight/4, movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth*2, screenHeight, movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth*3, screenHeight - movieHeight/4, movieWidth, movieHeight,  AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth * 4, screenHeight, movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth * 5, screenHeight - movieHeight / 4, movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth * 6, screenHeight, movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth * 7, screenHeight - movieHeight / 4, movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
    }

    private void setSmallEggs() {
        int screenWidth = AfekaEggsApplication.getScreenWidth() / 8;
        int screenHeight = AfekaEggsApplication.getScreenHeight();
        InputStream gifInputStream = playGameActivity.getRawResource(AfekaEggsApplication.SMALL_EGG_SRC);
        Movie gifMovie = Movie.decodeStream(gifInputStream);
        int movieWidth = gifMovie.width();
        int movieHeight = gifMovie.height();

        //-----------------------------------------------On left side--------------------------------------------------\\
        addEggToField(screenWidth*0, screenHeight/2 + screenHeight/40, movieWidth, movieHeight,  AfekaEggsApplication.SMALL_EGG_SRC);
        addEggToField(screenWidth*1, screenHeight/2 + screenHeight/40, movieWidth, movieHeight, AfekaEggsApplication.SMALL_EGG_SRC);
        addEggToField(screenWidth*2, screenHeight/2 + screenHeight/40, movieWidth, movieHeight, AfekaEggsApplication.SMALL_EGG_SRC);
        //-----------------------------------------------On right side--------------------------------------------------\\
        addEggToField(screenWidth*5, screenHeight/2 + screenHeight/10, movieWidth, movieHeight,  AfekaEggsApplication.SMALL_EGG_SRC);
        addEggToField(screenWidth*6, screenHeight/2 + screenHeight/10, movieWidth, movieHeight, AfekaEggsApplication.SMALL_EGG_SRC);
        addEggToField(screenWidth*7, screenHeight/2 + screenHeight/10, movieWidth, movieHeight, AfekaEggsApplication.SMALL_EGG_SRC);
        //-----------------------------------------------Moving eggs----------------------------------------------------\\
        addEggToField((int) (screenWidth * 4) - screenWidth/20, 2 * screenHeight / 3, movieWidth, movieHeight, AfekaEggsApplication.SMALL_EGG_SRC);
        addEggToField((int) (screenWidth * 4) - screenWidth/20, 2 * screenHeight / 3, movieWidth, movieHeight, AfekaEggsApplication.SMALL_EGG_SRC);
        addEggToField((int) (screenWidth * 4) - screenWidth/20, 2 * screenHeight / 3, movieWidth, movieHeight, AfekaEggsApplication.SMALL_EGG_SRC);
        addEggToField((int) (screenWidth * 4) - screenWidth/20, 2 * screenHeight / 3, movieWidth, movieHeight, AfekaEggsApplication.SMALL_EGG_SRC);
        addEggToField((int) (screenWidth * 4) - screenWidth/20, 2 * screenHeight / 3, movieWidth, movieHeight, AfekaEggsApplication.SMALL_EGG_SRC);
        addEggToField((int) (screenWidth * 4) - screenWidth/20, 2 * screenHeight / 3, movieWidth, movieHeight, AfekaEggsApplication.SMALL_EGG_SRC);

        setAnimations((screenWidth * 7) / 2);
    }

    private void setAnimations(final int translationX) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                setAnimationOnEggs(eggsMap.get(15), translationX, 7000);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setAnimationOnEggs(eggsMap.get(16), -translationX, 7000);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setAnimationOnEggs(eggsMap.get(17), translationX, 7000);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setAnimationOnEggs(eggsMap.get(18), -translationX, 7000);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setAnimationOnEggs(eggsMap.get(19), translationX, 7000);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setAnimationOnEggs(eggsMap.get(20), -translationX, 7000);
                setObstacleAnimation(AfekaEggsApplication.CHICK_SRC, translationX, 7000);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setObstacleAnimation(AfekaEggsApplication.CHICK_SRC, -translationX, 7000);
            }
        }).start();
    }
}