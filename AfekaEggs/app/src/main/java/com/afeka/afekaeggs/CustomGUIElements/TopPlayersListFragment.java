package com.afeka.afekaeggs.CustomGUIElements;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.afeka.afekaeggs.BL.TopPlayer;
import com.afeka.afekaeggs.Properties;
import com.afeka.afekaeggs.R;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman on 12/20/2015.
 */
public class TopPlayersListFragment extends ListFragment implements AdapterView.OnItemClickListener {
    private List<TopPlayer> topPlayersList;
    private List<String> topPlayersFormattedList;
    private TopPlayerClicked mCallback;

    public interface TopPlayerClicked {
        public void sendTopPlayerCoordinates(LatLng topPlayerCoordinates);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        topPlayersList = new ArrayList<TopPlayer>();
        buildTopPlayersFormattedList();
        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.my_custom_txt_view, topPlayersFormattedList);
        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (TopPlayerClicked) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null;
        super.onDetach();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            TopPlayer topPlayerClicked = topPlayersList.get(position);
            double latitude = Double.parseDouble(topPlayerClicked.getLocationLatitude());
            double longitude = Double.parseDouble(topPlayerClicked.getLocationLongitude());
            if (latitude != Properties.SPECIAL_SIGN_FOR_NON_EXISTING_COORDINATES && longitude != Properties.SPECIAL_SIGN_FOR_NON_EXISTING_COORDINATES) {
                LatLng coordinates = new LatLng(latitude, longitude);
                mCallback.sendTopPlayerCoordinates(coordinates);
            }
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Such element does not exist");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setTopPlayersListOnListView(List<TopPlayer> topPlayersList) {
        this.topPlayersList = topPlayersList;
        buildTopPlayersFormattedList();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.my_custom_txt_view, topPlayersFormattedList);
        setListAdapter(adapter);
    }

    private void buildTopPlayersFormattedList() {
        topPlayersFormattedList = new ArrayList<String>();
        for (int i = 0; i < topPlayersList.size(); i++) {
            topPlayersFormattedList.add(topPlayersList.get(i).toString());
        }
        // padding the empty list lines
        for (int i = 0; i < (Properties.TOP_PLAYERS_AMOUNT - topPlayersList.size()) + Properties.TOP_PLAYERS_AMOUNT; i++) {
            topPlayersFormattedList.add("");
        }
    }
}