package com.afeka.afekaeggs.GameMessages;

import java.io.Serializable;

/**
 * Created by Roman on 1/3/2016.
 */
public class JoinGameRequest implements Serializable {
    private static final long serialVersionUID	= 1L;
    private String playerName;

    public JoinGameRequest(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }
}
