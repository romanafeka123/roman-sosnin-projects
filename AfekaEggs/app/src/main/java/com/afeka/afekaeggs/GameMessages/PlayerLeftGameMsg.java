package com.afeka.afekaeggs.GameMessages;

import java.io.Serializable;

/**
 * Created by Roman on 1/5/2016.
 */
public class PlayerLeftGameMsg implements Serializable{
    private static final long serialVersionUID	= 1L;
    private String playerName;

    public PlayerLeftGameMsg(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }
}
