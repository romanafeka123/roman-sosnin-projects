package com.afeka.afekaeggs.CustomGUIElements;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.afeka.afekaeggs.BL.Player;
import com.afeka.afekaeggs.Properties;
import com.afeka.afekaeggs.R;

import java.util.List;

/**
 * Created by Roman on 12/25/2015.
 */
public class PlayersScoresAdapter extends BaseAdapter {
    private final Context context;
    private List<Player> playersList;

    public PlayersScoresAdapter(Context context, List<Player> playersList) {
        this.context = context;
        this.playersList = playersList;
    }

    @Override
    public int getCount() {
        return playersList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.players_scores_listview_layout, parent, false);
        }

        Player player = playersList.get(position);

        // setting player name
        TextView playerNameTxtView = (TextView) v.findViewById(R.id.player_name_txt_view);
        String groupNameFormattedString = getFormattedGroupNameString(player.getName());
        playerNameTxtView.setText(groupNameFormattedString);

        // setting player score
        TextView playerScoreTxtView = (TextView) v.findViewById(R.id.player_score_txt_view);
        playerScoreTxtView.setText(player.getScore() + "");

        return v;
    }

    private String getFormattedGroupNameString(String playerName) {
        StringBuffer formattedString = new StringBuffer();
        formattedString.append(playerName);
        int groupNameLength = playerName.length();
        for (int i = 0; i < Properties.MAX_PLAYER_NAME_LEN - groupNameLength; i++) {
            formattedString.append(' ');
        }
        return formattedString.toString();
    }

}
