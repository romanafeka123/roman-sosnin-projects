package com.afeka.afekaeggs.CustomGUIElements;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.RelativeLayout;

/**
 * Created by Roman on 1/4/2016.
 */
public class ProgressBarHelper {
    private ProgressDialog progressBar;
    private boolean progressbarActive;
    private RelativeLayout layout;
    private Context context;

    public ProgressBarHelper(RelativeLayout layout, Context context) {
        this.layout = layout;
        this.context = context;
        initializeProgressbar();
    }

    private void initializeProgressbar() {
        progressBar = new ProgressDialog(context);
        progressBar.setCancelable(true);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
    }

    public void showProgressbarOnWait(String msg) {
        progressBar.setMessage(msg);
        new Thread(new Runnable() {
            @Override
            public void run() {
                progressbarActive = true;
                int progressBarStatus = 0;
                layout.post(new Runnable() {
                    public void run() {
                        progressBar.show();
                    }
                });
                while (progressbarActive) {
                    progressBarStatus = (progressBarStatus + 10) % 100;
                    final int finalProgressBarStatus = progressBarStatus;
                    layout.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(finalProgressBarStatus);
                        }
                    });
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void stopShowingProgressbarOnWait() {
        progressbarActive = false;
        progressBar.dismiss();
    }
}
