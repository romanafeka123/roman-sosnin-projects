package com.afeka.afekaeggs.Levels;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.graphics.Movie;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afeka.afekaeggs.Activities.PlayGameActivity;
import com.afeka.afekaeggs.AfekaEggsApplication;
import com.afeka.afekaeggs.BL.Egg;
import com.afeka.afekaeggs.BL.EggBreakDelay;
import com.afeka.afekaeggs.BL.EggRandomizerThread;
import com.afeka.afekaeggs.BL.Player;
import com.afeka.afekaeggs.BL.TopPlayer;
import com.afeka.afekaeggs.BL.TopPlayersListServices;
import com.afeka.afekaeggs.CustomGUIElements.GifView;
import com.afeka.afekaeggs.CustomGUIElements.PlayersScoresAdapter;
import com.afeka.afekaeggs.GameCommunicationUtils.ParseCloudCommunication;
import com.afeka.afekaeggs.GameMessages.ClickedOnEggMsg;
import com.afeka.afekaeggs.GameMessages.FastestEggBreakerMsg;
import com.afeka.afekaeggs.GameMessages.GoToNextLevelMsg;
import com.afeka.afekaeggs.GameMessages.OpenEggMsg;
import com.afeka.afekaeggs.Properties;
import com.afeka.afekaeggs.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Roman on 12/27/2015.
 */
public abstract class Level implements Serializable, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    protected PlayGameActivity playGameActivity;
    protected RelativeLayout playGameLayout;
    protected ListView playersListView;
    protected PlayersScoresAdapter playersScoresAdapter;
    protected List<Player> playersInGameList;

    protected EggRandomizerThread eggRandomizer;
    protected Map<Integer,Egg> eggsMap;
    protected Random random = new Random();
    protected int currentEggNumber = 1;

    protected String currentPlayerName;
    protected List<EggBreakDelay> clientPlayersEggBreakDelays;
    protected String currentPlayerLeader;

    protected Map<Integer, Long> currentPlayerEggBreakDelays;
    protected Map<Integer, Long> openEggStartTimesMap;  // in millis

    protected boolean isServerPlayer;

    protected MediaPlayer backgroundMelodyMediaPlayer;
    protected MediaPlayer mediaPlayer;

    // Top Players variables section
    private ParseCloudCommunication parse;
    private List<TopPlayer> topPlayersList;
    // location tracking of current device
    private Geocoder geocoder;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private boolean locationTrackingCompletedSuccessfully;

    //************************* Initialization of BL & GUI & Sound Components *******************\\
    public Level(PlayGameActivity playGameActivity, boolean isServerPlayer) {
        this.playGameActivity = playGameActivity;
        this.isServerPlayer = isServerPlayer;
        currentPlayerName = playGameActivity.getCurrentPlayerName();
        playGameLayout = (RelativeLayout)playGameActivity.findViewById(R.id.play_game_layout);
        eggRandomizer = new EggRandomizerThread(this);
        eggsMap = new HashMap<Integer, Egg>();
        if (isServerPlayer)
            clientPlayersEggBreakDelays = new LinkedList<EggBreakDelay>();
        currentPlayerEggBreakDelays = new HashMap<Integer, Long>();
        openEggStartTimesMap = new HashMap<Integer, Long>();
        createPlayersScoresListView();
    }

    private void createPlayersScoresListView() {
        playersListView = new ListView(playGameActivity);
        int dpWidth = (int) (AfekaEggsApplication.getScreenWidth() / 3.5);
        int dpHeight = (int) (AfekaEggsApplication.getScreenHeight() / 3.5);
        final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(dpWidth, dpHeight);
        playGameLayout.post(new Runnable() {
            @Override
            public void run() {
                playGameLayout.addView(playersListView, params);
            }
        });
        playersInGameList = playGameActivity.getPlayersList();
        playersScoresAdapter = new PlayersScoresAdapter(playGameActivity, playersInGameList);
        playersListView.setAdapter(playersScoresAdapter);
    }

    // this method is for both server & client devices
    public void makeThingsBeforeLeavingThisLevel() {
        int nextLevel = -1;
        if (this instanceof Level1) {
            nextLevel = 2;
        }
        else if (this instanceof Level2) {
            nextLevel = 3;
        }
        if (isServerPlayer) {
            playGameActivity.sendMessageToClients(new GoToNextLevelMsg());
        }
        playGameActivity.setPlayersInGameList(playersInGameList);
        currentPlayerLeader = determineCurrentPlayerLeader();
        releaseResources();
        if (nextLevel != -1) {
            final int finalNextLevel = nextLevel;
            playGameLayout.post(new Runnable() {
                @Override
                public void run() {
                    playGameLayout.removeView(playersListView);
                    playGameActivity.goToLevelNumber(finalNextLevel);
                }
            });
        }
        else {  // end of game
            Properties.GAME_STATUS = Properties.GAME_OVER_STATUS;
            announceWinner();
            addBestScoreToCloudIfNeeded();
        }
    }

    private void backToMainMenuAfterXMSec(final int x) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(x);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                playGameActivity.backToMainMenu();
            }
        }).start();
    }

    private String determineCurrentPlayerLeader() {
        int biggestAmountOfScoreUntilNow = 0;
        String currentPlayerLeader = "";
        for (Player player : playersInGameList) {
            if (player.getScore() > biggestAmountOfScoreUntilNow) {
                biggestAmountOfScoreUntilNow = player.getScore();
                currentPlayerLeader = player.getName();
            }
        }
        return currentPlayerLeader;
    }

    public void releaseResources() {
        if (backgroundMelodyMediaPlayer != null) {
            backgroundMelodyMediaPlayer.stop();
            backgroundMelodyMediaPlayer.release();
            backgroundMelodyMediaPlayer = null;
        }
    }

    //************************************ Game Sounds ******************************************\\
    protected void playBackgroundMelody(int soundSourceID) {
        backgroundMelodyMediaPlayer = MediaPlayer.create(playGameActivity, soundSourceID);
        backgroundMelodyMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer player) {
                player.start();
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                backgroundMelodyMediaPlayer.start();
            }
        }).start();
    }

    public void playSound(int resourceSrc) {
        mediaPlayer = MediaPlayer.create(playGameActivity, resourceSrc);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer player) {
                player.release();
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                mediaPlayer.start();
            }
        }).start();
    }

    //******************************** Game BL and GUI Manipulation *****************************\\
    public abstract void setEggsOnField();

    protected void setListenersOnEggs() {
        Collection<Egg> collection = eggsMap.values();
        Iterator<Egg> it = collection.iterator();
        while (it.hasNext()) {
            final Egg egg = it.next();
            egg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (egg.isOpen()) {
                        egg.setOpen(false);
                        long openEggEndTimeInMillis = android.os.SystemClock.uptimeMillis();
                        long currentPlayerEggBreakDelay = openEggEndTimeInMillis - openEggStartTimesMap.get(egg.getEggNumber());
                        currentPlayerEggBreakDelays.put(egg.getEggNumber(), currentPlayerEggBreakDelay);
                        if (isServerPlayer) {  // server player
                            playGameActivity.sendMessageToClients(new ClickedOnEggMsg("", -1, egg.getEggNumber()));
                        }
                        else {  // client player
                            playGameActivity.sendClickedOnEggMsgToGameServer(currentPlayerEggBreakDelay, egg.getEggNumber());
                        }
                        egg.setBroken(true);
                        makeExplosionEffectOnEgg(egg);
                    }
                }
            });
        }
    }

    private void makeExplosionEffectOnEgg(Egg egg) {
        if (egg != null) {
            egg.setOpen(false);
            if (isServerPlayer) {
                String eggBreaker = getPlayerWithBestResultForBrokenEggNumber(egg.getEggNumber());
                playGameActivity.sendMessageToClients(new FastestEggBreakerMsg(eggBreaker, egg.getEggSize()));
                updateScoresList(eggBreaker, egg.getEggSize());
                removeAllEggBreakDelaysForEggNumber(egg.getEggNumber());
            }

            egg.setExplosionAnimationOnEgg(AfekaEggsApplication.BOOM_SRC);
            removeEggFromField(egg);
            playSound(Properties.EGG_BROKEN_SOUND_SRC);
        }
    }

    protected void announceWinner() {
        playGameLayout.post(new Runnable() {
            @Override
            public void run() {
                announceWinnerNameOnCenterOfScreen();
                playAnimationOnCenterOfScreen(AfekaEggsApplication.FIREWORK_SRC);
            }
        });
    }

    private void announceWinnerNameOnCenterOfScreen() {
        TextView winnerTxtView = (TextView)playGameActivity.findViewById(R.id.winnerTxtView);
        winnerTxtView.setText(currentPlayerLeader + " Won!");
    }

    public void playAnimationOnCenterOfScreen(int gifResourceID) {
        InputStream gifInputStream = playGameActivity.getRawResource(gifResourceID);
        Movie gifMovie = Movie.decodeStream(gifInputStream);
        int movieWidth = gifMovie.width();
        int movieHeight = gifMovie.height();
        int dur = gifMovie.duration();

        int screenWidth = (int) (AfekaEggsApplication.getScreenWidth());
        int screenHeight = (int) (AfekaEggsApplication.getScreenHeight());

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(movieWidth, movieHeight);
        params.leftMargin = screenWidth/2 - movieWidth/2;
        params.topMargin = screenHeight/2 - movieHeight/2;
        GifView anim = new GifView(playGameActivity, gifResourceID, playGameLayout, true, true, false);
        anim.setBackgroundColor(Color.TRANSPARENT);
        playGameLayout.addView(anim, params);
    }

    public void updateScoresList(String playerName, String eggSize) {

        for (Player player : playersInGameList) {
            if (player.getName().equals(playerName)) {
                if (eggSize.equals(Properties.SMALL_EGG_SIZE)) {
                    player.increaseScoreBy(Properties.SMALL_EGG_SIZE_POINTS);
                }
                else if (eggSize.equals(Properties.MEDIUM_EGG_SIZE)) {
                    player.increaseScoreBy(Properties.MEDIUM_EGG_SIZE_POINTS);
                }
                else if (eggSize.equals(Properties.BIG_EGG_SIZE)) {
                    player.increaseScoreBy(Properties.BIG_EGG_SIZE_POINTS);
                }
            }
        }
        updateListView();
    }

    public void updateListView() {
        playGameLayout.post(new Runnable() {
            @Override
            public void run() {
                playersScoresAdapter.notifyDataSetChanged();
            }
        });
    }

    //************************* Client & Server Components Manipulation *************************\\
    // this method is called only if the current device is game server
    public void startGame() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                eggRandomizer.start();
            }
        }).start();
    }

    public void openRandomEgg() {
        if (eggsMap.size() > 0) {
            int[] closedEggNumbers = getAllClosedEggsNumbers();
            int randomizedEggNumber = random.nextInt(eggsMap.size());
            playGameActivity.sendMessageToClients(new OpenEggMsg(closedEggNumbers[randomizedEggNumber]));
            try {
                // this sleep is for letting the OpenEggMsg reach clients on time
                // so that the client & server can see the egg opened approximately at the same time.
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            openEggAtIndex(closedEggNumbers[randomizedEggNumber]);
        }
        else {
            eggRandomizer.stopRandomizingEggs();
            makeThingsBeforeLeavingThisLevel();
        }
    }

    public int[] getAllClosedEggsNumbers() {
        int[] closedEggNumbers = new int[eggsMap.size()];
        Set<Integer> keySet = eggsMap.keySet();
        Iterator<Integer> it = keySet.iterator();
        int i = 0;
        while (it.hasNext()) {
            closedEggNumbers[i++] = it.next();
        }
        return closedEggNumbers;
    }

    // this method use both client & server players
    public void openEggAtIndex(int eggNumber) {
        if (eggsMap.containsKey(eggNumber)) {
            long openEggStartTimeInMillis = android.os.SystemClock.uptimeMillis();
            openEggStartTimesMap.put(eggNumber, openEggStartTimeInMillis);
            eggsMap.get(eggNumber).openEgg();
        }
    }

    // this method logic will be executed only if another player broke the egg
    // before the player playing on this device did (according to eggNumber)
    public void breakEggAtIndex(final int eggNumber) {
        if (eggsMap.containsKey(eggNumber)) {
            if (!eggsMap.get(eggNumber).isBroken()) {
                playGameLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        makeExplosionEffectOnEgg(eggsMap.get(eggNumber));
                    }
                });
            }
        }
    }

    public void removeEggFromField(Egg egg) {
        if (eggsMap.containsValue(egg)) {
            eggsMap.remove(egg.getEggNumber());
            //playGameLayout.removeView(egg);
            openEggStartTimesMap.remove(egg.getEggNumber());
            if (currentPlayerEggBreakDelays.containsKey(egg.getEggNumber()))
                currentPlayerEggBreakDelays.remove(egg.getEggNumber());
        }
    }

    public void addClientEggBreakDelay(String playerName, long eggBreakDelay, int brokenEggNumber) {
        clientPlayersEggBreakDelays.add(new EggBreakDelay(playerName, eggBreakDelay, brokenEggNumber));
    }

    private void removeAllEggBreakDelaysForEggNumber(int eggNumber) {
        for (int i = 0; i < clientPlayersEggBreakDelays.size(); i++) {
            if (clientPlayersEggBreakDelays.get(i).getBrokenEggNumber() == eggNumber) {
                clientPlayersEggBreakDelays.remove(i);
                i--;
            }
        }
    }

    private String getPlayerWithBestResultForBrokenEggNumber(int brokenEggNumber) {
        long bestResultSoFar = Properties.MAX_BREAK_EGG_DELAY;
        String playerWithBestResultAmongClients = "";
        for (int i = 0; i < clientPlayersEggBreakDelays.size(); i++) {
            if (clientPlayersEggBreakDelays.get(i).getBrokenEggNumber() == brokenEggNumber) {
                if (clientPlayersEggBreakDelays.get(i).getEggBreakDelay() < bestResultSoFar) {
                    bestResultSoFar = clientPlayersEggBreakDelays.get(i).getEggBreakDelay();
                    playerWithBestResultAmongClients = clientPlayersEggBreakDelays.get(i).getPlayerName();
                }
            }
        }
        if (currentPlayerEggBreakDelays.containsKey(brokenEggNumber)) {
            if (bestResultSoFar > currentPlayerEggBreakDelays.get(brokenEggNumber)) {
                return currentPlayerName;
            }
        }
        return playerWithBestResultAmongClients;
    }

    protected void setAnimationOnEggs(final Egg egg, final int translationX, final int moveDuration) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int moveToX = translationX;
                while (egg != null) {
                    ObjectAnimator animX = ObjectAnimator.ofFloat(egg, "translationX", moveToX);
                    final AnimatorSet animSet = new AnimatorSet();
                    animSet.setDuration(moveDuration);
                    animSet.setInterpolator(new LinearInterpolator());
                    animSet.playTogether(animX);
                    playGameLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            animSet.start();
                        }
                    });
                    try {
                        Thread.sleep(moveDuration);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    moveToX *= -1;
                }
            }
        }).start();
    }

    protected void setObstacleAnimation(int animationSrc, final int translationX, final int moveDurarion) {
        // create the moving obstacle and add it to the layout
        int screenWidth = AfekaEggsApplication.getScreenWidth() / 8;
        int screenHeight = AfekaEggsApplication.getScreenHeight();
        InputStream gifInputStream = playGameActivity.getRawResource(animationSrc);
        Movie gifMovie = Movie.decodeStream(gifInputStream);
        int movieWidth = gifMovie.width();
        int movieHeight = gifMovie.height();

        final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(movieWidth, movieHeight);
        params.leftMargin = (int) (screenWidth * 4) - screenWidth/5;
        if (animationSrc == AfekaEggsApplication.PIRATESHIP_SRC) {
            params.topMargin = (int) ((3 * screenHeight / 4) - movieHeight / 4 - movieHeight * 1.2);
        }
        else if (animationSrc == AfekaEggsApplication.BIRD_SRC) {
            params.topMargin = (int) (screenHeight/2 + screenHeight/10 - movieHeight * 1.25);
        }
        else {
            params.topMargin = (int) (screenHeight - movieHeight / 4 - movieHeight * 1.25);
        }

        final GifView chickAnim = new GifView(playGameActivity, animationSrc, playGameLayout, false, true, true);
        chickAnim.setBackgroundColor(Color.TRANSPARENT);
        playGameLayout.post(new Runnable() {
            @Override
            public void run() {
                playGameLayout.addView(chickAnim, params);
            }
        });

        // set animation on the moving obstacle
        new Thread(new Runnable() {
            @Override
            public void run() {
                int moveToX = translationX;
                while (!playGameActivity.isFinishing()) {
                    ObjectAnimator animX = ObjectAnimator.ofFloat(chickAnim, "translationX", moveToX);
                    final AnimatorSet animSet = new AnimatorSet();
                    animSet.setDuration(moveDurarion);
                    animSet.setInterpolator(new LinearInterpolator());
                    animSet.playTogether(animX);
                    playGameLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            animSet.start();
                        }
                    });
                    try {
                        Thread.sleep(moveDurarion);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    moveToX *= -1;
                }
            }
        }).start();
    }

    protected void addEggToField(int locationRelativeLeft, int locationRelativeTop, int movieWidth, int movieHeight, int gifResource) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(movieWidth, movieHeight);
        params.leftMargin = locationRelativeLeft;
        params.topMargin = (int) (locationRelativeTop - movieHeight*1.25);

        Egg egg = new Egg(playGameActivity, gifResource, playGameLayout, this, currentEggNumber++);
        egg.setBackgroundColor(Color.TRANSPARENT);
        playGameLayout.addView(egg, params);

        if (gifResource == AfekaEggsApplication.SMALL_EGG_SRC) {
            egg.setEggSize(Properties.SMALL_EGG_SIZE);
        }
        else if (gifResource == AfekaEggsApplication.MEDIUM_EGG_SRC) {
            egg.setEggSize(Properties.MEDIUM_EGG_SIZE);
        }
        else if (gifResource == AfekaEggsApplication.BIG_EGG_SRC) {
            egg.setEggSize(Properties.BIG_EGG_SIZE);
        }
        eggsMap.put(egg.getEggNumber(), egg);
    }

    public PlayGameActivity getPlayGameActivity() {return playGameActivity; }

    //***************************** Top Players and Best Scores section *************************\\
    // this method determinates whether there's a need to add a new best score to the ranks table in the cloud
    // if yes -> adding a new best score to the cloud.
    private void addBestScoreToCloudIfNeeded() {
        final ReentrantLock lock = new ReentrantLock();
        final CountDownLatch latch = new CountDownLatch(1);
        parse = new ParseCloudCommunication(playGameActivity, playersInGameList.size());
        parse.downloadTopPlayersListFromParse(lock, latch);
        createGoogleAPIClient();
        mGoogleApiClient.connect();  // connecting the GoogleAPIClient to Google Server in order to track current location of device
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                int currentPlayerScore = getCurrentPlayerScore();
                if (TopPlayersListServices.isNecessaryToAddTopPlayer(topPlayersList, currentPlayerScore)) {
                    addBestScoreToCloud(currentPlayerScore);
                }
                backToMainMenuAfterXMSec(1500);
            }
        }).start();
    }

    private void addBestScoreToCloud(int currentPlayerScore) {
        Location currentLocation = getCurrentLocation();
        if (currentLocation != null) { // current location is received successfully
            double latitude = currentLocation.getLatitude();
            double longitude = currentLocation.getLongitude();
            parse.saveTopPlayerToParse(currentPlayerName, currentPlayerScore, latitude + "", longitude + "", topPlayersList);
            mLastLocation = null;
            locationTrackingCompletedSuccessfully = false;
        } else { // current location is received unsuccessfully (connection problems)
            parse.saveTopPlayerToParse(currentPlayerName, currentPlayerScore, Properties.SPECIAL_SIGN_FOR_NON_EXISTING_COORDINATES + "", Properties.SPECIAL_SIGN_FOR_NON_EXISTING_COORDINATES + "", topPlayersList);
        }
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }

    private void createGoogleAPIClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(playGameActivity).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        geocoder = new Geocoder(playGameActivity, Locale.getDefault());
    }

    public void setTopPlayersList(List<TopPlayer> topPlayersList) {
        this.topPlayersList = topPlayersList;
    }

    private int getCurrentPlayerScore() {
        for (Player player : playersInGameList) {
            if (player.getName().equals(currentPlayerName)) {
                return player.getScore();
            }
        }
        return -1; // just to pass compilation. never will reach this line.
    }

    private Location getCurrentLocation() {
        if (locationTrackingCompletedSuccessfully)
            return mLastLocation;
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null)
            locationTrackingCompletedSuccessfully = true;
        else
            locationTrackingCompletedSuccessfully = false;
    }

    @Override
    public void onConnectionSuspended(int i) {
        locationTrackingCompletedSuccessfully = false;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        locationTrackingCompletedSuccessfully = false;
    }
}