package com.afeka.afekaeggs.BL;

import com.afeka.afekaeggs.Properties;

import java.util.Collections;
import java.util.List;

/**
 * Created by Roman on 12/20/2015.
 */
public class TopPlayersListServices {

    public static void setTopPlayersRanks(List<TopPlayer> topPlayersInDB) {
        // sort according to score (descending)
        Collections.sort(topPlayersInDB);
        for (int i = 0; i < topPlayersInDB.size(); i++) {
            topPlayersInDB.get(i).setRank(i + 1);
        }
    }

    public static boolean isNecessaryToAddTopPlayer(List<TopPlayer> topPlayersList, int newScore) {
        if (topPlayersList != null) {
            if (topPlayersList.size() < Properties.TOP_PLAYERS_AMOUNT)
                return true;
            setTopPlayersRanks(topPlayersList);
            if (topPlayersList.get(Properties.TOP_PLAYERS_AMOUNT - 1).getScore() < newScore)
                return true;
        }
        return false;
    }

    public static TopPlayer findPlayerWithWorstScore(List<TopPlayer> topPlayersList) {
        if (topPlayersList.size() > 0) {
            setTopPlayersRanks(topPlayersList);
            return topPlayersList.get(topPlayersList.size()-1);
        }
        return null;
    }
}
