package com.afeka.afekaeggs.BL;

/**
 * Created by Roman on 2/1/2016.
 */
public class TopPlayer implements Comparable<TopPlayer>{
    private String name;
    private int rank;
    private int score;
    private String locationLatitude;
    private String locationLongitude;

    public TopPlayer(String name, int score, String locationLatitude, String locationLongitude) {
        this.name = name;
        this.score = score;
        this.locationLatitude = locationLatitude;
        this.locationLongitude = locationLongitude;
    }

    public String getName() { return name;}
    public String getLocationLatitude() {return locationLatitude;}
    public String getLocationLongitude() {return locationLongitude;}

    public int getScore() { return score; }
    public void setScore(int score) { this.score = score; }

    public int getRank() { return rank; }
    public void setRank(int rank) { this.rank = rank;}

    @Override
    public int compareTo(TopPlayer anotherPlayer) {
        if (score > anotherPlayer.getScore())
            return -1;
        else if (score < anotherPlayer.getScore())
            return 1;
        return 0;
    }

    @Override
    public String toString() {
        String rankAndName;
        if (rank < 10)
            rankAndName = rank + " | " + name;
        else
            rankAndName = rank + "| " + name;
        return String.format("%-22s%s", rankAndName, score);
    }
}
