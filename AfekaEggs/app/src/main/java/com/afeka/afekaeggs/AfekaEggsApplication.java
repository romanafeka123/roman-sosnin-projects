package com.afeka.afekaeggs;

import android.app.Application;
import android.util.DisplayMetrics;

import com.parse.Parse;

/**
 * Created by Roman on 12/28/2015.
 */
public class AfekaEggsApplication extends Application {
    private String APPLICATION_ID = "mDZ2LWxbZPPjqmyAkxvIGlhlCuGhTGXdlwH0rQFt";
    private String CLIENT_KEY = "3zj4gCRZ6o6qaA4P89BNT52OKorE0P8U5Klv6onc";

    // Current device screen specs
    public static int screenHeight;
    public static int screenWidth;

    // Current device animations sizes, that will differentiate according to the device type (phone/tablet)
    public static int BIG_EGG_SRC, MEDIUM_EGG_SRC, SMALL_EGG_SRC;
    public static int CHICK_SRC, MONKEY_SRC, MOUSE_SRC, CRAZY_CHICKEN_SRC, PIRATESHIP_SRC, BIRD_SRC;
    public static int THREE_TWO_ONE_SRC, FIREWORK_SRC, BOOM_SRC;

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, APPLICATION_ID, CLIENT_KEY);
        initializeScreenWidthAndHeight();
        initializeAnimationsSizesAccordingToScreenSize();
    }

    private void initializeScreenWidthAndHeight() {
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        screenHeight = metrics.heightPixels;
        screenWidth = metrics.widthPixels;
    }

    private void initializeAnimationsSizesAccordingToScreenSize() {
        if (screenWidth < Properties.MIN_TABLET_SCREEN_WIDTH) {
            BIG_EGG_SRC = Properties.BIG_EGG_SRC_PHONE;
            MEDIUM_EGG_SRC = Properties.MEDIUM_EGG_SRC_PHONE;
            SMALL_EGG_SRC = Properties.SMALL_EGG_SRC_PHONE;
            BIRD_SRC = Properties.BIRD_SRC_PHONE;
            MOUSE_SRC = Properties.MOUSE_SRC_PHONE;
            PIRATESHIP_SRC = Properties.PIRATESHIP_SRC_PHONE;
            MONKEY_SRC = Properties.MONKEY_SRC_PHONE;
            CHICK_SRC = Properties.CHICK_SRC_PHONE;
            CRAZY_CHICKEN_SRC = Properties.CRAZY_CHICKEN_SRC_PHONE;
            THREE_TWO_ONE_SRC = Properties.THREE_TWO_ONE_SRC_PHONE;
            FIREWORK_SRC = Properties.FIREWORK_SRC_PHONE;
            BOOM_SRC = Properties.BOOM_SRC_PHONE;
        }
        else {
            BIG_EGG_SRC = Properties.BIG_EGG_SRC_TABLET;
            MEDIUM_EGG_SRC = Properties.MEDIUM_EGG_SRC_TABLET;
            SMALL_EGG_SRC = Properties.SMALL_EGG_SRC_TABLET;
            BIRD_SRC = Properties.BIRD_SRC_TABLET;
            MOUSE_SRC = Properties.MOUSE_SRC_TABLET;
            PIRATESHIP_SRC = Properties.PIRATESHIP_SRC_TABLET;
            MONKEY_SRC = Properties.MONKEY_SRC_TABLET;
            CHICK_SRC = Properties.CHICK_SRC_TABLET;
            CRAZY_CHICKEN_SRC = Properties.CRAZY_CHICKEN_SRC_TABLET;
            THREE_TWO_ONE_SRC = Properties.THREE_TWO_ONE_SRC_TABLET;
            FIREWORK_SRC = Properties.FIREWORK_SRC_TABLET;
            BOOM_SRC = Properties.BOOM_SRC_TABLET;
        }
    }

    public static int getScreenHeight() { return screenHeight; }
    public static int getScreenWidth() { return screenWidth; }
}
