package com.afeka.afekaeggs.Levels;

import android.graphics.Movie;
import com.afeka.afekaeggs.Activities.PlayGameActivity;
import com.afeka.afekaeggs.AfekaEggsApplication;
import com.afeka.afekaeggs.Properties;

import java.io.InputStream;

/**
 * Created by Roman on 12/27/2015.
 */
public class Level1 extends Level {

    public Level1(PlayGameActivity playGameActivity, boolean isServerPlayer) {
        super(playGameActivity, isServerPlayer);
        playGameActivity.setPlaygroundImage(Properties.PLAY_GAME_BACKGROUND_1);
        playBackgroundMelody(Properties.LEVEL_1_MELODY_SRC);
    }

    @Override
    public void setEggsOnField() {
        setBigEggs();
        setMediumEggs();
        setListenersOnEggs();
    }

    private void setBigEggs() {
        int screenWidth = AfekaEggsApplication.getScreenWidth() / 8;
        int screenHeight = AfekaEggsApplication.getScreenHeight();
        InputStream gifInputStream = playGameActivity.getRawResource(AfekaEggsApplication.BIG_EGG_SRC);
        Movie gifMovie = Movie.decodeStream(gifInputStream);
        int movieWidth = gifMovie.width();
        int movieHeight = gifMovie.height();

        addEggToField(screenWidth*0, screenHeight, movieWidth, movieHeight,  AfekaEggsApplication.BIG_EGG_SRC);
        addEggToField(screenWidth*1, screenHeight - movieHeight/4, movieWidth, movieHeight, AfekaEggsApplication.BIG_EGG_SRC);
        addEggToField(screenWidth*2, screenHeight, movieWidth, movieHeight, AfekaEggsApplication.BIG_EGG_SRC);
        addEggToField(screenWidth*3, screenHeight - movieHeight/4, movieWidth, movieHeight,  AfekaEggsApplication.BIG_EGG_SRC);
        addEggToField(screenWidth * 4, screenHeight, movieWidth, movieHeight, AfekaEggsApplication.BIG_EGG_SRC);
        addEggToField(screenWidth * 5, screenHeight - movieHeight / 4, movieWidth, movieHeight, AfekaEggsApplication.BIG_EGG_SRC);
        addEggToField(screenWidth * 6, screenHeight, movieWidth, movieHeight, AfekaEggsApplication.BIG_EGG_SRC);
        addEggToField(screenWidth * 7, screenHeight - movieHeight / 4, movieWidth, movieHeight, AfekaEggsApplication.BIG_EGG_SRC);
    }

    private void setMediumEggs() {
        int screenWidth = AfekaEggsApplication.getScreenWidth() / 8;
        int screenHeight = AfekaEggsApplication.getScreenHeight();
        InputStream gifInputStream = playGameActivity.getRawResource(AfekaEggsApplication.MEDIUM_EGG_SRC);
        Movie gifMovie = Movie.decodeStream(gifInputStream);
        int movieWidth = gifMovie.width();
        int movieHeight = gifMovie.height();

        addEggToField(screenWidth*0 + movieWidth/4, (int) (screenHeight - movieHeight*1.65), movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth*1 + movieWidth/4, (int) (screenHeight - movieHeight*1.9), movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth*2 + movieWidth/4, (int) (screenHeight - movieHeight*1.65), movieWidth, movieHeight,  AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth*3 + movieWidth/4, (int) (screenHeight - movieHeight*1.9), movieWidth, movieHeight,  AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth*4 + movieWidth/4, (int) (screenHeight - movieHeight*1.65), movieWidth, movieHeight,  AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth * 5 + movieWidth / 4, (int) (screenHeight - movieHeight * 1.9), movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth * 6 + movieWidth / 4, (int) (screenHeight - movieHeight * 1.65), movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
        addEggToField(screenWidth * 7 + movieWidth / 4, (int) (screenHeight - movieHeight * 1.9), movieWidth, movieHeight, AfekaEggsApplication.MEDIUM_EGG_SRC);
    }
}