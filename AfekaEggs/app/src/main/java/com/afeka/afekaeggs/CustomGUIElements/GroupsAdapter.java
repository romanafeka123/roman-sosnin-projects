package com.afeka.afekaeggs.CustomGUIElements;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afeka.afekaeggs.Activities.JoinGroupActivity;
import com.afeka.afekaeggs.BL.Group;
import com.afeka.afekaeggs.Properties;
import com.afeka.afekaeggs.R;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Roman on 12/25/2015.
 */
public class GroupsAdapter extends BaseAdapter {
    private JoinGroupActivity joinGroupActivity;
    private List<Group> groupsList;
    private List<View> groupsListViewChildren;

    public GroupsAdapter(JoinGroupActivity joinGroupActivity, List<Group> groupsList) {
        this.joinGroupActivity = joinGroupActivity;
        this.groupsList = groupsList;
        groupsListViewChildren = new LinkedList<View>();
    }

    @Override
    public int getCount() {
        return groupsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) joinGroupActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.groups_listview_layout, null);
        }

        Group group = groupsList.get(position);
        ImageView img = (ImageView) v.findViewById(R.id.groups_list_img);
        if (group.getNumOfPlayersInGroup() == Properties.MIN_PLAYERS_AMOUNT) {
            img.setImageResource(R.drawable.two_players_img);
        }
        else if (group.getNumOfPlayersInGroup() == 3) {
            img.setImageResource(R.drawable.three_players_img);
        }
        else if (group.getNumOfPlayersInGroup() == Properties.MAX_PLAYERS_AMOUNT) {
            img.setImageResource(R.drawable.four_players_img);
        }

        TextView groupNameTxtView = (TextView) v.findViewById(R.id.groups_list_group_name);
        String groupNameFormattedString = getFormattedGroupNameString(group.getGroupName());
        groupNameTxtView.setText(groupNameFormattedString);

        v.setAlpha(0.5f);
        if (!groupsListViewChildren.contains(v)) {
            groupsListViewChildren.add(v);
        }
        return v;
    }

    private String getFormattedGroupNameString(String groupNameStr) {
        StringBuffer formattedString = new StringBuffer();
        for (int i = 0; i < 8; i++) {
            formattedString.append(' ');
        }
        formattedString.append(groupNameStr);
        return formattedString.toString();
    }

    public List<View> getGroupsListViewChildren() {
        return groupsListViewChildren;
    }
}
