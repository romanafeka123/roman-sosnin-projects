package com.afeka.afekaeggs.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afeka.afekaeggs.CustomGUIElements.AlertDialogHelper;
import com.afeka.afekaeggs.GameCommunicationUtils.ClientConnection;
import com.afeka.afekaeggs.BL.Group;
import com.afeka.afekaeggs.CustomGUIElements.GroupsAdapter;
import com.afeka.afekaeggs.GameCommunicationUtils.NetworkConnectionUtils;
import com.afeka.afekaeggs.CustomGUIElements.ProgressBarHelper;
import com.afeka.afekaeggs.GameCommunicationUtils.ParseCloudCommunication;
import com.afeka.afekaeggs.Properties;
import com.afeka.afekaeggs.R;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Roman on 12/20/2015.
 */
public class JoinGroupActivity extends Activity implements AdapterView.OnItemClickListener{
    // UI elements
    private RelativeLayout joinGroupLayout;
    private ListView groupsListView;
    private GroupsAdapter groupsAdapter;
    private ImageButton joinGroupBtn, refreshBtn1, refreshBtn2;
    private EditText enteredNameEditTxt;
    private List<Group> openGroups;
    private ProgressBarHelper progressBarHelper;
    private AlertDialogHelper alertDialogHelper;

    // Communication
    private ParseCloudCommunication groupCreateCloudComminucation;
    private ClientConnection clientConnection;
    private JoinGroupActivity thisActivity = this;
    private NetworkConnectionUtils networkConnectionUtils;

    // Game BL elements
    private String playerName;
    private int selectedGroupIndex;
    private String selectedGroupName;
    private String groupInitializerName;
    private int selectedAmountOfPlayersInGroup;

    // For synchronizing purposes - to avoid Racing Condition
    private ReentrantLock lock;
    private CountDownLatch latch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_group_layout);
        initializeFields();
        updateOpenGroupsList();
        setListeners();
        showInputNameDialog();
    }

    // finish this activity if left the game while playing
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK){
                finish();
            }
        }
    }

    private void initializeFields() {
        playerName = Properties.DEFAULT_PLAYER_NAME;
        Properties.GAME_STATUS = Properties.GAME_IS_STARTING_STATUS;
        joinGroupLayout = (RelativeLayout) findViewById(R.id.joinGroupLayout);
        joinGroupBtn = (ImageButton) findViewById(R.id.join_group_btn_join_screen);
        disableJoinBtn();
        progressBarHelper = new ProgressBarHelper(joinGroupLayout, joinGroupBtn.getContext());
        alertDialogHelper = new AlertDialogHelper(this);

        refreshBtn1 = (ImageButton) findViewById(R.id.refresh_btn);
        refreshBtn2 = (ImageButton) findViewById(R.id.refresh_btn2);
        groupsListView = (ListView) findViewById(R.id.groups_ListView);

        openGroups = new LinkedList<Group>();
        groupsAdapter = new GroupsAdapter(this, openGroups);
        groupsListView.setAdapter(groupsAdapter);
        groupsListView.setOnItemClickListener(this);

        groupCreateCloudComminucation = new ParseCloudCommunication(this, -1);
        networkConnectionUtils = new NetworkConnectionUtils(this);
    }

    private void setListeners() {
        joinGroupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (networkConnectionUtils.isInternetConnectionUP()) {
                    boolean connectedToServer = connectToGameServer();
                    if (connectedToServer) {
                        Intent intent = new Intent(getApplicationContext(), AwaitingActivity.class);
                        intent.putExtra(Properties.GROUP_NAME, selectedGroupName);
                        intent.putExtra(Properties.PLAYER_NAME, enteredNameEditTxt.getText().toString());
                        intent.putExtra(Properties.GROUP_INITIALIZER_NAME, groupInitializerName);
                        intent.putExtra(Properties.NUMBER_OF_PLAYERS_IN_GAME, selectedAmountOfPlayersInGroup);
                        intent.putExtra(Properties.IS_GROUP_INITIALIZER, false);
                        startActivityForResult(intent, 1);
                    } else {
                        alertDialogHelper.showAlert(Properties.FAILED_TO_CONNECT_TO_GAME_SERVER_MSG);
                    }
                } else {
                    alertDialogHelper.showAlert(Properties.INTERNET_CONNECTION_DOWN_MSG);
                }
            }
        });
        refreshBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (networkConnectionUtils.isInternetConnectionUP()) {
                    updateOpenGroupsList();
                    disableJoinBtn();
                } else {
                    alertDialogHelper.showAlert(Properties.INTERNET_CONNECTION_DOWN_MSG);
                }
            }
        });
        refreshBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (networkConnectionUtils.isInternetConnectionUP()) {
                    updateOpenGroupsList();
                    disableJoinBtn();
                } else {
                    alertDialogHelper.showAlert(Properties.INTERNET_CONNECTION_DOWN_MSG);
                }
            }
        });
    }

    private void showInputNameDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.input_name_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);

        enteredNameEditTxt = (EditText) promptView.findViewById(R.id.entered_name_edit_txt);
        enteredNameEditTxt.setHint(Properties.ENTER_NAME_HINT);

        // setup a dialog window
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (enteredNameEditTxt.getText().length() < Properties.MIN_PLAYER_NAME_LEN || enteredNameEditTxt.getText().length() > Properties.MAX_PLAYER_NAME_LEN)
                    showInputNameDialog();
                else
                    playerName = enteredNameEditTxt.getText().toString();
            }
        });
        alertDialogBuilder.show();
    }

    public void setOpenGroups(List<Group> openGroups) {
        this.openGroups.clear();
        for (int i = 0; i < openGroups.size(); i++) {
            this.openGroups.add(openGroups.get(i));
        }
    }

    private void updateOpenGroupsList() {
        progressBarHelper.showProgressbarOnWait(Properties.OBSERVING_OPEN_GROUPS_MSG_ON_WAIT);
        lock = new ReentrantLock();
        latch = new CountDownLatch(1);
        groupCreateCloudComminucation.getGroupsListFromCloud(lock, latch);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                progressBarHelper.stopShowingProgressbarOnWait();
                joinGroupLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        groupsAdapter.notifyDataSetChanged();
                    }
                });
                if (openGroups.isEmpty()) {
                    joinGroupLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            alertDialogHelper.showAlert(Properties.NO_OPEN_GROUPS_MSG);
                        }
                    });
                }
            }
        }).start();
    }

    private boolean connectToGameServer() {
        Group group = openGroups.get(selectedGroupIndex);
        ReentrantLock connectionLock = new ReentrantLock();
        CountDownLatch connectionLatch = new CountDownLatch(1);
        clientConnection = ClientConnection.getInstance(true);
        clientConnection.setJoinGroupActivity(thisActivity);
        clientConnection.setServerIP(group.getInitializerIP());
        clientConnection.setPlayerName(playerName);
        clientConnection.setConnectionLock(connectionLock);
        clientConnection.setConnectionLatch(connectionLatch);
        new Thread(new Runnable() {
            @Override
            public void run() {
                clientConnection.connect();
            }
        }).start();
        try {
            connectionLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return clientConnection.isConnectedToServer();
    }

    private void disableJoinBtn() {
        joinGroupBtn.setEnabled(false);
        joinGroupBtn.setAlpha(0.5f);
    }

    public void enableJoinBtn() {
        joinGroupBtn.setEnabled(true);
        joinGroupBtn.setAlpha(1.0f);
    }


    @Override
    public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
        view.setAlpha(1.0f);
        List<View> groupsListViewChildren = groupsAdapter.getGroupsListViewChildren();
        for (int i = 0; i < groupsListViewChildren.size(); i++) {
            View v = groupsListViewChildren.get(i);
            if (v != view)
                v.setAlpha(0.5f);
        }
        selectedGroupIndex = position;
        selectedGroupName = openGroups.get(position).getGroupName();
        selectedAmountOfPlayersInGroup = openGroups.get(position).getNumOfPlayersInGroup();
        groupInitializerName = openGroups.get(position).getInitializerName();
        enableJoinBtn();
    }
}
