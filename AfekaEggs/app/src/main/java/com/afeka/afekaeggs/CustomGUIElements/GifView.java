package com.afeka.afekaeggs.CustomGUIElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.io.InputStream;

/**
 * Created by Roman on 12/4/2015.
 */
public class GifView extends ImageButton {
    protected ViewGroup containingElement;
    protected int gifResourceID;
    protected GifView thisView = this;
    protected Context context;

    protected InputStream gifInputStream;
    protected Movie gifMovie;
    protected int movieWidth, movieHeight;
    protected long movieDuration, movieRunDuration;
    protected long lastTick, nowTick;
    protected boolean repeat = false;
    protected boolean running = false;

    protected boolean removeViewAfterPlayed;

    public GifView(Context context) {
        super(context);
        init(context);
    }

    public GifView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GifView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public GifView(Context context, int gifResourceID, ViewGroup containingElement, boolean removeViewAfterPlayed, boolean playNow, boolean repeat) {
        super(context);
        this.context = context;
        this.gifResourceID = gifResourceID;
        this.containingElement = containingElement;
        this.removeViewAfterPlayed = removeViewAfterPlayed;
        running = playNow;
        this.repeat = repeat;
        init(context);
    }

    private void init(Context context) {
        setFocusable(true);
        gifInputStream = context.getResources().openRawResource(gifResourceID);
        gifMovie = Movie.decodeStream(gifInputStream);
        movieWidth = gifMovie.width();
        movieHeight = gifMovie.height();
        movieDuration = gifMovie.duration();
        if (removeViewAfterPlayed)
            stopAnimationtAfterDurationTime();
    }

    public void setRepeat(boolean r) {repeat = r;}
    public void setRunning(boolean r) {running = r;}
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {setMeasuredDimension(movieWidth, movieHeight);}
    public int getMovieWidth() {return movieWidth;}
    public int getMovieHeight() {return movieHeight;}
    public long getMovieDuration() {return movieDuration;}

    @Override
    protected void onDraw(Canvas canvas) {
        if(gifMovie == null){
            return;
        }
        nowTick = android.os.SystemClock.uptimeMillis();
        if (lastTick == 0) {
            movieRunDuration = 0;
        }
        else {
            if(running){
                movieRunDuration += nowTick-lastTick;
                if (movieRunDuration > movieDuration) {
                    if (repeat) {
                        movieRunDuration = 0;
                    }
                    else {
                        movieRunDuration = movieDuration;
                    }
                }
            }
        }
        gifMovie.setTime((int) movieRunDuration);
        gifMovie.draw(canvas, 0, 0);
        lastTick = nowTick;
        invalidate();
    }

    protected void stopAnimationtAfterDurationTime() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(movieDuration);
                    containingElement.post(new Runnable() {
                        public void run() {
                            containingElement.removeView(thisView);
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void setExplosionAnimationOnEgg(int animationSrc) {
        running = true;
        repeat = true;
        removeViewAfterPlayed = true;
        this.gifResourceID = animationSrc;
        init(context);
    }

}
