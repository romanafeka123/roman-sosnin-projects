package com.afeka.afekaeggs.GameCommunicationUtils;

import com.afeka.afekaeggs.Activities.AwaitingActivity;
import com.afeka.afekaeggs.Activities.JoinGroupActivity;
import com.afeka.afekaeggs.Activities.PlayGameActivity;
import com.afeka.afekaeggs.GameMessages.ClickedOnEggMsg;
import com.afeka.afekaeggs.GameMessages.CloseConnectionRequest;
import com.afeka.afekaeggs.GameMessages.FastestEggBreakerMsg;
import com.afeka.afekaeggs.GameMessages.GoToNextLevelMsg;
import com.afeka.afekaeggs.GameMessages.JoinGameRequest;
import com.afeka.afekaeggs.GameMessages.OpenEggMsg;
import com.afeka.afekaeggs.GameMessages.PlayerLeftGameMsg;
import com.afeka.afekaeggs.GameMessages.StartGameMsg;
import com.afeka.afekaeggs.Properties;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Roman on 1/4/2016.
 */
// This class is relevant only if the currect device is the game client
// I made this class to be Singleton, in order to access the same reference of this class in different activities.
// i.e in different phases of the game.
public class ClientConnection {
    private static ClientConnection clientConnectionSingleton = null;

    // Communication components
    private Socket socket;
    private String serverIP;
    private boolean connectedToServer;
    private ObjectInputStream serverInStream;
    private ObjectOutputStream serverOutStream;

    // Activities
    private JoinGroupActivity joinGroupActivity;
    private AwaitingActivity awaitingActivity;
    private PlayGameActivity playGameActivity;
    private String playerName;

    // For synchronizing purposes
    private ReentrantLock connectionLock;
    private CountDownLatch connectionLatch;
    private ReentrantLock awaitingActivityNotReadyLock;
    private CountDownLatch awaitingActivityNotReadyconnectionLatch;

    private ClientConnection() {
        awaitingActivityNotReadyLock = new ReentrantLock();
        awaitingActivityNotReadyconnectionLatch = new CountDownLatch(1);
    }

    public static ClientConnection getInstance(boolean newInstance) {
        if (clientConnectionSingleton == null || newInstance) {
            clientConnectionSingleton = new ClientConnection();
        }
        return clientConnectionSingleton;
    }

    public void connect() {
        try {
            InetAddress serverAddr = InetAddress.getByName(serverIP);
            socket = new Socket(serverAddr, Properties.GAME_SERVER_PORT);
            if (!socket.isConnected()) {
                connectedToServer = false;
            }
            serverOutStream = new ObjectOutputStream(socket.getOutputStream());
            serverInStream = new ObjectInputStream(socket.getInputStream());
            connectedToServer = true;
            listenToMessagesFromServer();
        } catch (IOException e) {
            connectedToServer = false;
            e.printStackTrace();
        }
        connectionLock.lock();
        connectionLatch.countDown();
        connectionLock.unlock();
    }

    public boolean disconnect() {
        try {
            if (connectedToServer) {
                serverOutStream.writeObject(new CloseConnectionRequest(playerName));
                connectedToServer = false;
                if (!socket.isClosed()) {
                    socket.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connectedToServer;
    }

    public void listenToMessagesFromServer() {
        if (!connectedToServer) {
            return;
        }
        // this thread receives all the messages from server
        new Thread(new Runnable() {
            public void run() {
                try {
                    sendJoinGameRequestToGameServer();
                    do {
                        Object obj = serverInStream.readObject();
                        if (obj instanceof CloseConnectionRequest) {
                            connectedToServer = false;
                        }
                        else if (obj instanceof JoinGameRequest) {
                            JoinGameRequest req = (JoinGameRequest)obj;
                            if (awaitingActivity == null) {
                                try {
                                    awaitingActivityNotReadyconnectionLatch.await();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            awaitingActivity.setPlayersToJoinOneLess();
                            awaitingActivity.addJoinedPlayerToListView(req.getPlayerName());
                        }
                        else if (obj instanceof PlayerLeftGameMsg) {
                            PlayerLeftGameMsg req = (PlayerLeftGameMsg)obj;
                            if (playGameActivity != null) {  // game in process state
                                playGameActivity.removeJoinedPlayerFromListView(req.getPlayerName());
                            }
                            else {
                                if (awaitingActivity == null) {
                                    try {
                                        awaitingActivityNotReadyconnectionLatch.await();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                awaitingActivity.setPlayersToJoinOneMore();
                                awaitingActivity.removeJoinedPlayerFromListView(req.getPlayerName());
                            }
                        }
                        else if (obj instanceof StartGameMsg) {
                            awaitingActivity.startGame();
                        }
                        else if (obj instanceof OpenEggMsg) {
                            OpenEggMsg openEggMsg = (OpenEggMsg)obj;
                            playGameActivity.getLevel().openEggAtIndex(openEggMsg.getEggNumberToOpen());
                        }
                        else if (obj instanceof ClickedOnEggMsg) {
                            ClickedOnEggMsg clickedOnEggMsg = (ClickedOnEggMsg) obj;
                            playGameActivity.getLevel().breakEggAtIndex(clickedOnEggMsg.getBrokenEggNumber());
                        }
                        else if (obj instanceof FastestEggBreakerMsg) {
                            FastestEggBreakerMsg fastestEggBreakerMsg = (FastestEggBreakerMsg) obj;
                            playGameActivity.getLevel().updateScoresList(fastestEggBreakerMsg.getPlayerName(), fastestEggBreakerMsg.getEggSize());
                        }
                        else if (obj instanceof GoToNextLevelMsg) {
                            playGameActivity.getLevel().makeThingsBeforeLeavingThisLevel();
                        }
                    } while (connectedToServer);
                } catch (EOFException e) {
                    connectedToServer = false;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        serverInStream.close();
                        if (playGameActivity != null) {
                            playGameActivity.finish();
                        }
                        if (!Properties.GAME_STATUS.equals(Properties.GAME_OVER_STATUS)) {
                            awaitingActivity.showAlertAfterServerDown(Properties.SERVER_IS_DOWN_MSG);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void sendJoinGameRequestToGameServer() {
        try {
            serverOutStream.writeObject(new JoinGameRequest(playerName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendClickedOnEggMsgToGameServer(String playerName, long playerEggBreakDelay, int brokenEggNumber) {
        try {
            serverOutStream.writeObject(new ClickedOnEggMsg(playerName, playerEggBreakDelay, brokenEggNumber));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void fireAwaitingActivityReadyEvent() {
        awaitingActivityNotReadyLock.lock();
        awaitingActivityNotReadyconnectionLatch.countDown();
        awaitingActivityNotReadyLock.unlock();
    }

    public boolean isConnectedToServer() {
        return connectedToServer;
    }

    public void setJoinGroupActivity(JoinGroupActivity joinGroupActivity) {this.joinGroupActivity = joinGroupActivity;}
    public void setAwaitingActivity(AwaitingActivity awaitingActivity) {this.awaitingActivity = awaitingActivity;}
    public void setPlayGameActivity(PlayGameActivity playGameActivity) {this.playGameActivity = playGameActivity;}

    public void setServerIP(String serverIP) {this.serverIP = serverIP;}
    public void setPlayerName(String playerName) {this.playerName = playerName;}
    public void setConnectionLock(ReentrantLock connectionLock) {this.connectionLock = connectionLock;}
    public void setConnectionLatch(CountDownLatch connectionLatch) {this.connectionLatch = connectionLatch;}
}
