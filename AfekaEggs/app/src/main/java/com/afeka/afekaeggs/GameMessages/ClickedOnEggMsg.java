package com.afeka.afekaeggs.GameMessages;

import java.io.Serializable;

/**
 * Created by Roman on 1/6/2016.
 */
public class ClickedOnEggMsg implements Serializable {
    private static final long serialVersionUID	= 1L;

    private String playerName;
    private long eggBreakDelay;
    private int brokenEggNumber;

    // eggBreakDelay = -1 in case when server sends this msg to client (to explode the egg)
    // otherwise(client sends this msg to server), eggBreakDelay contains the delay, so that server calculates who was faster.
    public ClickedOnEggMsg(String playerName, long eggBreakDelay, int brokenEggNumber) {
        this.playerName = playerName;
        this.eggBreakDelay = eggBreakDelay;
        this.brokenEggNumber = brokenEggNumber;
    }

    public String getPlayerName() {return playerName; }
    public long getEggBreakDelay() { return eggBreakDelay;}
    public int getBrokenEggNumber() {
        return brokenEggNumber;
    }
}
