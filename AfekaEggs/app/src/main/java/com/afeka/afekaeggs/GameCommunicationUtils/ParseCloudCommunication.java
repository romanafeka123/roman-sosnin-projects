package com.afeka.afekaeggs.GameCommunicationUtils;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.afeka.afekaeggs.Activities.JoinGroupActivity;
import com.afeka.afekaeggs.Activities.PlayGameActivity;
import com.afeka.afekaeggs.Activities.TopPlayersActivity;
import com.afeka.afekaeggs.BL.Group;
import com.afeka.afekaeggs.BL.TopPlayer;
import com.afeka.afekaeggs.BL.TopPlayersListServices;
import com.afeka.afekaeggs.Properties;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Roman on 12/17/2015.
 */
public class ParseCloudCommunication {
    private Activity activity;
    private int playersAmount;

    // Available groups part
    private static final String PARSE_GROUP_CLASS_NAME = "Group";
    private static final String PARSE_GROUP_NAME = "groupName";
    private static final String PARSE_GROUP_INITIALIZER_NAME = "initializerName";
    private static final String PARSE_GROUP_INITIALIZER_IP = "initializerIP";
    private static final String PARSE_GROUP_PLAYERS_AMOUNT = "playersAmount";

    // Top players part
    private static final String PARSE_TOP_PLAYER_CLASS_NAME = "TopPlayer";
    private static final String PARSE_TOP_PLAYER_NAME = "name";
    private static final String PARSE_TOP_PLAYER_SCORE = "score";
    private static final String PARSE_TOP_PLAYER_LOCATION_LATITUDE = "location_latitude";
    private static final String PARSE_TOP_PLAYER_LOCATION_LONGITUDE = "location_longitude";
    private static final String PARSE_TOP_PLAYER_PLAYERS_AMOUNT = "players_amount";

    private boolean succeededToCreateGroup;

    public ParseCloudCommunication(Activity activity, int playersAmount) {
        this.activity = activity;
        this.playersAmount = playersAmount;
    }

    //************************************ Available groups part *********************************\\
    // returns true if succeeded to open a group, false otherwise
    public void openGroupInCloud(String groupName, String initializerName, String initializerIP, int playersAmount, final ReentrantLock lock, final CountDownLatch latch) {
        succeededToCreateGroup = false;
        ParseObject newGroupObject = new ParseObject(PARSE_GROUP_CLASS_NAME);
        newGroupObject.put(PARSE_GROUP_NAME, groupName);
        newGroupObject.put(PARSE_GROUP_INITIALIZER_NAME, initializerName);
        newGroupObject.put(PARSE_GROUP_INITIALIZER_IP, initializerIP);
        newGroupObject.put(PARSE_GROUP_PLAYERS_AMOUNT, playersAmount);
        newGroupObject.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // Group created successfully.
                    succeededToCreateGroup = true;
                } else {
                    // Group creation failed.
                    Log.d(getClass().getSimpleName(), Properties.FAILED_TO_OPEN_GROUP_MSG);
                }
                lock.lock();
                latch.countDown();
                lock.unlock();
            }
        });
    }

    public boolean isSucceededToCreateGroup() {
        return succeededToCreateGroup;
    }

    public void deleteOpenGroupFromCloud(String groupName) {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(PARSE_GROUP_CLASS_NAME);
        query.whereEqualTo(PARSE_GROUP_NAME, groupName);
        try {
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject status, ParseException e) {
                    try {
                        if (status != null) {
                            status.delete();
                            status.saveInBackground();
                        }
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        } catch (NullPointerException ex) {
            System.out.println("There no appropriate group to delete in cloud!");
        }
    }

    public void getGroupsListFromCloud(final ReentrantLock lock, final CountDownLatch latch) {
        final List<Group> groups = new LinkedList<Group>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery(PARSE_GROUP_CLASS_NAME);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> openGroupsList, ParseException e) {
                if (e == null) {
                    for (ParseObject g : openGroupsList) {
                        Group group = new Group(g.getString(PARSE_GROUP_NAME), g.getInt(PARSE_GROUP_PLAYERS_AMOUNT), g.getString(PARSE_GROUP_INITIALIZER_NAME), g.getString(PARSE_GROUP_INITIALIZER_IP));
                        groups.add(group);
                    }
                }
                else {
                    Log.d(getClass().getSimpleName(), e.getMessage());
                }
                if (activity instanceof JoinGroupActivity) {
                    ((JoinGroupActivity)activity).setOpenGroups(groups);
                }
                lock.lock();
                latch.countDown();
                lock.unlock();
            }
        });
    }

    //************************************** Top players part ***********************************\\
    public void downloadTopPlayersListFromParse(final ReentrantLock lock, final CountDownLatch latch) {
        final List<TopPlayer> topPlayersList = new ArrayList<TopPlayer>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery(PARSE_TOP_PLAYER_CLASS_NAME);
        query.whereEqualTo(PARSE_TOP_PLAYER_PLAYERS_AMOUNT, playersAmount);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> playersList, ParseException e) {
                if (e == null) {
                    for (ParseObject p : playersList) {
                        TopPlayer topPlayer = new TopPlayer(p.getString(PARSE_TOP_PLAYER_NAME), p.getInt(PARSE_TOP_PLAYER_SCORE), p.getString(PARSE_TOP_PLAYER_LOCATION_LATITUDE), p.getString(PARSE_TOP_PLAYER_LOCATION_LONGITUDE));
                        topPlayersList.add(topPlayer);
                    }
                    if (activity instanceof TopPlayersActivity) {
                        ((TopPlayersActivity) activity).setTopPlayersList(topPlayersList);
                        ((TopPlayersActivity) activity).connectToGoogleApiClient();
                    } else if (activity instanceof PlayGameActivity) {
                        ((PlayGameActivity) activity).getLevel().setTopPlayersList(topPlayersList);
                        lock.lock();
                        latch.countDown();
                        lock.unlock();
                    }
                } else {
                    Log.d(getClass().getSimpleName(), e.getMessage());
                    if (activity instanceof TopPlayersActivity) {
                        // if the activity is being destroyed, don't show the alert
                        if (!activity.isFinishing()) {
                            ((TopPlayersActivity) activity).showConnectionDownAlert();
                        }
                    } else if (activity instanceof PlayGameActivity) {
                        lock.lock();
                        latch.countDown();
                        lock.unlock();
                    }
                }
            }
        });
    }

    public void saveTopPlayerToParse(String name, int score, String location_latitude, String location_longitude, List<TopPlayer> currentTopPlayersList) {
        if (currentTopPlayersList.size() >= Properties.TOP_PLAYERS_AMOUNT) {
            updateTopPlayerInParse(name, score, location_latitude, location_longitude, currentTopPlayersList);
        }
        else {
            insertNewTopPlayerToParse(name, score, location_latitude, location_longitude, currentTopPlayersList);
        }
    }

    public void updateTopPlayerInParse(String name, int score, String location_latitude, String location_longitude, List<TopPlayer> currentTopPlayersList) {
        TopPlayer playerWithWorstScore = TopPlayersListServices.findPlayerWithWorstScore(currentTopPlayersList);
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery(PARSE_TOP_PLAYER_CLASS_NAME);
        parseQuery.whereEqualTo(PARSE_TOP_PLAYER_SCORE, playerWithWorstScore.getScore()).whereEqualTo(PARSE_TOP_PLAYER_PLAYERS_AMOUNT, playersAmount);
        List<ParseObject> objects = null;
            try {
                objects = parseQuery.find();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (objects.size() > 0) {
                ParseObject topPlayerToUpdate = objects.get(0);
                // Update the topPlayersList in Parse
                topPlayerToUpdate.put(PARSE_TOP_PLAYER_NAME, name);
                topPlayerToUpdate.put(PARSE_TOP_PLAYER_SCORE, score);
                topPlayerToUpdate.put(PARSE_TOP_PLAYER_LOCATION_LATITUDE, location_latitude);
                topPlayerToUpdate.put(PARSE_TOP_PLAYER_LOCATION_LONGITUDE, location_longitude);
                topPlayerToUpdate.saveInBackground(new SaveCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                            // Saved successfully
                            Toast.makeText(activity, Properties.TOP_SCORE_SAVED_MSG, Toast.LENGTH_SHORT).show();
                        } else {
                            // The save failed
                            Toast.makeText(activity, Properties.FAILED_TO_SAVE_TOP_PLAYER_MSG, Toast.LENGTH_SHORT).show();
                            Log.d(getClass().getSimpleName(), Properties.FAILED_TO_SAVE_TOP_PLAYER_MSG);
                        }
                    }
                });
            }

    }

    private void insertNewTopPlayerToParse(String name, int score, String location_latitude, String location_longitude, List<TopPlayer> currentTopPlayersList) {
        ParseObject newTopPlayer = new ParseObject(PARSE_TOP_PLAYER_CLASS_NAME);
        newTopPlayer.put(PARSE_TOP_PLAYER_NAME, name);
        newTopPlayer.put(PARSE_TOP_PLAYER_SCORE, score);
        newTopPlayer.put(PARSE_TOP_PLAYER_LOCATION_LATITUDE, location_latitude);
        newTopPlayer.put(PARSE_TOP_PLAYER_LOCATION_LONGITUDE, location_longitude);
        newTopPlayer.put(PARSE_TOP_PLAYER_PLAYERS_AMOUNT, playersAmount);
        newTopPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // Saved successfully.
                    Toast.makeText(activity, Properties.TOP_SCORE_SAVED_MSG, Toast.LENGTH_SHORT).show();
                } else {
                    // The save failed.
                    Toast.makeText(activity, Properties.FAILED_TO_SAVE_TOP_PLAYER_MSG, Toast.LENGTH_SHORT).show();
                    Log.d(getClass().getSimpleName(), Properties.FAILED_TO_SAVE_TOP_PLAYER_MSG);
                }
            }
        });
    }
}
