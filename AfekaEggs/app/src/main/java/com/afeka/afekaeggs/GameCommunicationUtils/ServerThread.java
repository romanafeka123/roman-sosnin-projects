package com.afeka.afekaeggs.GameCommunicationUtils;

import com.afeka.afekaeggs.Activities.AwaitingActivity;
import com.afeka.afekaeggs.Activities.PlayGameActivity;
import com.afeka.afekaeggs.GameMessages.ClickedOnEggMsg;
import com.afeka.afekaeggs.GameMessages.CloseConnectionRequest;
import com.afeka.afekaeggs.GameMessages.JoinGameRequest;
import com.afeka.afekaeggs.GameMessages.PlayerLeftGameMsg;
import com.afeka.afekaeggs.Properties;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Roman on 1/3/2016.
 */
// this class is relevant only if the current device is the game server
// I made this class to be Singleton, in order to access the same reference of this class in different activities.
// i.e in different phases of the game.
public class ServerThread {
    private static ServerThread serverThreadSingleton = null;

    // Communication components
    private List<ClientSocket> connections = new ArrayList<ClientSocket>();
    private ServerSocket serverSocket;
    private boolean serverUp;
    private Socket socket;

    // Activities
    private AwaitingActivity awaitingActivity;
    private PlayGameActivity playGameActivity;

    private ServerThread() {}

    public static ServerThread getInstance(boolean newInstance) {
        if (serverThreadSingleton == null || newInstance) {
            serverThreadSingleton = new ServerThread();
        }
        return serverThreadSingleton;
    }

    public void startServer() {
        try {
            serverSocket = new ServerSocket(Properties.GAME_SERVER_PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        serverUp = true;
        listenToClients();
    }

    public void listenToClients() {
        while (serverUp) {
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                // could not open socket or serverSocket was closed
                return;
            }
            // for every new incoming client, open a new thread
            if (awaitingActivity.getPlayersInGame()-1 > connections.size())
                openThreadForIncomingClient();
        }
    }

    // for every new client, open a thread and communicate with the client on that thread.
    private void openThreadForIncomingClient() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ClientSocket currentSocket = null;
                try {
                    currentSocket = new ClientSocket(socket, awaitingActivity);
                    connections.add(currentSocket);
                    boolean connected = true;
                    do {
                        Object obj = currentSocket.getInputStream().readObject();
                        if (obj instanceof CloseConnectionRequest) {
                            CloseConnectionRequest req = (CloseConnectionRequest) obj;
                            connected = false;
                            currentSocket.closeConnection();
                            connections.remove(currentSocket);
                            if (playGameActivity != null) {  // game in process state
                                playGameActivity.removeJoinedPlayerFromListView(req.getPlayerName());
                            }
                            else {  // awaiting for players to join state
                                awaitingActivity.setPlayersToJoinOneMore();
                                awaitingActivity.removeJoinedPlayerFromListView(req.getPlayerName());
                            }
                            sendMessageToClients(new PlayerLeftGameMsg(req.getPlayerName()));
                        }
                        else if (obj instanceof JoinGameRequest) {
                            JoinGameRequest req = (JoinGameRequest) obj;
                            currentSocket.setPlayerName(req.getPlayerName());
                            awaitingActivity.setPlayersToJoinOneLess();
                            awaitingActivity.addJoinedPlayerToListView(req.getPlayerName());
                            sendJoinGameRequestToAllClients(req.getPlayerName(), currentSocket);
                            sendAlreadyJoinedPlayersMsg(currentSocket);
                        }
                        else if (obj instanceof ClickedOnEggMsg) {
                            ClickedOnEggMsg clickedOnEggMsg = (ClickedOnEggMsg) obj;
                            redirectClickedOnEggMsgToOtherClients(currentSocket, new ClickedOnEggMsg("", -1, clickedOnEggMsg.getBrokenEggNumber()));  // this line to test
                            playGameActivity.getLevel().breakEggAtIndex(clickedOnEggMsg.getBrokenEggNumber());
                            playGameActivity.getLevel().addClientEggBreakDelay(clickedOnEggMsg.getPlayerName(), clickedOnEggMsg.getEggBreakDelay(), clickedOnEggMsg.getBrokenEggNumber());
                        }
                    } while (connected);
                } catch (ClassNotFoundException e) { e.printStackTrace(); }
                catch (IOException e) { e.printStackTrace(); }
                finally {
                    try {
                        if (currentSocket != null) {
                            currentSocket.getInputStream().close();
                            if (!currentSocket.getSocket().isClosed()) {
                                currentSocket.getSocket().close();
                            }
                        }
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }
        }).start();
    }

    public void closeServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<ClientSocket> syncConnectionsList = Collections.synchronizedList(connections);
                synchronized (syncConnectionsList) {
                    for (ClientSocket socket : syncConnectionsList) {
                        try {
                            socket.getInputStream().close();
                            socket.getOutputStream().writeObject(new CloseConnectionRequest(socket.getPlayerName()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        socket.closeConnection();
                    }
                }
                try {
                    serverSocket.close();
                    serverUp = false;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void redirectClickedOnEggMsgToOtherClients(ClientSocket currentSocket, Object msg) {
        try {
            for (ClientSocket clientSocket : connections)
                if (clientSocket != currentSocket) {
                    clientSocket.getOutputStream().writeObject(msg);
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendJoinGameRequestToAllClients(String playerName, ClientSocket currentSocket) {
        try {
            for (ClientSocket clientSocket : connections)
            if (clientSocket != currentSocket) {
                clientSocket.getOutputStream().writeObject(new JoinGameRequest(playerName));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendAlreadyJoinedPlayersMsg(ClientSocket currentSocket) {
        try {
            for (ClientSocket clientSocket : connections)
                if (clientSocket != currentSocket) {
                    if (clientSocket.getPlayerName() == null) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    currentSocket.getOutputStream().writeObject(new JoinGameRequest(clientSocket.getPlayerName()));
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessageToClients(Object message) {
        try {
            for (ClientSocket clientSocket : connections) {
                clientSocket.getOutputStream().writeObject(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isServerUp() {
        return serverUp;
    }

    public void setAwaitingActivity(AwaitingActivity awaitingActivity) {this.awaitingActivity = awaitingActivity;}
    public void setPlayGameActivity(PlayGameActivity playGameActivity) {this.playGameActivity = playGameActivity;}
}
