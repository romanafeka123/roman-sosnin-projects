package com.afeka.afekaeggs.GameMessages;

import java.io.Serializable;

/**
 * Created by Roman on 1/6/2016.
 */
public class OpenEggMsg implements Serializable{
    private static final long serialVersionUID	= 1L;
    private int eggNumberToOpen;

    public OpenEggMsg(int eggNumberToOpen) {
        this.eggNumberToOpen = eggNumberToOpen;
    }

    public int getEggNumberToOpen() {
        return eggNumberToOpen;
    }
}
