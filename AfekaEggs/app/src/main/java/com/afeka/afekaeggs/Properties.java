package com.afeka.afekaeggs;

/**
 * Created by Roman on 12/20/2015.
 */
public class Properties {
    // Game params
    public static final int GAME_SERVER_PORT = 8588;
    public static final int MIN_PLAYERS_AMOUNT = 2;
    public static final int MAX_PLAYERS_AMOUNT = 4;
    public static final int MIN_PLAYER_NAME_LEN = 3;
    public static final int MAX_PLAYER_NAME_LEN = 8;
    public static final int MIN_GROUP_NAME_LEN = 3;
    public static final int MAX_GROUP_NAME_LEN = 20;

    public static final int MIN_EGG_OPEN_DELAY = 1000; // ms
    public static final int MAX_EGG_OPEN_DELAY = 4000; // ms
    public static final int MAX_BREAK_EGG_DELAY = 3000; // ms
    public static final String DEFAULT_PLAYER_NAME = "Noname";
    public static final int MIN_TABLET_SCREEN_WIDTH = 1920; // px - for setting appropriate animations sizes

    // ********************************** Game messages ******************************************\\
    public static final String INTERNET_CONNECTION_DOWN_MSG = "You must connect to internet!";
    public static final String WRONG_NAME_LENGTH = "Name length should contain " + Properties.MIN_PLAYER_NAME_LEN + " to " + Properties.MAX_PLAYER_NAME_LEN + " chars.";
    public static final String FAILED_TO_OPEN_GROUP_MSG = "Failed to open a group: ";
    public static final String CREATING_GROUP_MSG_ON_WAIT = "Creating group...please wait!";
    public static final String ENTER_NAME_HINT = "Name length should be " + Properties.MIN_PLAYER_NAME_LEN + "-" + Properties.MAX_PLAYER_NAME_LEN + " chars.";
    public static final String NO_OPEN_GROUPS_MSG = "There are no open groups at the moment!";
    public static final String OBSERVING_OPEN_GROUPS_MSG_ON_WAIT = "Observing open groups...please wait!";
    public static final String FAILED_TO_CONNECT_TO_GAME_SERVER_MSG = "Failed to connect to server!";
    public static final String SERVER_IS_DOWN_MSG = "Server is down!";
    public static final String LAST_PLAYER_LEFT_GAME_MSG = "Last player left the game!";

    // ********************************** Game statuses ******************************************\\
    public static final String GAME_OVER_STATUS = "GAME_OVER_STATUS";
    public static final String GAME_IS_STARTING_STATUS = "GAME_IS_STARTING_STATUS";
    public static final String GAME_IS_IN_ACTION_STATUS = "GAME_IS_IN_ACTION_STATUS";
    public static String GAME_STATUS;

    // ******************************** For intent extra data ************************************\\
    public static final String PLAYERS_AMOUNT = "PLAYERS_AMOUNT";
    public static final String CURRENT_PLAYER_NAME = "CURRENT_PLAYER_NAME";
    public static final String PLAYER_NAME = "PLAYER_NAME";
    public static final String GROUP_NAME = "GROUP_NAME";
    public static final String GROUP_INITIALIZER_NAME = "GROUP_INITIALIZER_NAME";
    public static final String IS_GROUP_INITIALIZER = "IS_GROUP_INITIALIZER";
    public static final String NUMBER_OF_PLAYERS_IN_GAME = "NUMBER_OF_PLAYERS_IN_GAME";
    public static final String JOINED_PLAYERS_ARRAY = "JOINED_PLAYERS_ARRAY";

    // ********************************* Animations sources **************************************\\
    // **************************** Animations sources for mobile phones *************************\\
    public static final int BIG_EGG_SRC_PHONE = R.raw.big_egg_phone;
    public static final int MEDIUM_EGG_SRC_PHONE = R.raw.medium_egg_phone;
    public static final int SMALL_EGG_SRC_PHONE = R.raw.small_egg_phone;

    public static final int BIRD_SRC_PHONE = R.raw.bird_phone;
    public static final int MOUSE_SRC_PHONE = R.raw.mouse_phone;
    public static final int PIRATESHIP_SRC_PHONE = R.raw.pirateship_phone;
    public static final int MONKEY_SRC_PHONE = R.raw.monkey_phone;
    public static final int CRAZY_CHICKEN_SRC_PHONE = R.raw.crazy_chicken_phone;
    public static final int CHICK_SRC_PHONE = R.raw.chicken_phone;
    public static final int BOOM_SRC_PHONE = R.raw.boom_phone;
    public static final int THREE_TWO_ONE_SRC_PHONE = R.raw.three_two_one_phone;
    public static final int FIREWORK_SRC_PHONE = R.raw.firework_phone;

    // **************************** Animations sources for tablets *******************************\\
    public static final int BIG_EGG_SRC_TABLET = R.raw.big_egg_tablet;
    public static final int MEDIUM_EGG_SRC_TABLET = R.raw.medium_egg_tablet;
    public static final int SMALL_EGG_SRC_TABLET = R.raw.small_egg_tablet;

    public static final int BIRD_SRC_TABLET = R.raw.bird_tablet;
    public static final int MOUSE_SRC_TABLET = R.raw.mouse_tablet;
    public static final int PIRATESHIP_SRC_TABLET = R.raw.pirateship_tablet;
    public static final int MONKEY_SRC_TABLET = R.raw.monkey_tablet;
    public static final int CRAZY_CHICKEN_SRC_TABLET = R.raw.crazy_chicken_tablet;
    public static final int CHICK_SRC_TABLET = R.raw.chicken_tablet;
    public static final int BOOM_SRC_TABLET = R.raw.boom_tablet;
    public static final int THREE_TWO_ONE_SRC_TABLET = R.raw.three_two_one_tablet;
    public static final int FIREWORK_SRC_TABLET = R.raw.firework_tablet;

    // ************************************** Game Sounds ****************************************\\
    public static final int LEVEL_1_MELODY_SRC = R.raw.level1_sound;
    public static final int LEVEL_2_MELODY_SRC = R.raw.level2_sound;
    public static final int LEVEL_3_MELODY_SRC = R.raw.level3_sound;
    public static final int EGG_BROKEN_SOUND_SRC = R.raw.egg_broken;

    // ************************************** Eggs Sizes *****************************************\\
    public static final String BIG_EGG_SIZE = "BIG_EGG_SIZE";
    public static final String MEDIUM_EGG_SIZE = "MEDIUM_EGG_SIZE";
    public static final String SMALL_EGG_SIZE = "SMALL_EGG_SIZE";

    // *********************************** Eggs Sizes Points *************************************\\
    public static final int SMALL_EGG_SIZE_POINTS = 3;
    public static final int MEDIUM_EGG_SIZE_POINTS = 2;
    public static final int BIG_EGG_SIZE_POINTS = 1;

    // ****************************** Playfield ground sources ***********************************\\
    public static final int PLAY_GAME_BACKGROUND_1 = R.drawable.play_game_background1;
    public static final int PLAY_GAME_BACKGROUND_2 = R.drawable.play_game_background2;
    public static final int PLAY_GAME_BACKGROUND_3 = R.drawable.play_game_background3;

    // ******************************************* DB ********************************************\\
    public static final int TOP_PLAYERS_AMOUNT = 10;
    public static final String TOP_SCORE_SAVED_MSG = "Top Score Saved!";
    public static final String FAILED_TO_SAVE_TOP_PLAYER_MSG = "Failed to Save Top Player";

    // ************************************** Google Maps ****************************************\\
    public static final int GOOGLE_MAP_ZOOM = 10;
    public static final int NUMBER_OF_GOOGLE_ADDRESS_PROVIDERS = 1;
    public static final int SPECIAL_SIGN_FOR_NON_EXISTING_COORDINATES = -1000;
    public static final String NO_ADDRESS_FOUND_STATEMENT = "Address was not found for the specified coordinates";
}
