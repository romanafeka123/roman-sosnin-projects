package com.afeka.afekaeggs.BL;

import android.content.Context;
import android.view.ViewGroup;

import com.afeka.afekaeggs.CustomGUIElements.GifView;
import com.afeka.afekaeggs.Levels.Level;

/**
 * Created by Roman on 1/6/2016.
 */
public class Egg extends GifView {
    private String eggSize;
    private boolean isOpen;
    private boolean isBroken;
    private Level level;
    private Egg thisEgg = this;
    private int eggNumber;

    public Egg(Context context, int gifResourceID, ViewGroup containingElement, Level level, int eggNumber) {
        super(context, gifResourceID, containingElement, false, false, false);
        isOpen = false;
        isBroken = false;
        this.level = level;
        this.eggNumber = eggNumber;
    }

    public String getEggSize() {
        return eggSize;
    }

    public void setEggSize(String eggSize) {
        this.eggSize = eggSize;
    }

    public void openEgg() {
        movieRunDuration = 0;
        setOpen(true);
        startAnimation();
    }

    public boolean isOpen() {
        return isOpen;
    }
    public boolean isBroken() {
        return isBroken;
    }
    public void setBroken(boolean isBroken) {
        this.isBroken = isBroken;
    }
    public void setOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public void startAnimation() {
        setRunning(true);
        stopAndRemoveAnimationtAfterDurationTime();
    }

    protected void stopAndRemoveAnimationtAfterDurationTime() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(movieDuration);
                    containingElement.post(new Runnable() {
                        public void run() {
                            setRunning(false);
                            isBroken = true;
                            //containingElement.removeView(thisView);  // Possibly this row throws exception
                            level.removeEggFromField(thisEgg);
                            containingElement.removeView(thisEgg);
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public int getEggNumber() {
        return eggNumber;
    }

}
