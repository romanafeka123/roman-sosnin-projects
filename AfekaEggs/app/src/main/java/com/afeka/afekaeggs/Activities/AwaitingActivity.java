package com.afeka.afekaeggs.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afeka.afekaeggs.CustomGUIElements.AlertDialogHelper;
import com.afeka.afekaeggs.GameCommunicationUtils.ClientConnection;
import com.afeka.afekaeggs.GameCommunicationUtils.ParseCloudCommunication;
import com.afeka.afekaeggs.GameCommunicationUtils.ServerThread;
import com.afeka.afekaeggs.GameMessages.StartGameMsg;
import com.afeka.afekaeggs.Properties;
import com.afeka.afekaeggs.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman on 12/20/2015.
 */
public class AwaitingActivity extends Activity {
    // UI elements
    private RelativeLayout awaitingGameLayout;
    private ImageButton startGameBtn;
    private ListView joiningPlayersListView;
    private TextView playersAmountToJoinTxtView;
    private TextView groupNameTxtView;
    private ArrayAdapter<String> joinedPlayersListAdapter;
    private Activity thisActivity = this;
    private AlertDialogHelper alertDialogHelper;

    // BL elements
    private String groupName;
    private String currentPlayerName;
    private String groupInitializerName;
    private int playersInGame;
    private int playersToJoin;
    private List<String> joinedPlayersList;
    private List<String> joinedPlayersListFormatted;
    private boolean isGroupInitializer;

    // Communication elements (cloud & sockets)
    private ParseCloudCommunication groupCreateCloudCommunication;
    private ServerThread serverThread;          // will be null if game client
    private ClientConnection clientConnection;  // will be null if game server

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.awaiting_layout);
        initializeFields();
        setListeners();
        if (isGroupInitializer) {
            runGameServer();
        }
        else {
            addCurrentPlayerToListView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeCommunication();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK, new Intent());
        finish();
    }

    // finish this activity if left the game while playing
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK){
                setResult(RESULT_OK, new Intent());
                finish();
            }
        }
    }

    private void initializeFields() {
        startGameBtn = (ImageButton) findViewById(R.id.start_game_btn);
        disableStartGameBtn();

        awaitingGameLayout = (RelativeLayout)findViewById(R.id.awaitingGameLayout);
        isGroupInitializer = getIntent().getBooleanExtra(Properties.IS_GROUP_INITIALIZER, false);

        currentPlayerName = getIntent().getStringExtra(Properties.PLAYER_NAME);
        groupNameTxtView = (TextView)findViewById(R.id.group_name_txt_view);
        groupName = getIntent().getStringExtra(Properties.GROUP_NAME);

        if (isGroupInitializer) {
            groupInitializerName = currentPlayerName;
        }
        else {
            groupInitializerName = getIntent().getStringExtra(Properties.GROUP_INITIALIZER_NAME);
        }
        groupNameTxtView.setText("Group: " + groupName + " opened by: " + groupInitializerName);

        joinedPlayersList = new ArrayList<String>();
        joinedPlayersListFormatted = new ArrayList<String>();
        joiningPlayersListView = (ListView) findViewById(R.id.joining_players_list_view);
        joinedPlayersListAdapter = new ArrayAdapter<String>(this, R.layout.list_view_layout, joinedPlayersListFormatted);
        joiningPlayersListView.setAdapter(joinedPlayersListAdapter);

        playersInGame = getIntent().getIntExtra(Properties.NUMBER_OF_PLAYERS_IN_GAME, Properties.MIN_PLAYERS_AMOUNT);
        playersAmountToJoinTxtView = (TextView) findViewById(R.id.players_to_join_txt_view);
        playersToJoin = playersInGame - 1;
        playersAmountToJoinTxtView.setText(playersToJoin + " players to join");

        if (!isGroupInitializer) {
            awaitingGameLayout.removeView(startGameBtn);
            clientConnection = ClientConnection.getInstance(false);
            clientConnection.setAwaitingActivity((AwaitingActivity)thisActivity);
            clientConnection.fireAwaitingActivityReadyEvent();
        }

        groupCreateCloudCommunication = new ParseCloudCommunication(this, -1);
        alertDialogHelper = new AlertDialogHelper(this);
    }

    private void setListeners() {
        // this startGameBtn is relevant only if the current device is game server
        startGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                groupCreateCloudCommunication.deleteOpenGroupFromCloud(groupName);
                serverThread.sendMessageToClients(new StartGameMsg());
                Intent intent = new Intent(getApplicationContext(), PlayGameActivity.class);
                intent.putExtra(Properties.GROUP_INITIALIZER_NAME, groupInitializerName);
                intent.putExtra(Properties.CURRENT_PLAYER_NAME, currentPlayerName);
                intent.putExtra(Properties.IS_GROUP_INITIALIZER, true);
                putJoinedPlayersInIntent(intent);
                startActivityForResult(intent, 1);
            }
        });
    }

    private void putJoinedPlayersInIntent(Intent intent) {
        String[] joinedPlayersArr = new String[joinedPlayersList.size()];
        for (int i = 0; i < joinedPlayersList.size(); i++) {
            joinedPlayersArr[i] = joinedPlayersList.get(i);
        }
        intent.putExtra(Properties.JOINED_PLAYERS_ARRAY, joinedPlayersArr);
    }

    private void runGameServer() {
        serverThread = ServerThread.getInstance(true);
        serverThread.setAwaitingActivity((AwaitingActivity)thisActivity);
        new Thread(new Runnable() {
            @Override
            public void run() {
                serverThread.startServer();
            }
        }).start();
    }

    public int getPlayersInGame() { return playersInGame; }

    public void setPlayersToJoinOneLess() {
        playersToJoin -= 1;
        updatePlayersAmountToJoinTxtView();
    }

    public void setPlayersToJoinOneMore() {
        playersToJoin += 1;
        updatePlayersAmountToJoinTxtView();
    }

    private void updatePlayersAmountToJoinTxtView() {
        playersAmountToJoinTxtView.post(new Runnable() {
            @Override
            public void run() {
                playersAmountToJoinTxtView.setText(playersToJoin + " players to join");
            }
        });
    }

    public void addJoinedPlayerToListView(String playerName) {
        joinedPlayersList.add(playerName);
        String joinedPlayerName = "       " + playerName + " joined group!";
        joinedPlayersListFormatted.add(joinedPlayerName);
        joiningPlayersListView.post(new Runnable() {
            @Override
            public void run() {
                joinedPlayersListAdapter.notifyDataSetChanged();
                if (playersToJoin == 0)
                    enableStartGameBtn();
            }
        });
    }

    public void removeJoinedPlayerFromListView(String playerName) {
        joinedPlayersList.remove(playerName);
        String joinedPlayerName = "       " + playerName + " joined group!";
        joinedPlayersListFormatted.remove(joinedPlayerName);
        joiningPlayersListView.post(new Runnable() {
            @Override
            public void run() {
                joinedPlayersListAdapter.notifyDataSetChanged();
                disableStartGameBtn();
            }
        });
    }

    private void enableStartGameBtn() {
        startGameBtn.setEnabled(true);
        startGameBtn.setAlpha(1.0f);
    }

    private void disableStartGameBtn() {
        startGameBtn.setEnabled(false);
        startGameBtn.setAlpha(0.5f);
    }

    public void showAlertAfterServerDown(final String msg) {
        awaitingGameLayout.post(new Runnable() {
            @Override
            public void run() {
                AlertDialog alertDialog = new AlertDialog.Builder(thisActivity).create();
                alertDialog.setMessage(msg);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                setResult(RESULT_OK, new Intent());
                                finish();
                            }
                        });
                alertDialog.show();
            }
        });
    }

    private void addCurrentPlayerToListView() {
        setPlayersToJoinOneLess();
        addJoinedPlayerToListView(currentPlayerName);
    }

    // this method is called only when the current player is game client (StartGameMsg received from server)
    public void startGame() {
        Intent intent = new Intent(getApplicationContext(), PlayGameActivity.class);
        intent.putExtra(Properties.GROUP_INITIALIZER_NAME, groupInitializerName);
        intent.putExtra(Properties.CURRENT_PLAYER_NAME, currentPlayerName);
        intent.putExtra(Properties.IS_GROUP_INITIALIZER, false);
        putJoinedPlayersInIntent(intent);
        startActivityForResult(intent, 1);
    }

    private void closeCommunication() {
        if (serverThread != null) { // do this only if current device is game server
            groupCreateCloudCommunication.deleteOpenGroupFromCloud(groupName);
            if (serverThread.isServerUp()) {
                serverThread.closeServer();
            }
        }
        if (clientConnection != null) { // do this only if current device is game client
            clientConnection.disconnect();
        }
    }

    public RelativeLayout getAwaitingGameLayout() {
        return awaitingGameLayout;
    }
}
