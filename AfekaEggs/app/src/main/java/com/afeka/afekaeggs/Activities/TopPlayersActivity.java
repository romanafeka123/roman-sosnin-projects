package com.afeka.afekaeggs.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import com.afeka.afekaeggs.BL.TopPlayer;
import com.afeka.afekaeggs.BL.TopPlayersListServices;
import com.afeka.afekaeggs.CustomGUIElements.TopPlayersListFragment;
import com.afeka.afekaeggs.GameCommunicationUtils.ParseCloudCommunication;
import com.afeka.afekaeggs.Properties;
import com.afeka.afekaeggs.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Roman on 12/20/2015.
 */
public class TopPlayersActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, TopPlayersListFragment.TopPlayerClicked {
    // GoogleMap API
    private GoogleMap googleMap;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Geocoder geocoder;
    private List<Marker> markers;

    // Cloud DB
    private ParseCloudCommunication parse;
    private List<TopPlayer> topPlayersList;
    private TopPlayersListFragment topPlayersListFragment;

    // Internet Connection checking variable
    private boolean isInternetConnectionPresent;
    // For synchronizing purposes - to avoid Racing Condition
    private ReentrantLock lock;
    private CountDownLatch latch;

    private int playersAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.top_players_layout);
        initializeFields();
        createGoogleAPIClient();
        parse.downloadTopPlayersListFromParse(null, null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }

    private void initializeFields() {
        playersAmount = getIntent().getIntExtra(Properties.PLAYERS_AMOUNT, -1);
        lock = new ReentrantLock();
        latch = new CountDownLatch(1);
        try {
            if (googleMap == null) {
                googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_fragment)).getMap();
            }
            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        topPlayersListFragment = ((TopPlayersListFragment) getFragmentManager().findFragmentById(R.id.list_fragment));
        parse = new ParseCloudCommunication(this, playersAmount);
    }

    public void connectToGoogleApiClient() {
        // connecting the GoogleAPIClient to Google Server in order to track current location
        mGoogleApiClient.connect();
    }

    private void createGoogleAPIClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        geocoder = new Geocoder(this, Locale.getDefault());
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isConnectingToInternet())
                    isInternetConnectionPresent = true;
                else
                    isInternetConnectionPresent = false;
                lock.lock();
                latch.countDown();
                lock.unlock();
            }
        }).start();

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (isInternetConnectionPresent) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            Marker myLocationMarker;
            if (mLastLocation != null) {
                LatLng myLocationCoordinates = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                String address = getAddressByLatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                myLocationMarker = googleMap.addMarker(new MarkerOptions().position(myLocationCoordinates).title("You are here. " + address).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocationCoordinates, Properties.GOOGLE_MAP_ZOOM));
            }
            setMarkersOnMap();
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setCompassEnabled(true);
        }
        else {
            showConnectionDownAlert();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        showConnectionDownAlert();
    }

    private String getAddressByLatLng(double latitude, double longitude) {
        String address = "";
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, Properties.NUMBER_OF_GOOGLE_ADDRESS_PROVIDERS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        if (addresses != null) {
            try {
                String city = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                address = city + ", " + country;
            } catch (IndexOutOfBoundsException ex) {
                Log.d(getClass().getSimpleName(), Properties.NO_ADDRESS_FOUND_STATEMENT);
            }
        }
        return address;
    }

    private void setMarkersOnMap() {
        markers = new LinkedList<Marker>();
        for (int i = 0; i < topPlayersList.size(); i++) {
            double latitude = Double.parseDouble(topPlayersList.get(i).getLocationLatitude());
            double longitude = Double.parseDouble(topPlayersList.get(i).getLocationLongitude());
            if (latitude != Properties.SPECIAL_SIGN_FOR_NON_EXISTING_COORDINATES && longitude != Properties.SPECIAL_SIGN_FOR_NON_EXISTING_COORDINATES) {
                LatLng locationCoordinates = new LatLng(latitude, longitude);
                String playerName = topPlayersList.get(i).getName();
                int playerScore = topPlayersList.get(i).getScore();
                markers.add(googleMap.addMarker(new MarkerOptions().position(locationCoordinates).title(playerName + ". " + playerScore + ". " + getAddressByLatLng(latitude, longitude))));
            }
        }
    }

    private boolean isConnectingToInternet() {
        if (networkConnectivity()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(3000);
                urlc.setReadTimeout(4000);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                return (false);
            }
        } else
            return false;
    }

    private boolean networkConnectivity() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public void showConnectionDownAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(TopPlayersActivity.this).create();
        alertDialog.setMessage("Internet Connection is down.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void sendTopPlayerCoordinates(LatLng topPlayerCoordinates) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(topPlayerCoordinates, Properties.GOOGLE_MAP_ZOOM));
        for (int i = 0; i < markers.size(); i++) {
            if (markers.get(i).getPosition().equals(topPlayerCoordinates))
                markers.get(i).showInfoWindow();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(new Configuration());
    }

    public void setTopPlayersList(List<TopPlayer> topPlayersList) {
        TopPlayersListServices.setTopPlayersRanks(topPlayersList);
        this.topPlayersList = topPlayersList;
        topPlayersListFragment.setTopPlayersListOnListView(topPlayersList);
    }
}