package com.afeka.afekaeggs.GameCommunicationUtils;

import com.afeka.afekaeggs.Activities.AwaitingActivity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by Roman on 1/3/2016.
 */

public class ClientSocket {
    private Socket socket;
    private ObjectInputStream inStream;
    private ObjectOutputStream outStream;
    private String clientIP;

    private AwaitingActivity awaitingActivity;
    private String playerName;

    public ClientSocket(Socket socket, AwaitingActivity awaitingActivity) {
        clientIP = socket.getInetAddress() + ":" + socket.getPort();
        this.awaitingActivity = awaitingActivity;
        this.socket = socket;
        try {
            inStream = new ObjectInputStream(socket.getInputStream());
            outStream = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
        }
    }

    public void closeConnection() {
        try {
            if (!socket.isClosed())
                socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }
    public ObjectInputStream getInputStream() {
        return inStream;
    }
    public ObjectOutputStream getOutputStream() {
        return outStream;
    }
    public String getClientIP() {return clientIP;}
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
    public String getPlayerName() {
        return playerName;
    }
}

