package com.afeka.afekaeggs.GameMessages;

import java.io.Serializable;

/**
 * Created by Roman on 1/6/2016.
 */
public class FastestEggBreakerMsg implements Serializable {
    private static final long serialVersionUID	= 1L;
    private String playerName;
    private String eggSize;

    public FastestEggBreakerMsg(String playerName, String eggSize) {
        this.playerName = playerName;
        this.eggSize = eggSize;
    }

    public String getPlayerName() {return playerName;}
    public String getEggSize() {
        return eggSize;
    }
}
