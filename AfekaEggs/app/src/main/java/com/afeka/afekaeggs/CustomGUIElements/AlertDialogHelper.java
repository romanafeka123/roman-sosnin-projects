package com.afeka.afekaeggs.CustomGUIElements;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by Roman on 1/5/2016.
 */
public class AlertDialogHelper {
    private Context context;

    public AlertDialogHelper(Context context) {
        this.context = context;
    }

    public void showAlert(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
