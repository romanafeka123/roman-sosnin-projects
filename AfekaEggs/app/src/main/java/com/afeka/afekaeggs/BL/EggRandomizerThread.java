package com.afeka.afekaeggs.BL;

import com.afeka.afekaeggs.Activities.PlayGameActivity;
import com.afeka.afekaeggs.Levels.Level;
import com.afeka.afekaeggs.Properties;

import java.util.Random;

/**
 * Created by Roman on 1/9/2016.
 */
public class EggRandomizerThread extends Thread {
    private Level level;
    private PlayGameActivity playGameActivity;
    private Random random;
    private boolean keepRandomizingEggs;

    public EggRandomizerThread(Level level) {
        this.level = level;
        random = new Random();
        keepRandomizingEggs = true;
        playGameActivity = level.getPlayGameActivity();
    }

    @Override
    public void run() {
        while (keepRandomizingEggs && !playGameActivity.isFinishing()) {
            int milliSecDelay = random.nextInt(Properties.MAX_EGG_OPEN_DELAY - Properties.MIN_EGG_OPEN_DELAY) + Properties.MIN_EGG_OPEN_DELAY;
            try {
                Thread.sleep(milliSecDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            level.openRandomEgg();
        }
    }

    public void stopRandomizingEggs() {
        this.keepRandomizingEggs = false;
    }
}
