package com.afeka.afekaeggs.GameMessages;

/**
 * Created by Roman on 1/6/2016.
 */
public class WinnerDeclareMsg {
    private static final long serialVersionUID	= 1L;
    private String winnerPlayerName;

    public WinnerDeclareMsg(String winnerPlayerName) {
        this.winnerPlayerName = winnerPlayerName;
    }

    public String getWinnerPlayerName() {
        return winnerPlayerName;
    }
}
