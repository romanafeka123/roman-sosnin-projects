package com.afeka.afekaeggs.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.afeka.afekaeggs.CustomGUIElements.AlertDialogHelper;
import com.afeka.afekaeggs.GameCommunicationUtils.NetworkConnectionUtils;
import com.afeka.afekaeggs.Properties;
import com.afeka.afekaeggs.R;

public class MainActivity extends Activity {
    private ImageButton createGroupBtn, joinGroupBtn, readMeBtn, exitBtn;
    private ImageButton twoPlayersTop10Btn, threePlayersTop10Btn, fourPlayersTop10Btn;
    private AlertDialogHelper alertDialogHelper;

    private NetworkConnectionUtils networkConnectionUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        initializeFields();
        setListeners();
    }

    private void initializeFields() {
        Properties.GAME_STATUS = Properties.GAME_IS_STARTING_STATUS;
        networkConnectionUtils = new NetworkConnectionUtils(this);
        alertDialogHelper = new AlertDialogHelper(this);
    }

    private void setListeners() {
        createGroupBtn = (ImageButton) findViewById(R.id.create_group_btn);
        createGroupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (networkConnectionUtils.isInternetConnectionUP()) {
                    Intent intent = new Intent(getApplicationContext(), CreateGroupActivity.class);
                    startActivity(intent);
                } else {
                    alertDialogHelper.showAlert(Properties.INTERNET_CONNECTION_DOWN_MSG);
                }

            }
        });
        joinGroupBtn = (ImageButton) findViewById(R.id.join_group_btn_main_screen);
        joinGroupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (networkConnectionUtils.isInternetConnectionUP()) {
                    Intent intent = new Intent(getApplicationContext(), JoinGroupActivity.class);
                    startActivity(intent);
                } else {
                    alertDialogHelper.showAlert(Properties.INTERNET_CONNECTION_DOWN_MSG);
                }
            }
        });
        exitBtn = (ImageButton) findViewById(R.id.exit_btn);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        twoPlayersTop10Btn = (ImageButton) findViewById(R.id.two_players_top10_btn);
        twoPlayersTop10Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TopPlayersActivity.class);
                intent.putExtra(Properties.PLAYERS_AMOUNT, 2);
                startActivity(intent);
            }
        });
        threePlayersTop10Btn = (ImageButton) findViewById(R.id.three_players_top10_btn);
        threePlayersTop10Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TopPlayersActivity.class);
                intent.putExtra(Properties.PLAYERS_AMOUNT, 3);
                startActivity(intent);

            }
        });
        fourPlayersTop10Btn = (ImageButton) findViewById(R.id.four_players_top10_btn);
        fourPlayersTop10Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TopPlayersActivity.class);
                intent.putExtra(Properties.PLAYERS_AMOUNT, 4);
                startActivity(intent);
            }
        });
        readMeBtn = (ImageButton) findViewById(R.id.read_me_btn);
        readMeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ReadmeActivity.class);
                startActivity(intent);
            }
        });
    }
}