package ProductFactory;

import BL.Properties;

public class MagnumIceCream extends IceCream{
	// *** add more fields, that are specific to this product ***
	
	public MagnumIceCream() {
		super(Properties.ICECREAM_MAGNUM_PRICE);
	}
	
	public String toString() {
		return Properties.ICECREAM_MAGNUM_NAME;
	}
}
