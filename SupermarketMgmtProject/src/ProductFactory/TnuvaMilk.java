package ProductFactory;

import BL.Properties;

public class TnuvaMilk extends Milk{
	// *** add more fields, that are specific to this product ***
	
	public TnuvaMilk() {
		super(Properties.MILK_TNUVA_PRICE);
	}
	
	public String toString() {
		return Properties.MILK_TNUVA_NAME;
	}
}
