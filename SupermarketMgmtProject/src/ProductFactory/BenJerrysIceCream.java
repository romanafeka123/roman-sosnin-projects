package ProductFactory;

import BL.Properties;

public class BenJerrysIceCream extends IceCream{
	// *** add more fields, that are specific to this product ***
	
	public BenJerrysIceCream() {
		super(Properties.ICECREAM_BEN_JERRYS_PRICE);
	}
	
	public String toString() {
		return Properties.ICECREAM_BEN_JERRYS_NAME;
	}
}
