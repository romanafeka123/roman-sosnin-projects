package ProductFactory;

import BL.Properties;

public class ChocolateFactory extends ProductFactory{
	
	@Override
	public Chocolate getChocolate(String chocolateManufacturer) {
		if (chocolateManufacturer == null) {
			return null;
		}
		if (chocolateManufacturer.equalsIgnoreCase(Properties.CHOCOLATE_ELITE_NAME)) {
			return new EliteChocolate();

		} else if (chocolateManufacturer.equalsIgnoreCase(Properties.CHOCOLATE_MILKA_NAME)) {
			return new MilkaChocolate();
		}
		return null;
	}

	@Override
	public Milk getMilk(String milkManufacturer) {return null;}
	@Override
	public IceCream getIceCream(String iceCream) {return null;}
}
