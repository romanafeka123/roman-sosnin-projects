package ProductFactory;

import BL.Properties;

public class MilkFactory extends ProductFactory {

	@Override
	public Milk getMilk(String milkManufacturer) {
		if (milkManufacturer == null) {
			return null;
		}

		if (milkManufacturer.equalsIgnoreCase(Properties.MILK_TNUVA_NAME)) {
			return new TnuvaMilk();

		} else if (milkManufacturer.equalsIgnoreCase(Properties.MILK_TARA_NAME)) {
			return new TaraMilk();

		} else if (milkManufacturer.equalsIgnoreCase(Properties.MILK_YOTVATA_NAME)) {
			return new YotvataMilk();
		}

		return null;
	}

	@Override
	public Chocolate getChocolate(String chocolate) {return null;}
	@Override
	public IceCream getIceCream(String iceCream) {return null;}
}