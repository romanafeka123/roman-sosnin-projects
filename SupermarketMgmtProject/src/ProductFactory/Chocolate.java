package ProductFactory;

public abstract class Chocolate extends Product{
	private String expireDate;
	
	public Chocolate(int price) {
		super(price);
		setExpireDate("01-01-2017");
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
}
