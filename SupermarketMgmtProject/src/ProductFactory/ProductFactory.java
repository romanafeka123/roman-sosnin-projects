package ProductFactory;

// Abstract Factory Design Pattern implementation
public abstract class ProductFactory {
	public abstract Milk getMilk(String milk);
	public abstract Chocolate getChocolate(String chocolate);
	public abstract IceCream getIceCream(String iceCream);
}
