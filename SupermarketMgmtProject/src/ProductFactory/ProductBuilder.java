package ProductFactory;

public class ProductBuilder {
	public static Product buildProduct(String productFactoryName, String productManufacturerName) {
		ProductFactory productFactory = ProductFactoryProducer.getFactory(productFactoryName);
	    Product product = null;
	    if (productFactory instanceof MilkFactory) {
	    	product = productFactory.getMilk(productManufacturerName);
	    }
	    else if (productFactory instanceof ChocolateFactory) {
	    	product = productFactory.getChocolate(productManufacturerName);
	    }
	    else if (productFactory instanceof IceCreamFactory) {
	    	product = productFactory.getIceCream(productManufacturerName);
	    }
	    return product;
	}
}
