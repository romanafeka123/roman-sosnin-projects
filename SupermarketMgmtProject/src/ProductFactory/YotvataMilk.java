package ProductFactory;

import BL.Properties;

public class YotvataMilk extends Milk{
	// *** add more fields, that are specific to this product ***
	
	public YotvataMilk() {
		super(Properties.MILK_YOTVATA_PRICE);
	}
	
	public String toString() {
		return Properties.MILK_YOTVATA_NAME;
	}
}
