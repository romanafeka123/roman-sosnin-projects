package ProductFactory;

public abstract class Milk extends Product{
	
	public Milk(int price) {
		super(price);
	}
}
