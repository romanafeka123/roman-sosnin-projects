package ProductFactory;

public abstract class Product {
	protected int price;
	
	public Product(int price) {
		this.setPrice(price);
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
