package ProductFactory;

import BL.Properties;

public class IceCreamFactory extends ProductFactory{

	@Override
	public IceCream getIceCream(String iceCreamManufacturer) {
		if (iceCreamManufacturer == null) {
			return null;
		}
		if (iceCreamManufacturer.equalsIgnoreCase(Properties.ICECREAM_MAGNUM_NAME)) {
			return new MagnumIceCream();

		} else if (iceCreamManufacturer.equalsIgnoreCase(Properties.ICECREAM_BEN_JERRYS_NAME)) {
			return new BenJerrysIceCream();
		}
		return null;
	}
	
	@Override
	public Milk getMilk(String milk) {return null;}
	@Override
	public Chocolate getChocolate(String chocolate) {return null;}
}
