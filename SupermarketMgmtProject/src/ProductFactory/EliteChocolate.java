package ProductFactory;

import BL.Properties;

public class EliteChocolate extends Chocolate{
	// *** add more fields, that are specific to this product ***
	
	public EliteChocolate() {
		super(Properties.CHOCOLATE_ELITE_PRICE);
	}
	
	public String toString() {
		return Properties.CHOCOLATE_ELITE_NAME;
	}
}
