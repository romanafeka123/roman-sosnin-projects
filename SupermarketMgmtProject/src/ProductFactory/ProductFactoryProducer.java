package ProductFactory;

import BL.Properties;

public class ProductFactoryProducer {
	public static ProductFactory getFactory(String factoryName) {
		if (factoryName.equalsIgnoreCase(Properties.MILK_FACTORY_NAME)) {
			return new MilkFactory();
		} else if (factoryName.equalsIgnoreCase(Properties.CHOCOLATE_FACTORY_NAME)) {
			return new ChocolateFactory();
		} else if (factoryName.equalsIgnoreCase(Properties.ICECREAM_FACTORY_NAME)) {
			return new IceCreamFactory();
		}
		return null;
	}
}