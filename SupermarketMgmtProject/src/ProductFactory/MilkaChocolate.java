package ProductFactory;

import BL.Properties;

public class MilkaChocolate extends Chocolate{
	// *** add more fields, that are specific to this product ***
	
	public MilkaChocolate() {
		super(Properties.CHOCOLATE_MILKA_PRICE);
	}
	
	public String toString() {
		return Properties.CHOCOLATE_MILKA_NAME;
	}
}
