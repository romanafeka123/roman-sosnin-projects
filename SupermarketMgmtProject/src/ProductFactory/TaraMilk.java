package ProductFactory;

import BL.Properties;

public class TaraMilk extends Milk{
	// *** add more fields, that are specific to this product ***
	
	public TaraMilk() {
		super(Properties.MILK_TARA_PRICE);
	}
	
	public String toString() {
		return Properties.MILK_TARA_NAME;
	}
}
