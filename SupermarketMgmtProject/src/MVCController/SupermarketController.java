package MVCController;

import java.util.List;
import java.util.Map;

import BL.Supermarket;
import BuildCartState.BuildCartState;
import GUI.MainFrame;
import DAL.DALConnector;
import Listeners.GUIEventsListener;
import ProductFactory.Product;
import Listeners.BLEventsListener;

// MVC Design Pattern implementation
public class SupermarketController implements BLEventsListener, GUIEventsListener {
	private Supermarket superMarket;
	private MainFrame mainFrame;
	private DALConnector dalConnector;
	
    public SupermarketController(Supermarket superMarket, MainFrame mainFrame, DALConnector dalConnector) {
    	this.superMarket = superMarket;
    	this.mainFrame = mainFrame;
    	this.dalConnector = dalConnector;
    	superMarket.registerListener(this);
    	mainFrame.registerListener(this);
    }
	  
	  //-----------------------------------------------------------------\\
	 //------------------------- Fire GUI Events -------------------------\\
	//---------------------------------------------------------------------\\
	public void fireAddCustomerToQueueEvent(Map<String, String> productsMap, int queueNum) {
		superMarket.addCustomerToQueueEventFired(productsMap, queueNum);
	} 
	
	public void fireAddQueueToSupermarketEvent(int queueNum) {
		superMarket.addQueueToSupermarketEventFired(queueNum);
	} 
	
	public void fireRemoveQueueFromSupermarketEvent() {
		superMarket.removeQueueFromSupermarketEventFired();
	}

	public void fireBuildCartStateChangedEvent(BuildCartState buildCartState) {
		superMarket.buildCartStateChangedEventFired(buildCartState);
	}
	
	@Override
	public void fireSupermarketClosingEvent() {
		superMarket.supermarketClosingEventFired();
		dalConnector.closeConnection();
	}
	
	  //-----------------------------------------------------------------\\
	 //------------------------- Fire BL Events --------------------------\\
	//---------------------------------------------------------------------\\
	
	public void fireCustomerLeavesEvent(int queueNum) {
		mainFrame.customerLeavesEventFired(queueNum);
	}

	public void fireAddTransactionToDALEvent(List<Product> products) {
		// transmitting data to DAL
		for (Product product : products) {
			dalConnector.insertNewTransactionToDAL(product);
		}
	}
}
