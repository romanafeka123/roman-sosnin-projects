package DAL;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import BL.Properties;

public class JSONFile implements DALConnection {
	private BufferedWriter writer;
	private JSONObject trasactionsJSON;
	private JSONArray listOfTransactions;
	
	@Override
	public void connectToDAL() {
		writer = null;
		trasactionsJSON = new JSONObject(); 
		listOfTransactions = new JSONArray();
		String fileName = "Transactions " + DateUtils.getCurrentDateTime() + Properties.JSON_FILE_EXTENSION;
        File logFile = new File(fileName);
        try {
			writer = new BufferedWriter(new FileWriter(logFile));
		} catch (IOException e) {
			e.printStackTrace();
			try {
				writer.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void insertNewTransactionToDAL(String productName, int price) {       
        try {        
        	String dateTime = DateUtils.getCurrentDateTime();
        	JSONObject transactionJSON = new JSONObject();  
        	transactionJSON.put("ProductName", productName);
        	transactionJSON.put("Price", price);
        	transactionJSON.put("DateAndTime", dateTime);
        	listOfTransactions.add(transactionJSON);  
        } catch (Exception e) {
            e.printStackTrace();
            try {
				writer.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        } 
    }

	@SuppressWarnings("unchecked")
	@Override
	public void closeConnection() {
		try {
			trasactionsJSON.put("Transactions", listOfTransactions);
			writer.write(trasactionsJSON.toJSONString());
			writer.flush(); 
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isConnected() {
		return true;
	}

	@Override
	public String getConnectionType() {
		return Properties.JSON_FILE_CONNECTION_TYPE;
	}

}
