package DAL;

import java.io.File;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import BL.Properties;

public class XMLFile implements DALConnection {
	private File logFile;
	private Document xmlDocument;
	private Element rootTransactionsElement;
	
	@Override
	public void connectToDAL() {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			xmlDocument = dBuilder.newDocument();
			// root element
			rootTransactionsElement = xmlDocument.createElement("transactions");
			xmlDocument.appendChild(rootTransactionsElement);
		} catch (ParserConfigurationException e2) {
			e2.printStackTrace();
		}
		
		String fileName = "Transactions " + DateUtils.getCurrentDateTime() + Properties.XML_FILE_EXTENSION;
        logFile = new File(fileName);
	}
	
	public void insertNewTransactionToDAL(String productName, int price) {       
        try {        
        	String dateTime = DateUtils.getCurrentDateTime();
        	Element product = xmlDocument.createElement("product");
        	
            Attr attrPrice = xmlDocument.createAttribute("price");
            attrPrice.setValue(price + "");
            product.setAttributeNode(attrPrice);
            
            Attr attrDateTime = xmlDocument.createAttribute("date_time");
            attrDateTime.setValue(dateTime);
            product.setAttributeNode(attrDateTime);
            
            product.appendChild(xmlDocument.createTextNode(productName));
            rootTransactionsElement.appendChild(product);
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }

	@Override
	public void closeConnection() {
		try {
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer;
			transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(xmlDocument);
			StreamResult result = new StreamResult(logFile);
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isConnected() {
		return true;
	}

	@Override
	public String getConnectionType() {
		return Properties.XML_FILE_CONNECTION_TYPE;
	}
}
