package DAL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import BL.Properties;

public class JDBCConnection implements DALConnection {
	private static Connection connection;
	// JDBC driver name and database URL
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost/supermarket";
	// Database credentials
	private static final String USER = "root";
	private static final String PASS = "";

	public void connectToDAL() {
	    connection = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			System.out.println("Error: unable to load driver class!!!");
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void insertNewTransactionToDAL(String productName, int price) {
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
			String sql = "INSERT INTO transaction(product_name, product_price, purchase_date, purchase_time) "
					+ "VALUES ('" + productName + "', '"  + price + "', " + "sysdate(), sysdate())";
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException se) {
			se.printStackTrace();			
			try {
				stmt.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	} // insertNewTransactionToDB
	
	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isConnected() {
		return connection != null;
	}
	
	public String getConnectionType() {
		return Properties.JDBC_CONNECTION_TYPE;
	}
}