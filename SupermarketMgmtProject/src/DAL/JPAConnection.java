package DAL;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import BL.Properties;

public class JPAConnection implements DALConnection{
	
	private static final String	PERSISTENCE_UNIT_NAME = "supermarketPU";

	@Override
	public void connectToDAL() {
		// connection to DB through JPA is done by configuration file supermarketPU
	}
	
	@Override
	public synchronized void insertNewTransactionToDAL(String productName, int price) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.createNativeQuery("INSERT INTO transaction(product_name, product_price, purchase_date, purchase_time) "
				+ "VALUES ('" + productName + "', '"  + price + "', " + "sysdate(), sysdate())").executeUpdate();
		et.commit();
        em.close();
	}
	
	@Override
	public void closeConnection() {	
		// connection closing through JPA is done by configuration file supermarketPU
	}

	@Override
	public boolean isConnected() {
		return true;
	}
	
	@Override
	public String getConnectionType() {
		return Properties.JPA_CONNECTION_TYPE;
	}
}
