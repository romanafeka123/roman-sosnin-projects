package DAL;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import BL.Properties;

public class TextFile implements DALConnection {
	private BufferedWriter writer;
	
	@Override
	public void connectToDAL() {
		writer = null;
		String fileName = "Transactions " + DateUtils.getCurrentDateTime() + Properties.TEXT_FILE_EXTENSION;
        File logFile = new File(fileName);
        try {
			writer = new BufferedWriter(new FileWriter(logFile));
		} catch (IOException e) {
			e.printStackTrace();
			try {
				writer.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public void insertNewTransactionToDAL(String productName, int price) {       
        try {        
        	String dateTime = DateUtils.getCurrentDateTime();
            writer.write("Product name: " + productName + ", price: " + price + ", date & time: " + dateTime);
            writer.newLine();
        } catch (Exception e) {
            e.printStackTrace();
            try {
				writer.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        } 
    }

	@Override
	public void closeConnection() {
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isConnected() {
		return true;
	}

	@Override
	public String getConnectionType() {
		return Properties.TEXT_FILE_CONNECTION_TYPE;
	}

}
