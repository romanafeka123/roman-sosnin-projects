package DAL;

public interface DALConnection {
	public void connectToDAL();
	public void insertNewTransactionToDAL(String productName, int price);
	public void closeConnection();	
	public boolean isConnected();
	public String getConnectionType();
}
