package DAL;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	
	public static String getCurrentDateTime() {
		Date curDate = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy MM dd - ");
		String dateToStr = simpleDateFormat.format(curDate);
		simpleDateFormat = new SimpleDateFormat("hh.mm.ss");
		dateToStr += simpleDateFormat.format(curDate);
		return dateToStr;
	}
}
