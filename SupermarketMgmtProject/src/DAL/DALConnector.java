package DAL;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ProductFactory.Product;

// Dependency Injection & Proxy Design Patterns implementation
public class DALConnector implements DALConnection {
	private static DALConnection connection;

	@SuppressWarnings("resource")
	public DALConnector() {
		// create the variable from the configuration
		ApplicationContext theContext = new ClassPathXmlApplicationContext("DBconnection.xml");

		// load the variable
		connection = (DALConnection) theContext.getBean("ConnectionType");
		connectToDAL();
	} 

	public DALConnection getDALConnection() {
		return connection;
	}
	
	public void connectToDAL() {
		connection.connectToDAL();
	}
	
	public void insertNewTransactionToDAL(Product product) {
		connection.insertNewTransactionToDAL(product.toString(), product.getPrice());
	}
	
	public void closeConnection() {
		connection.closeConnection();
	}
	
	public boolean isConnected() {
		return connection.isConnected();
	}

	@Override
	public void insertNewTransactionToDAL(String productName, int price) {
		connection.insertNewTransactionToDAL(productName, price);
	}

	@Override
	public String getConnectionType() {
		return connection.getConnectionType();
	}
}
