package DAL;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Transaction
 *
 */
@Entity

public class Transaction implements Serializable {

	@Id
	private int product_name;
	private int product_price;
	private int purchase_date;
	
	private static final long serialVersionUID = 1L;

	public Transaction() {
		super();
	}   
	public int getProduct_name() {
		return this.product_name;
	}

	public void setProduct_name(int product_name) {
		this.product_name = product_name;
	}
	public int getProduct_price() {
		return product_price;
	}
	public void setProduct_price(int product_price) {
		this.product_price = product_price;
	}
	public int getPurchase_date() {
		return purchase_date;
	}
	public void setPurchase_date(int purchase_date) {
		this.purchase_date = purchase_date;
	}
}
