package GUI;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JOptionPane;

import BL.Properties;
import BL.Supermarket;
import BuildCartState.BuildCartState;
import DAL.DALConnector;
import Listeners.GUIEventsListener;
import MVCController.SupermarketController;
import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainFrame extends Application {
	// UI Elements
	private Group root;
	private Scene scene;
	private Stage stage;
	private VBox mainScreenVbox;
	private HBox queuesHbox;
	private ScrollPane queuesScrollPane;
	private Button addQueueBtn, removeQueueBtn;
	private SwitchButton switchStateButton;
	private CheckBox loggerONOFFchkBox;
	private boolean buildCartAutomatically = true;
	private List<QueueView> queueViewsList;
	private Vector<GUIEventsListener> listeners;
	private int numOfCustomersAwaitingToGetService = 0;
	
	@SuppressWarnings("unused")
	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		initializeUIElements();
		stage.setScene(scene);
		stage.show();
		setListeners();
		SupermarketController controller = new SupermarketController(Supermarket.getInstance(), this, new DALConnector());							
	}
	
	private void initializeUIElements() {
		listeners = new Vector<GUIEventsListener>();
		stage.setTitle(Properties.APPLICATION_TITLE_TXT);
		root = new Group();
		scene = new Scene(root, Properties.MAIN_SCREEN_WIDTH, Properties.MAIN_SCREEN_HEIGHT);
		scene.getStylesheets().add(MainFrame.class.getResource("TheStyle.css").toExternalForm());
		queuesHbox = new HBox();
		queuesHbox.setMinSize(Properties.MAIN_SCREEN_WIDTH, Properties.QUEUES_HBOX_PANE_HEIGHT);
		queuesHbox.getStyleClass().add("vbox");
		
		addQueueBtn = new Button(Properties.ADD_QUEUE_BTN_TXT);
		addQueueBtn.getStyleClass().add("greenBtn");
		removeQueueBtn = new Button(Properties.REMOVE_QUEUE_BTN_TXT);
		removeQueueBtn.getStyleClass().add("greenBtn");
		switchStateButton = new SwitchButton(this);
		switchStateButton.getStyleClass().add("greenBtn");
		
		HBox buttonsVBox = new HBox();
		buttonsVBox.setAlignment(Pos.CENTER);
		buttonsVBox.getChildren().add(addQueueBtn);	
		buttonsVBox.getChildren().add(switchStateButton);
		buttonsVBox.getChildren().add(removeQueueBtn);
		
		queuesScrollPane = new ScrollPane();
		ImageView imageView = new ImageView();
		Image image = new Image(MainFrame.class.getResourceAsStream(Properties.SUPERMARKET_IMG_SRC));
		imageView.setImage(image);
		
		loggerONOFFchkBox = new CheckBox(Properties.ENABLE_DISABLE_CHK_BOX_TXT);
		loggerONOFFchkBox.getStyleClass().add("chkBox");
		loggerONOFFchkBox.setSelected(true);
		
		mainScreenVbox = new VBox(3);
		mainScreenVbox.setAlignment(Pos.CENTER);
		mainScreenVbox.getChildren().add(loggerONOFFchkBox);
		mainScreenVbox.getChildren().add(imageView);
		mainScreenVbox.getChildren().add(buttonsVBox);
		mainScreenVbox.getChildren().add(queuesScrollPane);
		mainScreenVbox.getStyleClass().add("vbox");
		
		queuesScrollPane.setContent(queuesHbox);
		queuesScrollPane.setHbarPolicy(ScrollBarPolicy.ALWAYS);
		queuesScrollPane.setMaxWidth(950);
		queuesScrollPane.setMinSize(Properties.MAIN_SCREEN_WIDTH, Properties.SCREEN_PANE_HEIGHT);
		
		root.getChildren().addAll(mainScreenVbox);
		queueViewsList = new LinkedList<QueueView>();
	}
	
	private void addQueueView() {
		QueueView queueView = new QueueView(this);
		queuesHbox.getChildren().add(queueView);
		queueViewsList.add(queueView);
	}
	
	private void removeQueueView() {
		if (queueViewsList.size() > 0) {
			queuesHbox.getChildren().remove(queueViewsList.size()-1);
			queueViewsList.remove(queueViewsList.size()-1);
			QueueView.decreaseQueueNumGenerator();
		}
	}
	
	public void registerListener(GUIEventsListener listener) {
		listeners.add(listener);
	}
	
	private void setListeners() {
		addQueueBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				addQueueView();
				fireAddQueueToSupermarketEvent(queueViewsList.size());	  
			}
		});
		removeQueueBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (numOfCustomersAwaitingToGetService == 0) {
					if (queueViewsList.size() > 0) {
						fireRemoveQueueFromSupermarketEvent();
					}
					removeQueueView();
				}
				else {
					JFXPanel fxPanel = new JFXPanel();
					JOptionPane.showMessageDialog(fxPanel, Properties.REMOVE_QUEUE_ERR,
		                    "Error",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		stage.setOnCloseRequest(event -> {
			fireSupermarketClosingEvent();
		});
		loggerONOFFchkBox.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (loggerONOFFchkBox.isSelected()) {
					Properties.enableDisableLogger(true);
				}
				else {
					Properties.enableDisableLogger(false);
				}
			}
		});	
	}  // setListeners
	
	public void fireSupermarketClosingEvent() {
		for (GUIEventsListener listener : listeners) {
			listener.fireSupermarketClosingEvent();
		}
	}

	public void fireBuildCartStateChangedEvent(BuildCartState buildCartState) {
		for (GUIEventsListener listener : listeners) {
			listener.fireBuildCartStateChangedEvent(buildCartState);
		}
	}
	
	public void fireAddCustomerToQueueEvent(Map<String, String> productsMap, int queueNum) {
		numOfCustomersAwaitingToGetService++;
		for (GUIEventsListener listener : listeners) {
			listener.fireAddCustomerToQueueEvent(productsMap, queueNum);
		}
	}
	
	public void fireAddQueueToSupermarketEvent(int queueNum) {
		for (GUIEventsListener listener : listeners) {
			listener.fireAddQueueToSupermarketEvent(queueNum);
		}
	}
	
	public void fireRemoveQueueFromSupermarketEvent() {
		for (GUIEventsListener listener : listeners) {
			listener.fireRemoveQueueFromSupermarketEvent();
		}
	}
	
	public void customerLeavesEventFired(int queueNum) {
		queueViewsList.get(queueNum-1).removeCustomerImgView();
		numOfCustomersAwaitingToGetService--;
	}
	
	public static void main(String[] args) {
		launch();	
	}

	public boolean isBuildCartAutomatically() {
		return buildCartAutomatically;
	}

	public void setBuildCartAutomatically(boolean buildCartAutomatically) {
		this.buildCartAutomatically = buildCartAutomatically;
	}
}

