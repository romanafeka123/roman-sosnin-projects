package GUI;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import BL.Properties;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class AddProductFXFrame {
	private JFrame containingFrame;
	private FlowPane flowPane;
	private Scene scene;
	private JFXPanel fxPanel;
	private QueueView queueView;
	private VBox mainVBox;
	private Button buyBtn;
	private List<RadioButton> productsRadBtnList;
	private List<RadioButton> chosenProductsRadBtnList;
	private Map<String, String> chosenProductsMap;
	
	private ToggleGroup milkGroup, chocolateGroup, icecreamGroup;
	private RadioButton tnuvaMilkRadBtn, taraMilkRadBtn, yotvataMilkRadBtn;
	private RadioButton eliteChocolateRadBtn, milkaChocolateRadBtn;
	private RadioButton magnumIcecreamRadBtn, benJerrysIcecreamRadBtn;
	
	// This method is invoked on the JavaFX thread
	public AddProductFXFrame(JFXPanel fxPanel, QueueView queueView, JFrame containingFrame) {
		this.fxPanel = fxPanel;
		this.containingFrame = containingFrame;
		this.queueView = queueView;
		productsRadBtnList = new LinkedList<RadioButton>();
		chosenProductsRadBtnList = new LinkedList<RadioButton>();
		chosenProductsMap = new HashMap<String, String>();
		initializeUIElements();
		buildImageViewsRadBtnList();
		setListenerOnBtn();
    }
	
	private void initializeUIElements() {
		mainVBox = new VBox();
		HBox queuesHbox = new HBox();
		queuesHbox.setMinSize(Properties.MAIN_SCREEN_WIDTH, Properties.QUEUES_HBOX_PANE_HEIGHT);
		queuesHbox.getStyleClass().add("vbox");
		
		buyBtn = new Button("Buy");
		buyBtn.getStyleClass().add("greenBtn");
		HBox productsHBox = new HBox(3);
		VBox milkVBox = new VBox();
		VBox chocolateVBox = new VBox();
		VBox icecreamVBox = new VBox();
		milkVBox.setAlignment(Pos.CENTER);
		chocolateVBox.setAlignment(Pos.CENTER);
		icecreamVBox.setAlignment(Pos.CENTER);
		
		milkGroup = new ToggleGroup();
		chocolateGroup = new ToggleGroup();
		icecreamGroup = new ToggleGroup();
		
		tnuvaMilkRadBtn = new RadioButton();
		taraMilkRadBtn = new RadioButton();
		yotvataMilkRadBtn = new RadioButton();
		tnuvaMilkRadBtn.setToggleGroup(milkGroup);
		taraMilkRadBtn.setToggleGroup(milkGroup);
		yotvataMilkRadBtn.setToggleGroup(milkGroup);
		
		tnuvaMilkRadBtn.setGraphic(getImageViewByURL(Properties.MILK_TNUVA_IMG_SRC));
		taraMilkRadBtn.setGraphic(getImageViewByURL(Properties.MILK_TARA_IMG_SRC));
		yotvataMilkRadBtn.setGraphic(getImageViewByURL(Properties.MILK_YOTVATA_IMG_SRC));
		milkVBox.getChildren().addAll(tnuvaMilkRadBtn, taraMilkRadBtn, yotvataMilkRadBtn);	
		
		eliteChocolateRadBtn = new RadioButton();
		milkaChocolateRadBtn = new RadioButton();
		eliteChocolateRadBtn.setToggleGroup(chocolateGroup);
		milkaChocolateRadBtn.setToggleGroup(chocolateGroup);
		eliteChocolateRadBtn.setGraphic(getImageViewByURL(Properties.CHOCOLATE_ELITE_IMG_SRC));
		milkaChocolateRadBtn.setGraphic(getImageViewByURL(Properties.CHOCOLATE_MILKA_IMG_SRC));
		chocolateVBox.getChildren().addAll(eliteChocolateRadBtn, milkaChocolateRadBtn);
		
		magnumIcecreamRadBtn = new RadioButton();
		benJerrysIcecreamRadBtn = new RadioButton();
		magnumIcecreamRadBtn.setToggleGroup(icecreamGroup);
		benJerrysIcecreamRadBtn.setToggleGroup(icecreamGroup);
		magnumIcecreamRadBtn.setGraphic(getImageViewByURL(Properties.ICECREAM_MAGNUM_IMG_SRC));
		benJerrysIcecreamRadBtn.setGraphic(getImageViewByURL(Properties.ICECREAM_BEN_JERRYS_IMG_SRC));
		icecreamVBox.getChildren().addAll(magnumIcecreamRadBtn, benJerrysIcecreamRadBtn);	
		
		productsHBox.getChildren().addAll(chocolateVBox, milkVBox, icecreamVBox);
		ImageView productsImageView = getImageViewByURL(Properties.CART_IMG_SRC);
		
		mainVBox.getChildren().addAll(productsImageView, productsHBox, buyBtn);	
		mainVBox.setAlignment(Pos.TOP_CENTER);
		mainVBox.getStyleClass().add("vbox");
		
		flowPane  = new FlowPane(); 
    	flowPane.setAlignment(Pos.CENTER);   	
    	flowPane.getChildren().add(mainVBox);
    	
    	scene = new Scene(flowPane, 400, 400);
    	scene.getStylesheets().add(MainFrame.class.getResource("TheStyle.css").toExternalForm());
    	fxPanel.setScene(scene);
	}
	
	private ImageView getImageViewByURL(String imageViewSrc) {
		ImageView imageView = new ImageView();
		Image image = new Image(MainFrame.class.getResourceAsStream(imageViewSrc));
		imageView.setImage(image);
		return imageView;
	}
	
	private void setListenerOnBtn() {
		for (int i = 0; i < productsRadBtnList.size(); i++) {
			final RadioButton productRadBtn = productsRadBtnList.get(i);
			productRadBtn.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					if (chosenProductsRadBtnList.contains(productRadBtn)) {
						chosenProductsRadBtnList.remove(productRadBtn);
					}
					else {
						chosenProductsRadBtnList.add(productRadBtn);
					}
				}
			});
		}
		
		buyBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (chosenProductsRadBtnList.size() == 0) {
					JOptionPane.showMessageDialog(fxPanel,"Choose at least 1 product",
		                    "Error",JOptionPane.ERROR_MESSAGE);
				}
				else {
					collectChosenProductsIntoCart();
					queueView.addCustomer(chosenProductsMap);
					containingFrame.dispose();
				}
			}
		});
	}
	
	private void buildImageViewsRadBtnList() {
		productsRadBtnList.add(tnuvaMilkRadBtn);
		productsRadBtnList.add(taraMilkRadBtn);
		productsRadBtnList.add(yotvataMilkRadBtn);
		productsRadBtnList.add(eliteChocolateRadBtn);
		productsRadBtnList.add(milkaChocolateRadBtn);
		productsRadBtnList.add(magnumIcecreamRadBtn);
		productsRadBtnList.add(benJerrysIcecreamRadBtn);
	}
	
	private void collectChosenProductsIntoCart() {
		for (int i = 0; i < chosenProductsRadBtnList.size(); i++) {
			if (chosenProductsRadBtnList.get(i) == tnuvaMilkRadBtn) {
				chosenProductsMap.put(Properties.MILK_FACTORY_NAME, Properties.MILK_TNUVA_NAME);
			}
			else if (chosenProductsRadBtnList.get(i) == taraMilkRadBtn) {
				chosenProductsMap.put(Properties.MILK_FACTORY_NAME, Properties.MILK_TARA_NAME);
			}
			else if (chosenProductsRadBtnList.get(i) == yotvataMilkRadBtn) {
				chosenProductsMap.put(Properties.MILK_FACTORY_NAME, Properties.MILK_YOTVATA_NAME);
			}
			else if (chosenProductsRadBtnList.get(i) == eliteChocolateRadBtn) {
				chosenProductsMap.put(Properties.CHOCOLATE_FACTORY_NAME, Properties.CHOCOLATE_ELITE_NAME);
			}
			else if (chosenProductsRadBtnList.get(i) == milkaChocolateRadBtn) {
				chosenProductsMap.put(Properties.CHOCOLATE_FACTORY_NAME, Properties.CHOCOLATE_MILKA_NAME);
			}
			else if (chosenProductsRadBtnList.get(i) == magnumIcecreamRadBtn) {
				chosenProductsMap.put(Properties.ICECREAM_FACTORY_NAME, Properties.ICECREAM_MAGNUM_NAME);
			}
			else if (chosenProductsRadBtnList.get(i) == benJerrysIcecreamRadBtn) {
				chosenProductsMap.put(Properties.ICECREAM_FACTORY_NAME, Properties.ICECREAM_BEN_JERRYS_NAME);
			}
		}
	}
}
