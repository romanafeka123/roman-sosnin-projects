package GUI;

import BL.Properties;
import BuildCartState.BuildCartAutomaticallyState;
import BuildCartState.BuildCartManuallyState;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;

public class SwitchButton extends Label {
	@SuppressWarnings("unused")
	private MainFrame mainFrame;
	private SimpleBooleanProperty switchedOn = new SimpleBooleanProperty(true);
	private Button switchBtn;

	public SwitchButton(final MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		switchBtn = new Button("Change");
		switchBtn.setPrefWidth(110);
		switchBtn.getStyleClass().add("redBtn");
		switchBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				switchedOn.set(!switchedOn.get());
			}
		});

		setGraphic(switchBtn);
		switchedOn.addListener(new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> ov,
					Boolean t, Boolean t1) {
				if (t1) {
					setText(Properties.SWITCH_BTN_ON_TXT);
					setStyle("-fx-background-color: green;-fx-text-fill:white;");
					setContentDisplay(ContentDisplay.RIGHT);
					switchBtn.getStyleClass().clear();
					switchBtn.getStyleClass().add("greenBtn");
					mainFrame.fireBuildCartStateChangedEvent(new BuildCartManuallyState());
					mainFrame.setBuildCartAutomatically(false);
				} else {
					setText(Properties.SWITCH_BTN_OFF_TXT);
					setStyle("-fx-background-color: red;-fx-text-fill:black;");
					setContentDisplay(ContentDisplay.LEFT);
					switchBtn.getStyleClass().clear();
					switchBtn.getStyleClass().add("redBtn");
					mainFrame.fireBuildCartStateChangedEvent(new BuildCartAutomaticallyState());
					mainFrame.setBuildCartAutomatically(true);
				}
			}
		});
		switchedOn.set(false);
	}

	public SimpleBooleanProperty switchOnProperty() {
		return switchedOn;
	}
}
