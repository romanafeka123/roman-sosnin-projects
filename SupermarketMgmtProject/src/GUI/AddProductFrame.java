package GUI;

import BL.Properties;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class AddProductFrame extends VBox{
	private Button buyBtn;
	
	public AddProductFrame(MainFrame mainFrame) {
		initializeUIElements();
	}
	
	private void initializeUIElements() {
		HBox queuesHbox = new HBox();
		queuesHbox.setMinSize(Properties.MAIN_SCREEN_WIDTH, Properties.QUEUES_HBOX_PANE_HEIGHT);
		queuesHbox.getStyleClass().add("vbox");
		
		buyBtn = new Button("Buy");
		buyBtn.getStyleClass().add("greenBtn");
		HBox productsHBox = new HBox(3);
		VBox milkVBox = new VBox();
		VBox chocolateVBox = new VBox();
		VBox icecreamVBox = new VBox();
		milkVBox.setAlignment(Pos.CENTER);
		chocolateVBox.setAlignment(Pos.CENTER);
		icecreamVBox.setAlignment(Pos.CENTER);
		
		RadioButton milk1ImageViewRadBtn = new RadioButton();
		RadioButton milk2ImageViewRadBtn = new RadioButton();
		RadioButton milk3ImageViewRadBtn = new RadioButton();
		milk1ImageViewRadBtn.setGraphic(getImageViewByURL(Properties.MILK_TNUVA_IMG_SRC));
		milk2ImageViewRadBtn.setGraphic(getImageViewByURL(Properties.MILK_TARA_IMG_SRC));
		milk3ImageViewRadBtn.setGraphic(getImageViewByURL(Properties.MILK_YOTVATA_IMG_SRC));
		milkVBox.getChildren().addAll(milk1ImageViewRadBtn, milk2ImageViewRadBtn, milk3ImageViewRadBtn);	
		
		RadioButton chocolate1ImageViewRadBtn = new RadioButton();
		RadioButton chocolate2ImageViewRadBtn = new RadioButton();
		chocolate1ImageViewRadBtn.setGraphic(getImageViewByURL(Properties.CHOCOLATE_MILKA_IMG_SRC));
		chocolate2ImageViewRadBtn.setGraphic(getImageViewByURL(Properties.CHOCOLATE_ELITE_IMG_SRC));
		chocolateVBox.getChildren().addAll(chocolate1ImageViewRadBtn, chocolate2ImageViewRadBtn);
		
		RadioButton icecream1ImageViewRadBtn = new RadioButton();
		RadioButton icecream2ImageViewRadBtn = new RadioButton();
		icecream1ImageViewRadBtn.setGraphic(getImageViewByURL(Properties.ICECREAM_BEN_JERRYS_IMG_SRC));
		icecream2ImageViewRadBtn.setGraphic(getImageViewByURL(Properties.ICECREAM_MAGNUM_IMG_SRC));
		icecreamVBox.getChildren().addAll(icecream1ImageViewRadBtn, icecream2ImageViewRadBtn);	
		
		productsHBox.getChildren().addAll(chocolateVBox, milkVBox, icecreamVBox);
		ImageView productsImageView = getImageViewByURL(Properties.CART_IMG_SRC);
		
		this.getChildren().addAll(productsImageView, productsHBox, buyBtn);	
		this.setAlignment(Pos.TOP_CENTER);
		this.getStyleClass().add("vbox");
	}
	
	private ImageView getImageViewByURL(String imageViewSrc) {
		ImageView imageView = new ImageView();
		Image image = new Image(MainFrame.class.getResourceAsStream(imageViewSrc));
		imageView.setImage(image);
		return imageView;
	}
}
