package GUI;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.JFrame;

import BL.Properties;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class QueueView extends VBox {
	private MainFrame mainFrame;
	private ScrollPane scrPane;
	private VBox vboxInScrPane;
	private Label headerLbl;
	private Label dashLbl;
	private Button addCustomerBtn;
	private List<ImageView> customersImgViewsList;
	private QueueView thisQueueView = this;
	
	private int queueNum;
	public static int queueNumGenerator = 1;
	private Random random = new Random();

	public QueueView(MainFrame mainFrame) {
		queueNum = queueNumGenerator++;
		this.mainFrame = mainFrame;
		initializeUIElements();
	}
	
	private void initializeUIElements() {
		customersImgViewsList = new LinkedList<ImageView>();
		scrPane = new ScrollPane();
		scrPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		scrPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrPane.setMinSize(Properties.QUEUE_PANE_WIDTH, Properties.QUEUE_PANE_HEIGHT);
		scrPane.setMaxSize(Properties.QUEUE_PANE_WIDTH, Properties.QUEUE_PANE_HEIGHT);
		scrPane.setId("pane");
		
		headerLbl = new Label(Properties.QUEUE_LBL_TXT + queueNum);
		headerLbl.setId("header-label");
		dashLbl = new Label("--------------------------");
		dashLbl.setId("dash-label");
		
		addCustomerBtn = new Button(Properties.ADD_CUSTOMER_BTN_TXT);
		addCustomerBtn.getStyleClass().add("greenBtn");
		setListeners();
		
		ImageView cashierImageView = new ImageView();
		Image image = new Image(QueueView.class.getResourceAsStream(Properties.CASHIER_IMG_SRC));
		cashierImageView.setImage(image);
	
		this.getChildren().addAll(scrPane);	
		this.setAlignment(Pos.TOP_CENTER);
		this.setMinWidth(150);

		vboxInScrPane = new VBox();
		vboxInScrPane.setAlignment(Pos.CENTER);
		vboxInScrPane.getChildren().addAll(headerLbl, addCustomerBtn, dashLbl, cashierImageView);
		scrPane.setContent(vboxInScrPane);
	}
	
	private void addCustomerImgView() {
		ImageView imv = new ImageView();
		Image image = new Image(QueueView.class.getResourceAsStream(getRandomizedCustomertImgSrc()));
		imv.setImage(image);
		customersImgViewsList.add(imv);
		vboxInScrPane.getChildren().add(imv);	
	}
	
	public void removeCustomerImgView() {
		if (customersImgViewsList.size() > 0) {
			Platform.runLater(new Runnable() {
				public void run() {
					vboxInScrPane.getChildren().remove(customersImgViewsList.get(0));
					customersImgViewsList.remove(0);
				}
			});
		}
	}
	
	public static void decreaseQueueNumGenerator(){
		queueNumGenerator--;
	}
	
	private void setListeners() {
		addCustomerBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (!mainFrame.isBuildCartAutomatically()) {	
					//final AddProductFrame addProductFrame = new AddProductFrame(mainFrame);
					final JFrame frame = new JFrame("Add Product");
			        final JFXPanel fxPanel = new JFXPanel();
			        frame.add(fxPanel);
			        frame.setSize(430, 570);
			        frame.setLocation(Properties.ADD_PRODUCT_FRAME_LOCATION_X, Properties.ADD_PRODUCT_FRAME_LOCATION_Y);
			        frame.setVisible(true);		  			 
					Platform.runLater(new Runnable() {
						@SuppressWarnings("unused")
						public void run() {
							AddProductFXFrame fxFrame = new AddProductFXFrame(fxPanel, thisQueueView, frame);
						}
					});
				}
				else {
					addCustomer(null);
				}
			}
		});
	}
	
	public void addCustomer(Map<String, String> productsMap) {
		addCustomerImgView();
		mainFrame.fireAddCustomerToQueueEvent(productsMap, queueNum);
	}
	
	private String getRandomizedCustomertImgSrc() {
		int customerRandomizedImgNum = random.nextInt(3) + 1;
		if (customerRandomizedImgNum == 1) {
			return Properties.CUSTOMER_1_IMG_SRC;
		}
		else if (customerRandomizedImgNum == 2) {
			return Properties.CUSTOMER_2_IMG_SRC;
		}
		return Properties.CUSTOMER_3_IMG_SRC;
	}
}
