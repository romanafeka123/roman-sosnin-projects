package BuildCartState;

import java.util.Map;

import BL.Cart;

//State Design Pattern implementation (switch to/from BuildCartManually State/BuildCartAutomatically State)
public abstract class BuildCartState {
	public abstract Cart buildCart(Map<String, String> productsMap);
}
