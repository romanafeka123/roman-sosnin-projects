package BuildCartState;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import BL.Cart;
import BL.Properties;

public class BuildCartAutomaticallyState extends BuildCartState {
	private Random randomizeProducts;
	
	public BuildCartAutomaticallyState() {
		randomizeProducts = new Random();
	}

	@Override
	public Cart buildCart(Map<String, String> productsMap) {
		return buildCartRandomly();
	}
	
	private Cart buildCartRandomly() {
		Map<String,String> productsMap = new HashMap<String,String>();
		// Randomizing Milk Manufacturer
		int randomizedMilkManufacturer = randomizeProducts.nextInt(Properties.NUM_OF_MILK_MANUFACTURERS);
		if (randomizedMilkManufacturer == 0) {
			productsMap.put(Properties.MILK_FACTORY_NAME, Properties.MILK_TNUVA_NAME);
		}
		else if (randomizedMilkManufacturer == 1) {
			productsMap.put(Properties.MILK_FACTORY_NAME, Properties.MILK_TARA_NAME);
		}
		else {
			productsMap.put(Properties.MILK_FACTORY_NAME, Properties.MILK_YOTVATA_NAME);
		}
		
		// Randomizing Chocolate Manufacturer
		int randomizedChocolateManufacturer = randomizeProducts.nextInt(Properties.NUM_OF_CHOCOLATE_MANUFACTURERS);
		if (randomizedChocolateManufacturer == 0) {
			productsMap.put(Properties.CHOCOLATE_FACTORY_NAME, Properties.CHOCOLATE_MILKA_NAME);
		}
		else {
			productsMap.put(Properties.CHOCOLATE_FACTORY_NAME, Properties.CHOCOLATE_ELITE_NAME);
		}
		
		// Randomizing IceCream Manufacturer
		int randomizedIceCreamManufacturer = randomizeProducts.nextInt(Properties.NUM_OF_ICECREAM_MANUFACTURERS);
		if (randomizedIceCreamManufacturer == 0) {
			productsMap.put(Properties.ICECREAM_FACTORY_NAME, Properties.ICECREAM_MAGNUM_NAME);
		} else {
			productsMap.put(Properties.ICECREAM_FACTORY_NAME, Properties.ICECREAM_BEN_JERRYS_NAME);
		}
		Cart cart = new Cart();
		cart.buildCart(productsMap);
		return cart;
	}
}
