package BuildCartState;

import java.util.Map;

import BL.Cart;

public class BuildCartManuallyState extends BuildCartState {

	@Override
	public Cart buildCart(Map<String, String> productsMap) {
		Cart cart = new Cart();
		cart.buildCart(productsMap);
		return cart;
	}
}
