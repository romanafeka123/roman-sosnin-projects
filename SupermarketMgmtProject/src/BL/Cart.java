package BL;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ProductFactory.Product;
import ProductFactory.ProductBuilder;

// Iterator Design Pattern implementation
public class Cart {
	private List<Product> products;
	
	public Cart() {
		products = new LinkedList<Product>();
	}
	
	public void buildCart(Map<String, String> productsMap) {
		Iterator<Entry<String, String>> entries = productsMap.entrySet().iterator();
		while (entries.hasNext()) {
		    Map.Entry<String, String> entry = entries.next();
		    String productFactoryName = (String)entry.getKey();
		    String productManufacturerName = (String)entry.getValue();
		    // Using Product Abstract Factory to create the product objects
		    Product product = ProductBuilder.buildProduct(productFactoryName, productManufacturerName);
		    products.add(product);
		}
		//System.out.println("Products: " + products);
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
}
