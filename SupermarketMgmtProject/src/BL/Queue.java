package BL;

import java.util.concurrent.ExecutorService;

public class Queue {
	private int queueNum;
	private Cashier cashier;
	
	public Queue(int queueNum) {
		this.queueNum = queueNum;
		cashier = new Cashier();
	}
	
	public void addCustomerToQueue(ExecutorService customersList, Cart cart) {
		Customer customer = new Customer(cashier, queueNum, cart);
		customersList.execute(customer);
	}

}
