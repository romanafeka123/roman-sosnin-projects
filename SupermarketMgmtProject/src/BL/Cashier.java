package BL;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

// Lock Design Pattern implementation
public class Cashier extends ReentrantLock {
	private static final long serialVersionUID = 1L;
	
	private Random random;
	
	public Cashier() {
		random = new Random();
	}
	
	public void takeCareOfCustomer() {
		lock();
		// randomizing a number between 2000 and 3000 (millisec to sleep)
		// timeToSleep = time to take care of the customer
		int timeToSleep = random.nextInt(1000) + 2000;
		try {
			Thread.sleep(timeToSleep);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		unlock();	
	}
	
	
}
