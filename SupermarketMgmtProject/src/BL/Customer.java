package BL;

public class Customer implements Runnable {
	private Cashier cashier;
	private int queueNum;
	private Cart cart;
	
	public Customer(Cashier cashier, int queueNum, Cart cart) {
		this.queueNum = queueNum;
		this.cashier = cashier;
		this.cart = cart;
	}
	
	public void run() {
		cashier.takeCareOfCustomer();
		Supermarket.getInstance().fireCustomerLeavesEvent(queueNum);
		Supermarket.getInstance().fireAddTransactionToDALEvent(cart.getProducts());
	}
	
}
