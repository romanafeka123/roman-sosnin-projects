package BL;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import BuildCartState.BuildCartAutomaticallyState;
import BuildCartState.BuildCartState;
import Listeners.BLEventsListener;
import ProductFactory.Product;

// Singleton and Thread Pool Design Patterns implementation
public class Supermarket {
	private static Supermarket supermarket;
	private List<Queue> queues;
	private Vector<BLEventsListener> listeners;
	private ExecutorService customersList;
	
	// Build Cart State
	private BuildCartState currentBuildCartState;
	
	private Supermarket() {
		queues = new LinkedList<Queue>();
		listeners = new Vector<BLEventsListener>();
		customersList = Executors.newFixedThreadPool(Properties.NUM_OF_THREADS_IN_THREADPOOL);
		currentBuildCartState = new BuildCartAutomaticallyState();
	}
	
	public static Supermarket getInstance() {
		if (supermarket == null) {
			supermarket = new Supermarket();
			//Properties.enableDisableLogger();
		}
		return supermarket;
	}
	
	public int getNumOfQueues() {
		return queues.size();
	}
	
	public void setBuildCartState(BuildCartState buildCartState) {
		this.currentBuildCartState = buildCartState;
	}
	
	public void registerListener(BLEventsListener listener) {
		listeners.add(listener);
	}
	
	public void fireCustomerLeavesEvent(int queueNum) {
		for (BLEventsListener listener : listeners) {
			listener.fireCustomerLeavesEvent(queueNum);
		}
	}
	
	public void fireAddTransactionToDALEvent(List<Product> products) {
		for (BLEventsListener listener : listeners) {
			listener.fireAddTransactionToDALEvent(products);
		}
	}
	
	// ------------------------------- Fired Events ------------------------------- \\
	
	public void addQueueToSupermarketEventFired(int queueNum) {
		queues.add(new Queue(queueNum));
	}
	
	public void removeQueueFromSupermarketEventFired() {
		queues.remove(queues.size()-1);
	}
	
	public void addCustomerToQueueEventFired(Map<String, String> productsMap, int queueNum) {
		Cart cart = currentBuildCartState.buildCart(productsMap);
		queues.get(queueNum-1).addCustomerToQueue(customersList, cart);
	}
	
	public void buildCartStateChangedEventFired(BuildCartState buildCartState) {
		currentBuildCartState = buildCartState;
	}
	
	public void supermarketClosingEventFired() {
		// do something on supermarket closing event...
	}

}
