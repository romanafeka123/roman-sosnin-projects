package BL;

import Aspects.LoggerAspect;

public class Properties {
	// Images sources
	public static final String CASHIER_IMG_SRC = "images/cashier.png";
	public static final String SUPERMARKET_IMG_SRC = "images/supermarketImg.JPG";
	public static final String CUSTOMER_1_IMG_SRC = "images/client1.png";
	public static final String CUSTOMER_2_IMG_SRC = "images/client2.jpg";
	public static final String CUSTOMER_3_IMG_SRC = "images/client3.png";
	
	public static final String CART_IMG_SRC = "images/cart.png";
	
	public static final String MILK_TNUVA_IMG_SRC = "images/tnuva.png";
	public static final String MILK_TARA_IMG_SRC = "images/tara.png";
	public static final String MILK_YOTVATA_IMG_SRC = "images/yotvata.png";
	public static final String CHOCOLATE_MILKA_IMG_SRC = "images/milka.png";
	public static final String CHOCOLATE_ELITE_IMG_SRC = "images/elite_chocolate.png";
	public static final String ICECREAM_BEN_JERRYS_IMG_SRC = "images/benjerrys_icecream.png";
	public static final String ICECREAM_MAGNUM_IMG_SRC = "images/magnum_icecream.png";
	
	// UI Elements sizes
	public static final int MAIN_SCREEN_WIDTH = 1050;
	public static final int MAIN_SCREEN_HEIGHT = 700;
	public static final int QUEUES_HBOX_PANE_HEIGHT = MAIN_SCREEN_HEIGHT - 140;
	public static final int SCREEN_PANE_HEIGHT = MAIN_SCREEN_HEIGHT - 130;
	public static final int QUEUE_PANE_HEIGHT = MAIN_SCREEN_HEIGHT - 170;
	public static final int QUEUE_PANE_WIDTH = 160;
	
	public static final int ADD_PRODUCT_FRAME_LOCATION_X = 400;
	public static final int ADD_PRODUCT_FRAME_LOCATION_Y = 100;
	
	// UI Elements texts
	public static final String APPLICATION_TITLE_TXT = "Supermarket";
	public static final String ADD_QUEUE_BTN_TXT = "Add Queue";
	public static final String REMOVE_QUEUE_BTN_TXT = "Remove Queue";
	public static final String QUEUE_LBL_TXT = "Queue #";
	public static final String ADD_CUSTOMER_BTN_TXT = "+ Customer";
	public static final String SWITCH_BTN_ON_TXT = "     Build Cart Manually     ";
	public static final String SWITCH_BTN_OFF_TXT = "Build Cart Automatically";
	public static final String REMOVE_QUEUE_ERR = "Some customers are getting service at the moment...";
	
	// BL elements
	public static final int NUM_OF_THREADS_IN_THREADPOOL = 10;
	
	// Product Factories names
	public static final String MILK_FACTORY_NAME = "MILK-FACTORY";
	public static final String CHOCOLATE_FACTORY_NAME = "CHOCOLATE-FACTORY";
	public static final String ICECREAM_FACTORY_NAME = "ICECREAM-FACTORY";
	
	// Products name
	public static final String MILK_TNUVA_NAME = "MILK-TNUVA";
	public static final String MILK_TARA_NAME = "MILK-TARA";
	public static final String MILK_YOTVATA_NAME = "MILK-YOTVATA";
	public static final String CHOCOLATE_ELITE_NAME = "CHOCOLATE-ELITE";
	public static final String CHOCOLATE_MILKA_NAME = "CHOCOLATE-MILKA";
	public static final String ICECREAM_MAGNUM_NAME = "ICECREAM-MAGNUM";
	public static final String ICECREAM_BEN_JERRYS_NAME = "ICECREAM-BEN-JERRYS";
	
	// Products prices
	public static final int MILK_TNUVA_PRICE = 10;
	public static final int MILK_TARA_PRICE = 15;
	public static final int MILK_YOTVATA_PRICE = 20;
	public static final int CHOCOLATE_ELITE_PRICE = 25;
	public static final int CHOCOLATE_MILKA_PRICE = 30;
	public static final int ICECREAM_MAGNUM_PRICE = 35;
	public static final int ICECREAM_BEN_JERRYS_PRICE = 40;
	
	// Amounts of manufacturers
	public static final int NUM_OF_MILK_MANUFACTURERS = 3;
	public static final int NUM_OF_CHOCOLATE_MANUFACTURERS = 2;
	public static final int NUM_OF_ICECREAM_MANUFACTURERS = 2;
	
	// file extension
	public static final String TEXT_FILE_EXTENSION = ".txt";
	public static final String JSON_FILE_EXTENSION = ".json";
	public static final String XML_FILE_EXTENSION = ".xml";
	
	// DAL connection types
	public static final String JDBC_CONNECTION_TYPE = "JDBC Connection";
	public static final String JPA_CONNECTION_TYPE = "JPA Connection";
	public static final String TEXT_FILE_CONNECTION_TYPE = "Text File Connection";
	public static final String JSON_FILE_CONNECTION_TYPE = "JSON File Connection";
	public static final String XML_FILE_CONNECTION_TYPE = "XML File Connection";
	
	// Enabling / Disabling logger
	public static final String ENABLE_DISABLE_CHK_BOX_TXT = "Enable Logger";
	public static void enableDisableLogger(boolean enableLogger) {
		LoggerAspect.logger.setUseParentHandlers(enableLogger);
	}
}
