package Listeners;

import java.util.Map;

import BuildCartState.BuildCartState;

public interface GUIEventsListener {
	public void fireAddCustomerToQueueEvent(Map<String, String> productsMap, int queueNum);
	public void fireAddQueueToSupermarketEvent(int queueNum);
	public void fireRemoveQueueFromSupermarketEvent();
	public void fireBuildCartStateChangedEvent(BuildCartState buildCartState);
	public void fireSupermarketClosingEvent();
}
