package Listeners;

import java.util.List;

import ProductFactory.Product;

public interface BLEventsListener {
	public void fireCustomerLeavesEvent(int queueNum);
	public void fireAddTransactionToDALEvent(List<Product> products);
}