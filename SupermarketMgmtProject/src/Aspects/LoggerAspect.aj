package Aspects;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import BL.Cart;
import BL.Supermarket;
import DAL.DALConnector;

// Visitor Design Pattern implementation
public aspect LoggerAspect {
	
	public static Logger logger = Logger.getLogger("supermarketLogger");
	
	private static final String SUPER_MARKET_OPENED_MSG = "Supermarket is open";
	private static final String NEW_PURCHASE_WAS_MADE_MSG = "Purchase was made: ";
	private static final String NEW_QUEUE_IS_AVAILABLE_MSG = "A new queue was opened";
	private static final String QUEUE_WAS_CLOSED_MSG = "A queue was closed";
	private static final String NUM_OF_QUEUES_MSG = "Number of queues at the moment is: ";
	private static final String SUPERMARKET_CLOSING_MSG = "Supermarket is closed";
	private static final String DAL_CONNECTION_UP_MSG = "Connected to DAL: ";
	private static final String DAL_CONNECTION_DOWN_MSG = "Disconnected from DAL: ";

	// Pointcut #1 buildCart method of Cart class is the join point
	pointcut buildCartLogging(Map<String, String> productsMap, Cart cart) : execution (public void buildCart(Map<String, String>)) && args(productsMap) && target(cart);
	
	after(Map<String, String> productsMap, Cart cart) : buildCartLogging(productsMap, cart) {		
		logger.log(Level.INFO, NEW_PURCHASE_WAS_MADE_MSG + cart.getProducts().toString());
	}
	
	// Pointcut #2 Supermarket constructor is the join point
	after() returning(Supermarket supermarket): call((Supermarket).new(..)) {
		logger.log(Level.INFO, SUPER_MARKET_OPENED_MSG);
	}
	
	// Pointcut #3 addQueueToSupermarketEventFired method in Supermarket class is the join point
	pointcut newQueueOpened(int queueNum, Supermarket supermarket) : execution (public void addQueueToSupermarketEventFired(int)) && args(queueNum) && target(supermarket);
	
	after(int queueNum, Supermarket supermarket) : newQueueOpened(queueNum, supermarket) {		
		logger.log(Level.INFO, NEW_QUEUE_IS_AVAILABLE_MSG + " " + NUM_OF_QUEUES_MSG + " " + supermarket.getNumOfQueues());
	}
	
	// Pointcut #4 removeQueueFromSupermarketEventFired method in Supermarket class is the join point
	pointcut queueClosed(Supermarket supermarket) : execution (public void removeQueueFromSupermarketEventFired()) && args() && target(supermarket);
	
	after(Supermarket supermarket) : queueClosed(supermarket) {		
		logger.log(Level.INFO, QUEUE_WAS_CLOSED_MSG + " " + NUM_OF_QUEUES_MSG + " " + supermarket.getNumOfQueues());
	}
	
	// Pointcut #5 supermarketClosingEventFired method in Supermarket class is the join point
	pointcut supermarketClosing() : execution (public void supermarketClosingEventFired()) && args();
	
	before() : supermarketClosing() {		
		logger.log(Level.INFO, SUPERMARKET_CLOSING_MSG);
	}
	
	// Pointcut #6 connectToDAL method in DALConnector class is the join point
	pointcut connectToDAL(DALConnector dalConnector) : execution (public void connectToDAL()) && args() && target(dalConnector);
	
	after(DALConnector dalConnector) : connectToDAL(dalConnector) {		
		logger.log(Level.INFO, DAL_CONNECTION_UP_MSG + dalConnector.getDALConnection().getConnectionType());
	}
	
	// Pointcut #7 closeConnection method in DALConnector class is the join point
	pointcut disconnectFromDAL(DALConnector dalConnector) : execution (public void closeConnection()) && args() && target(dalConnector);

	after(DALConnector dalConnector) : disconnectFromDAL(dalConnector) {
		logger.log(Level.INFO, DAL_CONNECTION_DOWN_MSG + dalConnector.getDALConnection().getConnectionType());
	}

}
