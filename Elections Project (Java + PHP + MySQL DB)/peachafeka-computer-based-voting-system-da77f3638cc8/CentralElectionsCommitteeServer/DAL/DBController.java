package DAL;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import BL.Voter;
import Messages.EscortValidationAnswer;
import Messages.ValidateBallotAreaIDAnswer;
import Messages.VoterRegistryMessage;
////////////////////////// GUY /////////////////////////////////
public class DBController {
	private static Connection con;
	// JDBC driver name and database URL
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost/elections";
	// Database credentials
	private static final String USER = "root";
	private static final String PASS = "";

	public static void connectToDB() {
	    con = null;
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("DB Connection established");
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException ex) {
			System.out.println("Error: unable to load driver class!");
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized static void addVoteToDB(int party_id, int ballot_area_id) {
		CallableStatement stmt = null;
		try {
			System.out.println("Writing to DB...................");
			String sql = "{call add_vote (?, ?)}";
			// calling the DB - defined stored procedure
			stmt = con.prepareCall(sql);
			// Setting the input parameters
			stmt.setInt(1, party_id); 
			stmt.setInt(2, ballot_area_id);
			// Executing the stored procedure
			stmt.execute();
		    stmt.close();		
		} catch (SQLException se) {
			try {
				stmt.close();
			} catch (SQLException e) {}
		} 
	}  // insertFuelTransactionIntoDB

	public synchronized static void removeVoterFromDB(int voter_id) {
		Statement stmt = null;
		try {
			System.out.println("Deleting ID: " + voter_id);
			stmt = con.createStatement();
			String sql = "DELETE FROM voter_registry WHERE id = " + voter_id;
			// executing the query
			stmt.executeUpdate(sql);
		    stmt.close();		
		} catch (SQLException se) {
			try {
				stmt.close();
			} catch (SQLException e) {}
		} 
	}  // removeVoterFromDB
	
		public synchronized static void updateTimesEscortedInDB(int escort_id, boolean isCitizen, String nonCitizenEscortWorkplace) {
		CallableStatement call_stmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		boolean alreadyInDB = false;
		try {
			stmt = con.createStatement();
			System.out.println("hahahah " + nonCitizenEscortWorkplace);
			if (!isCitizen) {
				sql = "select id from person";
				boolean alreadyInPersonTable = false;
				rs = stmt.executeQuery(sql);
				while(rs.next()){					
					int id  = rs.getInt("id");
					if (escort_id == id) {
						alreadyInPersonTable = true;
					}
				}
				if (!alreadyInPersonTable) {
					sql = "INSERT INTO person" +
						 " VALUES (" + escort_id + ",'" + nonCitizenEscortWorkplace + "', 'NotCitizen', 'NotCitizen')";
					stmt.executeUpdate(sql);
				}
			}
			
			sql = "select id from escort_registry";
			rs = stmt.executeQuery(sql);
			while(rs.next()){					
				int id  = rs.getInt("id");
				if (escort_id == id) {
					alreadyInDB = true;
				}
			}
			if (!alreadyInDB) {
				if (isCitizen) {
					sql = "INSERT INTO escort_registry" +
						 " VALUES (" + escort_id + ", 1, 0)";
				}
				else {
					sql = "INSERT INTO escort_registry" +
						 " VALUES (" + escort_id + ", 0, 0)";
				}
				stmt.executeUpdate(sql);
			}
			
			// calling the stored procedure
			sql = "{call update_times_escorted (?)}";
			call_stmt = con.prepareCall(sql);
			// Setting the input parameters
			call_stmt.setInt(1, escort_id); 
			// Executing the stored procedure
			call_stmt.execute();
			
			rs.close();
		    stmt.close();		
		} catch (SQLException se) {
			try {
				stmt.close();
				rs.close();
			} catch (SQLException e) {}
		} 
	}  // addEscortToDB

//////////////////////////ROTEM /////////////////////////////////
	public synchronized static VoterRegistryMessage getVoterRegistryFromDB(int ballot_area_id) {
		VoterRegistryMessage voters = new VoterRegistryMessage();
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		try {
			stmt = con.createStatement();		
			sql = "SELECT id, can_be_escorted FROM voter_registry" +
				 " WHERE ballot_area_id = " + ballot_area_id;
			rs = stmt.executeQuery(sql);
			while(rs.next()){					
				int id  = rs.getInt("id");
				boolean can_be_escorted  = rs.getBoolean("can_be_escorted");
				Voter voter = new Voter(id, can_be_escorted);
				voters.addVoterToRegistry(voter);
			}
			// executing the query
		    stmt.close();		
		    rs.close();
		} catch (SQLException se) {
			try {
				stmt.close();
				rs.close();
			} catch (SQLException e) {}
		} 
		return voters;
	}  // VoterRegistryMessage
	
	public synchronized static EscortValidationAnswer isEscortEligibleToEscort(int voter_id, int escort_id, String nonCitizenWorkPlace) {
		EscortValidationAnswer ans = new EscortValidationAnswer();
		ans.setEligibleToEscort(true);
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		try {
			stmt = con.createStatement();	
			
			sql = " SELECT can_be_escorted FROM voter_registry"+ " WHERE id = " + voter_id;
			rs = stmt.executeQuery(sql);
			rs.next();
			int canBeEscorted = rs.getInt("can_be_escorted");
			if (canBeEscorted == 0) {
				ans.setEligibleToEscort(false);
				stmt.close();
				rs.close();
				return ans;
			}
			
			sql =" SELECT times_escorted FROM escort_registry" +
				 " WHERE id = " + escort_id;
			rs = stmt.executeQuery(sql);
			while(rs.next()){		
				int times_escorted  = rs.getInt("times_escorted");
				if (times_escorted == 2) {				
					ans.setEligibleToEscort(false);
				}
				else {
					ans.setEligibleToEscort(true);
				}
			}
					
			sql = "select check_escort_voter_workplaces(" + voter_id + "," +  escort_id + ")" +
			      " from dual";			
			rs = stmt.executeQuery(sql);
			rs.next();
			int isSameWorkPlace = rs.getInt(1);
			System.out.println("Voter id: " + voter_id);
			System.out.println("Escort id: " + escort_id);
			System.out.println("DB returned: " + isSameWorkPlace);
			if (isSameWorkPlace == 1) {
				ans.setEligibleToEscort(false);
			}
			
			if (!nonCitizenWorkPlace.equals("")) {
				sql = " SELECT work_place FROM person WHERE id = " + voter_id;
				rs = stmt.executeQuery(sql);
				rs.next();
				String voterWorkPlace = rs.getString(1);
				if (voterWorkPlace.equals(nonCitizenWorkPlace)) {
					ans.setEligibleToEscort(false);
				}
			}
			
		    stmt.close();		
		    rs.close();
		} catch (SQLException se) {
			try {
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				System.out.println("Problem");
				return ans;
			}
		} 
		return ans;
	}  // isEscortEligibleToEscort
	
	public synchronized static ValidateBallotAreaIDAnswer validateCorrectBallotAreaIDRequest(int ballot_area_id) {
		ValidateBallotAreaIDAnswer ans = new ValidateBallotAreaIDAnswer(false);
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		try {
			stmt = con.createStatement();		
			sql = "SELECT id FROM ballot_area";
			rs = stmt.executeQuery(sql);
			while(rs.next()){					
				int id  = rs.getInt("id");
				if (id == ballot_area_id) {
					ans.setBallot_area_exists(true);
				}
			}
			// executing the query
		    stmt.close();		
		    rs.close();
		} catch (SQLException se) {
			try {
				stmt.close();
				rs.close();
			} catch (SQLException e) {}
		} 
		return ans;
	}  // ValidateBallotAreaIDAnswer
		
	public static void closeConnection() {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isConnected() {
		return con != null;
	}
}