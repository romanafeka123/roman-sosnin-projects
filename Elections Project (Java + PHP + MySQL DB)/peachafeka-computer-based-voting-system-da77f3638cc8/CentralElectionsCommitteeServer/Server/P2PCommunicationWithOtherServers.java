package Server;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import BL.Voter;
import Messages.CloseConnectionRequest;
import Messages.EscortValidationAnswer;
import Messages.EscortValidationRequest;
import Messages.RemoveVoterMessage;
import Messages.UpdateTimesEscortedMessage;
import Messages.ValidateBallotAreaIDAnswer;
import Messages.ValidateCorrectBallotAreaIDRequest;
import Messages.VoterRegistryMessage;
import Messages.VoterRegistryRequest;
//////////////////////////ROMAN /////////////////////////////////
public class P2PCommunicationWithOtherServers {

	private final int SERVER_PORT;
	private final String SERVER_ADDRESS;
	private Socket socket;
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;
	private boolean connectedToServer;

	public P2PCommunicationWithOtherServers(String addr, int port) {
		SERVER_ADDRESS = addr;
		SERVER_PORT = port;
	}

	public boolean connect() {
		try {
			socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
			if (!socket.isConnected()) {
				return false;
			}
			outStream = new ObjectOutputStream(socket.getOutputStream());
			inStream = new ObjectInputStream(socket.getInputStream());
			connectedToServer = true;
		} catch (IOException e) {
			connectedToServer = false;
		}
		return connectedToServer;
	}

	
	public boolean disconnect() {
		try {
			outStream.writeObject(new CloseConnectionRequest());
			connectedToServer = false;
		} catch (IOException e) {
		}
		return connectedToServer;
	}

	public void writeToStream(Object obj) {
		try {
			outStream.writeObject(obj);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void listenToMessagesFromOtherServers() {
		if (!isConnected()) {
			return;
		}
		// this thread receives all the messages from central elections committee
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					boolean connected = true;
					do {
						Object obj = inStream.readObject();
						if (obj instanceof CloseConnectionRequest) {
							connectedToServer = false;
							socket.close();
						}					
					} while (connected);
				} catch (EOFException e) {
					connectedToServer = false;
				} catch (ClassNotFoundException e) {
				} catch (IOException e) {
				} finally {
					try {
						socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
		
	public boolean isConnected() {
		return connectedToServer;
	}
}
