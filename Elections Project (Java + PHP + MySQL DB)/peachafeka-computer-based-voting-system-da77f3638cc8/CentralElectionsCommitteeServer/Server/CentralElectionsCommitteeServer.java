package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

import DAL.DBController;
import Messages.AddVoteMessage;
import Messages.CloseConnectionRequest;
import Messages.EscortValidationAnswer;
import Messages.EscortValidationRequest;
import Messages.P2PAddVoteMessage;
import Messages.P2PUpdateTimesEscortedMessage;
import Messages.RemoveVoterMessage;
import Messages.UpdateTimesEscortedMessage;
import Messages.ValidateBallotAreaIDAnswer;
import Messages.ValidateCorrectBallotAreaIDRequest;
import Messages.VoterRegistryMessage;
import Messages.VoterRegistryRequest;
////////////////////////// ROMAN /////////////////////////////////
public class CentralElectionsCommitteeServer implements Runnable {
	
	private static ArrayList<ClientSocket> ballotAreasConnections = new ArrayList<ClientSocket>();
	
	private final int PORT;
	private ServerSocket server;
	private boolean serverUp;
	private Socket socket;
	
	private String[] serverIPAddresses;
	private static final int NUM_OF_OTHER_SERVERS = 5;
	// if a server failed to send the P2P msg to other server, resend it again after this specified time
	private static final long TIME_TO_WAIT_UNTIL_TO_SEND_MSG_AGAIN = 5 * 60 * 1000; // 5 mins

	@Override
	public void run() {
		DBController.connectToDB();
		setServerIPAddresses();
		listenToClients();	
	}
	
	public CentralElectionsCommitteeServer(int portNum) {
		PORT = portNum;
		try {
			server = new ServerSocket(PORT);
			serverUp = true;
			System.out.println("Server is online");
		} catch (IOException e) {}
	}

	public void listenToClients() {
		while (serverUp) {
			try {
				socket = server.accept();
			} catch (IOException e) {
				// could not open socket or server was closed
				return;
			}
			// for every new incoming client, open a new thread
			new Thread(new Runnable() {
				@Override
				public void run() {
					ClientSocket currentSocketData = null;
					try {
						currentSocketData = new ClientSocket(socket);
						ballotAreasConnections.add(currentSocketData);
						boolean connected = true;
						do {
							Object obj = currentSocketData.getInputStream().readObject();	
							if (obj instanceof AddVoteMessage) {
								AddVoteMessage msg = (AddVoteMessage)obj;
								DBController.addVoteToDB(msg.getParty_id(), msg.getBallot_area_id());
								P2PAddVoteMessage p2pMsg = new P2PAddVoteMessage(msg.getParty_id(), msg.getBallot_area_id());
								sendMessagesToOtherServers(p2pMsg);
								currentSocketData.writeToStream(new CloseConnectionRequest());
							}
							else if (obj instanceof UpdateTimesEscortedMessage) {
								UpdateTimesEscortedMessage msg = (UpdateTimesEscortedMessage)obj;
								DBController.updateTimesEscortedInDB(msg.getEscort_id(), msg.isCitizen(), msg.getNonCitizenEscortWorkplace());
								P2PUpdateTimesEscortedMessage p2pMsg = new P2PUpdateTimesEscortedMessage(msg.getEscort_id(), msg.isCitizen(), msg.getNonCitizenEscortWorkplace());
								sendMessagesToOtherServers(p2pMsg);
								currentSocketData.writeToStream(new CloseConnectionRequest());
							}
							else if (obj instanceof RemoveVoterMessage) {
								RemoveVoterMessage msg = (RemoveVoterMessage)obj;
								DBController.removeVoterFromDB(msg.getVoter_id());
								currentSocketData.writeToStream(new CloseConnectionRequest());
							}
							else if (obj instanceof VoterRegistryRequest) {
								VoterRegistryRequest req = (VoterRegistryRequest)obj;
								VoterRegistryMessage msg = DBController.getVoterRegistryFromDB(req.getBallot_area_id());
								currentSocketData.writeToStream(msg);
								currentSocketData.writeToStream(new CloseConnectionRequest());
							}
							else if (obj instanceof EscortValidationRequest) {
								EscortValidationRequest req = (EscortValidationRequest)obj;
								EscortValidationAnswer msg = DBController.isEscortEligibleToEscort(req.getVoter_id(), req.getEscort_id(), req.getNonCitizenEscortWorkplace());
								currentSocketData.writeToStream(msg);
								currentSocketData.writeToStream(new CloseConnectionRequest());
							}
							else if (obj instanceof ValidateCorrectBallotAreaIDRequest) {
								ValidateCorrectBallotAreaIDRequest req = (ValidateCorrectBallotAreaIDRequest)obj;
								ValidateBallotAreaIDAnswer msg = DBController.validateCorrectBallotAreaIDRequest(req.getBallot_area_id());
								currentSocketData.writeToStream(msg);
								currentSocketData.writeToStream(new CloseConnectionRequest());
							}	
							else if (obj instanceof P2PAddVoteMessage) {
								P2PAddVoteMessage msg = (P2PAddVoteMessage)obj;
								DBController.addVoteToDB(msg.getParty_id(), msg.getBallot_area_id());
								currentSocketData.writeToStream(new CloseConnectionRequest());
							}
							else if (obj instanceof P2PUpdateTimesEscortedMessage) {
								P2PUpdateTimesEscortedMessage msg = (P2PUpdateTimesEscortedMessage)obj;
								DBController.updateTimesEscortedInDB(msg.getEscort_id(), msg.isCitizen(), msg.getNonCitizenEscortWorkplace());
								currentSocketData.writeToStream(new CloseConnectionRequest());
							}
						} while (connected);
					} catch (ClassNotFoundException e) {} catch (IOException e) {} finally {
						try {
							if (currentSocketData != null) {
								currentSocketData.getSocket().close();
							}
						} catch (IOException e) {}
					}
				}
			}).start();
		}
	}

	public boolean isServerUp() {
		return serverUp;
	}

	public void closeServer() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Iterator<ClientSocket> it = ballotAreasConnections.iterator();
				while (it.hasNext()) {
					try {
						ClientSocket cs = it.next();
						cs.getOutputStream().writeObject(new CloseConnectionRequest());
						cs.closeConnection();
						it.remove();
					} catch (IOException e) {
					}
				}
			}
		}).start();
		try {
			server.close();
			serverUp = false;
			DBController.closeConnection();
		} catch (IOException e) {
		}
	}
	
	// this function is for sending AddVoteMsg and UpdateTimesEscorted to other servers (P2P)
	private void sendMessagesToOtherServers(final Object obj) {		
		for (int i = 0; i < NUM_OF_OTHER_SERVERS; i++) {
			sendMessageToSingleServer(obj, i);
		}
	}
	
	private void sendMessageToSingleServer(final Object obj, final int index) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				boolean notSentMsgToOtherServer = true;
				while (notSentMsgToOtherServer) {
					P2PCommunicationWithOtherServers comm = new P2PCommunicationWithOtherServers(serverIPAddresses[index], 8001);
					comm.connect();
					if (comm.isConnected()) {
						comm.writeToStream(obj);
						notSentMsgToOtherServer = false;
						comm.listenToMessagesFromOtherServers();
						return;
					}
					try {
						Thread.sleep(TIME_TO_WAIT_UNTIL_TO_SEND_MSG_AGAIN);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public String[] getServerIPAddresses() {
		return serverIPAddresses;
	}

	public void setServerIPAddresses() {
		String server_1_IP = "111.111.111.111";
		String server_2_IP = "222.222.222.222";
		String server_3_IP = "333.333.333.333";
		String server_4_IP = "444.444.444.444";
		String server_5_IP = "555.555.555.555";
		serverIPAddresses = new String[5];
		serverIPAddresses[0] = server_1_IP;
		serverIPAddresses[1] = server_2_IP;
		serverIPAddresses[2] = server_3_IP;
		serverIPAddresses[3] = server_4_IP;
		serverIPAddresses[4] = server_5_IP;
	}
	
	public static CentralElectionsCommitteeServer runServer() {
		CentralElectionsCommitteeServer server = new CentralElectionsCommitteeServer(8001);
		Thread t = new Thread(server);
		t.start();
		return server;
	}

	public static void main(String[] args) {
		CentralElectionsCommitteeServer.runServer();
	}
}
