package Messages;

import java.io.Serializable;
//////////////////////////ROTEM /////////////////////////////////
public class EscortValidationRequest implements Serializable{
	private static final long serialVersionUID = -1533949853510059939L;
	private int voter_id;
	private int escort_id;
	private String nonCitizenEscortWorkplace;
	
	public EscortValidationRequest(int voter_id, int escort_id, String nonCitizenEscortWorkplace) {
		this.voter_id = voter_id;
		this.escort_id = escort_id;
		this.nonCitizenEscortWorkplace = nonCitizenEscortWorkplace;
	}
	public int getVoter_id() {
		return voter_id;
	}
	public int getEscort_id() {
		return escort_id;
	}
	public String getNonCitizenEscortWorkplace() {
		return nonCitizenEscortWorkplace;
	}
	public void setNonCitizenEscortWorkplace(String nonCitizenEscortWorkplace) {
		this.nonCitizenEscortWorkplace = nonCitizenEscortWorkplace;
	}
}
