package Messages;

import java.io.Serializable;
//////////////////////////GUY /////////////////////////////////
public class UpdateTimesEscortedMessage implements Serializable {
	private static final long serialVersionUID = 7430271574258697650L;
	private int escort_id;
	private boolean isCitizen;
	private String nonCitizenEscortWorkplace;

	public UpdateTimesEscortedMessage(int escort_id, boolean isCitizen, String nonCitizenEscortWorkplace) {
		this.escort_id = escort_id;
		this.isCitizen = isCitizen;
		this.nonCitizenEscortWorkplace = nonCitizenEscortWorkplace;
	}

	public int getEscort_id() {
		return escort_id;
	}

	public boolean isCitizen() {
		return isCitizen;
	}

	public String getNonCitizenEscortWorkplace() {
		return nonCitizenEscortWorkplace;
	}

	public void setNonCitizenEscortWorkplace(String nonCitizenEscortWorkplace) {
		this.nonCitizenEscortWorkplace = nonCitizenEscortWorkplace;
	}
}
