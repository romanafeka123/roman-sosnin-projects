package Messages;

import java.io.Serializable;
//////////////////////////ROTEM /////////////////////////////////
public class EscortValidationAnswer implements Serializable{
	private static final long serialVersionUID = 8900332428677378970L;
	private boolean eligibleToEscort;

	public EscortValidationAnswer() {
	}

	public boolean isEligibleToEscort() {
		return eligibleToEscort;
	}

	public void setEligibleToEscort(boolean eligibleToEscort) {
		this.eligibleToEscort = eligibleToEscort;
	}
}
