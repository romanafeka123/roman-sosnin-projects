package Messages;
//////////////////////////ROMAN /////////////////////////////////
import java.io.Serializable;

public class P2PUpdateTimesEscortedMessage implements Serializable {
	private static final long serialVersionUID = -7126314634165431513L;
	private int escort_id;
	private boolean isCitizen;
	private String nonCitizenEscortWorkplace;

	public P2PUpdateTimesEscortedMessage(int escort_id, boolean isCitizen, String nonCitizenEscortWorkplace) {
		this.escort_id = escort_id;
		this.isCitizen = isCitizen;
		this.nonCitizenEscortWorkplace = nonCitizenEscortWorkplace;
	}

	public int getEscort_id() {
		return escort_id;
	}

	public boolean isCitizen() {
		return isCitizen;
	}

	public String getNonCitizenEscortWorkplace() {
		return nonCitizenEscortWorkplace;
	}

	public void setNonCitizenEscortWorkplace(String nonCitizenEscortWorkplace) {
		this.nonCitizenEscortWorkplace = nonCitizenEscortWorkplace;
	}
}
