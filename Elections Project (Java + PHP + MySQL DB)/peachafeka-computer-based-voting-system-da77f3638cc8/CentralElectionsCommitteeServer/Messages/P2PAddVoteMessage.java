package Messages;
//////////////////////////ROMAN /////////////////////////////////
import java.io.Serializable;

public class P2PAddVoteMessage implements Serializable {
	private static final long serialVersionUID = 5751122760706056061L;
	private int party_id;
	private int ballot_area_id;
	
	public P2PAddVoteMessage(int party_id, int ballot_area_id) {
		this.party_id = party_id;
		this.ballot_area_id = ballot_area_id;
	}

	public int getParty_id() {
		return party_id;
	}

	public int getBallot_area_id() {
		return ballot_area_id;
	}
}
