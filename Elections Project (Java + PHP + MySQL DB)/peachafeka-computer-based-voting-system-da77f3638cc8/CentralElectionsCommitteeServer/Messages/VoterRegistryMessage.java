package Messages;

import java.io.Serializable;
import java.util.ArrayList;

import BL.Voter;
////////////////////////// ROTEM /////////////////////////////////
public class VoterRegistryMessage implements Serializable{
	private static final long serialVersionUID = -6313538144885023967L;
	private ArrayList<Voter> voters = new ArrayList<Voter>();

	public VoterRegistryMessage() {
	}
	
	public void addVoterToRegistry(Voter voter) {
		voters.add(voter);
	}

	public ArrayList<Voter> getVoters() {
		return voters;
	}
}
