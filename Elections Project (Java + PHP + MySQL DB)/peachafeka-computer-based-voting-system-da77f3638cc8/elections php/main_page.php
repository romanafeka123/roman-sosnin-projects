<html>
<head>
<link rel="stylesheet" type="text/css" href="my_style.css" />
</head>
<body>
<div align="center" dir="ltr">
<h2 align="center"><u>Elections results 2015</u></h2>
<?php
header('Content-type: text/html; charset=utf-8');
mb_internal_encoding('UTF-8');
include "db_connect.php";  //connecting to MySQL server and DB

//building the sql query:
$show_query="select * from results_view order by votes_amount desc";

 //execute the query or exit on failure
$result=@mysql_query($show_query, $con) or die("<br><b>ERROR:</b> error occured in mysql query.<br>"); 

//display table of the students and thier grades:
?>
<table border=1 cellpadding=2 cellspacing=2>
	<tr>
   	  <th width=200 align="center">	Party name		</th>
      <th width=100 align="center">	Abbreviation	</th>
      <th width=50 align="center">	Mandates		</th>
      <th width=50 align="center">	Percentage		</th>
      <th width=100 align="center">	Votes amount	</th>
      <th width=150 align="center">	Votes amount bar</th>
   </tr>

   <?php
   	//printing all the rows of the students:
      while ($row=mysql_fetch_array($result))  //as long as there are rows in the result
      {
      	 echo "<tr>";
         echo "<td align=\"left\"><b>".$row["party_name"]."			</b></td>";
         echo "<td align=\"center\"><b>".$row["abbreviation"]."		</b></td>";
         echo "<td align=\"center\"><b>".$row["mandates_amount"]."	</b></td>";
         echo "<td align=\"center\"><b>".$row["percentage"]."%		</b></td>";
         echo "<td align=\"center\"><b>".$row["votes_amount"]."		</b></td>";
         echo "<td align=\"center\"><div style='width:" . $row["percentage"]*2 . "%;' class='DataBar Blue''></div></td>";
         echo "</tr>";
      }
   ?>
</table>
</div>
</body>
</html>