package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import Listeners.GUIEventsListener;
import Messages.OpenSystemForVotingMessage;
import Messages.VoterRegistryRequest;
//////////////////////////PRIEL /////////////////////////////////
public class ValidatorMainFrame extends JFrame {
	private static final long serialVersionUID = 4727330736917785070L;
	
	public static final Color color = new Color(0, 255, 255);
	private JTabbedPane tabs;
	private JPanel voterValidationPanel;
	private JPanel escortValidationPnl;
	private JPanel otherActionsPanel;
	private JPanel panelForVoterValidationComponents = new JPanel();
	private JPanel panelForEscortValidationComponents1 = new JPanel();
	private JPanel panelForEscortValidationComponents2 = new JPanel();
	private JPanel panelForOtherActionsComponents = new JPanel();
	
	private JRadioButton yesRadBtn = new JRadioButton("yes");
	private JRadioButton noRadBtn = new JRadioButton("no");
	private JCheckBox isBeingEscortedChkBox = new JCheckBox("Is the voter being escorted?");
	
	private JButton checkVoterBtn = new JButton("Check voter");
	private JTextField checkVoterTxtFld = new JTextField("Enter voter's ID", 20);
	private JButton checkEscortBtn = new JButton("Check escort");
	private JTextField escortWorkPlaceTxtFld = new JTextField("Enter escort workplace if he's not citizen", 20);
	private JTextField checkEscortTxtFld = new JTextField("Enter escort's ID", 20);
	private JTextField ballotAreaIDTxtFld = new JTextField("Enter ballot area id", 20);
	private JButton enterBallotAreaIDBtn = new JButton("Enter this ballot area ID");
	private JButton requestVoterRegistryFromServerBtn = new JButton("Request voter registry");
	private JButton openSystemForVotingBtn = new JButton("Open system for voting");
	
	private ArrayList<GUIEventsListener> listeners = new ArrayList<GUIEventsListener>();
	
	private int ballot_area_id = -1;
	private int currentVoterID;
	private int currentEscortID;
	private boolean isCitizen;
	private boolean isBeingEscorted;
	private String currentEscortWorkplace = "";  // this is only for non - citizen escort
	
	public ValidatorMainFrame() {
		setTitle("Validation System");
		setSize(400, 240); // Set the frame size
		setLocationRelativeTo(null); // null - center of the screen
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setAlwaysOnTop(true);
		setVisible(true); // Display the frame	
		setResizable(false);
		
		tabs = new JTabbedPane();	
		createVoterValidationTab();
		createEscortValidationTab();
		createBallotInitializationTab();
		defineBackgroud();
		setListeners();
		yesRadBtn.setSelected(true);
		
		// Add the tabs to the frame
		tabs.addTab("Voter Validation", voterValidationPanel);
		tabs.addTab("Escort Validation", escortValidationPnl);
		tabs.addTab("Ballot Initialization", otherActionsPanel);
		add(tabs);
	} // Constructor
	
	private void createVoterValidationTab() {
		voterValidationPanel = new JPanel(new BorderLayout());
		JLabel voterValidationLbl = new JLabel("Voter Validation");
		// Set the position of the label in the center
		voterValidationLbl.setHorizontalAlignment(JTextField.CENTER);
		Map<TextAttribute, Integer> fontAttributes2 = new HashMap<TextAttribute, Integer>();
		fontAttributes2.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		voterValidationLbl.setFont(new Font("Comic Sans MS", Font.BOLD, 25).deriveFont(fontAttributes2));
		checkVoterBtn.setPreferredSize(new Dimension(225, 25));// Change button size
		RemoveField(checkVoterTxtFld); 	
		voterValidationPanel.add(voterValidationLbl, BorderLayout.NORTH);	
		panelForVoterValidationComponents.add(checkVoterTxtFld);
		panelForVoterValidationComponents.add(checkVoterBtn);
		panelForVoterValidationComponents.add(openSystemForVotingBtn);
		panelForVoterValidationComponents.add(isBeingEscortedChkBox);	
		voterValidationPanel.add(panelForVoterValidationComponents, BorderLayout.CENTER);
	}
	
	private void createEscortValidationTab() {
		escortValidationPnl = new JPanel(new BorderLayout());
		JLabel escortValidationLbl = new JLabel("Escort Validation");
		// Set the position of the label in the center
		escortValidationLbl.setHorizontalAlignment(JTextField.CENTER);
		// Set the label to underline
		Map<TextAttribute, Integer> fontAttributes1 = new HashMap<TextAttribute, Integer>();
		fontAttributes1.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		escortValidationLbl.setFont(new Font("Comic Sans MS", Font.BOLD, 25).deriveFont(fontAttributes1));
		checkEscortBtn.setPreferredSize(new Dimension(225, 25));// Change button size
		escortValidationPnl.add(escortValidationLbl, BorderLayout.NORTH);
		// create text fields
		// Remove the text from the text field in order to write
		RemoveField(checkEscortTxtFld);
		RemoveField(escortWorkPlaceTxtFld);
		// Add the elements to the panel
		panelForEscortValidationComponents1.add(checkEscortTxtFld);
		// Creating new panel for the radio buttons
		JLabel isCitizen = new JLabel("Is citizen:");
		// Group the radio buttons.
		ButtonGroup group = new ButtonGroup();
		group.add(yesRadBtn);
		group.add(noRadBtn);
		// add elements to panel5
		
		panelForEscortValidationComponents2.add(isCitizen);
		panelForEscortValidationComponents2.add(yesRadBtn);
		panelForEscortValidationComponents2.add(noRadBtn);	
		panelForEscortValidationComponents1.add(escortWorkPlaceTxtFld);
		panelForEscortValidationComponents1.add(panelForEscortValidationComponents2);
		panelForEscortValidationComponents1.add(checkEscortBtn);		
		escortValidationPnl.add(panelForEscortValidationComponents1, BorderLayout.CENTER);
	}
	
	private void createBallotInitializationTab() {
		otherActionsPanel = new JPanel(new BorderLayout());
		JLabel otherActionsLbl = new JLabel("Ballot Initialization");
		// Set the position of the label in the center
		otherActionsLbl.setHorizontalAlignment(JTextField.CENTER);

		Map<TextAttribute, Integer> fontAttributes3 = new HashMap<TextAttribute, Integer>();
		fontAttributes3.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		otherActionsLbl.setFont(new Font("Comic Sans MS", Font.BOLD, 25).deriveFont(fontAttributes3));

		enterBallotAreaIDBtn.setPreferredSize(new Dimension(225, 25));																		
		requestVoterRegistryFromServerBtn.setPreferredSize(new Dimension(225, 25));																																			
		openSystemForVotingBtn.setPreferredSize(new Dimension(225, 25));																	
		RemoveField(ballotAreaIDTxtFld); 
		
		otherActionsPanel.add(otherActionsLbl, BorderLayout.NORTH);
		panelForOtherActionsComponents.add(ballotAreaIDTxtFld);
		panelForOtherActionsComponents.add(enterBallotAreaIDBtn);
		panelForOtherActionsComponents.add(requestVoterRegistryFromServerBtn);
		otherActionsPanel.add(panelForOtherActionsComponents, BorderLayout.CENTER);
	}
	
	private void defineBackgroud() {
		yesRadBtn.setBackground(color);
		noRadBtn.setBackground(color);
		voterValidationPanel.setBackground(color);
		panelForVoterValidationComponents.setBackground(color);
		escortValidationPnl.setBackground(color);
		panelForEscortValidationComponents1.setBackground(color);
		panelForEscortValidationComponents2.setBackground(color);
		panelForOtherActionsComponents.setBackground(color);
		otherActionsPanel.setBackground(color);
		isBeingEscortedChkBox.setBackground(color);
	}
	
	public static void RemoveField(final JTextField field) {
		field.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				field.setText("");
			}
		});
	}
	
	private void setListeners() {
		checkVoterBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (requestVoterRegistryFromServerBtn.isEnabled()) {
						showErrorDialogMessage("Request the voter registry firstly!");
					} else {
						currentVoterID = Integer.parseInt(checkVoterTxtFld.getText());
						if (currentVoterID < 1) {
							throw new Exception();
						}
						boolean voterExists = fireCheckVoterIDEvent(currentVoterID);
						if (!voterExists) {
							showErrorDialogMessage("The voter registry doesn't contain this ID!");
						}
						else {
							showWarningDialogMessage("The voter exists in the registry!");
						}
					}
				} catch (Exception e) {
					showErrorDialogMessage("Please enter correct voter ID");
				}
				// checking here the voter...inside the project
			} // actionPerformed
		});
		
		checkEscortBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					currentEscortID = Integer.parseInt(checkEscortTxtFld.getText());
					currentVoterID = Integer.parseInt(checkVoterTxtFld.getText());
					if (currentVoterID < 1 || currentEscortID < 1) {
						throw new Exception();
					}
					currentEscortWorkplace = escortWorkPlaceTxtFld.getText();
					if (noRadBtn.isSelected() && escortWorkPlaceTxtFld.getText().equals("")) {
						showErrorDialogMessage("Please enter correct workplace!");
					}
					else {
						fireCheckEscortInDBEvent(currentVoterID, currentEscortID, currentEscortWorkplace);
					}
				} catch (Exception e) {
					showErrorDialogMessage("Please enter correct ID");
				}
			} // actionPerformed
		});
		
		openSystemForVotingBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					currentVoterID = Integer.parseInt(checkVoterTxtFld.getText());
					if (yesRadBtn.isSelected()) {
						isCitizen = true;
					}
					else if (noRadBtn.isSelected()){
						isCitizen = false;
					}
					
					if (isBeingEscortedChkBox.isSelected()) {
						isBeingEscorted = true;
						currentEscortID = Integer.parseInt(checkEscortTxtFld.getText());
					}
					else {
						isBeingEscorted = false;
					}
					
					if (enterBallotAreaIDBtn.isEnabled()) {
						showErrorDialogMessage("Please enter correct Ballot Area ID!");
					}
					else if (requestVoterRegistryFromServerBtn.isEnabled()) {
						showErrorDialogMessage("Request the voter registry firstly!");
					}	
					else {
						currentEscortWorkplace = escortWorkPlaceTxtFld.getText();
						if (noRadBtn.isSelected() && escortWorkPlaceTxtFld.getText().equals("")) {
							showErrorDialogMessage("Please enter correct workplace!");
						}
						else {
							fireOpenSystemForVotingMessage(currentVoterID, currentEscortID, isCitizen, currentEscortWorkplace, isBeingEscorted, ballot_area_id);
						}
					}
				} catch (NumberFormatException e) {
					showErrorDialogMessage("Please enter correct ID!");
				} catch (Exception e) {
					showErrorDialogMessage("The Voting Stand is not connected!");
				}
			} // actionPerformed
		});

		requestVoterRegistryFromServerBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (ballot_area_id != -1) {
					VoterRegistryRequest msg = new VoterRegistryRequest(ballot_area_id);
					fireVoterRegistryRequest(msg);
					showWarningDialogMessage("The Voter Registry was received!");
					requestVoterRegistryFromServerBtn.setEnabled(false);
				}
				else {
					showErrorDialogMessage("Please enter correct Ballot Area ID!");
				}
			} // actionPerformed
		});
		
		enterBallotAreaIDBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ballot_area_id = Integer.parseInt(ballotAreaIDTxtFld.getText());
					if (ballot_area_id < 1) {
						throw new Exception();
					}
					fireValidateCorrectBallotAreaIDEvent(ballot_area_id);
				} catch (Exception e) {
					showErrorDialogMessage("Please enter correct Ballot Area ID!");
				}
			} // actionPerformed
		});	
	}
//////////////////////////ROMAN /////////////////////////////////
	public void disableBallotAreaIDOptions() {
		enterBallotAreaIDBtn.setEnabled(false);
		ballotAreaIDTxtFld.setEnabled(false);
	}
	
	public void registerListener(GUIEventsListener listener) {
		listeners.add(listener);
	}
	
	protected void fireCheckEscortInDBEvent(int voter_id, int escort_id, String currentEscortWorkplace) {
		for (GUIEventsListener l : listeners)
			l.checkEscortInDBEvent(voter_id, escort_id, currentEscortWorkplace);
	}
	
	protected void fireOpenSystemForVotingMessage(int currentVoterID, int currentEscortID, boolean isCitizen, String currentEscortWorkplace, boolean isBeingEscorted, int ballot_area_id) {
		for (GUIEventsListener l : listeners)
			l.openSystemForVotingMessage(currentVoterID, currentEscortID, isCitizen, currentEscortWorkplace, isBeingEscorted, ballot_area_id);
	}
	
	protected void fireVoterRegistryRequest(VoterRegistryRequest msg) {
		for (GUIEventsListener l : listeners)
			l.voterRegistryRequest(msg);
	}
	
	protected void fireValidateCorrectBallotAreaIDEvent(int ballot_area_id) {
		for (GUIEventsListener l : listeners)
			l.validateCorrectBallotAreaIDEvent(ballot_area_id);
	}
	
	protected boolean fireCheckVoterIDEvent(int voter_id) {
		for (GUIEventsListener l : listeners)
			return l.checkVoterIDEvent(voter_id);
		return false;
	}

	public int getCurrentVoterID() {
		return currentVoterID;
	}

	public int getCurrentEscortID() {
		return currentEscortID;
	}

	public void setCurrentEscortID(int currentEscortID) {
		this.currentEscortID = currentEscortID;
	}

	public boolean isBeingEscorted() {
		return isBeingEscorted;
	}

	public void setBeingEscorted(boolean isBeingEscorted) {
		this.isBeingEscorted = isBeingEscorted;
	}	
	
	private void showWarningDialogMessage(String msg) {
		JFrame fr = new JFrame();
		fr.setLocation(620, 540);
		fr.setVisible(true);
		JOptionPane.showMessageDialog(fr, msg, "Voter Registry Request",JOptionPane.INFORMATION_MESSAGE);
		fr.dispose();
	}
	
	private void showErrorDialogMessage(String msg) {
		JFrame fr = new JFrame();
		fr.setLocation(620,540);
		fr.setVisible(true);				
		JOptionPane.showMessageDialog(fr, msg, "Error", JOptionPane.ERROR_MESSAGE);
		fr.dispose();
	}
}
