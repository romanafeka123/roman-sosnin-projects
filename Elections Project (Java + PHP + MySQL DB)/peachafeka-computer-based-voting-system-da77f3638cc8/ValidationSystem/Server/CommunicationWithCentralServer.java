package Server;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import BL.Voter;
import Messages.CloseConnectionRequest;
import Messages.EscortValidationAnswer;
import Messages.EscortValidationRequest;
import Messages.RemoveVoterMessage;
import Messages.UpdateTimesEscortedMessage;
import Messages.ValidateBallotAreaIDAnswer;
import Messages.ValidateCorrectBallotAreaIDRequest;
import Messages.VoterRegistryMessage;
import Messages.VoterRegistryRequest;
//////////////////////////ROMAN /////////////////////////////////
public class CommunicationWithCentralServer {

	private final int SERVER_PORT;
	private final String SERVER_ADDRESS;
	private Socket socket;
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;
	private boolean connectedToServer;
	private ValidationStand stand;

	public CommunicationWithCentralServer(String addr, int port, ValidationStand stand) {
		SERVER_ADDRESS = addr;
		SERVER_PORT = port;
		this.stand = stand;
	}

	public boolean connect() {
		try {
			socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
			if (!socket.isConnected()) {
				return false;
			}
			outStream = new ObjectOutputStream(socket.getOutputStream());
			inStream = new ObjectInputStream(socket.getInputStream());
			connectedToServer = true;
		} catch (IOException e) {
			connectedToServer = false;
		}
		return connectedToServer;
	}

	
	public boolean disconnect() {
		try {
			outStream.writeObject(new CloseConnectionRequest());
			connectedToServer = false;
		} catch (IOException e) {
		}
		return connectedToServer;
	}

	
	public void sendVoterRegistryRequest(VoterRegistryRequest msg) {
		if (!isConnected())
			return;
		try {
			outStream.writeObject(msg);
		} catch (IOException e) {
		}
	}
	
	public void checkEscortInDB(EscortValidationRequest req) {
		if (!isConnected())
			return;
		try {
			outStream.writeObject(req);
		} catch (IOException e) {
		}
	}
	
	public void sendRemoveVoterMessage(int voter_id) {
		if (!isConnected())
			return;
		RemoveVoterMessage msg = new RemoveVoterMessage(voter_id);
		try {
			outStream.writeObject(msg);
		} catch (IOException e) {
		}
	}

	public void updateTimesEscortedMessage(int escort_id, boolean isCitizen, String currentEscortWorkplace) {
		if (!isConnected())
			return;
		UpdateTimesEscortedMessage msg = new UpdateTimesEscortedMessage(escort_id, isCitizen, currentEscortWorkplace);
		try {
			outStream.writeObject(msg);
		} catch (IOException e) {
		}
	}
	
	public void sendValidateCorrectBallotAreaIDRequest(int ballot_area_id) {
		if (!isConnected())
			return;
		ValidateCorrectBallotAreaIDRequest req = new ValidateCorrectBallotAreaIDRequest(ballot_area_id);
		try {
			outStream.writeObject(req);
		} catch (IOException e) {
		}
	}

	public void listenToMessagesFromCentralServer() {
		if (!isConnected()) {
			return;
		}
		// this thread receives all the messages from central elections committee
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					boolean connected = true;
					do {
						Object obj = inStream.readObject();
						if (obj instanceof CloseConnectionRequest) {
							connectedToServer = false;
							socket.close();
						}
						else if (obj instanceof VoterRegistryMessage) {
							VoterRegistryMessage msg = (VoterRegistryMessage)obj;
							voterRegistryMessageArrived(msg.getVoters());
						}
						else if (obj instanceof EscortValidationAnswer) {
							EscortValidationAnswer msg = (EscortValidationAnswer)obj;
							fireEscortValidationAnswerMsgArrived(msg.isEligibleToEscort());
						}
						else if (obj instanceof ValidateBallotAreaIDAnswer) {
							ValidateBallotAreaIDAnswer msg = (ValidateBallotAreaIDAnswer)obj;
							fireBallotAreaIDAnswerArrived(msg.isBallot_area_exists());
						}					
					} while (connected);
				} catch (EOFException e) {
					connectedToServer = false;
				} catch (ClassNotFoundException e) {
				} catch (IOException e) {
				} finally {
					try {
						socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	private void voterRegistryMessageArrived(ArrayList<Voter> voters) {
		System.out.println("The Registry is:");
		for (int i = 0; i < voters.size(); i++) {
			System.out.print("ID:   " + voters.get(i).getVoter_id() + "    " );
			System.out.println("Is eligible to be escorted:   " + voters.get(i).isCanBeEscorted());
		}
		stand.getVoterRegistry().setVoters(voters);
	}
	
	private void fireBallotAreaIDAnswerArrived(boolean is_ballot_area_exists) {
		if (!is_ballot_area_exists) {
			try {
				throw new Exception();
			} catch (Exception e) {
				JFrame fr = new JFrame();
				fr.setLocation(620,540);
				fr.setVisible(true);	
				JOptionPane.showMessageDialog(fr, "Please enter correct Ballot Area ID!",
						"Error", JOptionPane.ERROR_MESSAGE);
				fr.dispose();
			}
		}
		else {
			stand.fireDisableBallotAreaIDOptions();
		}
	}
		
	private void fireEscortValidationAnswerMsgArrived(boolean isEligibleToEscort) {
		JFrame fr = new JFrame();
		fr.setLocation(620,540);
		fr.setVisible(true);
		if (isEligibleToEscort) {
			JOptionPane.showMessageDialog(fr, "The person is eligible to escort!",
					"Escort validation", JOptionPane.INFORMATION_MESSAGE);
		}
		else {
			JOptionPane.showMessageDialog(fr, "The person is not eligible to escort!",
					"Escort validation", JOptionPane.INFORMATION_MESSAGE);
		}
		fr.dispose();
	}
		
	public boolean isConnected() {
		return connectedToServer;
	}
}
