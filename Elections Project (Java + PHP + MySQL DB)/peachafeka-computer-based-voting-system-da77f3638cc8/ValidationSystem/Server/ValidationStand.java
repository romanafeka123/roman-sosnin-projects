package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import BL.VoterRegistry;
import GUI.ValidatorMainFrame;
import Listeners.BLEventsListeners;
import Listeners.GUIEventsListener;
import MVCController.Controller;
import Messages.CloseConnectionRequest;
import Messages.EscortValidationRequest;
import Messages.OpenSystemForVotingMessage;
import Messages.VoteSentApprovalMessage;
import Messages.VoterRegistryRequest;
//////////////////////////ROMAN /////////////////////////////////
public class ValidationStand implements Runnable {
	
	private ClientSocket votingStandConnection;
	private CommunicationWithCentralServer electionsCommitteeConnection;
	private final int PORT;
	private ServerSocket server;
	private boolean serverUp;
	private Socket socket;
	
	private Vector<BLEventsListeners> listeners = new Vector<BLEventsListeners>();
	private int currentVoterID;
	private int currentEscortID;
	private boolean isCitizen;
	private boolean isBeingEscorted;
	private String currentEscortWorkplace;
	
	private VoterRegistry voterRegistry = new VoterRegistry();
 
	@Override
	public void run() {
		ValidatorMainFrame frame = new ValidatorMainFrame();
		Controller controller = new Controller(this, frame);
		listenToClients();	
	}
	
	public ValidationStand(int portNum) {
		PORT = portNum;
		try {
			server = new ServerSocket(PORT);
			serverUp = true;
			System.out.println("Server is online");
		} catch (IOException e) {}
	}

	public void listenToClients() {
		while (serverUp) {
			try {
				socket = server.accept();
			} catch (IOException e) {
				// could not open socket or server was closed
				return;
			}
			// for every new incoming client, open a new thread
			new Thread(new Runnable() {
				@Override
				public void run() {
					ClientSocket currentSocketData = null;
					try {
						votingStandConnection = new ClientSocket(socket);
						boolean connected = true;
						do {
							Object obj = votingStandConnection.getInputStream().readObject();	
							if (obj instanceof VoteSentApprovalMessage) {
								removeVoterMessageEvent();
								if (isBeingEscorted) {
									updateTimesEscortedMessage();
								}
								voterRegistry.removeVoterFromRegistry(currentVoterID);
							}
						} while (connected);
					} catch (ClassNotFoundException e) {} catch (IOException e) {} finally {
						try {
							if (currentSocketData != null) {
								currentSocketData.getSocket().close();
							}
						} catch (IOException e) {}
					}
				}
			}).start();
		}
	}

	public boolean isServerUp() {
		return serverUp;
	}

	public void closeServer() {
		try {
			votingStandConnection.getOutputStream().writeObject(new CloseConnectionRequest());
			server.close();
			serverUp = false;
		} catch (IOException e) {}
	}
	
	public void registerListener(BLEventsListeners listener) {
		listeners.add(listener);
	}
		
	public void openSystemForVotingMessageFired(int currentVoterID, int currentEscortID, boolean isCitizen, String currentEscortWorkplace, boolean isBeingEscorted, int ballot_area_id) {
		try {
			setCurrentVoterID(currentVoterID);
			setCurrentEscortID(currentEscortID);
			setCitizen(isCitizen);
			setCurrentEscortWorkplace(currentEscortWorkplace);
			setBeingEscorted(isBeingEscorted);
			OpenSystemForVotingMessage msg = new OpenSystemForVotingMessage();
			msg.setBallot_area_id(ballot_area_id);
			votingStandConnection.getOutputStream().writeObject(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void connectToCentralServer() {
		electionsCommitteeConnection = new CommunicationWithCentralServer("localhost", 8001, this);
		electionsCommitteeConnection.connect();
	}

	public void checkEscortInDBEventFired(int voter_id, int escort_id, String currentEscortWorkplace) {
		connectToCentralServer();
		if (electionsCommitteeConnection.isConnected()) {
			electionsCommitteeConnection.checkEscortInDB(new EscortValidationRequest(voter_id, escort_id, currentEscortWorkplace));
			electionsCommitteeConnection.listenToMessagesFromCentralServer();
		}
	}
	
	public void voterRegistryRequestFired(VoterRegistryRequest msg) {
		connectToCentralServer();
		if (electionsCommitteeConnection.isConnected()) {
			electionsCommitteeConnection.sendVoterRegistryRequest(msg);
			electionsCommitteeConnection.listenToMessagesFromCentralServer();
		}
	}
	
	public void removeVoterMessageEvent() {
		connectToCentralServer();
		if (electionsCommitteeConnection.isConnected()) {
			electionsCommitteeConnection.sendRemoveVoterMessage(currentVoterID);
			electionsCommitteeConnection.listenToMessagesFromCentralServer();
		}
	}
	
	private void updateTimesEscortedMessage() {
		connectToCentralServer();
		if (electionsCommitteeConnection.isConnected()) {
			electionsCommitteeConnection.updateTimesEscortedMessage(currentEscortID, isCitizen, currentEscortWorkplace);
			electionsCommitteeConnection.listenToMessagesFromCentralServer();
		}
	}
	
	public void validateCorrectBallotAreaIDEventFired(int ballot_area_id) {
		connectToCentralServer();
		if (electionsCommitteeConnection.isConnected()) {
			electionsCommitteeConnection.sendValidateCorrectBallotAreaIDRequest(ballot_area_id);
			electionsCommitteeConnection.listenToMessagesFromCentralServer();
		}
	}
	
	public void fireDisableBallotAreaIDOptions() {
		for (BLEventsListeners l : listeners)
			l.disableBallotAreaIDOptions();
	}
	        
	public int getCurrentVoterID() {
		return currentVoterID;
	}

	public void setCurrentVoterID(int currentVoterID) {
		this.currentVoterID = currentVoterID;
	}

	public int getCurrentEscortID() {
		return currentEscortID;
	}

	public void setCurrentEscortID(int currentEscortID) {
		this.currentEscortID = currentEscortID;
	}

	public boolean isBeingEscorted() {
		return isBeingEscorted;
	}

	public void setBeingEscorted(boolean isBeingEscorted) {
		this.isBeingEscorted = isBeingEscorted;
	}

	public boolean isCitizen() {
		return isCitizen;
	}

	public void setCitizen(boolean isCitizen) {
		this.isCitizen = isCitizen;
	}
	

	public VoterRegistry getVoterRegistry() {
		return voterRegistry;
	}

	public void setVoterRegistry(VoterRegistry voterRegistry) {
		this.voterRegistry = voterRegistry;
	}

	public String getCurrentEscortWorkplace() {
		return currentEscortWorkplace;
	}

	public void setCurrentEscortWorkplace(String currentEscortWorkplace) {
		this.currentEscortWorkplace = currentEscortWorkplace;
	}

	public static void main(String[] args) {
		ValidationStand validator = new ValidationStand(7000);
		Thread t = new Thread(validator);
		t.start();
	}
}
