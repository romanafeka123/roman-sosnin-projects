package BL;

import java.io.Serializable;
//////////////////////////PRIEL /////////////////////////////////
public class Voter implements Serializable {
	private static final long serialVersionUID = -1463128818028095318L;
	private int voter_id;
	private boolean canBeEscorted;
	
	public Voter(int voter_id, boolean canBeEscorted) {
		this.voter_id = voter_id;
		this.canBeEscorted = canBeEscorted;
	}
	
	public int getVoter_id() {
		return voter_id;
	}
	public boolean isCanBeEscorted() {
		return canBeEscorted;
	}
}
