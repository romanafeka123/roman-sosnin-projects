package BL;

import java.util.ArrayList;
//////////////////////////PRIEL /////////////////////////////////
public class VoterRegistry {
	private ArrayList<Voter> voters;

	public VoterRegistry() {
	}

	public ArrayList<Voter> getVoters() {
		return voters;
	}

	public void setVoters(ArrayList<Voter> voters) {
		this.voters = voters;
	}
	
	public boolean checkVoterID(int currentVoterID) {
		for (int i = 0; i < voters.size(); i++) {
			Voter v = voters.get(i);
			if (v.getVoter_id() == currentVoterID) {
				return true;
			}
		}
		return false;
	}
	
	public void removeVoterFromRegistry(int currentVoterID) {
		for (int i = 0; i < voters.size(); i++) {
			Voter v = voters.get(i);
			if (v.getVoter_id() == currentVoterID) {
				voters.remove(i);
			}
		}
	}
}
