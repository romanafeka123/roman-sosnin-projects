package Messages;

import java.io.Serializable;
//////////////////////////////ROTEM////////////////////
public class ValidateBallotAreaIDAnswer implements Serializable {
	private static final long serialVersionUID = -1897963546969170160L;
	private boolean ballot_area_exists;

	public ValidateBallotAreaIDAnswer(boolean ballot_area_exists) {
		this.ballot_area_exists = ballot_area_exists;
	}

	public boolean isBallot_area_exists() {
		return ballot_area_exists;
	}

	public void setBallot_area_exists(boolean ballot_area_exists) {
		this.ballot_area_exists = ballot_area_exists;
	}
}
