package Messages;

import java.io.Serializable;
//////////////////////////////ROTEM////////////////////
public class ValidateCorrectBallotAreaIDRequest implements Serializable {
	private static final long serialVersionUID = 8945213642052907305L;
	private int ballot_area_id;

	public ValidateCorrectBallotAreaIDRequest(int ballot_area_id) {
		super();
		this.ballot_area_id = ballot_area_id;
	}

	public int getBallot_area_id() {
		return ballot_area_id;
	}
}
