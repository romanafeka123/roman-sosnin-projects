package Messages;

import java.io.Serializable;
//////////////////////////GUY /////////////////////////////////
public class RemoveVoterMessage implements Serializable{
	private static final long serialVersionUID = -8414471736965731953L;
	private int voter_id;

	public RemoveVoterMessage(int voter_id) {
		this.voter_id = voter_id;
	}

	public int getVoter_id() {
		return voter_id;
	}
}
