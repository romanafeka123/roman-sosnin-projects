package Messages;

import java.io.Serializable;
//////////////////////////////ROTEM////////////////////
public class OpenSystemForVotingMessage implements Serializable {
	private static final long serialVersionUID = 166163374761653526L;
	
	private int ballot_area_id;

	public OpenSystemForVotingMessage() {
	}

	public int getBallot_area_id() {
		return ballot_area_id;
	}

	public void setBallot_area_id(int ballot_area_id) {
		this.ballot_area_id = ballot_area_id;
	}
}
