package Messages;

import java.io.Serializable;
//////////////////////////////ROTEM////////////////////
public class VoterRegistryRequest implements Serializable{
	private static final long serialVersionUID = 5222264901095067463L;
	private int ballot_area_id;

	public VoterRegistryRequest(int ballot_area_id) {
		this.ballot_area_id = ballot_area_id;
	}

	public int getBallot_area_id() {
		return ballot_area_id;
	}
}
