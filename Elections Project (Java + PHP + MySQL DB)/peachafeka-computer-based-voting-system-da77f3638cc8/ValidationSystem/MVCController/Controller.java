package MVCController;
//////////////////////////ROMAN /////////////////////////////////
import GUI.ValidatorMainFrame;
import Listeners.BLEventsListeners;
import Listeners.GUIEventsListener;
import Messages.VoterRegistryRequest;
import Server.ValidationStand;

public class Controller implements GUIEventsListener, BLEventsListeners {

	private ValidationStand validator;
	private ValidatorMainFrame frame;

	public Controller(ValidationStand validator, ValidatorMainFrame frame) {
		this.validator = validator;
		this.frame = frame;
		this.validator.registerListener(this);
		this.frame.registerListener(this);
	}

	@Override
	public void voterRegistryRequest(VoterRegistryRequest msg) {
		validator.voterRegistryRequestFired(msg);
	}

	@Override
	public void openSystemForVotingMessage(int currentVoterID, int currentEscortID, boolean isCitizen, String currentEscortWorkplace, boolean isBeingEscorted, int ballot_area_id) {
		validator.openSystemForVotingMessageFired(currentVoterID, currentEscortID, isCitizen, currentEscortWorkplace, isBeingEscorted, ballot_area_id);
	}

	@Override
	public void checkEscortInDBEvent(int voter_id, int escort_id, String currentEscortWorkplace) {
		validator.checkEscortInDBEventFired(voter_id, escort_id, currentEscortWorkplace);
	}

	@Override
	public void validateCorrectBallotAreaIDEvent(int ballot_area_id) {
		validator.validateCorrectBallotAreaIDEventFired(ballot_area_id);
	}

	@Override
	public void disableBallotAreaIDOptions() {
		frame.disableBallotAreaIDOptions();
	}

	@Override
	public boolean checkVoterIDEvent(int voter_id) {
		return validator.getVoterRegistry().checkVoterID(voter_id);
	}
}
