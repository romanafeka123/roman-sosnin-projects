package Listeners;
//////////////////////////ROMAN /////////////////////////////////
import Messages.VoterRegistryRequest;

public interface GUIEventsListener {
	public void voterRegistryRequest(VoterRegistryRequest msg);
	public void openSystemForVotingMessage(int currentVoterID, int currentEscortID, boolean isCitizen, String currentEscortWorkplace, boolean isBeingEscorted, int ballot_area_id);
	public void checkEscortInDBEvent(int voter_id, int escort_id, String currentEscortWorkplace);
	public void validateCorrectBallotAreaIDEvent(int ballot_area_id);
	public boolean checkVoterIDEvent(int voter_id);
}
