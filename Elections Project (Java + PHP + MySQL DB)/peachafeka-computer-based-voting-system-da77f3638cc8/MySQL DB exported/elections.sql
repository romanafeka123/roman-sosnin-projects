-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2015 at 10:15 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `elections`
--
CREATE DATABASE IF NOT EXISTS `elections` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `elections`;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `add_vote`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_vote`(IN `party_id_in` INT(3), IN `ballot_area_id_in` INT(4))
    NO SQL
begin
declare check_if_exists INT(6);

select count(votes_amount) into check_if_exists
from ballot_box as b
where b.party_id = party_id_in AND
	  b.ballot_area_id = ballot_area_id_in;
      
if (check_if_exists = 0) then
	insert into ballot_box
    values (party_id_in, ballot_area_id_in, 0);
end if;


update ballot_box
set votes_amount = votes_amount + 1
where ballot_box.party_id = party_id 
  AND ballot_box.ballot_area_id = ballot_area_id;
 
end$$

DROP PROCEDURE IF EXISTS `update_times_escorted`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_times_escorted`(IN `id_in` INT(9))
    NO SQL
UPDATE escort_registry
SET times_escorted = times_escorted + 1
WHERE id = id_in$$

--
-- Functions
--
DROP FUNCTION IF EXISTS `check_escort_voter_workplaces`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `check_escort_voter_workplaces`(`voter_id` INT(9), `escort_id` INT(9)) RETURNS tinyint(1)
    NO SQL
begin 
	declare res              INT(1);
    declare voter_workplace  VARCHAR(25);
    declare escort_workplace VARCHAR(25);
    
    
	select work_place into voter_workplace
    from person p, voter_registry vr
    where p.id = vr.id AND vr.id = voter_id;
    
    select work_place into escort_workplace
    from person p, escort_registry er
    where p.id = er.id AND er.id = escort_id;
    
    if (voter_workplace = escort_workplace) then
    	set res = 1;
    elseif (voter_workplace != escort_workplace) then
    	set res = 0;
    end if;
    
    return res; 
end$$

DROP FUNCTION IF EXISTS `mandates_calculation`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `mandates_calculation`(`votes_received` INT(6)) RETURNS int(3)
    NO SQL
begin 
	declare res int(6);
    
	select distinct votes_received / (total_votes_amount()/120) 	into res 
	from ballot_box; 
    
    return res; 
end$$

DROP FUNCTION IF EXISTS `total_votes_amount`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `total_votes_amount`() RETURNS int(6)
    NO SQL
begin
  declare res int(6);

  select sum(votes_amount) into res
  from ballot_box;

  return res;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ballot_area`
--

DROP TABLE IF EXISTS `ballot_area`;
CREATE TABLE IF NOT EXISTS `ballot_area` (
  `id` int(4) NOT NULL,
  `city` varchar(20) NOT NULL,
  `street` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ballot_area`
--

INSERT INTO `ballot_area` (`id`, `city`, `street`) VALUES
(1122, 'Tel Aviv', 'Dizengoff 45'),
(1177, 'Kfar Sava', 'Rothschild 67'),
(3344, 'Rishon Le Zion', 'Herzel 12');

-- --------------------------------------------------------

--
-- Table structure for table `ballot_box`
--

DROP TABLE IF EXISTS `ballot_box`;
CREATE TABLE IF NOT EXISTS `ballot_box` (
  `party_id` int(3) NOT NULL,
  `ballot_area_id` int(4) NOT NULL,
  `votes_amount` int(6) NOT NULL,
  PRIMARY KEY (`party_id`,`ballot_area_id`),
  KEY `fk3_ballot_area_id` (`ballot_area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ballot_box`
--

INSERT INTO `ballot_box` (`party_id`, `ballot_area_id`, `votes_amount`) VALUES
(100, 1122, 73),
(100, 1177, 158),
(100, 3344, 3),
(101, 1177, 45),
(101, 3344, 86),
(102, 1122, 146),
(102, 3344, 71),
(103, 1177, 82),
(103, 3344, 111),
(104, 1122, 34),
(104, 1177, 83),
(104, 3344, 102),
(105, 1122, 2),
(105, 1177, 69),
(105, 3344, 46),
(106, 1122, 52),
(106, 3344, 33);

--
-- Triggers `ballot_box`
--
DROP TRIGGER IF EXISTS `ballot_box_delete_trg`;
DELIMITER //
CREATE TRIGGER `ballot_box_delete_trg` BEFORE DELETE ON `ballot_box`
 FOR EACH ROW begin
    declare msg varchar(40);
   	set msg = 'Impossible to delete entries from ballot box table!';
    signal sqlstate '45000' set message_text = msg;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `escort_registry`
--

DROP TABLE IF EXISTS `escort_registry`;
CREATE TABLE IF NOT EXISTS `escort_registry` (
  `id` int(9) NOT NULL,
  `is_citizen` tinyint(1) NOT NULL,
  `times_escorted` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `escort_registry`
--

INSERT INTO `escort_registry` (`id`, `is_citizen`, `times_escorted`) VALUES
(123, 0, 1),
(111444555, 1, 1),
(222333444, 1, 1),
(333555888, 1, 1),
(888555333, 1, 2);

--
-- Triggers `escort_registry`
--
DROP TRIGGER IF EXISTS `check_times_escorted_ins_trg`;
DELIMITER //
CREATE TRIGGER `check_times_escorted_ins_trg` BEFORE INSERT ON `escort_registry`
 FOR EACH ROW set NEW.times_escorted = 0
//
DELIMITER ;
DROP TRIGGER IF EXISTS `check_times_escorted_upd_trg`;
DELIMITER //
CREATE TRIGGER `check_times_escorted_upd_trg` BEFORE UPDATE ON `escort_registry`
 FOR EACH ROW if NEW.times_escorted > 2 then 
	set NEW.times_escorted = 2;
elseif NEW.times_escorted < 0 then
    set NEW.times_escorted = 0;
end if
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `parties`
--

DROP TABLE IF EXISTS `parties`;
CREATE TABLE IF NOT EXISTS `parties` (
  `party_id` int(3) NOT NULL,
  `party_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abbreviation` varchar(10) NOT NULL,
  PRIMARY KEY (`party_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parties`
--

INSERT INTO `parties` (`party_id`, `party_name`, `abbreviation`) VALUES
(100, 'Likud', 'mahal'),
(101, 'Ha Mahane Ha Zioni', 'emet'),
(102, 'Yesh Atid', 'po'),
(103, 'Kulanu', 'kaf'),
(104, 'Ha Bait Ha Yehudi', 'tav'),
(105, 'Israel Beiteinu', 'lamed'),
(106, 'Ha Smol Shel Israel', 'merez');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(9) NOT NULL,
  `work_place` varchar(25) DEFAULT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `work_place`, `first_name`, `last_name`) VALUES
(123, 'Sababa', 'NotCitizen', 'NotCitizen'),
(123321, 'MyWorkPlace', 'NotCitizen', 'NotCitizen'),
(111222333, 'Microsoft', 'Roman', 'Sosnin'),
(111444555, 'Microsoft', 'Guy', 'Tuval'),
(111888999, 'World Snooker', 'Judd', 'Trump'),
(123123123, 'World Snooker', 'Ronnie', 'Osullivan'),
(123321125, 'asdasds', 'gggg', 'hhhh'),
(222333444, 'Matrix', 'Johny', 'Walker'),
(333555888, 'Intel', 'Michael', 'Jones'),
(444555666, 'Intel', 'Rotem', 'Rousseau'),
(888555333, 'Matrix', 'Priel', 'Smadja');

-- --------------------------------------------------------

--
-- Stand-in structure for view `results_view`
--
DROP VIEW IF EXISTS `results_view`;
CREATE TABLE IF NOT EXISTS `results_view` (
`party_name` varchar(20)
,`abbreviation` varchar(10)
,`mandates_amount` int(3)
,`percentage` decimal(38,2)
,`votes_amount` decimal(32,0)
);
-- --------------------------------------------------------

--
-- Table structure for table `voter_registry`
--

DROP TABLE IF EXISTS `voter_registry`;
CREATE TABLE IF NOT EXISTS `voter_registry` (
  `id` int(9) NOT NULL,
  `can_be_escorted` tinyint(1) NOT NULL,
  `ballot_area_id` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk2_ballot_area_id` (`ballot_area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voter_registry`
--

INSERT INTO `voter_registry` (`id`, `can_be_escorted`, `ballot_area_id`) VALUES
(111222333, 1, 1177),
(111444555, 0, 1122),
(111888999, 1, 1177),
(444555666, 0, 1177);

-- --------------------------------------------------------

--
-- Structure for view `results_view`
--
DROP TABLE IF EXISTS `results_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `results_view` AS select `p`.`party_name` AS `party_name`,`p`.`abbreviation` AS `abbreviation`,`mandates_calculation`(sum(`bb`.`votes_amount`)) AS `mandates_amount`,round(((sum(`bb`.`votes_amount`) / `total_votes_amount`()) * 100),2) AS `percentage`,sum(`bb`.`votes_amount`) AS `votes_amount` from (`ballot_box` `bb` join `parties` `p`) where (`bb`.`party_id` = `p`.`party_id`) group by `p`.`party_name`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ballot_box`
--
ALTER TABLE `ballot_box`
  ADD CONSTRAINT `fk3_ballot_area_id` FOREIGN KEY (`ballot_area_id`) REFERENCES `ballot_area` (`id`),
  ADD CONSTRAINT `fk_party_id` FOREIGN KEY (`party_id`) REFERENCES `parties` (`party_id`);

--
-- Constraints for table `escort_registry`
--
ALTER TABLE `escort_registry`
  ADD CONSTRAINT `fk_escort_id` FOREIGN KEY (`id`) REFERENCES `person` (`id`);

--
-- Constraints for table `voter_registry`
--
ALTER TABLE `voter_registry`
  ADD CONSTRAINT `fk2_ballot_area_id` FOREIGN KEY (`ballot_area_id`) REFERENCES `ballot_area` (`id`),
  ADD CONSTRAINT `fk_voter_id` FOREIGN KEY (`id`) REFERENCES `person` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
