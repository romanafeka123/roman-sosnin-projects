package BL;

public class Party {
//////////////////////////PRIEL /////////////////////////////////
	private String name;
	private int id;
	private String image_src;
	
	public Party(String name, int id, String image_src) {
		this.name = name;
		this.id = id;
		this.image_src = image_src;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getImage_src() {
		return image_src;
	}
	public void setImage_src(String image_src) {
		this.image_src = image_src;
	}
}
