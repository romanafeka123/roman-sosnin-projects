package BL;

import java.util.ArrayList;
//////////////////////////PRIEL /////////////////////////////////
public class PartyList {
	
	public static ArrayList<Party> parties = new ArrayList<Party>();	
	
	public static void addEngParties() {
		parties.clear();
		parties.add(new Party("Likud", 				100, "images/mahal.jpg"));
		parties.add(new Party("Ha Mahane Ha Zioni", 101, "images/mahane_zioni.jpg"));
		parties.add(new Party("Yesh Atid",			102, "images/yesh_atid.jpg"));
		parties.add(new Party("Kulanu",				103, "images/kulanu.jpg"));
		parties.add(new Party("Ha Bait Ha Yehudi",	104, "images/bait_yehudi.jpg"));
		parties.add(new Party("Israel Beiteinu",	105, "images/liberman.png"));
		parties.add(new Party("Ha Smol Shel Israel",106, "images/merez.JPG"));
	}
	
	public static void addHebParties() {
		parties.clear();
		parties.add(new Party("ליכוד",				100, "images/mahal.jpg"));
		parties.add(new Party("המחנה הציוני",			101, "images/mahane_zioni.jpg")); 
		parties.add(new Party("יש עתיד",				102, "images/yesh_atid.jpg"));
		parties.add(new Party("כולנו",				103, "images/kulanu.jpg"));
        parties.add(new Party("הבית היהודי",			104, "images/bait_yehudi.jpg"));
        parties.add(new Party("ישראל ביתינו",			105, "images/liberman.png"));
        parties.add(new Party("השמאל של ישראל",	106, "images/merez.JPG"));
	}

	public static void addRusParties() {
		parties.clear();
		parties.add(new Party("Ликуд",				100, "images/mahal.jpg"));
		parties.add(new Party("Cионистский лагерь",	101, "images/mahane_zioni.jpg"));  
		parties.add(new Party("Ешь атид",			102, "images/yesh_atid.jpg"));
		parties.add(new Party("Куляну",				103, "images/kulanu.jpg"));
        parties.add(new Party("Еврейский дом",		104, "images/bait_yehudi.jpg"));
        parties.add(new Party("Израиль наш дом",	105, "images/liberman.png"));
        parties.add(new Party("Левый Израиль",		106, "images/merez.JPG"));
	}
}
