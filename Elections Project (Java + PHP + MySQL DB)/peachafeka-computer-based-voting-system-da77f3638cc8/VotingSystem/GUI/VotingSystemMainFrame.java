package GUI;
import java.util.ArrayList;

import javax.swing.JFrame;

import BL.PartyList;
import Client.ClientCommunication;
import Listeners.GUIEventsListener;
import MVCController.Controller;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
//////////////////////////ROMAN /////////////////////////////////
public class VotingSystemMainFrame {
	
	
	private ClientCommunication validatorCommunication;
	private ClientCommunication serverCommunication;
	private String[] serverIPAddresses;
	private static final int NUM_OF_SERVERS_TO_SUBMIT_VOTE_TO = 6;
	private ArrayList<GUIEventsListener> listeners = new ArrayList<GUIEventsListener>();
	
	private PoliticalViewsPopUpDialog dialog;
	private VBox vbox;
	
	private Button submitBtn;
	private Label chooseLbl;
	private ComboBox<String> partiesComboBox;
	private RadioButton hebRadBtn;
	private RadioButton engRadBtn ;
	private RadioButton rusRadBtn;
	private HBox langHBox = new HBox();	
	
	private String choosenLanguage = "Hebrew";	
	private int ballot_area_id;
	private int chosenPartyID;
	
	private ImageView imv;
	private Image image;
	
	private JFrame frame;
	private JFXPanel fxPanel;
 
    public VotingSystemMainFrame() {   	
    	frame = new JFrame("תהליך הבחירה");
        fxPanel = new JFXPanel();
        frame.setContentPane(fxPanel);
        frame.setSize(300, 430);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    	
        vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(20);
        vbox.setId("pane");
        
        createComponents();
        PartyList.addHebParties();
        setPropertiesToComponents();
        setListeners();
        hebRadBtn.setSelected(true);
        vbox.getChildren().addAll(langHBox, chooseLbl, partiesComboBox);

        addImage("images/box.jpg");
        
        vbox.getChildren().add(submitBtn);
         
        Platform.runLater(new Runnable() {
            @Override
            public void run() {   
                Scene scene = new Scene(vbox);
                scene.getStylesheets().add(VotingSystemMainFrame.class.getResource("TheStyle.css").toExternalForm());
                fxPanel.setScene(scene);
            }
       });
        
        setServerIPAddresses();
        
        validatorCommunication = new ClientCommunication("localhost", 7000);
        validatorCommunication.connect();
        validatorCommunication.listenToMessagesFromValidator();
		
		@SuppressWarnings("unused")
		Controller cont = new Controller(validatorCommunication, this);		
    }
    
    private void createComponents() {
    	submitBtn = new Button();
    	chooseLbl = new Label();
    	partiesComboBox = new ComboBox<String>();
    	hebRadBtn = new RadioButton("עברית");
    	engRadBtn = new RadioButton("English");
    	rusRadBtn = new RadioButton("Русский");
    	langHBox = new HBox();	
    }
	
	private void setListeners() {
        submitBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (partiesComboBox.getValue() == null) {
					if (choosenLanguage.equals("Hebrew")) {
						PopUpDialog.openPopUpDialog("שגיאה", "תבחרו במפלגה!");
					}
					else if (choosenLanguage.equals("English")) {
						PopUpDialog.openPopUpDialog("Error", "Choose a party!");
					}
					else if (choosenLanguage.equals("Russian")) {
						PopUpDialog.openPopUpDialog("Ошибка", "Выберете партию!");				
					}
				}
				else {
					if (choosenLanguage.equals("Hebrew")) {
						dialog = new PoliticalViewsPopUpDialog("האם אחד החברי וועדת קלפי ניסה לשנות את הדעה הפוליטית שלך?", choosenLanguage);	
					}
					else if (choosenLanguage.equals("English")) {
						dialog = new PoliticalViewsPopUpDialog("Did one of the committee members try to change your political views?", choosenLanguage);	
					}
					else if (choosenLanguage.equals("Russian")) {
						dialog = new PoliticalViewsPopUpDialog("Пытались ли члены комитета изменить ваши политические взгляды?", choosenLanguage);	
					}
					new Thread(new Runnable() {
						@Override
						public void run() {		
							submitVoteToServer();
							sendVoteSentApprovalToValidator();
						}				
					}).start();					
					frame.setVisible(false);
				}
			}
		});
        
        partiesComboBox.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (partiesComboBox.getItems().size() != 0) {
					if (partiesComboBox.getValue().equals(PartyList.parties.get(0).getName())) {
						setImage(PartyList.parties.get(0).getImage_src());
					} else if (partiesComboBox.getValue().equals(PartyList.parties.get(1).getName())) {
						setImage(PartyList.parties.get(1).getImage_src());
					} else if (partiesComboBox.getValue().equals(PartyList.parties.get(2).getName())) {
						setImage(PartyList.parties.get(2).getImage_src());
					} else if (partiesComboBox.getValue().equals(PartyList.parties.get(3).getName())) {
						setImage(PartyList.parties.get(3).getImage_src());
					} else if (partiesComboBox.getValue().equals(PartyList.parties.get(4).getName())) {
						setImage(PartyList.parties.get(4).getImage_src());
					} else if (partiesComboBox.getValue().equals(PartyList.parties.get(5).getName())) {
						setImage(PartyList.parties.get(5).getImage_src());
					} else if (partiesComboBox.getValue().equals(PartyList.parties.get(6).getName())) {
						setImage(PartyList.parties.get(6).getImage_src());
					}
				}
			}
		});
        
        hebRadBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (hebRadBtn.isSelected()) {
					partiesComboBox.getItems().clear();
					PartyList.addHebParties();				
					for (int i = 0; i < PartyList.parties.size(); i++) {
						partiesComboBox.getItems().add(PartyList.parties.get(i).getName());
					}			
					choosenLanguage = "Hebrew";
					submitBtn.setText("שלח");
					chooseLbl.setText("בחרו מפלגה");
					frame.setTitle("תהליך הבחירה");
				}
			}
		});
        
        engRadBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {		
				partiesComboBox.getItems().clear();
				PartyList.addEngParties();
				for (int i = 0; i < PartyList.parties.size(); i++) {
					partiesComboBox.getItems().add(PartyList.parties.get(i).getName());
				}
				choosenLanguage = "English";
				submitBtn.setText("Submit");
				chooseLbl.setText("Choose a party");	
				frame.setTitle("Voting process");
			}
		});
        
        rusRadBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				partiesComboBox.getItems().clear();
				PartyList.addRusParties();
				for (int i = 0; i < PartyList.parties.size(); i++) {
					partiesComboBox.getItems().add(PartyList.parties.get(i).getName());
				}
				choosenLanguage = "Russian";
				submitBtn.setText("Отправить");
				chooseLbl.setText("Выберете партию");
				frame.setTitle("Процесс голосования");
			}
		});
	}
	
	private void addImage(String src) {
		imv = new ImageView();
		image = new Image(VotingSystemMainFrame.class.getResourceAsStream(src));
		imv.setImage(image);
		vbox.getChildren().addAll(imv);	
	}
	
	private void setImage(String src) {
		image = new Image(VotingSystemMainFrame.class.getResourceAsStream(src));
		imv.setImage(image);	
	}
	
	private void setPropertiesToComponents() {
		langHBox.setAlignment(Pos.CENTER);
		langHBox.setSpacing(20);
		langHBox.setId("pane");
		
		ToggleGroup group = new ToggleGroup();	
		hebRadBtn.setToggleGroup(group);
		engRadBtn.setToggleGroup(group);
		rusRadBtn.setToggleGroup(group);

		langHBox.getChildren().addAll(hebRadBtn, engRadBtn, rusRadBtn);
		
		chooseLbl.setText("בחרו מפלגה");
		chooseLbl.setId("choose-label");
		for (int i = 0; i < PartyList.parties.size(); i++) {
			partiesComboBox.getItems().addAll(PartyList.parties.get(i).getName());
		}
        partiesComboBox.setEditable(true);       
        
        submitBtn.setText("שלח");
        submitBtn.setId("just-btn");
	}
	
	private void submitVoteToServer() {
		// looking for the chosen party ID
		for (int i = 0; i < PartyList.parties.size(); i++) {
			if (PartyList.parties.get(i).getName().equals(partiesComboBox.getValue())) {
				chosenPartyID = PartyList.parties.get(i).getId();
			}
		}
		for (int i = 0; i <= NUM_OF_SERVERS_TO_SUBMIT_VOTE_TO; i++) {
			serverCommunication = new ClientCommunication(serverIPAddresses[i], (8001));
	        serverCommunication.connect();
	        if (serverCommunication.isConnected()) {
	        	serverCommunication.sendVoteToServer(chosenPartyID, ballot_area_id);
	        	serverCommunication.disconnect();
	        	return;
	        }
		}
	}
	
	public void setServerIPAddresses() {
		String server_1_IP = "localhost";
		String server_2_IP = "111.111.111.111";
		String server_3_IP = "222.222.222.222";
		String server_4_IP = "333.333.333.333";
		String server_5_IP = "444.444.444.444";
		String server_6_IP = "555.555.555.555";
		serverIPAddresses = new String[6];
		serverIPAddresses[0] = server_1_IP;
		serverIPAddresses[1] = server_2_IP;
		serverIPAddresses[2] = server_3_IP;
		serverIPAddresses[3] = server_4_IP;
		serverIPAddresses[4] = server_5_IP;
		serverIPAddresses[5] = server_6_IP;
	}
	
	private void sendVoteSentApprovalToValidator() {
		validatorCommunication.sendVoteSentApprovalMessage();
	}
	
	public void registerListener(GUIEventsListener listener) {
		listeners.add(listener);
	}
	
	public void openSystemForVotingEventFired(int ballot_area_id) {
		this.ballot_area_id = ballot_area_id;
        frame.setVisible(true);
	}

    public static void main(String[] args) {
    	VotingSystemMainFrame frame = new VotingSystemMainFrame();
    }
}