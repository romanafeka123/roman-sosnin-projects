package GUI;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
//////////////////////////ROMAN /////////////////////////////////
public class PoliticalViewsPopUpDialog {
	
	private RadioButton yesRadBtn;
	private RadioButton noRadBtn;
	private VBox centralVBox = new VBox();
	private HBox radBtnHBox = new HBox();
	private Button btn;
	private Stage dialogStage = new Stage();
	private String language;

	public PoliticalViewsPopUpDialog(String str, String lang) {	
		language = lang;
		dialogStage.setResizable(false);	
		Text text = new Text(str);
		btn = new Button("   OK   ");
		btn.setId("just-btn");
		text.setId("pop-up-dialog-text");
		
		radBtnHBox.setAlignment(Pos.CENTER);
		radBtnHBox.setSpacing(80);
		
		ToggleGroup group = new ToggleGroup();
		if (language.equals("Hebrew")) {
			yesRadBtn = new RadioButton("כן");
			noRadBtn = new RadioButton("לא");
		}
		else if (language.equals("English")) {
			yesRadBtn = new RadioButton("Yes");
			noRadBtn = new RadioButton("No");
		}
		else if (language.equals("Russian")) {
			yesRadBtn = new RadioButton("Да");
			noRadBtn = new RadioButton("Нет");
		}
		yesRadBtn.setId("pop-up-dialog-text");
		noRadBtn.setId("pop-up-dialog-text");
		yesRadBtn.setToggleGroup(group);
		noRadBtn.setToggleGroup(group);
		
		radBtnHBox.getChildren().addAll(yesRadBtn, noRadBtn);
		
		addListeners();

		centralVBox.setId("pane");
		centralVBox.setAlignment(Pos.CENTER);
		centralVBox.setSpacing(10);
		centralVBox.getChildren().addAll(text, radBtnHBox, btn);
		Scene scene = new Scene(centralVBox, 600, 120);
		scene.getStylesheets().add(VotingSystemMainFrame.class.getResource("TheStyle.css").toExternalForm());
		dialogStage.setScene(scene);
		dialogStage.show();
	}
	
	private void addListeners() {
		btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	if (!yesRadBtn.isSelected() && !noRadBtn.isSelected()) {
            		if (language.equals("Hebrew")) {
            			PopUpDialog.openPopUpDialog("שגיאה", "תבחרו באופציה כן או לא!");
            		}
            		if (language.equals("English")) {
            			PopUpDialog.openPopUpDialog("Error", "Choose yes or no!");
            		}
            		if (language.equals("Russian")) {
            			PopUpDialog.openPopUpDialog("Ошибка", "Выберете да или нет");
            		}
            	}
            	else {
            		dialogStage.close();
            		if (language.equals("Hebrew")) {
            			PopUpDialog.openPopUpDialog("שליחה", "הקול שלך נשלח!");
            		}
            		if (language.equals("English")) {
            			PopUpDialog.openPopUpDialog("Submit", "Your vote has been submitted");
            		}
            		if (language.equals("Russian")) {
            			PopUpDialog.openPopUpDialog("Отправка", "Ваш голос был отправлен!");
            		}
            	}
            }
        });
	}
}
