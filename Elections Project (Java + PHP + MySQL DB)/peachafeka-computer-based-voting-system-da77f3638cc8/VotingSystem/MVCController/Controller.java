package MVCController;
//////////////////////////ROMAN /////////////////////////////////
import Client.ClientCommunication;
import GUI.VotingSystemMainFrame;
import Listeners.ClientEventsListeners;
import Listeners.GUIEventsListener;

public class Controller implements GUIEventsListener, ClientEventsListeners {

	private ClientCommunication comm;
	private VotingSystemMainFrame frame;

	public Controller(ClientCommunication comm, VotingSystemMainFrame frame) {
		this.comm = comm;
		this.frame = frame;
		
		this.comm.registerListener(this);
		this.frame.registerListener(this);
	}

	@Override
	public void openSystemForVotingEvent(int ballot_area_id) {
		frame.openSystemForVotingEventFired(ballot_area_id);
	}
}
