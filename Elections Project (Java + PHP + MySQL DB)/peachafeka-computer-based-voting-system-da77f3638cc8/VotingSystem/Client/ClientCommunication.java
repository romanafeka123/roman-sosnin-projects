package Client;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;




import Listeners.ClientEventsListeners;
import Messages.AddVoteMessage;
import Messages.CloseConnectionRequest;
import Messages.OpenSystemForVotingMessage;
import Messages.VoteSentApprovalMessage;
////////////////////////// ROMAN /////////////////////////////////
public class ClientCommunication {

	private final int SERVER_PORT;
	private final String SERVER_ADDRESS;
	private Socket socket;
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;
	private boolean connectedToServer;
	private Vector<ClientEventsListeners> listeners = new Vector<ClientEventsListeners>();

	public ClientCommunication(String addr, int port) {
		SERVER_ADDRESS = addr;
		SERVER_PORT = port;
	}

	public boolean connect() {
		try {
			socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
			if (!socket.isConnected()) {
				return false;
			}
			outStream = new ObjectOutputStream(socket.getOutputStream());
			inStream = new ObjectInputStream(socket.getInputStream());
			connectedToServer = true;
			System.out.println("Connected to Validator");
		} catch (IOException e) {
			connectedToServer = false;
		}
		return connectedToServer;
	}

	
	public boolean disconnect() {
		try {
			outStream.writeObject(new CloseConnectionRequest());
			connectedToServer = false;
		} catch (IOException e) {
		}
		return connectedToServer;
	}

	public void sendVoteToServer(int party_id, int ballot_area_id) {
		if (!isConnected())
			return;
		AddVoteMessage msg = new AddVoteMessage(party_id, ballot_area_id);
		try {
			outStream.writeObject(msg);
		} catch (IOException e) {
		}
	}
	
	public void sendVoteSentApprovalMessage() {
		if (!isConnected())
			return;
		VoteSentApprovalMessage msg = new VoteSentApprovalMessage();
		try {
			System.out.println("Sending apporoval to validator");
			outStream.writeObject(msg);
		} catch (IOException e) {
		}
	}

	public void listenToMessagesFromValidator() {
		if (!isConnected()) {
			return;
		}
		// this thread receives all the messages from validator (Parties List, Open System for voting)
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					boolean connected = true;
					do {
						Object obj = inStream.readObject();
						if (obj instanceof CloseConnectionRequest) {
							connectedToServer = false;
							socket.close();
						} 
						else if (obj instanceof OpenSystemForVotingMessage) {
							OpenSystemForVotingMessage msg = (OpenSystemForVotingMessage)obj;
							fireOpenSystemForVotingEvent(msg.getBallot_area_id());
						}
					} while (connected);
				} catch (EOFException e) {
					connectedToServer = false;
				} catch (ClassNotFoundException e) {
				} catch (IOException e) {
				} finally {
					try {
						socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	protected void fireOpenSystemForVotingEvent(int ballot_area_id) {
		for (ClientEventsListeners l : listeners)
			l.openSystemForVotingEvent(ballot_area_id);
	}
	
	public void registerListener(ClientEventsListeners listener) {
		listeners.add(listener);
	}

	public boolean isConnected() {
		return connectedToServer;
	}
}
