package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import BL.Cart;
import BL.Invoice;
import BL.Main;
import BL.Order;
import BL.OrderComparatorByID_Descending;
import BL.Status;
import DAL.InvoiceDB;
import DAL.OrderDB;

public class InvoiceReceptionFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private ImageIcon iconSales, iconInv;
	private URL url;
	private JButton recInvBtn;
	private JLabel lbl1, lbl2;
	private JLabel chooseOrdLbl;
	private JComboBox<Order> ordersCmbBox;
	private JPanel mainPanel;

	public InvoiceReceptionFrame(final Connection con) {
		setTitle("Receive Invoice");
		setSize(370, 540);
		setLocation(350, 100);
		setResizable(false);

		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);
		
		url = getClass().getResource("image/inv.jpg");
		iconInv = new ImageIcon(url);

		mainPanel = new JPanel(new BorderLayout());

		lbl1 = new JLabel(iconSales);
		lbl2 = new JLabel(iconInv);
		chooseOrdLbl = new JLabel("Choose an order to make an invoice for");
		chooseOrdLbl.setFont(new Font("Arial", Font.BOLD, 18));
		recInvBtn = new JButton("Make Invoice");
		recInvBtn.setFont(new Font("Arial", Font.BOLD, 18));

		ordersCmbBox = new JComboBox<Order>();
		Order[] ord = OrderDB.getOrdersFromDB(con, Status.open);
		OrderComparatorByID_Descending comp = new OrderComparatorByID_Descending();
		Main.mergeSort(ord, comp);
		for (int i = 0; i < ord.length; i++) {
			ordersCmbBox.addItem(ord[i]);
		}

		JPanel midPnl = new JPanel();
		SpringLayout sprLayout = new SpringLayout();
		midPnl.setLayout(sprLayout);
		midPnl.setBackground(Color.CYAN);

		JPanel southPanel = new JPanel();
		southPanel.setBackground(Color.CYAN);

		midPnl.add(chooseOrdLbl);
		midPnl.add(ordersCmbBox);	
		midPnl.add(lbl2);

		sprLayout.putConstraint(SpringLayout.WEST, chooseOrdLbl, 10,SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, chooseOrdLbl, 30,SpringLayout.NORTH, midPnl);
		sprLayout.putConstraint(SpringLayout.WEST, ordersCmbBox, 12,SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, ordersCmbBox, 20,SpringLayout.SOUTH, chooseOrdLbl);
		sprLayout.putConstraint(SpringLayout.WEST, lbl2, 55,SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, lbl2, 20,SpringLayout.SOUTH, ordersCmbBox);

		southPanel.add(recInvBtn);

		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(midPnl, BorderLayout.CENTER);
		mainPanel.add(southPanel, BorderLayout.SOUTH);

		setContentPane(mainPanel);

		recInvBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {		
				// getting the chosen order details
				int order_id = ((Order)ordersCmbBox.getSelectedItem()).getOrder_id();
				String order_date = ((Order)ordersCmbBox.getSelectedItem()).getOrder_date();
				Cart cart = ((Order)ordersCmbBox.getSelectedItem()).getCart();
				Invoice inv = new Invoice(order_id, order_date, cart);
				boolean succeeded = InvoiceDB.writeNewInvoiceToDB(con, inv);
				closeFrame();
				if (succeeded) {
				JOptionPane.showMessageDialog(null,
						"The invoice was received successfully!",
						"Receive Invoice", JOptionPane.INFORMATION_MESSAGE);
				}
			} // actionPerformed
		});
	} // InvoiceReceptionFrame constructor

	private void closeFrame() {
		this.setVisible(false);
	} // closeFrame
	
}  // InvoiceReceptionFrame
