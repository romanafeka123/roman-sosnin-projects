package GUI;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import BL.Customer;
import BL.Item;
import BL.Main;

public class InvoicesFrame extends JFrame{

	static final long serialVersionUID = 1L;
	private ImageIcon iconOracle;
	private ImageIcon iconSales;
	private URL url;
	private JButton invReceptionBtn, cancelInvBtn;
	private JLabel lbl1;
	private JLabel lbl2;
	private JPanel mainPanel;
	private InvoiceReceptionFrame invRecFrame;
	private CancelInvoiceFrame cancelInvFrame;
	
	public InvoicesFrame(final Connection con, Customer[] custArr, Item[] itemArr) {
		setTitle("Invoices");
		setSize(370, 450);
		setLocation(750, 250);
		setResizable(false);
		mainPanel = new JPanel(new BorderLayout());
		lbl1 = new JLabel();
		lbl2 = new JLabel();
		
		JPanel btnPanel = new JPanel(new GridLayout(2, 1));
		
		url = getClass().getResource("image/oracle.jpg");
		iconOracle = new ImageIcon(url);
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);	
		
		lbl1.setIcon(iconSales);
		lbl2.setIcon(iconOracle);
		
		invReceptionBtn = new JButton("Invoice Reception");
		cancelInvBtn = new JButton("Cancel an Invoice");
		
		invReceptionBtn.setFont(new Font("Arial", Font.BOLD, 18));
		cancelInvBtn.setFont(new Font("Arial", Font.BOLD, 18));	

		btnPanel.add(invReceptionBtn);
		btnPanel.add(cancelInvBtn);
		
		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(btnPanel, BorderLayout.CENTER);
		mainPanel.add(lbl2, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		setVisible(true);
		
		invReceptionBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.drawOpenOrdersFromDB(con);
				if (Main.openOrders.size() != 0) {
					if (invRecFrame != null)
						invRecFrame.setVisible(false);
					invRecFrame = new InvoiceReceptionFrame(con);
					invRecFrame.setVisible(true);
				}
			} // actionPerformed
		});
		
		cancelInvBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.drawClosedOrdersFromDB(con);
				if (cancelInvFrame != null)
					cancelInvFrame.setVisible(false);
				cancelInvFrame = new CancelInvoiceFrame(con);
			} // actionPerformed
		});
	}  // MainFrame constructor
}  // MainFrame
