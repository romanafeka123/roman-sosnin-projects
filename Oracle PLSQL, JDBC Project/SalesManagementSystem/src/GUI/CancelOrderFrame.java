package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import BL.Main;
import BL.Order;
import BL.OrderComparatorByID_Descending;
import BL.Status;
import DAL.OrderDB;

public class CancelOrderFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private ImageIcon iconSales, iconCancel;
	private URL url;
	private JButton cancelOrdBtn;
	private JLabel lbl1, lbl2;
	private JLabel chooseOrdLbl;
	private JComboBox<Order> ordersCmbBox;
	private JPanel mainPanel;
	
	public CancelOrderFrame(final Connection con) {
		setTitle("Cancel an Order");
		setSize(370, 540);
		setLocation(350, 100);
		setResizable(false);
		
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);
		
		url = getClass().getResource("image/cancel.jpg");
		iconCancel = new ImageIcon(url);
		
		mainPanel = new JPanel(new BorderLayout());
		
		lbl1 = new JLabel(iconSales);
		lbl2 = new JLabel(iconCancel);
		chooseOrdLbl = new JLabel("Choose an order to cancel it");
		chooseOrdLbl.setFont(new Font("Arial", Font.BOLD, 18));
		cancelOrdBtn = new JButton("Cancel order");
		cancelOrdBtn.setFont(new Font("Arial", Font.BOLD, 18));
		
		ordersCmbBox = new JComboBox<Order>();
		Order[] ord = OrderDB.getOrdersFromDB(con, Status.open);
		OrderComparatorByID_Descending comp = new OrderComparatorByID_Descending();
		Main.mergeSort(ord, comp);
		for (int i = 0; i < ord.length; i++) {
			ordersCmbBox.addItem(ord[i]);
		}
		
		JPanel midPnl = new JPanel();
		SpringLayout sprLayout = new SpringLayout();
		midPnl.setLayout(sprLayout);
		midPnl.setBackground(Color.CYAN);
		
		JPanel southPanel = new JPanel();
		southPanel.setBackground(Color.CYAN);
		
		midPnl.add(chooseOrdLbl);
		midPnl.add(ordersCmbBox);
		midPnl.add(lbl2);
		
		sprLayout.putConstraint(SpringLayout.WEST, chooseOrdLbl, 50, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, chooseOrdLbl, 30, SpringLayout.NORTH, midPnl);
		sprLayout.putConstraint(SpringLayout.WEST, ordersCmbBox, 12, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, ordersCmbBox, 20, SpringLayout.SOUTH, chooseOrdLbl);
		sprLayout.putConstraint(SpringLayout.WEST, lbl2, 10,SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, lbl2, 15,SpringLayout.SOUTH, chooseOrdLbl);
        
		southPanel.add(cancelOrdBtn);
		
		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(midPnl, BorderLayout.CENTER);
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		
		cancelOrdBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.deleteOpenOrderFromMain((Order)ordersCmbBox.getSelectedItem());
				OrderDB.deleteOpenOrderFromDB(con, (Order)ordersCmbBox.getSelectedItem());		
				
				ordersCmbBox.removeAllItems();
				Order[] ord = OrderDB.getOrdersFromDB(con, Status.open);
				if (ord != null && ord.length != 0) {
					for (int i = 0; i < ord.length; i++) {
						ordersCmbBox.addItem(ord[i]);
					}
					JOptionPane.showMessageDialog(null,
							"The order was canceled successfully",
							"Cancel an Order", JOptionPane.INFORMATION_MESSAGE);
					if (ordersCmbBox.getSelectedItem() == null)
						closeFrame();
				}
				else
					closeFrame();
			} // actionPerformed
		});
	}  // CancelOrderFrame constructor
	
	private void closeFrame() {
			this.setVisible(false);
	}  // closeFrame
	
}  // CancelOrderFrame
