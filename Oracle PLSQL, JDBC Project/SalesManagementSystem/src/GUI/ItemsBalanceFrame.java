package GUI;

import java.awt.Color;
import java.awt.Font;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import DAL.StockBalanceDB;

public class ItemsBalanceFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	private JScrollPane scrPane;
	// this is for JTable
	private Object[][] dbInfo;
	private Object[] cols = {"Item_ID" ,"Description", "Total_Amount"};
	private ResultSet rows;
	private DefaultTableModel model = new DefaultTableModel(dbInfo, cols);
	private JTable jtable = new JTable(model);
	
	public ItemsBalanceFrame(Connection con) {
		setTitle("Items Balance Report");
		setSize(470,380);
		setLocationRelativeTo(null);
		setResizable(false);
		
		mainPanel = new JPanel();
		mainPanel.setBackground(Color.CYAN);
		
		jtable.setRowHeight(jtable.getRowHeight() + 10);
		jtable.setFont(new Font("Consolas", Font.BOLD, 14));
		jtable.setBackground(Color.CYAN);
		jtable.setAutoCreateRowSorter(true);
		jtable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jtable.setEnabled(false);
		
		TableColumn col1 = jtable.getColumnModel().getColumn(0);
		col1.setPreferredWidth(100);
		TableColumn col2 = jtable.getColumnModel().getColumn(1);
		col2.setPreferredWidth(245);
		TableColumn col3 = jtable.getColumnModel().getColumn(2);
		col3.setPreferredWidth(100);
		
		scrPane = new JScrollPane(jtable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		displayAllOnJList(con);
		
		//mainPanel.add(scrPane);
		//add(mainPanel);
	    this.setContentPane(scrPane);
		
		setVisible(true);
	}  // ItemsBalanceFrame constructor
	
	private void displayAllOnJList(Connection con) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);	
			// Show all results
			rows = StockBalanceDB.getStockBalanceFromDB(con);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getString(2), rows.getInt(3)};
				model.addRow(tempRow);
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} // displayAllOnJList
}