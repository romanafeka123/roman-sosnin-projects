package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import BL.Main;
import BL.Status;
import DAL.DateSQLQueriesDB;
import DAL.OrderDB;

public class OrdersReportFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private ImageIcon icon;
	private URL url;
	private JPanel mainPanel;
	private JRadioButton datesRadBtn, showAllRadBtn;
	private JLabel fromLbl, toLbl, dayLbl1, monthLbl1, yearLbl1, dayLbl2, monthLbl2, yearLbl2;
	private JComboBox<Integer> dayCmbBox1 ,dayCmbBox2, yearCmbBox1, yearCmbBox2;
	private JComboBox<String> monthCmbBox1, monthCmbBox2;
	private JButton searchBtn;
	private JScrollPane scrPane;
	private final int NUM_OF_DAYS = 31;
	private final int NUM_OF_MONTHS = 12;
	private static ArrayList<String> months = new ArrayList<String>();	
	// this is for JTable
	private Object[][] dbInfo;
	private Object[] cols = {"Order ID" ,"Order Date", "Item ID", "Description", "Amount"};
	private ResultSet rows;
	private DefaultTableModel model = new DefaultTableModel(dbInfo, cols);
	private JTable jtable = new JTable(model);
	
	// according to Order ID components
	private JRadioButton orderIDRadBtn;
	private JLabel orderIDLbl;
	private JComboBox<Integer> orderIDsCmbBox;
	
	public OrdersReportFrame(final Connection con, final Status status, String title) {
		setTitle(title);
		setSize(470,600);
		setLocationRelativeTo(null);
		setResizable(false);
		
		setMonths();
		
		mainPanel = new JPanel(new BorderLayout());
		mainPanel = new JPanel();
		SpringLayout sprLayout = new SpringLayout();
		mainPanel.setLayout(sprLayout);
		mainPanel.setBackground(Color.CYAN);
		
		datesRadBtn = new JRadioButton("According to dates");
		showAllRadBtn = new JRadioButton("Show all");
		orderIDRadBtn = new JRadioButton("According to order ID");
		datesRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		showAllRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		orderIDRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		datesRadBtn.setBackground(Color.CYAN);
		showAllRadBtn.setBackground(Color.CYAN);
		orderIDRadBtn.setBackground(Color.CYAN);
		ButtonGroup group = new ButtonGroup();
		group.add(datesRadBtn);
		group.add(showAllRadBtn);
		group.add(orderIDRadBtn);
		
		orderIDLbl = new JLabel("Order ID:");
		orderIDsCmbBox = new JComboBox<Integer>();
		
		if (status == Status.closed) {
			for (int i = 0; i < Main.closedOrders.size(); i++) {
				orderIDsCmbBox.addItem(Main.closedOrders.get(i).getOrder_id());
			}
		} 
		else if (status == Status.open) {
			for (int i = 0; i < Main.openOrders.size(); i++) {
				orderIDsCmbBox.addItem(Main.openOrders.get(i).getOrder_id());
			}
		}

		fromLbl = new JLabel("From:");
		toLbl = new JLabel("To:");
		fromLbl.setFont(new Font("Arial", Font.BOLD, 15));
		toLbl.setFont(new Font("Arial", Font.BOLD, 15));
		dayLbl1 = new JLabel("Day:");
		monthLbl1 = new JLabel("Month:");
		yearLbl1 = new JLabel("Year:");
		dayLbl2 = new JLabel("Day:");
		monthLbl2 = new JLabel("Month:");
		yearLbl2 = new JLabel("Year:");
		
		dayCmbBox1 = new JComboBox<Integer>();
		monthCmbBox1 = new JComboBox<String>();
		yearCmbBox1 = new JComboBox<Integer>();
		dayCmbBox2 = new JComboBox<Integer>();
		monthCmbBox2 = new JComboBox<String>();
		yearCmbBox2 = new JComboBox<Integer>();
		
		for (int i = 0; i < NUM_OF_DAYS; i++) {
			dayCmbBox1.addItem(i+1);
			dayCmbBox2.addItem(i+1);
		}
		for (int i = 0; i < NUM_OF_MONTHS; i++) {
			monthCmbBox1.addItem(months.get(i));
			monthCmbBox2.addItem(months.get(i));
		}
		// drawing the relevant years of orders from the DB and inserting them into the ComboBoxes
		int year1 = DateSQLQueriesDB.getRelevantYear(con, status, "min", "order");
		int year2 = DateSQLQueriesDB.getRelevantYear(con, status, "max", "order");
		for (int i = year1; i <= year2; i++) {
			yearCmbBox1.addItem(i);
			yearCmbBox2.addItem(i);
		}
		
		url = getClass().getResource("image/search.jpg");
		icon = new ImageIcon(url);
		searchBtn = new JButton(icon);
		searchBtn.setBackground(Color.WHITE);
		
		jtable.setRowHeight(jtable.getRowHeight() + 10);
		jtable.setFont(new Font("Consolas", Font.BOLD, 14));
		jtable.setBackground(Color.CYAN);
		jtable.setAutoCreateRowSorter(true);
		jtable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jtable.setEnabled(false);
		setJTableColumnsSize();
		
		scrPane = new JScrollPane(jtable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		displayAllOnJList(con, status);
		showAllRadBtn.setSelected(true);
		dayCmbBox1.setEnabled(false);
		monthCmbBox1.setEnabled(false);
		yearCmbBox1.setEnabled(false);
		dayCmbBox2.setEnabled(false);
		monthCmbBox2.setEnabled(false);
		yearCmbBox2.setEnabled(false);
		searchBtn.setEnabled(false);
		
		mainPanel.add(datesRadBtn);
		mainPanel.add(fromLbl);
		mainPanel.add(dayLbl1);
		mainPanel.add(dayCmbBox1);
		mainPanel.add(monthLbl1);
		mainPanel.add(monthCmbBox1);
		mainPanel.add(yearLbl1);
		mainPanel.add(yearCmbBox1);
		mainPanel.add(toLbl);
		mainPanel.add(dayLbl2);
		mainPanel.add(dayCmbBox2);
		mainPanel.add(monthLbl2);
		mainPanel.add(monthCmbBox2);
		mainPanel.add(yearLbl2);
		mainPanel.add(yearCmbBox2);
		mainPanel.add(searchBtn);
		mainPanel.add(showAllRadBtn);
		
		mainPanel.add(orderIDRadBtn);
		mainPanel.add(orderIDLbl);
		mainPanel.add(orderIDsCmbBox);
		
		mainPanel.add(scrPane);
		
		// datesRadBtn Block
		sprLayout.putConstraint(SpringLayout.WEST, datesRadBtn, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, datesRadBtn, 10, SpringLayout.NORTH, mainPanel);
		// fromLbl Block
		sprLayout.putConstraint(SpringLayout.WEST, fromLbl, 10, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, fromLbl, 35, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, dayLbl1, 10, SpringLayout.EAST, fromLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, dayLbl1, 36, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, dayCmbBox1, 5, SpringLayout.EAST, dayLbl1);
		sprLayout.putConstraint(SpringLayout.NORTH, dayCmbBox1, 33, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, monthLbl1, 10, SpringLayout.EAST, dayCmbBox1);
		sprLayout.putConstraint(SpringLayout.NORTH, monthLbl1, 36, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, monthCmbBox1, 5, SpringLayout.EAST, monthLbl1);
		sprLayout.putConstraint(SpringLayout.NORTH, monthCmbBox1, 33, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, yearLbl1, 10, SpringLayout.EAST, monthCmbBox1);
		sprLayout.putConstraint(SpringLayout.NORTH, yearLbl1, 36, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, yearCmbBox1, 5, SpringLayout.EAST, yearLbl1);
		sprLayout.putConstraint(SpringLayout.NORTH, yearCmbBox1, 33, SpringLayout.NORTH, datesRadBtn);
		// toLbl Block
		sprLayout.putConstraint(SpringLayout.WEST, toLbl, 10, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, toLbl, 35, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, dayLbl2, 10, SpringLayout.EAST, fromLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, dayLbl2, 36, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, dayCmbBox2, 5, SpringLayout.EAST, dayLbl2);
		sprLayout.putConstraint(SpringLayout.NORTH, dayCmbBox2, 33, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, monthLbl2, 10, SpringLayout.EAST, dayCmbBox2);
		sprLayout.putConstraint(SpringLayout.NORTH, monthLbl2, 36, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, monthCmbBox2, 5, SpringLayout.EAST, monthLbl2);
		sprLayout.putConstraint(SpringLayout.NORTH, monthCmbBox2, 33, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, yearLbl2, 10, SpringLayout.EAST, monthCmbBox2);
		sprLayout.putConstraint(SpringLayout.NORTH, yearLbl2, 36, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, yearCmbBox2, 5, SpringLayout.EAST, yearLbl2);
		sprLayout.putConstraint(SpringLayout.NORTH, yearCmbBox2, 33, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, searchBtn, 35, SpringLayout.WEST, monthCmbBox2);
		sprLayout.putConstraint(SpringLayout.NORTH, searchBtn, 108, SpringLayout.NORTH, mainPanel);
		// showAll Block
		sprLayout.putConstraint(SpringLayout.WEST, showAllRadBtn, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, showAllRadBtn, 30, SpringLayout.NORTH, toLbl);
		// According to Order IDs Block
		sprLayout.putConstraint(SpringLayout.WEST, orderIDRadBtn, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, orderIDRadBtn, 10, SpringLayout.SOUTH, showAllRadBtn);
        // storeHouseIDsCmbBox Block
		sprLayout.putConstraint(SpringLayout.WEST, orderIDLbl, 10, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, orderIDLbl, 35, SpringLayout.NORTH, orderIDRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, orderIDsCmbBox, 10, SpringLayout.EAST, orderIDLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, orderIDsCmbBox, 30, SpringLayout.NORTH, orderIDRadBtn);
		// scrPane Block
		sprLayout.putConstraint(SpringLayout.WEST, scrPane, 0, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, scrPane, 10, SpringLayout.SOUTH, orderIDsCmbBox);
		sprLayout.putConstraint(SpringLayout.SOUTH, scrPane, 0, SpringLayout.SOUTH, mainPanel);
		
		add(mainPanel);
		
		showAllRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dayCmbBox1.setEnabled(false);
				monthCmbBox1.setEnabled(false);
				yearCmbBox1.setEnabled(false);
				dayCmbBox2.setEnabled(false);
				monthCmbBox2.setEnabled(false);
				yearCmbBox2.setEnabled(false);
				searchBtn.setEnabled(false);	
				orderIDsCmbBox.setEnabled(false);
				displayAllOnJList(con, status);
			} // actionPerformed
		});
		
		orderIDRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dayCmbBox1.setEnabled(false);
				monthCmbBox1.setEnabled(false);
				yearCmbBox1.setEnabled(false);
				dayCmbBox2.setEnabled(false);
				monthCmbBox2.setEnabled(false);
				yearCmbBox2.setEnabled(false);
				searchBtn.setEnabled(true);	
				orderIDsCmbBox.setEnabled(true);		
				displayAllOnJList(con, status);
			} // actionPerformed
		});
		
		datesRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.setNumRows(0);
				String[] tempRow;
				for (int i = 0; i < 15; i++) {
					tempRow = new String[] { "", "", "", "", "" };
					model.addRow(tempRow);
				}
				dayCmbBox1.setEnabled(true);
				monthCmbBox1.setEnabled(true);
				yearCmbBox1.setEnabled(true);
				dayCmbBox2.setEnabled(true);
				monthCmbBox2.setEnabled(true);
				yearCmbBox2.setEnabled(true);
				searchBtn.setEnabled(true);	
				orderIDsCmbBox.setEnabled(false);
			} // actionPerformed
		});

		searchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (yearCmbBox1.isEnabled()) {
					int year1 = (Integer) yearCmbBox1.getSelectedItem();
					int year2 = (Integer) yearCmbBox2.getSelectedItem();
					String month1 = (String) monthCmbBox1.getSelectedItem();
					String month2 = (String) monthCmbBox2.getSelectedItem();
					int day1 = (Integer) dayCmbBox1.getSelectedItem();
					int day2 = (Integer) dayCmbBox2.getSelectedItem();

					if (year1 > year2
							|| (year1 == year2 && months.indexOf(month1) > months
									.indexOf(month2))
							|| (year1 == year2 && month1.equals(month2) && day1 > day2)) {
						JOptionPane.showMessageDialog(null,
								"The dates entered wrong.", "Error",
								JOptionPane.ERROR_MESSAGE);
					} else {
						String date1 = "'" + day1 + "-" + month1 + "-" + year1
								+ "'";
						String date2 = "'" + day2 + "-" + month2 + "-" + year2
								+ "'";
						displayByDatesOnJList(con, status, date1, date2);
					}
				}
				else {
					int order_id = (Integer)orderIDsCmbBox.getSelectedItem();
					displayByIDsOnJList(con, status, order_id);			
				}
			} // actionPerformed
		});
		
		// When a month or a year is changed, the number of days should be changed also
		monthCmbBox1.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				int year1 = (Integer)yearCmbBox1.getSelectedItem();
				String month1 = (String)monthCmbBox1.getSelectedItem();
				int numOfDays = DateSQLQueriesDB.getNumOfDays(con, year1, month1);
				dayCmbBox1.removeAllItems();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox1.addItem(i+1);
				}
			} // actionPerformed
		});
		yearCmbBox1.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				int year1 = (Integer)yearCmbBox1.getSelectedItem();
				String month1 = (String)monthCmbBox1.getSelectedItem();
				int numOfDays = DateSQLQueriesDB.getNumOfDays(con, year1, month1);
				dayCmbBox1.removeAllItems();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox1.addItem(i+1);
				}
			} // actionPerformed
		});
		monthCmbBox2.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				int year2 = (Integer)yearCmbBox2.getSelectedItem();
				String month2 = (String)monthCmbBox2.getSelectedItem();
				int numOfDays = DateSQLQueriesDB.getNumOfDays(con, year2, month2);
				dayCmbBox2.removeAllItems();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox2.addItem(i+1);
				}
			} // actionPerformed
		});
		yearCmbBox2.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				int year2 = (Integer)yearCmbBox2.getSelectedItem();
				String month2 = (String)monthCmbBox2.getSelectedItem();
				int numOfDays = DateSQLQueriesDB.getNumOfDays(con, year2, month2);
				dayCmbBox2.removeAllItems();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox2.addItem(i+1);
				}
			} // actionPerformed
		});
		
		setVisible(true);
	}  // OrdersReportFrame constructor
	
	private void displayAllOnJList(Connection con, Status status) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);	
			int year1 = (Integer)yearCmbBox1.getItemAt(0);
			int year2 = (Integer)yearCmbBox2.getItemAt(yearCmbBox2.getItemCount()-1);
			String month1 = (String)monthCmbBox1.getItemAt(0);
			String month2 = (String)monthCmbBox2.getItemAt(monthCmbBox2.getItemCount()-1);
			int day1 = (Integer)dayCmbBox1.getItemAt(0);
			int day2 = (Integer)dayCmbBox2.getItemAt(dayCmbBox2.getItemCount()-1);
			
			String date1 = "'" + day1 + "-" + month1 + "-" + year1 + "'";
			String date2 = "'" + day2 + "-" + month2 + "-" + year2 + "'";
			// Showing from the earliest date until the last date of all existing orders
			rows = OrderDB.getOrdersByDatesFromDB(con, status, date1, date2);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getDate(2),
						rows.getInt(3), rows.getString(4), rows.getInt(5) };
				model.addRow(tempRow);
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	} // displayAllOnJList
	
	private void displayByDatesOnJList(Connection con, Status status, String date1, String date2) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);				
			rows = OrderDB.getOrdersByDatesFromDB(con, status, date1, date2);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getDate(2),
						rows.getInt(3), rows.getString(4), rows.getInt(5) };
				model.addRow(tempRow);
			}
			if (model.getRowCount() == 0) {
				JOptionPane.showMessageDialog(null,
						"Didn't find any " + status + " orders between these dates", "Error",
						JOptionPane.ERROR_MESSAGE);			
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} // displayByDatesOnJList
	
	private void displayByIDsOnJList(Connection con, Status status, int order_id) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);				
			rows = OrderDB.getOrdersByIDsFromDB(con, order_id);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getDate(2),
						rows.getInt(3), rows.getString(4), rows.getInt(5) };
				model.addRow(tempRow);
			}
			if (model.getRowCount() == 0) {
				JOptionPane.showMessageDialog(null,
						"Didn't find any " + status + " orders between these dates", "Error",
						JOptionPane.ERROR_MESSAGE);			
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} // displayByDatesOnJList
	
	public void setMonths() {
		months.add("JAN"); months.add("FEB"); months.add("MAR"); months.add("APR");
		months.add("MAY"); months.add("JUN"); months.add("JUL"); months.add("AUG");
		months.add("SEP"); months.add("OCT"); months.add("NOV"); months.add("DEC");	
	}  // setMonths
	
	public void setJTableColumnsSize() {
		TableColumn col1 = jtable.getColumnModel().getColumn(0);
		col1.setPreferredWidth(80);
		TableColumn col2 = jtable.getColumnModel().getColumn(1);
		col2.setPreferredWidth(100);
		TableColumn col3 = jtable.getColumnModel().getColumn(2);
		col3.setPreferredWidth(70);
		TableColumn col4 = jtable.getColumnModel().getColumn(3);
		col4.setPreferredWidth(150);
		TableColumn col5 = jtable.getColumnModel().getColumn(4);
		col5.setPreferredWidth(50);
	}  // setJTableColumnsSize
	
}  // OrdersReportFrame
