package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import BL.Customer;
import BL.Item;

public class MainFrame extends JFrame{

	static final long serialVersionUID = 1L;
	private ImageIcon iconOracle;
	private ImageIcon iconSales;
	private URL url;
	private JButton ordersBtn ,invoicesBtn, orderToStockBtn, reportsBtn;
	private JButton addCustBtn, addItemBtn, addSHBtn;
	private JLabel lbl1;
	private JLabel lbl2;
	private JPanel mainPanel;
	private OrdersFrame ordersFrame;
	private InvoicesFrame invFrame;
	private OrderToStockFrame orderToStockFrame;
	private ReportsFrame repFrame;
	private AddCustomerFrame addCustFrame;
	private AddItemFrame addItemFrame;
	private AddStoreHouseFrame addSHFrame;
	
	public MainFrame(final Connection con, final Customer[] cust, final Item[] items) {
		setTitle("Sales Management System");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(360, 500);
		setLocation(700, 200);
		setResizable(false);
		mainPanel = new JPanel(new BorderLayout());
		lbl1 = new JLabel();
		lbl2 = new JLabel();
		
		JPanel btnPanel = new JPanel(new GridLayout(3, 1));
		JPanel ordInvPanel = new JPanel(new GridLayout(1, 2));
		JPanel stockReportPanel = new JPanel(new GridLayout(1, 2));
		JPanel threeBtnPanel = new JPanel(new GridLayout(1, 3));
		btnPanel.add(ordInvPanel);
		btnPanel.add(stockReportPanel);
		btnPanel.add(threeBtnPanel);
		
		url = getClass().getResource("image/oracle.jpg");
		iconOracle = new ImageIcon(url);
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);	
		
		lbl1.setIcon(iconSales);
		lbl2.setIcon(iconOracle);
		
		url = getClass().getResource("image/orders.jpg");
		ImageIcon ic1 = new ImageIcon(url);	
		url = getClass().getResource("image/invoices.jpg");
		ImageIcon ic2 = new ImageIcon(url);	
		url = getClass().getResource("image/delivery.JPG");
		ImageIcon ic3 = new ImageIcon(url);
		url = getClass().getResource("image/reports.jpg");
		ImageIcon ic4 = new ImageIcon(url);
		
		url = getClass().getResource("image/addCustomer.JPG");
		ImageIcon ic5 = new ImageIcon(url);
		url = getClass().getResource("image/addItem.JPG");
		ImageIcon ic6 = new ImageIcon(url);
		url = getClass().getResource("image/addStorehouse.JPG");
		ImageIcon ic7 = new ImageIcon(url);
		
		ordersBtn = new JButton(ic1);
		ordersBtn.setBackground(Color.WHITE);
		invoicesBtn = new JButton(ic2);
		invoicesBtn.setBackground(Color.WHITE);
		orderToStockBtn = new JButton(ic3);
		orderToStockBtn.setBackground(Color.WHITE);
		reportsBtn = new JButton(ic4);
		reportsBtn.setBackground(Color.WHITE);
		addCustBtn = new JButton(ic5);
		addCustBtn.setBackground(Color.WHITE);
        addItemBtn = new JButton(ic6);
        addItemBtn.setBackground(Color.WHITE);
        addSHBtn = new JButton(ic7);
        addSHBtn.setBackground(Color.WHITE);

        ordInvPanel.add(ordersBtn);
        ordInvPanel.add(invoicesBtn);
		stockReportPanel.add(orderToStockBtn);
		stockReportPanel.add(reportsBtn);
		
		threeBtnPanel.add(addCustBtn);
		threeBtnPanel.add(addItemBtn);
		threeBtnPanel.add(addSHBtn);
		
		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(btnPanel, BorderLayout.CENTER);
		mainPanel.add(lbl2, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		setVisible(true);
		
		ordersBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (ordersFrame != null)
					ordersFrame.setVisible(false);
				ordersFrame = new OrdersFrame(con, cust, items);
				ordersFrame.setVisible(true);
			} // actionPerformed
		});
		
		invoicesBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (invFrame != null)
					invFrame.setVisible(false);
				invFrame = new InvoicesFrame(con, cust, items);
				invFrame.setVisible(true);
			} // actionPerformed
		});
		
		orderToStockBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (orderToStockFrame != null)
					orderToStockFrame.setVisible(false);
				orderToStockFrame = new OrderToStockFrame(con);
				orderToStockFrame.setVisible(true);
			} // actionPerformed
		});
		
		reportsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (repFrame != null)
					repFrame.setVisible(false);
				repFrame = new ReportsFrame(con);
				repFrame.setVisible(true);
			} // actionPerformed
		});
		
		addCustBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (addCustFrame != null)
					addCustFrame.setVisible(false);
				addCustFrame = new AddCustomerFrame(con);
				addCustFrame.setVisible(true);
			} // actionPerformed
		});
		
		addItemBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (addItemFrame != null)
					addItemFrame.setVisible(false);
				addItemFrame = new AddItemFrame(con);
				addItemFrame.setVisible(true);
			} // actionPerformed
		});
		
		addSHBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (addSHFrame != null)
					addSHFrame.setVisible(false);
				addSHFrame = new AddStoreHouseFrame(con);
				addSHFrame.setVisible(true);
			} // actionPerformed
		});
	}  // MainFrame constructor
}  // MainFrame
