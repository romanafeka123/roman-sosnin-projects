package GUI;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import BL.Main;
import BL.Status;

public class ReportsFrame extends JFrame{

	static final long serialVersionUID = 1L;
	private ImageIcon iconOracle;
	private ImageIcon iconSales;
	private URL url;
	private JButton itemsBtn ,storehouseBtn, openOrdersBtn, transactionsBtn, soldItemsBtn, lackBtn;
	private JLabel lbl1;
	private JLabel lbl2;
	private JPanel mainPanel;
	private OrdersReportFrame openOrdersFrame, closedOrdersFrame;
	private ItemsBalanceFrame itemsBalanceFrame;
	private StoreHousesBalanceFrame shBalanceFrame;
	private TransactionsReportFrame transFrame; 
	private LackingItemsFrame lackingFrame;
	
	public ReportsFrame(final Connection con) {
		setTitle("Reports");
		setSize(360, 500);
		setLocation(750, 250);
		setResizable(false);
		mainPanel = new JPanel(new BorderLayout());
		lbl1 = new JLabel();
		lbl2 = new JLabel();
		
		JPanel btnPanel = new JPanel(new GridLayout(6, 1));
		
		url = getClass().getResource("image/oracle.jpg");
		iconOracle = new ImageIcon(url);
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);	
		
		lbl1.setIcon(iconSales);
		lbl2.setIcon(iconOracle);
		  
		itemsBtn = new JButton("Items balance report");
		storehouseBtn = new JButton("StoreHouse balance report");
		openOrdersBtn = new JButton("Open orders report");
		transactionsBtn = new JButton("Transactions report");
		soldItemsBtn = new JButton("Sold items report");
		lackBtn = new JButton("Lacking Items report");
		  
		itemsBtn.setFont(new Font("Arial", Font.BOLD, 18));
		storehouseBtn.setFont(new Font("Arial", Font.BOLD, 18));
		openOrdersBtn.setFont(new Font("Arial", Font.BOLD, 18));
		transactionsBtn.setFont(new Font("Arial", Font.BOLD, 18));
		soldItemsBtn.setFont(new Font("Arial", Font.BOLD, 18));
		lackBtn.setFont(new Font("Arial", Font.BOLD, 18));

		btnPanel.add(itemsBtn);
		btnPanel.add(storehouseBtn);
		btnPanel.add(openOrdersBtn);
		btnPanel.add(transactionsBtn);
		btnPanel.add(soldItemsBtn);
		btnPanel.add(lackBtn);
		
		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(btnPanel, BorderLayout.CENTER);
		mainPanel.add(lbl2, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		setVisible(true);
		
		itemsBtn.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				if (itemsBalanceFrame != null)
					itemsBalanceFrame.setVisible(false);
				itemsBalanceFrame = new ItemsBalanceFrame(con);
				itemsBalanceFrame.setVisible(true);
			} // actionPerformed
		});
		
		storehouseBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (shBalanceFrame != null)
					shBalanceFrame.setVisible(false);
				shBalanceFrame = new StoreHousesBalanceFrame(con);
				shBalanceFrame.setVisible(true);
			} // actionPerformed
		});
		
		openOrdersBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.drawOpenOrdersFromDB(con);
				if (openOrdersFrame != null)
					openOrdersFrame.setVisible(false);
				if (Main.openOrders.size() != 0) {
					openOrdersFrame = new OrdersReportFrame(con, Status.open,"Open orders report");
					openOrdersFrame.setVisible(true);
				}
			} // actionPerformed
		});
		 
		soldItemsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.drawClosedOrdersFromDB(con);
				if (closedOrdersFrame != null)
					closedOrdersFrame.setVisible(false);

				if (Main.closedOrders.size() != 0) {
					closedOrdersFrame = new OrdersReportFrame(con,Status.closed, "Sold items report");
					closedOrdersFrame.setVisible(true);
				}
			} // actionPerformed
		});
		
		transactionsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.drawClosedOrdersFromDB(con);
				if (transFrame != null)
					transFrame.setVisible(false);
				if (Main.closedOrders.size() == 0) {
					JOptionPane.showMessageDialog(null,
							"There are no transactions yet!",
							"Error", JOptionPane.ERROR_MESSAGE);
				} else {
					transFrame = new TransactionsReportFrame(con);
					transFrame.setVisible(true);
				}
			} // actionPerformed
		});

		
		lackBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (lackingFrame != null)
					lackingFrame.setVisible(false);
				lackingFrame = new LackingItemsFrame(con);
				lackingFrame.setVisible(true);
			} // actionPerformed
		});

	}  // MainFrame constructor

}  // MainFrame
