package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import BL.StoreHouse;
import DAL.StoreHouseDB;

public class AddStoreHouseFrame extends JFrame{
	
	static final long serialVersionUID = 1L;
	private ImageIcon iconOracle;
	private ImageIcon iconSales;
	private URL url;
	private JLabel lbl1, lbl2;
	private JLabel shListLbl, enterDetLbl, nameLbl;
	private JComboBox<StoreHouse> shCmbBox;
	private JTextField nameTxtFld;
	private JButton addSHBtn;
	private JPanel mainPanel, componentsPanel;
	private SpringLayout sprLayout;
	
	public AddStoreHouseFrame(final Connection con) {
		setTitle("Add a new storehouse to the system");
		setSize(355, 410);
		setLocation(750, 250);
		setResizable(false);
		
		makeGUIComponents(con);
		setSpringLayoutConstraints();
		setListeners(con);
		
		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(componentsPanel, BorderLayout.CENTER);
		mainPanel.add(lbl2, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		setVisible(true);
	}  // MainFrame constructor
	
	private void makeGUIComponents(Connection con) {
		mainPanel = new JPanel(new BorderLayout());
		lbl1 = new JLabel();
		lbl2 = new JLabel();
		
		componentsPanel = new JPanel();
		sprLayout = new SpringLayout();
		componentsPanel.setLayout(sprLayout);
		componentsPanel.setBackground(Color.CYAN);
		
		url = getClass().getResource("image/oracle.jpg");
		iconOracle = new ImageIcon(url);
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);	
		
		lbl1.setIcon(iconSales);
		lbl2.setIcon(iconOracle);
		
		shListLbl = new JLabel("Existing storehouses:");
		enterDetLbl = new JLabel("Enter details to add a new storehouse");
		enterDetLbl.setFont(new Font("Arial", Font.BOLD, 16));
		nameLbl = new JLabel("Name:");
	
		shCmbBox = new JComboBox<StoreHouse>(StoreHouseDB.getStorehousesFromDB(con));
		nameTxtFld = new JTextField(14);
		nameTxtFld.setFont(new Font("Arial", Font.BOLD, 16));
		addSHBtn = new JButton("Add Storehouse");
		addSHBtn.setFont(new Font("Arial", Font.BOLD, 16));
		
		componentsPanel.add(shListLbl);
		componentsPanel.add(shCmbBox);
		componentsPanel.add(enterDetLbl);
		componentsPanel.add(nameLbl);
		componentsPanel.add(nameTxtFld);
		componentsPanel.add(addSHBtn);
	}
	
	private void setSpringLayoutConstraints() {
		// ComboBox block
		sprLayout.putConstraint(SpringLayout.WEST, shListLbl, 10, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, shListLbl, 13, SpringLayout.NORTH, componentsPanel);
		sprLayout.putConstraint(SpringLayout.WEST, shCmbBox, 5, SpringLayout.EAST, shListLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, shCmbBox, 10, SpringLayout.NORTH, componentsPanel);
		// enter details block
		sprLayout.putConstraint(SpringLayout.WEST, enterDetLbl, 25, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, enterDetLbl, 35, SpringLayout.NORTH, shCmbBox);
		// description block
		sprLayout.putConstraint(SpringLayout.WEST, nameLbl, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, nameLbl, 38, SpringLayout.NORTH, enterDetLbl);
		sprLayout.putConstraint(SpringLayout.WEST, nameTxtFld, 5, SpringLayout.EAST, nameLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, nameTxtFld, 35, SpringLayout.NORTH, enterDetLbl);
		// button block
		sprLayout.putConstraint(SpringLayout.WEST, addSHBtn, 88, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, addSHBtn, 30, SpringLayout.NORTH, nameTxtFld);	
	}
	
	private void setListeners(final Connection con) {
		addSHBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String name = nameTxtFld.getText();
					if (name.length() == 0 || name == null)
					    throw new Exception("Please enter the storehouse name");
					
					int succedeed = StoreHouseDB.addNewStorehouseToDB(con, name);
					if (succedeed != -1) {
					    JOptionPane.showMessageDialog(null, "Storehouse was added successfully!",
							"Error", JOptionPane.INFORMATION_MESSAGE);
					    nameTxtFld.setText("");
					    updateStorehousesInCmbBox(con);
					}
				
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage(),
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			} // actionPerformed
		});
	}
	
	public void updateStorehousesInCmbBox(Connection con) {
		// update the ComboBox
		shCmbBox.removeAllItems();
		StoreHouse[] sh = StoreHouseDB.getStorehousesFromDB(con);
		for (int i = 0; i < sh.length; i++) {
			shCmbBox.addItem(sh[i]);
		}
	} // updateStorehousesInCmbBox
}  // MainFrame
