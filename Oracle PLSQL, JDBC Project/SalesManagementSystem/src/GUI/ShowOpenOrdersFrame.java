package GUI;

import java.awt.Color;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;

import BL.Cart;

public class ShowOpenOrdersFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JScrollPane scrPane;
	
	private DefaultListModel<String> model = new DefaultListModel<String>();
	private JList<String> jlist = new JList<String>(model);
	
	public ShowOpenOrdersFrame(Cart cart) {
		setTitle("Open Orders");
		setSize(300,300);
		setLocationRelativeTo(null);
		setResizable(false);
		
		jlist.setBackground(Color.CYAN);
		jlist.setBorder(new LineBorder(Color.BLACK, 2));
		jlist.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		jlist.setFont(new Font("Consolas", Font.BOLD, 14));
		scrPane = new JScrollPane(jlist, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		displayOnJList(cart);
		add(scrPane);
		setVisible(true);
	}  // ShowOpenOrdersFrame constructor
	
	private void displayOnJList(Cart cart) {
		String str = String.format("%4s%-10s", "Order ID: " + "    " , "Customer");
		model.addElement(str);
		model.addElement(" ");
		/*
		for (int i = 0; i < cart.cartSize(); i++) {
			String res = String.format("%-10s%-15s%3d", num,
					cart.getItemAtIndex(i).getItem().getDescription(), cart
							.getItemAtIndex(i).getAmount());
			model.addElement(res);
		} */
	} // displayProperlyOnTheJList
}  // ShowOpenOrdersFrame
