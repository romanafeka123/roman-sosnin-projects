package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import BL.Item;
import BL.ItemInCart;
import BL.Main;
import BL.Order;
import BL.OrderComparatorByID_Descending;
import BL.OrderUpdateType;
import BL.Status;
import DAL.OrderDB;

public class UpdateOrderFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private ImageIcon iconSales;
	private URL url;
	private JLabel lbl, mainLbl, itemLbl1, newAmountLbl, itemLbl2, itemLbl3, amountLbl;
	private JLabel chooseItemLbl1, chooseItemLbl2, chooseItemLbl3;
	private JRadioButton updAmountRadBtn, addNewItemRadBtn, delItemRadBtn;
	private JComboBox<Order> ordersCmbBox1, ordersCmbBox2, ordersCmbBox3;
	private JComboBox<Item> itemsCmbBox1, itemsCmbBox2, itemsCmbBox3;
	private JTextField newAmountTxtFld, amountTxtFld;
	private JButton updateOrdBtn;
	private JPanel mainPanel;
	private boolean canAddItems = true;
	
	public UpdateOrderFrame(final Connection con) {
		setTitle("Update an Order");
		setSize(370, 640);
		setLocation(350, 50);
		setResizable(false);
		
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);
		
		mainPanel = new JPanel(new BorderLayout());
		
		lbl = new JLabel(iconSales);
		mainLbl = new JLabel("Choose an order to update it");
		mainLbl.setFont(new Font("Arial", Font.BOLD, 18));
		itemLbl1 = new JLabel("Item:");
		newAmountLbl = new JLabel("New amount:");
		
		chooseItemLbl1 = new JLabel("Choose an item to change amount for it");
		chooseItemLbl1.setFont(new Font("Arial", Font.BOLD, 16));
		chooseItemLbl2 = new JLabel("Choose an item to add");
		chooseItemLbl2.setFont(new Font("Arial", Font.BOLD, 16));
		chooseItemLbl3 = new JLabel("Choose an item to delete");
		chooseItemLbl3.setFont(new Font("Arial", Font.BOLD, 16));
		
		itemLbl2 = new JLabel("Item:");
		amountLbl = new JLabel("Amount:");
		itemLbl3 = new JLabel("Item:");
		
		updAmountRadBtn = new JRadioButton("Update amount in an order");
		addNewItemRadBtn = new JRadioButton("Add a new item to an order");
		delItemRadBtn = new JRadioButton("Delete an item from an order");
		updAmountRadBtn.setBackground(Color.CYAN);
		addNewItemRadBtn.setBackground(Color.CYAN);
		delItemRadBtn.setBackground(Color.CYAN);
		updAmountRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		addNewItemRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		delItemRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		ButtonGroup group = new ButtonGroup();
		group.add(updAmountRadBtn);
		group.add(addNewItemRadBtn);
		group.add(delItemRadBtn);
		
		// Adding all the open orders to the ComboBoxes
		ordersCmbBox1 = new JComboBox<Order>();
		ordersCmbBox2 = new JComboBox<Order>();
		ordersCmbBox3 = new JComboBox<Order>();
		Order[] ord = OrderDB.getOrdersFromDB(con, Status.open);
		OrderComparatorByID_Descending comp = new OrderComparatorByID_Descending();
		Main.mergeSort(ord, comp);
		for (int i = 0; i < ord.length; i++) {
			ordersCmbBox1.addItem(ord[i]);
			ordersCmbBox2.addItem(ord[i]);
			ordersCmbBox3.addItem(ord[i]);
		}

		// Adding relevant items to the ComboBoxes
		itemsCmbBox1 = new JComboBox<Item>();
		itemsCmbBox2 = new JComboBox<Item>();
		itemsCmbBox3 = new JComboBox<Item>();
		
		Order ord1 = ((Order)ordersCmbBox1.getSelectedItem());
		for (int i = 0; i < ord1.getCart().cartSize(); i++) {
			for (int j = 0; j < Main.items.length; j++) {
				if (ord1.getCart().getItemInCartAtIndex(i).getItem().equals(Main.items[j])) {
					itemsCmbBox1.addItem(Main.items[j]);
					itemsCmbBox3.addItem(Main.items[j]);
				}
			}		
		}
		ArrayList<Item> arr = new ArrayList<Item>();
		for (int i = 0; i < ord1.getCart().cartSize(); i++) {
			arr.add(itemsCmbBox1.getItemAt(i));
		}

		for (int i = 0; i < Main.items.length; i++) {
			if (!arr.contains(Main.items[i]))
				itemsCmbBox2.addItem(Main.items[i]);
		}	

		newAmountTxtFld = new JTextField(3);
		amountTxtFld = new JTextField(3);
		newAmountTxtFld.setFont(new Font("Arial", Font.BOLD, 18));
		amountTxtFld.setFont(new Font("Arial", Font.BOLD, 18));
		
		updateOrdBtn = new JButton("Update order");
		updateOrdBtn.setFont(new Font("Arial", Font.BOLD, 18));
		
		if (checkIfCanAddItems() == true) {
			int i = 0;
			while (itemsCmbBox2.getSelectedItem() == null) {
				ordersCmbBox2.setSelectedIndex(i++);
				updateItemsCmbBox2();
			}
		}
		else {
			canAddItems = false;
			addNewItemRadBtn.setEnabled(false);
			ordersCmbBox2.setEnabled(false);
			itemsCmbBox2.setEnabled(false);
			amountTxtFld.setEnabled(false);
		}
		
		JPanel midPnl = new JPanel();
		SpringLayout sprLayout = new SpringLayout();
		midPnl.setLayout(sprLayout);
		midPnl.setBackground(Color.CYAN);
		
		JPanel southPanel = new JPanel();
		southPanel.setBackground(Color.CYAN);
		
		midPnl.add(mainLbl);
		midPnl.add(updAmountRadBtn);
		midPnl.add(ordersCmbBox1);
		midPnl.add(itemLbl1);
		midPnl.add(itemsCmbBox1);
		midPnl.add(chooseItemLbl1);
		midPnl.add(newAmountLbl);
		midPnl.add(newAmountTxtFld);
		midPnl.add(addNewItemRadBtn);
		midPnl.add(ordersCmbBox2);
		midPnl.add(chooseItemLbl2);
		midPnl.add(itemLbl2);
		midPnl.add(itemsCmbBox2);
		midPnl.add(amountLbl);
		midPnl.add(amountTxtFld);
		midPnl.add(delItemRadBtn);
		midPnl.add(ordersCmbBox3);
		midPnl.add(chooseItemLbl3);
		midPnl.add(itemLbl3);
		midPnl.add(itemsCmbBox3);

		sprLayout.putConstraint(SpringLayout.WEST, mainLbl, 50, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, mainLbl, 10, SpringLayout.NORTH, midPnl);
		// After first RadioButton
		sprLayout.putConstraint(SpringLayout.WEST, updAmountRadBtn, 5, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, updAmountRadBtn, 15, SpringLayout.SOUTH, mainLbl);
		sprLayout.putConstraint(SpringLayout.WEST, ordersCmbBox1, 12, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, ordersCmbBox1, 10, SpringLayout.SOUTH, updAmountRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, chooseItemLbl1, 25, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, chooseItemLbl1, 10, SpringLayout.SOUTH, ordersCmbBox1);
		sprLayout.putConstraint(SpringLayout.WEST, itemLbl1, 12, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, itemLbl1, 10, SpringLayout.SOUTH, chooseItemLbl1);
		sprLayout.putConstraint(SpringLayout.WEST, itemsCmbBox1, 5, SpringLayout.EAST, itemLbl1);
		sprLayout.putConstraint(SpringLayout.NORTH, itemsCmbBox1, 5, SpringLayout.SOUTH, chooseItemLbl1);
		sprLayout.putConstraint(SpringLayout.WEST, newAmountLbl, 5, SpringLayout.EAST, itemsCmbBox1);
		sprLayout.putConstraint(SpringLayout.NORTH, newAmountLbl, 10, SpringLayout.SOUTH, chooseItemLbl1);
		sprLayout.putConstraint(SpringLayout.WEST, newAmountTxtFld, 5, SpringLayout.EAST, newAmountLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, newAmountTxtFld, 5, SpringLayout.SOUTH, chooseItemLbl1);
		// After second RadioButton
		sprLayout.putConstraint(SpringLayout.WEST, addNewItemRadBtn, 5, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, addNewItemRadBtn, 30, SpringLayout.SOUTH, itemLbl1);
		sprLayout.putConstraint(SpringLayout.WEST, ordersCmbBox2, 12, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, ordersCmbBox2, 10, SpringLayout.SOUTH, addNewItemRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, chooseItemLbl2, 45, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, chooseItemLbl2, 10, SpringLayout.SOUTH, ordersCmbBox2);
		sprLayout.putConstraint(SpringLayout.WEST, itemLbl2, 12, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, itemLbl2, 10, SpringLayout.SOUTH, chooseItemLbl2);
		sprLayout.putConstraint(SpringLayout.WEST, itemsCmbBox2, 5, SpringLayout.EAST, itemLbl1);
		sprLayout.putConstraint(SpringLayout.NORTH, itemsCmbBox2, 5, SpringLayout.SOUTH, chooseItemLbl2);
		sprLayout.putConstraint(SpringLayout.WEST, amountLbl, 5, SpringLayout.EAST, itemsCmbBox2);
		sprLayout.putConstraint(SpringLayout.NORTH, amountLbl, 10, SpringLayout.SOUTH, chooseItemLbl2);
		sprLayout.putConstraint(SpringLayout.WEST, amountTxtFld, 5, SpringLayout.EAST, amountLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, amountTxtFld, 5, SpringLayout.SOUTH, chooseItemLbl2);
		// After third RadioButton
		sprLayout.putConstraint(SpringLayout.WEST, delItemRadBtn, 5, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, delItemRadBtn, 30, SpringLayout.SOUTH, amountTxtFld);
		sprLayout.putConstraint(SpringLayout.WEST, ordersCmbBox3, 12, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, ordersCmbBox3, 10, SpringLayout.SOUTH, delItemRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, chooseItemLbl3, 45, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, chooseItemLbl3, 10, SpringLayout.SOUTH, ordersCmbBox3);
		sprLayout.putConstraint(SpringLayout.WEST, itemLbl3, 12, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, itemLbl3, 10, SpringLayout.SOUTH, chooseItemLbl3);
		sprLayout.putConstraint(SpringLayout.WEST, itemsCmbBox3, 5, SpringLayout.EAST, itemLbl3);
		sprLayout.putConstraint(SpringLayout.NORTH, itemsCmbBox3, 5, SpringLayout.SOUTH, chooseItemLbl3);
		
		southPanel.add(updateOrdBtn);
		
		mainPanel.add(lbl, BorderLayout.NORTH);
		mainPanel.add(midPnl, BorderLayout.CENTER);
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		
		updAmountRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ordersCmbBox2.setEnabled(false);
				itemsCmbBox2.setEnabled(false);
				amountTxtFld.setEnabled(false);
				ordersCmbBox3.setEnabled(false);
				itemsCmbBox3.setEnabled(false);
				ordersCmbBox1.setEnabled(true);
				itemsCmbBox1.setEnabled(true);
				newAmountTxtFld.setEnabled(true);	
			} // actionPerformed
		});
		
		addNewItemRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (canAddItems == true) {
					ordersCmbBox1.setEnabled(false);
					itemsCmbBox1.setEnabled(false);
					newAmountTxtFld.setEnabled(false);
					ordersCmbBox3.setEnabled(false);
					itemsCmbBox3.setEnabled(false);
					ordersCmbBox2.setEnabled(true);
					itemsCmbBox2.setEnabled(true);
					amountTxtFld.setEnabled(true);
				}
			} // actionPerformed
		});
		
		delItemRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (canAddItems == true) {
					ordersCmbBox1.setEnabled(false);
					itemsCmbBox1.setEnabled(false);
					newAmountTxtFld.setEnabled(false);
					ordersCmbBox2.setEnabled(false);
					itemsCmbBox2.setEnabled(false);
					amountTxtFld.setEnabled(false);
					ordersCmbBox3.setEnabled(true);
					itemsCmbBox3.setEnabled(true);
				}
			} // actionPerformed
		});
		
		// update the items in itemsCmbBox1 according to the appropriate chosen order
		ordersCmbBox1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateItemsCmbBox1();
			} // actionPerformed
		});
		
		// update the items in itemsCmbBox2 according to the appropriate chosen order
		ordersCmbBox2.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				updateItemsCmbBox2();
				if (itemsCmbBox2.getSelectedItem() == null) {
					JOptionPane.showMessageDialog(null,
							"This order has all possible items!", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
				int i = 0;
				while (itemsCmbBox2.getSelectedItem() == null) {
					ordersCmbBox2.setSelectedIndex(++i);
					updateItemsCmbBox2();		
				}		
			} // actionPerformed
		});
		
		// update the items in itemsCmbBox3 according to the appropriate chosen order
		ordersCmbBox3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateItemsCmbBox3();
			} // actionPerformed
		});
		
		updateOrdBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OrderUpdateType updType;
				Order order;
				Item item;
				int amount;
				
				if (!updAmountRadBtn.isSelected() && !addNewItemRadBtn.isSelected() && !delItemRadBtn.isSelected()) {
					JOptionPane.showMessageDialog(null,
							"Please choose what update you want to make!", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
				else {
					
					try {
						if (updAmountRadBtn.isSelected()) {
							updType = OrderUpdateType.upd_amount;
							order = (Order)ordersCmbBox1.getSelectedItem();
							item = (Item)itemsCmbBox1.getSelectedItem();
							amount = Integer.parseInt(newAmountTxtFld.getText());
							if (amount < 1)
								throw new Exception();
							closeFrame();
							int flag = OrderDB.updateOpenOrderInDB(con, updType, null, amount, item.getId(), order);
							if (flag != -1) {
								Main.updateOpenOrdersInMain(con);
								closeFrame();
								JOptionPane.showMessageDialog(null,
										"The order was updated successfully",
										"Update an Order",
										JOptionPane.INFORMATION_MESSAGE);
								
							}
						}
 else if (delItemRadBtn.isSelected()) {
							try {
								closeFrame();
								updType = OrderUpdateType.deleteItem;
								order = (Order) ordersCmbBox3.getSelectedItem();
								item = (Item) itemsCmbBox3.getSelectedItem();
								int flag = OrderDB.updateOpenOrderInDB(con,updType, null, -1, item.getId(), order);
								if (flag != -1) {
									Main.updateOpenOrdersInMain(con);
									closeFrame();
									JOptionPane.showMessageDialog( null, "The order was updated successfully", "Update an Order", JOptionPane.INFORMATION_MESSAGE);
								}
							} catch (Exception e) { }
						} else {
							updType = OrderUpdateType.upd_addItem;
							order = (Order)ordersCmbBox2.getSelectedItem();
							item = (Item)itemsCmbBox2.getSelectedItem();
							amount = Integer.parseInt(amountTxtFld.getText());
							if (amount < 1)
								throw new Exception();
							closeFrame();
							ItemInCart itemInCart = new ItemInCart(item, amount);
							int num = OrderDB.updateOpenOrderInDB(con, updType, itemInCart, -1, -1, order);
							if (num != -1) {
								Main.updateOpenOrdersInMain(con);
								closeFrame();
								JOptionPane.showMessageDialog(null,
								"   The order was updated successfully \n Total number of items after updating is: "+ num,
								"Update an Order", JOptionPane.INFORMATION_MESSAGE);
							}
						}								
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null,
								"Enter correct amount!", "Error",
								JOptionPane.ERROR_MESSAGE);
					}	
				}
			} // actionPerformed
		});
	}  // UpdateOrderFrame constructor
	
	private void updateItemsCmbBox1() {
		itemsCmbBox1.removeAllItems();
		Order ord = ((Order)ordersCmbBox1.getSelectedItem());
		for (int i = 0; i < ord.getCart().cartSize(); i++) {
			for (int j = 0; j < Main.items.length; j++) {
				if (ord.getCart().getItemInCartAtIndex(i).getItem().equals(Main.items[j]))
					itemsCmbBox1.addItem(Main.items[j]);
			}		
		}		
	}  // updateItemsCmbBox1
	
	private void updateItemsCmbBox2() {
		itemsCmbBox2.removeAllItems();
		Order ord = ((Order)ordersCmbBox2.getSelectedItem());
		ArrayList<Item> arr = new ArrayList<Item>();
		for (int i = 0; i < ord.getCart().cartSize(); i++) {
			arr.add(ord.getCart().getItemInCartAtIndex(i).getItem());
		}
		for (int i = 0; i < Main.items.length; i++) {
			if (!arr.contains(Main.items[i]))
				itemsCmbBox2.addItem(Main.items[i]);
		}
	}  // updateItemsCmbBox2
	
	private void updateItemsCmbBox3() {
		itemsCmbBox3.removeAllItems();
		Order ord = ((Order)ordersCmbBox3.getSelectedItem());
		for (int i = 0; i < ord.getCart().cartSize(); i++) {
			for (int j = 0; j < Main.items.length; j++) {
				if (ord.getCart().getItemInCartAtIndex(i).getItem().equals(Main.items[j]))
					itemsCmbBox3.addItem(Main.items[j]);
			}		
		}		
	}  // updateItemsCmbBox1
	
	// Check whether one of the orders has less items than exist. If it has one - return true
	// If all open orders have all existing items - return false
	private boolean checkIfCanAddItems() {
		for (int i = 0; i < Main.openOrders.size(); i++) {
			if (Main.openOrders.get(i).getCart().cartSize() != Main.items.length)
				return true;
		}
		return false;
	}  // checkIfCanAddItems
	
	private void closeFrame() {
		this.setVisible(false);
	} // closeFrame
	
}  // UpdateOrderFrame
