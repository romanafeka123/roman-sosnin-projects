package GUI;

import java.awt.Color;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ImageFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private ImageIcon loadingImg;
	private URL url;
	private JLabel lbl;
	
	public ImageFrame(String title, String path, int x, int y) {
		setTitle(title);
		setSize(x, y);
		setLocationRelativeTo(null);
		setResizable(false);

		url = getClass().getResource(path);
		loadingImg = new ImageIcon(url);
		lbl = new JLabel(loadingImg);
		lbl.setBackground(Color.WHITE);
		lbl.setOpaque(true);
		add(lbl);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
	}  // LoadingFrame constructor

}  // LoadingFrame
