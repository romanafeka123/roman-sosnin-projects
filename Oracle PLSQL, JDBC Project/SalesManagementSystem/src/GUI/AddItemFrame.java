package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import BL.Item;
import DAL.ItemDB;

public class AddItemFrame extends JFrame{
	
	static final long serialVersionUID = 1L;
	private ImageIcon iconOracle;
	private ImageIcon iconSales;
	private URL url;
	private JLabel lbl1, lbl2;
	private JLabel itemListLbl, enterDetLbl, descrLbl, priceLbl;
	private JComboBox<Item> itemCmbBox;
	private JTextField descrTxtFld, priceTxtFld;
	private JButton addItemBtn;
	private JPanel mainPanel, componentsPanel;
	private SpringLayout sprLayout;
	
	public AddItemFrame(final Connection con) {
		setTitle("Add a new item to the system");
		setSize(355, 445);
		setLocation(750, 250);
		setResizable(false);
		
		makeGUIComponents(con);
		setSpringLayoutConstraints();
		setListeners(con);
		
		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(componentsPanel, BorderLayout.CENTER);
		mainPanel.add(lbl2, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		setVisible(true);
	}  // MainFrame constructor
	
	private void makeGUIComponents(Connection con) {
		mainPanel = new JPanel(new BorderLayout());
		lbl1 = new JLabel();
		lbl2 = new JLabel();
		
		componentsPanel = new JPanel();
		sprLayout = new SpringLayout();
		componentsPanel.setLayout(sprLayout);
		componentsPanel.setBackground(Color.CYAN);
		
		url = getClass().getResource("image/oracle.jpg");
		iconOracle = new ImageIcon(url);
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);	
		
		lbl1.setIcon(iconSales);
		lbl2.setIcon(iconOracle);
		
		itemListLbl = new JLabel("Existing items:");
		enterDetLbl = new JLabel("Enter details to add a new item");
		enterDetLbl.setFont(new Font("Arial", Font.BOLD, 16));
		descrLbl = new JLabel("Description:");
		priceLbl = new JLabel("Price:");
	
		itemCmbBox = new JComboBox<Item>(ItemDB.getItemsFromDB(con));
		descrTxtFld = new JTextField(12);
		descrTxtFld.setFont(new Font("Arial", Font.BOLD, 16));
		priceTxtFld = new JTextField(12);
		priceTxtFld.setFont(new Font("Arial", Font.BOLD, 16));
		addItemBtn = new JButton("Add Item");
		addItemBtn.setFont(new Font("Arial", Font.BOLD, 16));
		
		componentsPanel.add(itemListLbl);
		componentsPanel.add(itemCmbBox);
		componentsPanel.add(enterDetLbl);
		componentsPanel.add(descrLbl);
		componentsPanel.add(descrTxtFld);
		componentsPanel.add(priceLbl);
		componentsPanel.add(priceTxtFld);
		componentsPanel.add(addItemBtn);
	}
	
	private void setSpringLayoutConstraints() {
		// ComboBox block
		sprLayout.putConstraint(SpringLayout.WEST, itemListLbl, 10, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, itemListLbl, 13, SpringLayout.NORTH, componentsPanel);
		sprLayout.putConstraint(SpringLayout.WEST, itemCmbBox, 5, SpringLayout.EAST, itemListLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, itemCmbBox, 10, SpringLayout.NORTH, componentsPanel);
		// enter details block
		sprLayout.putConstraint(SpringLayout.WEST, enterDetLbl, 25, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, enterDetLbl, 35, SpringLayout.NORTH, itemCmbBox);
		// description block
		sprLayout.putConstraint(SpringLayout.WEST, descrLbl, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, descrLbl, 38, SpringLayout.NORTH, enterDetLbl);
		sprLayout.putConstraint(SpringLayout.WEST, descrTxtFld, 5, SpringLayout.EAST, descrLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, descrTxtFld, 35, SpringLayout.NORTH, enterDetLbl);
		// price block
		sprLayout.putConstraint(SpringLayout.WEST, priceLbl, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, priceLbl, 38, SpringLayout.NORTH, descrTxtFld);
		sprLayout.putConstraint(SpringLayout.WEST, priceTxtFld, 0, SpringLayout.WEST, descrTxtFld);
		sprLayout.putConstraint(SpringLayout.NORTH, priceTxtFld, 35, SpringLayout.NORTH, descrTxtFld);
		// button block
		sprLayout.putConstraint(SpringLayout.WEST, addItemBtn, 120, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, addItemBtn, 30, SpringLayout.NORTH, priceTxtFld);	
	}
	
	private void setListeners(final Connection con) {
		addItemBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String description = descrTxtFld.getText();
					if (description.length() == 0 || description == null)
					    throw new Exception("Please enter the item description");			
					Integer price = Integer.parseInt(priceTxtFld.getText());
					int succedeed = ItemDB.addNewItemToDB(con, description, price);
					if (succedeed != -1) {
					    JOptionPane.showMessageDialog(null, "Item was added successfully!",
							"Error", JOptionPane.INFORMATION_MESSAGE);
					    descrTxtFld.setText("");
					    priceTxtFld.setText("");
					    updateItemsInCmbBox(con);
					}
				} catch (NumberFormatException e) {
					JFrame fr = new JFrame();
					JOptionPane.showMessageDialog(fr, "Please enter correct price!",
							"Error", JOptionPane.ERROR_MESSAGE);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage(),
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			} // actionPerformed
		});	
	}
	
	public void updateItemsInCmbBox(Connection con) {
		// update the ComboBox
		itemCmbBox.removeAllItems();
		Item[] items = ItemDB.getItemsFromDB(con);
		for (int i = 0; i < items.length; i++) {
			itemCmbBox.addItem(items[i]);
		}
	} // updateItemsInCmbBox
}  // MainFrame
