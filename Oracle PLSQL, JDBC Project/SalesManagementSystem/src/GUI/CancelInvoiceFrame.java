package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import BL.Invoice;
import BL.InvoiceComparatorByID_Descending;
import BL.Main;
import DAL.InvoiceDB;

public class CancelInvoiceFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private ImageIcon iconSales, iconInv;
	private URL url;
	private JButton cancelInvBtn;
	private JLabel lbl1, lbl2;
	private JLabel chooseInvLbl;
	private JComboBox<Invoice> invCmbBox;
	private JPanel mainPanel;

	public CancelInvoiceFrame(final Connection con) {
		setTitle("Cancel Invoice");
		setSize(370, 540);
		setLocation(350, 100);
		setResizable(false);

		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);
		
		url = getClass().getResource("image/cancel_inv.jpg");
		iconInv = new ImageIcon(url);

		mainPanel = new JPanel(new BorderLayout());

		lbl1 = new JLabel(iconSales);
		lbl2 = new JLabel(iconInv);
		chooseInvLbl = new JLabel("Choose an invoice you want to cancel");
		chooseInvLbl.setFont(new Font("Arial", Font.BOLD, 18));
		cancelInvBtn = new JButton("Cancel Invoice");
		cancelInvBtn.setFont(new Font("Arial", Font.BOLD, 18));

		invCmbBox = new JComboBox<Invoice>();
		Invoice[] inv = InvoiceDB.getInvoicesFromDB(con);
		if (inv == null) {
			closeFrame();
		}
		else {
			InvoiceComparatorByID_Descending comp = new InvoiceComparatorByID_Descending();
			Main.mergeSort(inv, comp);
			for (int i = 0; i < inv.length; i++) {
				invCmbBox.addItem(inv[i]);
			}

			JPanel midPnl = new JPanel();
			SpringLayout sprLayout = new SpringLayout();
			midPnl.setLayout(sprLayout);
			midPnl.setBackground(Color.CYAN);

			JPanel southPanel = new JPanel();
			southPanel.setBackground(Color.CYAN);

			midPnl.add(chooseInvLbl);
			midPnl.add(invCmbBox);
			midPnl.add(lbl2);

			sprLayout.putConstraint(SpringLayout.WEST, chooseInvLbl, 10,
					SpringLayout.WEST, midPnl);
			sprLayout.putConstraint(SpringLayout.NORTH, chooseInvLbl, 30,
					SpringLayout.NORTH, midPnl);
			sprLayout.putConstraint(SpringLayout.WEST, invCmbBox, 12,
					SpringLayout.WEST, midPnl);
			sprLayout.putConstraint(SpringLayout.NORTH, invCmbBox, 20,
					SpringLayout.SOUTH, chooseInvLbl);
			sprLayout.putConstraint(SpringLayout.WEST, lbl2, 55,
					SpringLayout.WEST, midPnl);
			sprLayout.putConstraint(SpringLayout.NORTH, lbl2, 20,
					SpringLayout.SOUTH, invCmbBox);

			southPanel.add(cancelInvBtn);

			mainPanel.add(lbl1, BorderLayout.NORTH);
			mainPanel.add(midPnl, BorderLayout.CENTER);
			mainPanel.add(southPanel, BorderLayout.SOUTH);

			setContentPane(mainPanel);
			setVisible(true);

			cancelInvBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Invoice inv = (Invoice) invCmbBox.getSelectedItem();
					InvoiceDB.cancelInvoice(con, inv);
					closeFrame();
					JOptionPane.showMessageDialog(null,
							"The invoice was canceled successfully!",
							"Cancel Invoice", JOptionPane.INFORMATION_MESSAGE);

				} // actionPerformed
			});
		}
	} // CancelInvoiceFrame constructor

	private void closeFrame() {
		this.setVisible(false);
	} // closeFrame
	
}  // CancelInvoiceFrame
