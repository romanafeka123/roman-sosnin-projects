package GUI;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import BL.Customer;
import BL.Item;
import BL.Main;

public class OrdersFrame extends JFrame{

	static final long serialVersionUID = 1L;
	private ImageIcon iconOracle;
	private ImageIcon iconSales;
	private URL url;
	private JButton makeOrdBtn, updateOrdBtn, cancelOrdBtn;
	private JLabel lbl1;
	private JLabel lbl2;
	private JPanel mainPanel;
	private MakeOrderFrame makeOrdFrame;
	private UpdateOrderFrame updOrdFrame;
	private CancelOrderFrame cancelOrdFrame;
	
	public OrdersFrame(final Connection con, final Customer[] custArr, final Item[] itemArr) {
		setTitle("Orders");
		setSize(370, 450);
		setLocation(750, 250);
		setResizable(false);
		mainPanel = new JPanel(new BorderLayout());
		lbl1 = new JLabel();
		lbl2 = new JLabel();
		
		JPanel btnPanel = new JPanel(new GridLayout(3, 1));
		
		url = getClass().getResource("image/oracle.jpg");
		iconOracle = new ImageIcon(url);
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);	
		
		lbl1.setIcon(iconSales);
		lbl2.setIcon(iconOracle);
		
		makeOrdBtn = new JButton("Make an Order");
		updateOrdBtn = new JButton("Update an Order");
		cancelOrdBtn = new JButton("Cancel an Order");
		
		makeOrdBtn.setFont(new Font("Arial", Font.BOLD, 18));
		updateOrdBtn.setFont(new Font("Arial", Font.BOLD, 18));
		cancelOrdBtn.setFont(new Font("Arial", Font.BOLD, 18));

		btnPanel.add(makeOrdBtn);
		btnPanel.add(updateOrdBtn);
		btnPanel.add(cancelOrdBtn);
		
		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(btnPanel, BorderLayout.CENTER);
		mainPanel.add(lbl2, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		setVisible(true);
		
		makeOrdBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (makeOrdFrame != null)
					makeOrdFrame.setVisible(false);
				makeOrdFrame = new MakeOrderFrame(con, custArr, itemArr);
				makeOrdFrame.setVisible(true);
			} // actionPerformed
		});
		
		updateOrdBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.drawOpenOrdersFromDB(con);
				if (Main.openOrders.size() != 0) {
					if (updOrdFrame != null)
						updOrdFrame.setVisible(false);
					updOrdFrame = new UpdateOrderFrame(con);
					updOrdFrame.setVisible(true);
				}
			} // actionPerformed
		});
		
		cancelOrdBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.drawOpenOrdersFromDB(con);
				if (Main.openOrders.size() != 0) {
					if (cancelOrdFrame != null)
						cancelOrdFrame.setVisible(false);
					cancelOrdFrame = new CancelOrderFrame(con);
					cancelOrdFrame.setVisible(true);
				}
			} // actionPerformed
		});
		
	}  // MainFrame constructor

}  // MainFrame
