package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import DAL.StockBalanceDB;
import DAL.StoreHouseDB;

public class LackingItemsFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private ImageIcon icon;
	private URL url;
	private JPanel mainPanel;
	private JLabel shNumLbl;
	private JRadioButton shNumRadBtn, showAllRadBtn;
	private JComboBox<Integer> storeHouseIDsCmbBox;
	private JButton searchBtn;
	private JScrollPane scrPane;
	// this is for JTable
	private Object[][] dbInfo;
	private Object[] cols = {"Number" ,"Name", "Item ID", "Description", "Amount"};
	private ResultSet rows;
	private DefaultTableModel model = new DefaultTableModel(dbInfo, cols);
	private JTable jtable = new JTable(model);
	
	public LackingItemsFrame(final Connection con) {
		setTitle("Lacking Items Report");
		setSize(470,600);
		setLocationRelativeTo(null);
		setResizable(false);
		
		mainPanel = new JPanel(new BorderLayout());
		mainPanel = new JPanel();
		SpringLayout sprLayout = new SpringLayout();
		mainPanel.setLayout(sprLayout);
		mainPanel.setBackground(Color.CYAN);
		
		shNumRadBtn = new JRadioButton("According to StoreHouse Number");
		showAllRadBtn = new JRadioButton("Show all");
		shNumRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		showAllRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		shNumRadBtn.setBackground(Color.CYAN);
		showAllRadBtn.setBackground(Color.CYAN);
		ButtonGroup group = new ButtonGroup();
		group.add(shNumRadBtn);
		group.add(showAllRadBtn);
		
		shNumLbl = new JLabel("StoreHouse Number:");
		storeHouseIDsCmbBox = new JComboBox<Integer>();
		
		// drawing all the existing StoreHouse IDs from the DB
		int[] ids = StoreHouseDB.getAllStoreHouseIDsInLackingItems(con);
		for (int i = 0; i < ids.length; i++) {
			storeHouseIDsCmbBox.addItem(ids[i]);
		}
		
		url = getClass().getResource("image/search.jpg");
		icon = new ImageIcon(url);
		searchBtn = new JButton(icon);
		searchBtn.setBackground(Color.WHITE);
		
		jtable.setRowHeight(jtable.getRowHeight() + 10);
		jtable.setFont(new Font("Consolas", Font.BOLD, 14));
		jtable.setBackground(Color.CYAN);
		jtable.setAutoCreateRowSorter(true);
		jtable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jtable.setEnabled(false);
		
		TableColumn col1 = jtable.getColumnModel().getColumn(0);
		col1.setPreferredWidth(80);
		TableColumn col2 = jtable.getColumnModel().getColumn(1);
		col2.setPreferredWidth(100);
		TableColumn col3 = jtable.getColumnModel().getColumn(2);
		col3.setPreferredWidth(70);
		TableColumn col4 = jtable.getColumnModel().getColumn(3);
		col4.setPreferredWidth(150);
		TableColumn col5 = jtable.getColumnModel().getColumn(4);
		col5.setPreferredWidth(50);
		
		scrPane = new JScrollPane(jtable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		displayAllOnJList(con);
		showAllRadBtn.setSelected(true);
		
		storeHouseIDsCmbBox.setEnabled(false);
		searchBtn.setEnabled(false);
		
		mainPanel.add(shNumRadBtn);
		mainPanel.add(shNumLbl);
		mainPanel.add(storeHouseIDsCmbBox);
		mainPanel.add(searchBtn);
		mainPanel.add(showAllRadBtn);
		mainPanel.add(scrPane);

		// shNumRadBtn Block
		sprLayout.putConstraint(SpringLayout.WEST, shNumRadBtn, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, shNumRadBtn, 10, SpringLayout.NORTH, mainPanel);
        // storeHouseIDsCmbBox Block
		sprLayout.putConstraint(SpringLayout.WEST, shNumLbl, 10, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, shNumLbl, 35, SpringLayout.NORTH, shNumRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, storeHouseIDsCmbBox, 10, SpringLayout.EAST, shNumLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, storeHouseIDsCmbBox, 30, SpringLayout.NORTH, shNumRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, searchBtn, 160, SpringLayout.WEST, storeHouseIDsCmbBox);
		sprLayout.putConstraint(SpringLayout.NORTH, searchBtn, 5, SpringLayout.NORTH, mainPanel);
		// showAll Block
		sprLayout.putConstraint(SpringLayout.WEST, showAllRadBtn, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, showAllRadBtn, 30, SpringLayout.NORTH, storeHouseIDsCmbBox);
		// scrPane Block
		sprLayout.putConstraint(SpringLayout.WEST, scrPane, 0, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, scrPane, 10, SpringLayout.SOUTH, showAllRadBtn);
		sprLayout.putConstraint(SpringLayout.SOUTH, scrPane, 0, SpringLayout.SOUTH, mainPanel);
		
		add(mainPanel);
		
		shNumRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				storeHouseIDsCmbBox.setEnabled(true);
				searchBtn.setEnabled(true);	
				displayAllOnJList(con);
			} // actionPerformed
		});
		
		showAllRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				storeHouseIDsCmbBox.setEnabled(false);
				searchBtn.setEnabled(false);	
				displayAllOnJList(con);
			} // actionPerformed
		});

		searchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				displayByIDsOnJList(con, (Integer)storeHouseIDsCmbBox.getSelectedItem());
			} // actionPerformed
		});
		
		setVisible(true);
	}  // StoreHousesBalanceFrame constructor
	
	private void displayAllOnJList(Connection con) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);	
			
			// Show all results
			rows = StockBalanceDB.getLackingItemsViewFromDB(con);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getString(2),
						rows.getInt(3), rows.getString(4), rows.getInt(5) };
				model.addRow(tempRow);
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} // displayAllOnJList
	
	private void displayByIDsOnJList(Connection con, int id) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);				
			rows = StockBalanceDB.getLackingItemsByIDsViewFromDB(con, id);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getString(2),
						rows.getInt(3), rows.getString(4), rows.getInt(5) };
				model.addRow(tempRow);
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} // displayByIDsOnJList
	
}  // StoreHousesBalanceFrame
