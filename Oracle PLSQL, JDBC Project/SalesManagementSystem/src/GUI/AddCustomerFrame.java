package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import BL.Customer;
import DAL.CustomerDB;

public class AddCustomerFrame extends JFrame{
	
	static final long serialVersionUID = 1L;
	private ImageIcon iconOracle;
	private ImageIcon iconSales;
	private URL url;
	private JLabel lbl1, lbl2;
	private JLabel custLbl, enterDetLbl, idLbl, firstNameLbl, lastNameLbl;
	private JComboBox<Customer> custCmbBox;
	private JTextField idTxtFld, firstNameTxtFld, lastNameTxtFld;
	private JButton addCustBtn;
	private JPanel mainPanel, componentsPanel;
	private SpringLayout sprLayout;
	
	public AddCustomerFrame(final Connection con) {
		setTitle("Add a customer to the system");
		setSize(355, 480);
		setLocation(750, 250);
		setResizable(false);
		
		makeGUIComponents(con);
		setSpringLayoutConstraints();
		setListeners(con);
		
		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(componentsPanel, BorderLayout.CENTER);
		mainPanel.add(lbl2, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		setVisible(true);	
	}  // MainFrame constructor
	
	private void makeGUIComponents(Connection con) {
		mainPanel = new JPanel(new BorderLayout());
		lbl1 = new JLabel();
		lbl2 = new JLabel();
		
		componentsPanel = new JPanel();
		sprLayout = new SpringLayout();
		componentsPanel.setLayout(sprLayout);
		componentsPanel.setBackground(Color.CYAN);
		
		url = getClass().getResource("image/oracle.jpg");
		iconOracle = new ImageIcon(url);
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);	
		
		lbl1.setIcon(iconSales);
		lbl2.setIcon(iconOracle);
		
		custLbl = new JLabel("Existing customers:");
		enterDetLbl = new JLabel("Enter details to add a new customer");
		enterDetLbl.setFont(new Font("Arial", Font.BOLD, 16));
		idLbl = new JLabel("ID:");
		firstNameLbl = new JLabel("First name:");
		lastNameLbl = new JLabel("Last name:");
	
		custCmbBox = new JComboBox<Customer>(CustomerDB.getCustFromDB(con));
		idTxtFld = new JTextField(12);
		idTxtFld.setFont(new Font("Arial", Font.BOLD, 16));
		firstNameTxtFld = new JTextField(12);
		firstNameTxtFld.setFont(new Font("Arial", Font.BOLD, 16));
		lastNameTxtFld = new JTextField(12);
		lastNameTxtFld.setFont(new Font("Arial", Font.BOLD, 16));
		
		addCustBtn = new JButton("Add Customer");
		addCustBtn.setFont(new Font("Arial", Font.BOLD, 16));
		
		componentsPanel.add(custLbl);
		componentsPanel.add(custCmbBox);
		componentsPanel.add(enterDetLbl);
		componentsPanel.add(idLbl);
		componentsPanel.add(idTxtFld);
		componentsPanel.add(firstNameLbl);
		componentsPanel.add(firstNameTxtFld);
		componentsPanel.add(lastNameLbl);
		componentsPanel.add(lastNameTxtFld);
		componentsPanel.add(addCustBtn);
	}  // makeGUIComponents
	
	private void setSpringLayoutConstraints() {
		// ComboBox block
		sprLayout.putConstraint(SpringLayout.WEST, custLbl, 10,SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, custLbl, 13,SpringLayout.NORTH, componentsPanel);
		sprLayout.putConstraint(SpringLayout.WEST, custCmbBox, 5,SpringLayout.EAST, custLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, custCmbBox, 10,SpringLayout.NORTH, componentsPanel);
		// enter details block
		sprLayout.putConstraint(SpringLayout.WEST, enterDetLbl, 25,SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, enterDetLbl, 35,SpringLayout.NORTH, custCmbBox);
		// id block
		sprLayout.putConstraint(SpringLayout.WEST, idLbl, 5, SpringLayout.WEST,mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, idLbl, 40,SpringLayout.NORTH, enterDetLbl);
		sprLayout.putConstraint(SpringLayout.WEST, idTxtFld, 54,SpringLayout.EAST, idLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, idTxtFld, 38,SpringLayout.NORTH, enterDetLbl);
		// first name block
		sprLayout.putConstraint(SpringLayout.WEST, firstNameLbl, 5,SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, firstNameLbl, 38,SpringLayout.NORTH, idTxtFld);
		sprLayout.putConstraint(SpringLayout.WEST, firstNameTxtFld, 5,SpringLayout.EAST, firstNameLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, firstNameTxtFld, 35,SpringLayout.NORTH, idTxtFld);
		// last name block
		sprLayout.putConstraint(SpringLayout.WEST, lastNameLbl, 5,SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, lastNameLbl, 38,SpringLayout.NORTH, firstNameTxtFld);
		sprLayout.putConstraint(SpringLayout.WEST, lastNameTxtFld, 5,SpringLayout.EAST, lastNameLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, lastNameTxtFld, 35,SpringLayout.NORTH, firstNameTxtFld);
		// button block
		sprLayout.putConstraint(SpringLayout.WEST, addCustBtn, 95,SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, addCustBtn, 30,SpringLayout.NORTH, lastNameTxtFld);
	}  // setSpringLayoutConstraints
	
	private void setListeners(final Connection con) {
		addCustBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					int id = Integer.parseInt(idTxtFld.getText());
					String firstName = firstNameTxtFld.getText();
					if (firstName.length() == 0)
					    throw new Exception("Please enter first name");
					String lastName = lastNameTxtFld.getText();
					if (lastName.length() == 0)
						throw new Exception("Please enter last name");
					int succedeed = CustomerDB.addNewCustomerToDB(con, id, firstName, lastName);
					if (succedeed != -1) {
					    JOptionPane.showMessageDialog(null, "Customer was added successfully!",
							"Error", JOptionPane.INFORMATION_MESSAGE);
					    idTxtFld.setText("");
					    firstNameTxtFld.setText("");
					    lastNameTxtFld.setText("");
					    updateCustomersInCmbBox(con);
					}
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(null, "Please enter a correct ID !",
							"Error", JOptionPane.ERROR_MESSAGE);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage(),
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			} // actionPerformed
		});
	}
	
	public void updateCustomersInCmbBox(Connection con) {
		// update the ComboBox
		custCmbBox.removeAllItems();
		Customer[] c = CustomerDB.getCustFromDB(con);
		for (int i = 0; i < c.length; i++) {
			custCmbBox.addItem(c[i]);
		}
	} // updateCustomersInCmbBox
}  // MainFrame
