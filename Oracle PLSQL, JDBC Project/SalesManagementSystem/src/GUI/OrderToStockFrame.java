package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import BL.Item;
import BL.Main;
import BL.StoreHouse;
import DAL.ItemDB;
import DAL.StoreHouseDB;

public class OrderToStockFrame extends JFrame{
	
	static final long serialVersionUID = 1L;
	private static final ImageFrame jf = Main.delivery;
	private ImageIcon iconOracle;
	private ImageIcon iconSales;
	private URL url;
	private JLabel lbl1, lbl2;
	private JLabel itemLbl, shLbl, amountLbl;
	private JComboBox<Item> itemsCmbBox;
	private JComboBox<StoreHouse> shCmbBox;
	private JTextField amountTxt;
	private JButton ordBtn;
	private JPanel mainPanel;
	
	public OrderToStockFrame(final Connection con) {
		setTitle("Order items to the stock");
		setSize(355, 420);
		setLocation(750, 250);
		setResizable(false);
		
		mainPanel = new JPanel(new BorderLayout());
		lbl1 = new JLabel();
		lbl2 = new JLabel();
		
		JPanel componentsPanel = new JPanel();
		SpringLayout sprLayout = new SpringLayout();
		componentsPanel.setLayout(sprLayout);
		componentsPanel.setBackground(Color.CYAN);
		
		url = getClass().getResource("image/oracle.jpg");
		iconOracle = new ImageIcon(url);
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);	
		
		lbl1.setIcon(iconSales);
		lbl2.setIcon(iconOracle);
		
		itemLbl = new JLabel("Item:");
		shLbl = new JLabel("StoreHouse:");
		amountLbl = new JLabel("Amount:");
	
		itemsCmbBox = new JComboBox<Item>(ItemDB.getItemsFromDB(con));
		shCmbBox = new JComboBox<StoreHouse>(StoreHouseDB.getStorehousesFromDB(con));
		amountTxt = new JTextField(8);
		amountTxt.setFont(new Font("Arial", Font.BOLD, 16));
		ordBtn = new JButton("Order");
		ordBtn.setFont(new Font("Arial", Font.BOLD, 16));
		
		componentsPanel.add(itemLbl);
		componentsPanel.add(itemsCmbBox);
		componentsPanel.add(shLbl);
		componentsPanel.add(shCmbBox);
		componentsPanel.add(amountLbl);
		componentsPanel.add(amountTxt);
		componentsPanel.add(ordBtn);
		
		sprLayout.putConstraint(SpringLayout.WEST, itemLbl, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, itemLbl, 13, SpringLayout.NORTH, componentsPanel);
		sprLayout.putConstraint(SpringLayout.WEST, itemsCmbBox, 5, SpringLayout.EAST, shLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, itemsCmbBox, 10, SpringLayout.NORTH, componentsPanel);
		
		sprLayout.putConstraint(SpringLayout.WEST, shLbl, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, shLbl, 40, SpringLayout.NORTH, itemLbl);
		sprLayout.putConstraint(SpringLayout.WEST, shCmbBox, 5, SpringLayout.EAST, shLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, shCmbBox, 40, SpringLayout.NORTH, itemsCmbBox);
		
		sprLayout.putConstraint(SpringLayout.WEST, amountLbl, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, amountLbl, 38, SpringLayout.NORTH, shLbl);
		sprLayout.putConstraint(SpringLayout.WEST, amountTxt, 5, SpringLayout.EAST, shLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, amountTxt, 40, SpringLayout.NORTH, shCmbBox);
		
		sprLayout.putConstraint(SpringLayout.WEST, ordBtn, 44, SpringLayout.EAST, shLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, ordBtn, 30, SpringLayout.NORTH, amountTxt);
		
		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(componentsPanel, BorderLayout.CENTER);
		mainPanel.add(lbl2, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		setVisible(true);
		
		ordBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					int amount = Integer.parseInt(amountTxt.getText());
					if (amount < 1)
						throw new Exception();
					int item_to_add = ((Item)itemsCmbBox.getSelectedItem()).getId();
                    int sh_to_add_to = ((StoreHouse)shCmbBox.getSelectedItem()).getId();
					boolean flag = StoreHouseDB.orderItemToStock(con, item_to_add, amount, sh_to_add_to);
					
					if(!flag)
						return;
					JOptionPane.showMessageDialog(null,
							"Your order has been submitted!",
							"Info", JOptionPane.INFORMATION_MESSAGE);
					waitForDelivery();				
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Amount must be a number, greater than 0!",
							"Error", JOptionPane.ERROR_MESSAGE);
				}

			} // actionPerforme
		});
	}  // MainFrame constructor
	
	private void waitForDelivery() {
		new Thread(new Runnable() {
			public void run() {
				jf.setVisible(true);
				try {
					Thread.sleep(2100);
				} catch (InterruptedException e) { }
				jf.setVisible(false);
				
				JOptionPane.showMessageDialog(null,
						"Your order has arrived!",
						"Info", JOptionPane.INFORMATION_MESSAGE);
			}
		}).start();
	}
}  // MainFrame
