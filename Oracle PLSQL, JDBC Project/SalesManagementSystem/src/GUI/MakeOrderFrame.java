package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.border.LineBorder;

import BL.Cart;
import BL.Customer;
import BL.Item;
import BL.Order;
import DAL.OrderDB;
import DAL.StockBalanceDB;

public class MakeOrderFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private ImageIcon iconSales, addToCart, submitOrd;
	private URL url;
	private JButton addCartBtn, submitOrdBtn;
	private JLabel lbl1;
	private JLabel enterDetailsLbl, custLbl, itemLbl, amountLbl, priceLbl, totalPriceLbl;
	private JTextField amountTextF;
	private JComboBox<Customer> custCmbBox;
	private JComboBox<Item> itemCmbBox;
	private JPanel mainPanel;
	private Cart cart;
	private JScrollPane scrPane;	
	private DefaultListModel<String> model = new DefaultListModel<String>();
	private JList<String> jlist = new JList<String>(model);
	private int totalPrice;
	
	public MakeOrderFrame(final Connection con, Customer[] custArr, Item[] itemArr) {
		setTitle("Make an Order");
		setSize(370, 570);
		setLocation(350, 100);
		setResizable(false);
		totalPrice = 0;
		
		url = getClass().getResource("image/sales.jpg");
		iconSales = new ImageIcon(url);
		url = getClass().getResource("image/addtocart.jpg");
		addToCart = new ImageIcon(url);
		url = getClass().getResource("image/submitorder.jpg");
		submitOrd = new ImageIcon(url);
		
		mainPanel = new JPanel(new BorderLayout());
		cart = new Cart();
		
		lbl1 = new JLabel(iconSales);
		enterDetailsLbl = new JLabel("Enter Details to make an Order");
		enterDetailsLbl.setFont(new Font("Arial", Font.BOLD, 18));
		custLbl = new JLabel("Customer: ");
		itemLbl = new JLabel("Item: ");
		amountLbl = new JLabel("Amount: ");
		priceLbl = new JLabel("");
		totalPriceLbl = new JLabel("Total Price:");
		totalPriceLbl.setFont(new Font("Arial", Font.BOLD, 18));
		
		custCmbBox = new JComboBox<Customer>();
		for (int i = 0; i < custArr.length; i++) {
			custCmbBox.addItem(custArr[i]);
		}
		
		itemCmbBox = new JComboBox<Item>();	
		for (int i = 0; i < itemArr.length; i++) {
			itemCmbBox.addItem(itemArr[i]);
		}
		priceLbl.setText("Price: " + itemCmbBox.getItemAt(0).getPrice() + " $");
		
		amountTextF = new JTextField(3);
		amountTextF.setFont(new Font("Arial", Font.BOLD, 18));
		
		JPanel midPnl = new JPanel();
		SpringLayout sprLayout = new SpringLayout();
		midPnl.setLayout(sprLayout);
		midPnl.setBackground(Color.CYAN);
		
		JPanel southPanel = new JPanel();
		southPanel.setBackground(Color.CYAN);

		addCartBtn = new JButton(addToCart);	
		addCartBtn.setBackground(Color.white);
		//
		jlist.setBackground(Color.CYAN);
		jlist.setBorder(new LineBorder(Color.BLACK, 1));
		jlist.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		jlist.setFont(new Font("Consolas", Font.BOLD, 14));
		scrPane = new JScrollPane(jlist, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		displayOnJList(cart);
		
		submitOrdBtn = new JButton(submitOrd);
		submitOrdBtn.setSize(10, 10);
		submitOrdBtn.setBackground(Color.white);
		
		midPnl.add(enterDetailsLbl);
		midPnl.add(custLbl);
		midPnl.add(itemLbl);
		midPnl.add(amountLbl);
		midPnl.add(custCmbBox);
		midPnl.add(itemCmbBox);
		midPnl.add(priceLbl);
		midPnl.add(amountTextF);
		midPnl.add(addCartBtn);
		midPnl.add(scrPane);
		midPnl.add(totalPriceLbl);
		
		sprLayout.putConstraint(SpringLayout.WEST, enterDetailsLbl, 50, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, enterDetailsLbl, 30, SpringLayout.NORTH, midPnl);
		sprLayout.putConstraint(SpringLayout.WEST, custLbl, 30, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, custLbl, 20, SpringLayout.SOUTH, enterDetailsLbl);
		sprLayout.putConstraint(SpringLayout.WEST, custCmbBox, 5, SpringLayout.EAST, custLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, custCmbBox, 15, SpringLayout.SOUTH, enterDetailsLbl);
		sprLayout.putConstraint(SpringLayout.WEST, itemLbl, 30, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, itemLbl, 25, SpringLayout.SOUTH, custLbl);
		sprLayout.putConstraint(SpringLayout.WEST, itemCmbBox, 5, SpringLayout.EAST, itemLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, itemCmbBox, 15, SpringLayout.SOUTH, custCmbBox);
		sprLayout.putConstraint(SpringLayout.WEST, priceLbl, 10, SpringLayout.EAST, itemCmbBox);
		sprLayout.putConstraint(SpringLayout.NORTH, priceLbl, 25, SpringLayout.SOUTH, custLbl);
		
		sprLayout.putConstraint(SpringLayout.WEST, amountLbl, 30, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, amountLbl, 25, SpringLayout.SOUTH, priceLbl);
		sprLayout.putConstraint(SpringLayout.WEST, amountTextF, 5, SpringLayout.EAST, amountLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, amountTextF, 23, SpringLayout.SOUTH, priceLbl);
		
		sprLayout.putConstraint(SpringLayout.WEST, addCartBtn, 160, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, addCartBtn, 15, SpringLayout.SOUTH, itemLbl);
		sprLayout.putConstraint(SpringLayout.WEST, scrPane, 30, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, scrPane, 10, SpringLayout.SOUTH, addCartBtn);
		sprLayout.putConstraint(SpringLayout.WEST, totalPriceLbl, 110, SpringLayout.WEST, midPnl);
		sprLayout.putConstraint(SpringLayout.NORTH, totalPriceLbl, 15, SpringLayout.SOUTH, scrPane);
			
		southPanel.add(submitOrdBtn);
		
		mainPanel.add(lbl1, BorderLayout.NORTH);
		mainPanel.add(midPnl, BorderLayout.CENTER);
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		
		setContentPane(mainPanel);
		
		addCartBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {		
				try {			
				    int amount = Integer.parseInt(amountTextF.getText());
				    if (amountTextF.getText() == "" || amount < 1)
					    throw new Exception();
				    
				    if (!cart.contains((Item)itemCmbBox.getSelectedItem())) {
				    	int items_in_stock = StockBalanceDB.getItemAmountInStock(con, ((Item)itemCmbBox.getSelectedItem()).getId());
				    	if (amount <= items_in_stock)  {
				            cart.addToCart((Item)itemCmbBox.getSelectedItem(), amount);
				            updateItemsInCart();
				    	}
				    	else {
				    		JOptionPane.showMessageDialog(null, "There are " + items_in_stock + " items in the stock.",
									"Cart Update", JOptionPane.INFORMATION_MESSAGE);    		
				    	}
				    }
				    else {
						JOptionPane.showMessageDialog(null,
								"The item is already in the cart.",
								"Cart Update", JOptionPane.INFORMATION_MESSAGE);
				    }
				    			    	
				    if (cart.getCust() == null) {
				    	cart.setCust((Customer)custCmbBox.getSelectedItem());
				    	custCmbBox.setEnabled(false);
				    }			    
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Wrong input! Please enter correct amount",
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			} // actionPerformed
		});
		
		submitOrdBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Order order;
				int total_num_of_open_orders;
				if (cart.cartSize() > 0) {
				    order = new Order(cart);
				    //Main.addOpenOrder(order);			    
				    setVisible(false);
				    total_num_of_open_orders = OrderDB.writeNewOrderToDB(con, order);
				    if(total_num_of_open_orders > 0) {
					   JOptionPane.showMessageDialog(null,
							"   Order has been submitted!\nTotal number of open orders is: "
									+ (total_num_of_open_orders), "Order Submit",
							JOptionPane.WARNING_MESSAGE);
				    }
				} 
				else {
					JOptionPane.showMessageDialog(null, "The Cart is empty!",
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			} // actionPerformed
		});
		// set appropriate price on the price label	
		itemCmbBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				priceLbl.setText("Price: " + ((Item)itemCmbBox.getSelectedItem()).getPrice() + " $");
			} // actionPerformed
		});
	}  // MakeAnOrderFrame constructor

	private void displayOnJList(Cart cart) {
		String str1 = "#" + "    ";
		String str2 = String.format("%2s%-15s%-10s%-3s", str1 , "Description", "Amount", "Total $$");
		model.addElement(str2);
		model.addElement("--------------------------------------");
	} // displayProperlyOnTheJList
	
	private void updateItemsInCart() {
		int i = model.getSize() - 2;
		String num = (i + 1) + "";
		String res = String.format("%-5s%-15s%-10s%3s", num, cart
				.getItemInCartAtIndex(i).getItem().getDescription(), cart
				.getItemInCartAtIndex(i).getAmount(), cart
				.getItemInCartAtIndex(i).getAmount()*cart
				.getItemInCartAtIndex(i).getItem().getPrice());
		model.addElement(res);
		totalPrice = totalPrice + cart.getItemInCartAtIndex(i).getAmount()
				* cart.getItemInCartAtIndex(i).getItem().getPrice();
		totalPriceLbl.setText("Total Price: " + totalPrice + " $");
	} // updateItemsInCart
}  // MakeAnOrderFrame
