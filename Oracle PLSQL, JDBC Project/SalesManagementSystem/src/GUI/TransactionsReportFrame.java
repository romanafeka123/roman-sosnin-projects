
package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import BL.TransactionType;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import BL.Status;
import DAL.CustomerDB;
import DAL.DateSQLQueriesDB;
import DAL.TransactionDB;

public class TransactionsReportFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private ImageIcon icon;
	private URL url;
	private JPanel mainPanel;
	private JRadioButton datesRadBtn, showAllRadBtn, debitRadBtn, creditRadBtn,custIdRadBtn, suppRadBtn;
	private JLabel fromLbl, toLbl, dayLbl1, monthLbl1, yearLbl1, dayLbl2, monthLbl2, yearLbl2;
	private JComboBox<Integer> dayCmbBox1 ,dayCmbBox2, yearCmbBox1, yearCmbBox2;
	private JComboBox<String> monthCmbBox1, monthCmbBox2;
	private JComboBox<Integer> custCmbBox;
	private JButton searchBtn;
	private JScrollPane scrPane;
	private final int NUM_OF_DAYS = 31;
	private final int NUM_OF_MONTHS = 12;
	private static ArrayList<String> months = new ArrayList<String>();	
	// this is for JTable
	private Object[][] dbInfo;
	private Object[] cols = {"Transac. ID" ,"Transac. Date", "Item ID", "Amount", "Type", "Performed by", "Last Name"};
	private ResultSet rows;
	private DefaultTableModel model = new DefaultTableModel(dbInfo, cols);
	private JTable jtable = new JTable(model);
	
	public TransactionsReportFrame(final Connection con) {
		setTitle("Transactions");
		setSize(560,600);
		setLocationRelativeTo(null);
		setResizable(false);
		
		setMonths();
		
		mainPanel = new JPanel(new BorderLayout());
		mainPanel = new JPanel();
		SpringLayout sprLayout = new SpringLayout();
		mainPanel.setLayout(sprLayout);
		mainPanel.setBackground(Color.CYAN);
		
		datesRadBtn = new JRadioButton("According to dates");
		showAllRadBtn = new JRadioButton("Show all");
		debitRadBtn = new JRadioButton("Only Debit");
		creditRadBtn = new JRadioButton("Only Credit");
		suppRadBtn = new JRadioButton("Only Supplier");
		custIdRadBtn = new JRadioButton("By Customer ID");
		
		
		datesRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		showAllRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		debitRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		creditRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		suppRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		custIdRadBtn.setFont(new Font("Arial", Font.BOLD, 16));
		
		datesRadBtn.setBackground(Color.CYAN);
		showAllRadBtn.setBackground(Color.CYAN);
		debitRadBtn.setBackground(Color.CYAN);
		creditRadBtn.setBackground(Color.CYAN);
		suppRadBtn.setBackground(Color.CYAN);
		custIdRadBtn.setBackground(Color.CYAN);
		ButtonGroup group = new ButtonGroup();
		group.add(datesRadBtn);
		group.add(showAllRadBtn);
		group.add(debitRadBtn);
		group.add(creditRadBtn);
		group.add(suppRadBtn);
		group.add(custIdRadBtn);

		fromLbl = new JLabel("From:");
		toLbl = new JLabel("To:");
		fromLbl.setFont(new Font("Arial", Font.BOLD, 15));
		toLbl.setFont(new Font("Arial", Font.BOLD, 15));
		dayLbl1 = new JLabel("Day:");
		monthLbl1 = new JLabel("Month:");
		yearLbl1 = new JLabel("Year:");
		dayLbl2 = new JLabel("Day:");
		monthLbl2 = new JLabel("Month:");
		yearLbl2 = new JLabel("Year:");
		
		dayCmbBox1 = new JComboBox<Integer>();
		monthCmbBox1 = new JComboBox<String>();
		yearCmbBox1 = new JComboBox<Integer>();
		dayCmbBox2 = new JComboBox<Integer>();
		monthCmbBox2 = new JComboBox<String>();
		yearCmbBox2 = new JComboBox<Integer>();
		custCmbBox = new JComboBox<Integer>();
			
		for (int i = 0; i < NUM_OF_DAYS; i++) {
			dayCmbBox1.addItem(i+1);
			dayCmbBox2.addItem(i+1);
		}
		for (int i = 0; i < NUM_OF_MONTHS; i++) {
			monthCmbBox1.addItem(months.get(i));
			monthCmbBox2.addItem(months.get(i));
		}
		// drawing the relevant years of orders from the DB and inserting them into the ComboBoxes
		int year1 = DateSQLQueriesDB.getRelevantYear(con, Status.closed, "min", "trans");
		int year2 = DateSQLQueriesDB.getRelevantYear(con, Status.closed, "max", "trans");
		for (int i = year1; i <= year2; i++) {
			yearCmbBox1.addItem(i);
			yearCmbBox2.addItem(i);
		}
		
		url = getClass().getResource("image/search.jpg");
		icon = new ImageIcon(url);
		searchBtn = new JButton(icon);
		searchBtn.setBackground(Color.WHITE);
		
		jtable.setRowHeight(jtable.getRowHeight() + 10);
		jtable.setFont(new Font("Consolas", Font.BOLD, 14));
		jtable.setBackground(Color.CYAN);
		jtable.setAutoCreateRowSorter(true);
		jtable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);	
		jtable.setEnabled(false);
		setJTableColumnsSize();
		
		scrPane = new JScrollPane(jtable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		displayAllOnJList(con);
		showAllRadBtn.setSelected(true);
		dayCmbBox1.setEnabled(false);
		custCmbBox.setEnabled(false);
		monthCmbBox1.setEnabled(false);
		yearCmbBox1.setEnabled(false);
		dayCmbBox2.setEnabled(false);
		monthCmbBox2.setEnabled(false);
		yearCmbBox2.setEnabled(false);
		searchBtn.setEnabled(false);
		
		mainPanel.add(datesRadBtn);
		mainPanel.add(fromLbl);
		mainPanel.add(dayLbl1);
		mainPanel.add(dayCmbBox1);
		mainPanel.add(monthLbl1);
		mainPanel.add(monthCmbBox1);
		mainPanel.add(yearLbl1);
		mainPanel.add(yearCmbBox1);
		mainPanel.add(toLbl);
		mainPanel.add(dayLbl2);
		mainPanel.add(dayCmbBox2);
		mainPanel.add(monthLbl2);
		mainPanel.add(monthCmbBox2);
		mainPanel.add(yearLbl2);
		mainPanel.add(yearCmbBox2);
		mainPanel.add(searchBtn);
		mainPanel.add(showAllRadBtn);
		mainPanel.add(debitRadBtn);
		mainPanel.add(creditRadBtn);
		mainPanel.add(suppRadBtn);
		mainPanel.add(custIdRadBtn);
		mainPanel.add(custCmbBox);
		mainPanel.add(scrPane);
		
		// datesRadBtn Block
		sprLayout.putConstraint(SpringLayout.WEST, datesRadBtn, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, datesRadBtn, 10, SpringLayout.NORTH, mainPanel);
		// fromLbl Block
		sprLayout.putConstraint(SpringLayout.WEST, fromLbl, 10, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, fromLbl, 35, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, dayLbl1, 10, SpringLayout.EAST, fromLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, dayLbl1, 36, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, dayCmbBox1, 5, SpringLayout.EAST, dayLbl1);
		sprLayout.putConstraint(SpringLayout.NORTH, dayCmbBox1, 33, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, monthLbl1, 10, SpringLayout.EAST, dayCmbBox1);
		sprLayout.putConstraint(SpringLayout.NORTH, monthLbl1, 36, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, monthCmbBox1, 5, SpringLayout.EAST, monthLbl1);
		sprLayout.putConstraint(SpringLayout.NORTH, monthCmbBox1, 33, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, yearLbl1, 10, SpringLayout.EAST, monthCmbBox1);
		sprLayout.putConstraint(SpringLayout.NORTH, yearLbl1, 36, SpringLayout.NORTH, datesRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, yearCmbBox1, 5, SpringLayout.EAST, yearLbl1);
		sprLayout.putConstraint(SpringLayout.NORTH, yearCmbBox1, 33, SpringLayout.NORTH, datesRadBtn);
		// toLbl Block
		sprLayout.putConstraint(SpringLayout.WEST, toLbl, 10, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, toLbl, 35, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, dayLbl2, 10, SpringLayout.EAST, fromLbl);
		sprLayout.putConstraint(SpringLayout.NORTH, dayLbl2, 36, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, dayCmbBox2, 5, SpringLayout.EAST, dayLbl2);
		sprLayout.putConstraint(SpringLayout.NORTH, dayCmbBox2, 33, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, monthLbl2, 10, SpringLayout.EAST, dayCmbBox2);
		sprLayout.putConstraint(SpringLayout.NORTH, monthLbl2, 36, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, monthCmbBox2, 5, SpringLayout.EAST, monthLbl2);
		sprLayout.putConstraint(SpringLayout.NORTH, monthCmbBox2, 33, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, yearLbl2, 10, SpringLayout.EAST, monthCmbBox2);
		sprLayout.putConstraint(SpringLayout.NORTH, yearLbl2, 36, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, yearCmbBox2, 5, SpringLayout.EAST, yearLbl2);
		sprLayout.putConstraint(SpringLayout.NORTH, yearCmbBox2, 33, SpringLayout.NORTH, fromLbl);
		sprLayout.putConstraint(SpringLayout.WEST, searchBtn, 100, SpringLayout.WEST, yearCmbBox2);
		sprLayout.putConstraint(SpringLayout.NORTH, searchBtn, 25, SpringLayout.NORTH, mainPanel);
		// radio buttons Block
		sprLayout.putConstraint(SpringLayout.WEST, showAllRadBtn, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, showAllRadBtn, 30, SpringLayout.NORTH, toLbl);
		
		sprLayout.putConstraint(SpringLayout.WEST, debitRadBtn, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, debitRadBtn, 30, SpringLayout.NORTH, showAllRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, creditRadBtn, 10, SpringLayout.EAST, suppRadBtn);
		sprLayout.putConstraint(SpringLayout.NORTH, creditRadBtn, 30, SpringLayout.NORTH, showAllRadBtn);
		
		sprLayout.putConstraint(SpringLayout.WEST, suppRadBtn, 5, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, suppRadBtn, 30, SpringLayout.NORTH, creditRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, custIdRadBtn, 10, SpringLayout.EAST, suppRadBtn);
		sprLayout.putConstraint(SpringLayout.NORTH, custIdRadBtn, 30, SpringLayout.NORTH, creditRadBtn);
		
		sprLayout.putConstraint(SpringLayout.NORTH, custCmbBox, 30, SpringLayout.NORTH, creditRadBtn);
		sprLayout.putConstraint(SpringLayout.WEST, custCmbBox, 10, SpringLayout.EAST, custIdRadBtn);
		// scrPane Block
		sprLayout.putConstraint(SpringLayout.WEST, scrPane, 0, SpringLayout.WEST, mainPanel);
		sprLayout.putConstraint(SpringLayout.EAST, scrPane, 0, SpringLayout.EAST, mainPanel);
		sprLayout.putConstraint(SpringLayout.NORTH, scrPane, 80, SpringLayout.SOUTH, showAllRadBtn);
		sprLayout.putConstraint(SpringLayout.SOUTH, scrPane, 0, SpringLayout.SOUTH, mainPanel);
		
		add(mainPanel);
		
		showAllRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				disableDates();
				custCmbBox.setEnabled(false);
				displayAllOnJList(con);
			} // actionPerformed
		});
		debitRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				disableDates();
				displayByDebitCredit(con, TransactionType.debit);
				custCmbBox.setEnabled(false);
			} // actionPerformed
		});
		creditRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				disableDates();
				displayByDebitCredit(con, TransactionType.credit);
				custCmbBox.setEnabled(false);
			} // actionPerformed
		});
		suppRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				disableDates();
				displayOnlySuppliers(con);
				custCmbBox.setEnabled(false);
			} // actionPerformed
		});
		custIdRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				disableDates();
				searchBtn.setEnabled(true);
				custCmbBox.removeAllItems();
				Integer[] arr = CustomerDB.getTransactionsCustomers(con);
				for (int i = 0; i < arr.length; i++) {
					custCmbBox.addItem(arr[i]);
				}
				custCmbBox.setEnabled(true);
			} // actionPerformed
		});
		
		datesRadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				custCmbBox.setEnabled(false);
				model.setNumRows(0);
				String[] tempRow;
				for (int i = 0; i < 15; i++) {
					tempRow = new String[] { "", "", "", "", "" };
					model.addRow(tempRow);
				}
				dayCmbBox1.setEnabled(true);
				monthCmbBox1.setEnabled(true);
				yearCmbBox1.setEnabled(true);
				dayCmbBox2.setEnabled(true);
				monthCmbBox2.setEnabled(true);
				yearCmbBox2.setEnabled(true);
				searchBtn.setEnabled(true);	
			} // actionPerformed
		});

		searchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (yearCmbBox1.isEnabled()) {
					int year1 = (Integer) yearCmbBox1.getSelectedItem();
					int year2 = (Integer) yearCmbBox2.getSelectedItem();
					String month1 = (String) monthCmbBox1.getSelectedItem();
					String month2 = (String) monthCmbBox2.getSelectedItem();
					int day1 = (Integer) dayCmbBox1.getSelectedItem();
					int day2 = (Integer) dayCmbBox2.getSelectedItem();

					if (year1 > year2
							|| (year1 == year2 && months.indexOf(month1) > months
									.indexOf(month2))
							|| (year1 == year2 && month1.equals(month2) && day1 > day2)) {
						JOptionPane.showMessageDialog(null,
								"The dates entered wrong.", "Error",
								JOptionPane.ERROR_MESSAGE);
					} else {
						String date1 = "'" + day1 + "-" + month1 + "-" + year1
								+ "'";
						String date2 = "'" + day2 + "-" + month2 + "-" + year2
								+ "'";
						displayByDatesOnJList(con, date1, date2);
					}
				}
				else {
					displayByCustIDs(con, (Integer)custCmbBox.getSelectedItem());
				}
			} // actionPerformed
		});
		
		// When a month or a year is changed, the number of days should be changed also
		monthCmbBox1.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				int year1 = (Integer)yearCmbBox1.getSelectedItem();
				String month1 = (String)monthCmbBox1.getSelectedItem();
				int numOfDays = DateSQLQueriesDB.getNumOfDays(con, year1, month1);
				dayCmbBox1.removeAllItems();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox1.addItem(i+1);
				}
			} // actionPerformed
		});
		yearCmbBox1.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				int year1 = (Integer)yearCmbBox1.getSelectedItem();
				String month1 = (String)monthCmbBox1.getSelectedItem();
				int numOfDays = DateSQLQueriesDB.getNumOfDays(con, year1, month1);
				dayCmbBox1.removeAllItems();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox1.addItem(i+1);
				}
			} // actionPerformed
		});
		monthCmbBox2.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				int year2 = (Integer)yearCmbBox2.getSelectedItem();
				String month2 = (String)monthCmbBox2.getSelectedItem();
				int numOfDays = DateSQLQueriesDB.getNumOfDays(con, year2, month2);
				dayCmbBox2.removeAllItems();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox2.addItem(i+1);
				}
			} // actionPerformed
		});
		yearCmbBox2.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				int year2 = (Integer)yearCmbBox2.getSelectedItem();
				String month2 = (String)monthCmbBox2.getSelectedItem();
				int numOfDays = DateSQLQueriesDB.getNumOfDays(con, year2, month2);
				dayCmbBox2.removeAllItems();
				for (int i = 0; i < numOfDays; i++) {
					dayCmbBox2.addItem(i+1);
				}
			} // actionPerformed
		});
		
		setVisible(true);
	}  // TransactionsReportFrame constructor
	
	private void displayByCustIDs(Connection con, int cust_id) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);				
			rows = TransactionDB.getTransactionsByCustId(con,cust_id);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getDate(2),
						rows.getInt(3), rows.getInt(4), rows.getString(5),
						rows.getInt(6), rows.getString(7) };
				model.addRow(tempRow);
			}
			if (model.getRowCount() == 0) {
				JOptionPane.showMessageDialog(null,
						"Didn't find any transactions between these dates", "Error",
						JOptionPane.ERROR_MESSAGE);			
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} // displayByDatesOnJList
	
	private void displayOnlySuppliers(Connection con) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);				
			rows = TransactionDB.getTransactionsOnlyWithSupplierFromDB(con);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getDate(2),
						rows.getInt(3), rows.getInt(4), rows.getString(5),
						rows.getInt(6), rows.getString(7) };
				model.addRow(tempRow);
			}
			if (model.getRowCount() == 0) {
				JOptionPane.showMessageDialog(null,
						"Didn't find any transactions between these dates", "Error",
						JOptionPane.ERROR_MESSAGE);			
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} // displayByDatesOnJList
	
	private void displayByDebitCredit(Connection con, TransactionType type) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);				
			rows = TransactionDB.getTransactionsByTypeFromDB(con, type);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getDate(2),
						rows.getInt(3), rows.getInt(4), rows.getString(5),
						rows.getInt(6), rows.getString(7) };
				model.addRow(tempRow);
			}
			if (model.getRowCount() == 0) {
				JOptionPane.showMessageDialog(null,
						"Didn't find any transactions between these dates", "Error",
						JOptionPane.ERROR_MESSAGE);			
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} // displayByDatesOnJList
	
	private void displayAllOnJList(Connection con) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);	
			int year1 = (Integer)yearCmbBox1.getItemAt(0);
			int year2 = (Integer)yearCmbBox2.getItemAt(yearCmbBox2.getItemCount()-1);
			String month1 = (String)monthCmbBox1.getItemAt(0);
			String month2 = (String)monthCmbBox2.getItemAt(monthCmbBox2.getItemCount()-1);
			int day1 = (Integer)dayCmbBox1.getItemAt(0);
			int day2 = (Integer)dayCmbBox2.getItemAt(dayCmbBox2.getItemCount()-1);
			
			String date1 = "'" + day1 + "-" + month1 + "-" + year1 + "'";
			String date2 = "'" + day2 + "-" + month2 + "-" + year2 + "'";
			// Showing from the earliest date until the last date of all existing orders
			rows = TransactionDB.getTransactionsByDatesFromDB(con, date1, date2);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getDate(2),
						rows.getInt(3), rows.getInt(4), rows.getString(5),
						rows.getInt(6), rows.getString(7) };
				model.addRow(tempRow);
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	} // displayAllOnJList
	
	private void displayByDatesOnJList(Connection con, String date1, String date2) {
		try {
			// clear the model before adding to it (in order not to add duplicates)
			model.setNumRows(0);				
			rows = TransactionDB.getTransactionsByDatesFromDB(con, date1, date2);
			Object[] tempRow;
			while (rows.next()) {
				tempRow = new Object[] { rows.getInt(1), rows.getDate(2),
						rows.getInt(3), rows.getInt(4), rows.getString(5),
						rows.getInt(6), rows.getString(7) };
				model.addRow(tempRow);
			}
			if (model.getRowCount() == 0) {
				JOptionPane.showMessageDialog(null,
						"Didn't find any transactions between these dates", "Error",
						JOptionPane.ERROR_MESSAGE);			
			}
			rows.close();
			rows.getStatement().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} // displayByDatesOnJList
	
	private void setMonths() {
		months.add("JAN"); months.add("FEB"); months.add("MAR"); months.add("APR");
		months.add("MAY"); months.add("JUN"); months.add("JUL"); months.add("AUG");
		months.add("SEP"); months.add("OCT"); months.add("NOV"); months.add("DEC");	
	} // setMonths
	
	public void setJTableColumnsSize() {
		TableColumn col1 = jtable.getColumnModel().getColumn(0);
		col1.setPreferredWidth(70);
		TableColumn col2 = jtable.getColumnModel().getColumn(1);
		col2.setPreferredWidth(100);
		TableColumn col3 = jtable.getColumnModel().getColumn(2);
		col3.setPreferredWidth(50);
		TableColumn col4 = jtable.getColumnModel().getColumn(3);
		col4.setPreferredWidth(50);
		TableColumn col5 = jtable.getColumnModel().getColumn(4);
		col5.setPreferredWidth(60);
		TableColumn col6 = jtable.getColumnModel().getColumn(5);
		col6.setPreferredWidth(80);
		TableColumn col7 = jtable.getColumnModel().getColumn(6);
		col7.setPreferredWidth(150);	
		
	}  // setJTableColumnsSize
	
	private void disableDates() {
		dayCmbBox1.setEnabled(false);
		monthCmbBox1.setEnabled(false);
		yearCmbBox1.setEnabled(false);
		dayCmbBox2.setEnabled(false);
		monthCmbBox2.setEnabled(false);
		yearCmbBox2.setEnabled(false);
		searchBtn.setEnabled(false);
	}  // disableDates
	
}  // TransactionsReportFrame
