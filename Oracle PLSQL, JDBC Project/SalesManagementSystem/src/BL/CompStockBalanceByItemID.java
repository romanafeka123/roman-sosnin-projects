package BL;

import java.util.Comparator;

// This comparator is for sorting the stock balances in Item Balance Report Frame By Item ID
public class CompStockBalanceByItemID implements Comparator<StockBalance> {

	public int compare(StockBalance s1, StockBalance s2) {
		if (s1.getItem().getId() > s2.getItem().getId())
			return 1;
		if (s1.getItem().getId() < s2.getItem().getId())
			return -1;
		return 0;
	}
}
