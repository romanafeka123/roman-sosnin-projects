package BL;

public class Invoice {
	private int invoice_id;
	private int order_id;
	private String order_date;
	private Cart cart;  // chosen items in the cart
	
	public Invoice(int order_id, String order_date, Cart cart) {
		this.order_id = order_id;
		this.order_date = order_date;
		this.cart = cart;
	}
	
	public Invoice(int invoice_id, int order_id, String order_date, int customer_id) {
		this.invoice_id = invoice_id;
		this.order_id = order_id;
		this.order_date = order_date;
		this.cart = new Cart();
		this.cart.setCust(new Customer(customer_id));
	}
	
	public String getOrder_date() {
		return order_date;
	}
	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}
	public Cart getCart() {
		return cart;
	}
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	public int getInvoice_id() {
		return invoice_id;
	}
	public void setInvoice_id(int invoice_id) {
		this.invoice_id = invoice_id;
	}
	public int getOrder_id() {
		return order_id;
	}
	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}
	@Override
	public String toString() {
		return "Invoice ID: " + invoice_id + ", Order ID: " + order_id
				+ ", Customer ID: " + cart.getCust().getId();
	}
}  // Order
