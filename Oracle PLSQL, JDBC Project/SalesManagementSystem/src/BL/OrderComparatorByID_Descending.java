package BL;

import java.util.Comparator;

public class OrderComparatorByID_Descending implements Comparator<Order> {

	public int compare(Order arg0, Order arg1) {
		if (arg0.getOrder_id() > arg1.getOrder_id())
			return -1;
		if (arg0.getOrder_id() < arg1.getOrder_id())
			return 1;
		return 0;
	}
}
