package BL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import DAL.CustomerDB;
import DAL.ItemDB;
import DAL.OrderDB;
import DAL.StoreHouseDB;
import GUI.ImageFrame;
import GUI.MainFrame;

// The Loading Frame is shown while the application is connecting to the DB
// After the Loading Frame, all the relevant data is being downloaded from the DB
public class Main {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
	static final String DB_URL = "jdbc:oracle:thin:@localhost:1521:xe";
	// orders,... are stored in ArrayLists, because the list is updatable in the application
	public static ArrayList<Order> openOrders = new ArrayList<Order>();
	public static ArrayList<Order> closedOrders = new ArrayList<Order>();
	// these are stored in arrays because they aren't updatable in the application
	public static Customer[] custs;
	public static Item[] items;
	public static StoreHouse[] storehouses;

	// Database credentials
	private static final String USER = "hr";
	private static final String PASS = "725488";
	
	public static ImageFrame delivery;

	public static void main(String[] args) {
		ImageFrame fr = new ImageFrame("Loading...","image/loading.png", 250, 180);
		delivery = new ImageFrame("On the way...","image/orderArrived.gif", 474, 356);
		
		fr.setVisible(true);
		Connection con = null;
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (ClassNotFoundException ex) {
			System.out.println("Error: unable to load driver class!");
			System.exit(1);
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
		/* this function is for receiving a cursor from DB and working on it
		public static Item[] stamFunc(Connection con) in Item class*/

		custs = CustomerDB.getCustFromDB(con);
		items = ItemDB.getItemsFromDB(con);
		storehouses = StoreHouseDB.getStorehousesFromDB(con);

		fr.dispose();
		
		@SuppressWarnings("unused")
		MainFrame m = new MainFrame(con, custs, items);
	}

	public static void drawOpenOrdersFromDB(Connection con) {
		openOrders = new ArrayList<Order>();
		Order[] ord = OrderDB.getOrdersFromDB(con, Status.open);
		if (ord != null) {
			for (int i = 0; i < ord.length; i++) {
				openOrders.add(ord[i]);
			}
		}
	} // drawOpenOrdersFromDB

	public static void drawClosedOrdersFromDB(Connection con) {
		closedOrders = new ArrayList<Order>();
		Order[] ord = OrderDB.getOrdersFromDB(con, Status.closed);
		if (ord != null) {
			for (int i = 0; i < ord.length; i++) {
				closedOrders.add(ord[i]);
			}
		}
	} // drawClosedOrdersFromDB

	public static void addOpenOrder(Order ord) {
		openOrders.add(ord);
	} // addOpenOrder

	public static void deleteOpenOrderFromMain(Order order) {
		openOrders.remove(order);
	} // deleteOrderFromMain

	// update the orders in Main, so that it is synchronized with the orders in
	// DB
	public static void updateOpenOrdersInMain(Connection con) {
		openOrders.clear();
		Order[] ord = OrderDB.getOrdersFromDB(con, Status.open);
		for (int i = 0; i < ord.length; i++) {
			openOrders.add(ord[i]);
		}
	} // updateOrdersInMain

	// MergeSort sorts any array with any comparator (Generic)
	public static <T> void mergeSort(T[] arr, Comparator<T> c) {
		if (arr.length > 1) { // Merge sort the first half
			T[] firstHalf = Arrays.copyOf(arr, arr.length / 2);
			mergeSort(firstHalf, c);
			// Merge sort the second half
			T[] secondHalf = Arrays
					.copyOfRange(arr, arr.length / 2, arr.length);
			mergeSort(secondHalf, c);
			// Merge firstHalf with secondHalf
			T[] temp = merge(firstHalf, secondHalf, c);
			System.arraycopy(temp, 0, arr, 0, temp.length);
		}
	}

	/** Merge two sorted lists */
	private static <T> T[] merge(T[] arr1, T[] arr2, Comparator<T> c) {
		T[] temp = Arrays.copyOf(arr1, arr1.length + arr2.length);
		int current1 = 0; // Current index in list1
		int current2 = 0; // Current index in list2
		int current3 = 0; // Current index in temp
		while (current1 < arr1.length && current2 < arr2.length) {
			int x = c.compare(arr1[current1], arr2[current2]);
			if (x < 0)
				temp[current3++] = arr1[current1++];
			else
				temp[current3++] = arr2[current2++];
		}
		while (current1 < arr1.length)
			temp[current3++] = arr1[current1++];
		while (current2 < arr2.length)
			temp[current3++] = arr2[current2++];
		return temp;
	}
}