package BL;

import java.util.ArrayList;

public class Cart {
	private ArrayList<ItemInCart> arr;
	private Customer cust;
	
	public Cart() {
		arr = new ArrayList<ItemInCart>();
		cust = null;
	}  // Cart constructor
	
	public void addToCart(Item it, int amount) {
		arr.add(new ItemInCart(it, amount));
	}  // addToCart
	
	public ArrayList<ItemInCart> getArr() {
		return arr;
	}
	public int cartSize() {
		return arr.size();	
	}  // getItemsInCart
	
	public ItemInCart getItemInCartAtIndex(int ind) {
		return arr.get(ind);
	}  // getItemAtIndex
	
	public ItemInCart getLastAddedItem() {
		return arr.get(arr.size()-1);
	}  // getLastAddedItem
	
	public Customer getCust() {
		return cust;
	}
	
	public void setCust(Customer cust) {
		this.cust = cust;
	}
	
	public boolean contains(Item item) {
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i).getItem().getId() == item.getId())
				return true;
		}
		return false;
	}  // contains
	
	public void updateAmount(Item item, int amount) {
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i).getItem().getId() == item.getId())
				arr.get(i).updateAmount(amount);
		}
	}  // updateAmount
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < arr.size(); i++) {
			builder.append(arr.get(i) + "\n");
		}
		// Add the last element in the list to the string builder
		return builder.toString();
	}
}  // Cart
