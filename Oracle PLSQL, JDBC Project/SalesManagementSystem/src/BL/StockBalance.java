package BL;

public class StockBalance {
	private Item item;
	private StoreHouse storehouse;
	private int amount;
	
	public StockBalance(int item_id, int storehouse_id, int amount) {
		this.item = new Item(item_id);
		this.storehouse = new StoreHouse(storehouse_id);
		for (int i = 0; i < Main.items.length; i++) {
			if (Main.items[i].getId() == item_id)
				this.item.setDescription(Main.items[i].getDescription());
		}
		for (int i = 0; i < Main.storehouses.length; i++) {
			if (Main.storehouses[i].getId() == storehouse_id)
				this.storehouse.setName(Main.storehouses[i].getName());
		}
		this.amount = amount;
	}
	public StockBalance(StockBalance sb) {
		this.item = sb.item;
		this.storehouse = sb.storehouse;
		this.amount = sb.amount;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public StoreHouse getStorehouse() {
		return storehouse;
	}
	public void setStorehouse(StoreHouse storehouse) {
		this.storehouse = storehouse;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockBalance other = (StockBalance) obj;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "StockBalance [item=" + item + ", storehouse=" + storehouse
				+ ", amount=" + amount + "]";
	}
}  // StockBalance