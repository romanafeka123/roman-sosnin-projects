package BL;

public class Order {
	private int order_id;
	private String order_date;
	private Status status = Status.open;
	private Cart cart;  // chosen items in the cart + Customer
	
	public Order(Cart cart) {
		this.cart = cart;
	}
	public Order(int order_id, String order_date, int customer_id) {
		this.order_id = order_id;
		this.order_date = order_date;
		this.cart = new Cart();
		for (int i = 0; i < Main.custs.length; i++) {
			if (Main.custs[i].getId() == customer_id)
				this.cart.setCust(Main.custs[i]);
		}
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Cart getCart() {
		return cart;
	}
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	public int getOrder_id() {
		return order_id;
	}
	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}
	public String getOrder_date() {
		return order_date;
	}
	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}
	@Override
	public String toString() {
		return "Order ID: " + order_id + "   Date: " + order_date.substring(0, 10)
				+ "  Customer ID: " + cart.getCust().getId();
	}
}  // Order
