package BL;

public class ItemInCart {

	private Item item;
	private int amount;

	public ItemInCart(Item item, int amount) {
		this.item = item;
		this.amount = amount;
	}

	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public void updateAmount(int amount) {
		this.amount += amount;
	}  // updateAmount
	
	@Override
	public String toString() {
		return item.toString() + ", amount: " + amount;
	}
}
