package BL;

import java.util.Comparator;

public class InvoiceComparatorByID_Descending implements Comparator<Invoice> {

	public int compare(Invoice arg0, Invoice arg1) {
		if (arg0.getInvoice_id() > arg1.getInvoice_id())
			return -1;
		if (arg0.getInvoice_id() < arg1.getInvoice_id())
			return 1;
		return 0;
	}
}

