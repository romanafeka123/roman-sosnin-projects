package BL;

import java.util.Comparator;

// This comparator is for sorting the stock balances in StoreHouse Balance Report By StoreHouse ID
public class CompStockBalanceByStoreHouseID implements Comparator<StockBalance> {

	public int compare(StockBalance s1, StockBalance s2) {
		if (s1.getStorehouse().getId() > s2.getStorehouse().getId())
			return 1;
		if (s1.getStorehouse().getId() < s2.getStorehouse().getId())
			return -1;
		return 0;
	}
}
