package BL;

public class StoreHouse {
	private int id;
	private String name;
	
	public StoreHouse(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public StoreHouse(int id) {
		this.id = id;
		this.name = "";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "ID: " + id + (name==null?"":", "+name);
	}
}
