package DAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import BL.StoreHouse;

public class StoreHouseDB {

	// this function returns an array of all the storehouses in the DB
	public static StoreHouse[] getStorehousesFromDB(Connection con) {
		StoreHouse[] sh = null;
		Statement st1 = null;
		Statement st2 = null;
		try {
			st1 = con.createStatement();
			st2 = con.createStatement();
			// calling DB-embedded function that returns the total number of
			// storehouses in the DB
			String sql1 = "select num_of_storehouses from dual";
			String sql2 = "select storehouse_id, storehouse_name"
					+ " from storehouse";
			ResultSet rs1 = st1.executeQuery(sql1);
			ResultSet rs2 = st2.executeQuery(sql2);
			rs1.next();
			int num_of_storehouses = rs1.getInt(1);
			sh = new StoreHouse[num_of_storehouses];
			int i = 0;
			while (rs2.next()) {
				int sh_id = rs2.getInt("storehouse_id");
				String sh_name = rs2.getString("storehouse_name");
				sh[i++] = (new StoreHouse(sh_id, sh_name));
			}

			rs1.close();
			rs2.close();
			st1.close();
			st2.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return sh;
	} // getStorehousesFromDB

	// get all the existing StoreHouse IDs from the DB
	public static int[] getAllStoreHouseIDs(Connection con) {
		int[] ids = null;
		Statement st = null;
		try {
			st = con.createStatement();
			String sql = "select storehouse_id" + " from storehouse";
			ResultSet rs = st.executeQuery(sql);
			ArrayList<Integer> arr = new ArrayList<Integer>();
			while (rs.next()) {
				int sh_id = rs.getInt("storehouse_id");
				arr.add(new Integer(sh_id));
			}
			ids = new int[arr.size()];
			for (int i = 0; i < ids.length; i++) {
				ids[i] = arr.get(i);
			}
			rs.close();
			st.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return ids;
	} // getAllStoreHouseIDs

	// this function adds a new storehouse to the DB, returns 1 if succeeded, -1
	// if failed.
	public static int addNewStorehouseToDB(Connection con, String name) {
		CallableStatement st = null;
		try {
			// calling the stored procedure "create_new_storehouse" in the DB
			String sql = "{call create_new_storehouse (?)}";
			st = con.prepareCall(sql);
			// Bind IN parameters
			st.setString(1, name);
			// Executing the stored procedure
			st.execute();
			st.close();
		} catch (SQLException se) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return -1;
		}
		return 1;
	} // addNewStorehouseToDB

	// get all the existing StoreHouse IDs from the DB
	public static int[] getAllStoreHouseIDsInLackingItems(Connection con) {
		int[] ids = null;
		Statement st = null;
		try {
			st = con.createStatement();
			String sql = "select distinct storehouse_id"
					+ " from lacking_items";
			ResultSet rs = st.executeQuery(sql);
			ArrayList<Integer> arr = new ArrayList<Integer>();
			while (rs.next()) {
				int sh_id = rs.getInt("storehouse_id");
				arr.add(new Integer(sh_id));
			}
			ids = new int[arr.size()];
			for (int i = 0; i < ids.length; i++) {
				ids[i] = arr.get(i);
			}
			rs.close();
			st.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return ids;
	} // getAllStoreHouseIDs

	public static boolean orderItemToStock(Connection con, int item_to_add,
			int amount, int sh_to_add_to) {
		CallableStatement cstmt = null;
		try {
			// calling the stored procedure "add_new_order_header" that is in
			// the DB
			String sql = "{call order_item_to_stock (?, ?, ?)}";
			cstmt = con.prepareCall(sql);
			// Bind IN parameter
			cstmt.setInt(1, item_to_add);
			cstmt.setInt(2, amount);
			cstmt.setInt(3, sh_to_add_to);
			// Executing the stored procedure
			cstmt.execute();
			cstmt.close();
		} catch (SQLException se) {
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	} // orderItemToStock
}
