package DAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import BL.Customer;

public class CustomerDB {
	
	public static Customer[] getCustFromDB(Connection con) {
		Customer[] cust = null;
		Statement st = null;
		try {
			st = con.createStatement();
			String sql = "select customer_id, first_name, last_name"
					  + " from customer";
			ResultSet rs = st.executeQuery(sql);
			ArrayList<Customer> arr = new ArrayList<Customer>();
			while (rs.next()) {
				int cust_id = rs.getInt("customer_id");
				String first = rs.getString("first_name");
				String last = rs.getString("last_name");
				arr.add(new Customer(cust_id, first, last));
			}
			cust = new Customer[arr.size()];
			for (int i = 0; i < arr.size(); i++) {
				cust[i] = arr.get(i);
			}
			rs.close();
			st.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		} 
		return cust;
	}  // getCustFromDB
	
	public static Integer[] getTransactionsCustomers(Connection con) {
		Integer[] cust = null;
		Statement st = null;
		try {
			st = con.createStatement();
			String sql = "select distinct customer_id"
                      + " from transactions_view where customer_id is not null";
			ResultSet rs = st.executeQuery(sql);
			ArrayList<Integer> arr = new ArrayList<Integer>();
			while (rs.next()) {
				int cust_id = rs.getInt("customer_id");
				arr.add(new Integer(cust_id));
			}
			cust = new Integer[arr.size()];
			for (int i = 0; i < arr.size(); i++) {
				cust[i] = arr.get(i);
			}
			rs.close();
			st.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		} 
		return cust;
	}  // getTransactionsCustomers
	
	// this function adds a new customer to the DB, returns 1 if succeeded, -1 if failed.
	public static int addNewCustomerToDB(Connection con, int id, String firstName, String lastName) {
		CallableStatement st = null;
		try {
			// calling the stored procedure "add_customer" in the DB
			String sql = "{call create_new_customer (?, ?, ?)}";
			st = con.prepareCall(sql);
			//Bind IN parameters
			st.setInt(1, id); 
			st.setString(2, firstName);  
			st.setString(3, lastName);  
			// Executing the stored procedure
			st.execute();			
			st.close();
		} catch (SQLException se) {	
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return -1;
		}
		return 1;
	}  // addNewCustomerToDB
}
