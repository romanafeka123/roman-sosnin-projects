package DAL;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import BL.Status;

public class DateSQLQueriesDB {

	// this function returns the number of days for specified month and year
	public static int getNumOfDays(Connection con, int year, String month) {
		int numOfDays = 0;
		ResultSet rs = null;
		Statement st = null;
		try {
			String date = "'1-" + month + "-" + year + "'";
			st = con.createStatement();
			// calling the DB-embedded Function num_of_days_in_date
			String sql = "select num_of_days_in_date(" + date + ") from dual";
			rs = st.executeQuery(sql);
			rs.next();
			numOfDays = rs.getInt(1);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return numOfDays;
	}  // getNumOfDays
	
	// this function returns the earliest/last year of orders/transactions from the DB
	// for the earliest year the type will be MIN, for the last it will be MAX
	public static int getRelevantYear(Connection con, Status s, String type, String ord_or_tr) {
		int year = 0;
		ResultSet rs = null;
		Statement st = null;
		String status;
		String sql = null;
		if (s == Status.open)
			status = "open";
		else
			status = "closed";
		try {
			st = con.createStatement();
			if (ord_or_tr == "order") {
				// calling the Function earliest_or_last_year that is in the DB
				sql = "select distinct earliest_or_last_year_of_order('" + type + "', '"
						+ status + "')" + " from order_header";
			}
			else {
				sql = "select distinct earliest_or_last_year_of_tr('" + type + "') from transaction";			
			}
			rs = st.executeQuery(sql);
			rs.next();
			year = rs.getInt(1);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return year;
	}  // getRelevantYear
}  // DateSQLQueriesDB