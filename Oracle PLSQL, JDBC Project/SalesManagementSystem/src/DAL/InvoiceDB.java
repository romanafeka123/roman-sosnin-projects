package DAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import BL.Invoice;

public class InvoiceDB {

	// Write the new Invoice to the DB. Using store-procedures calling
	// returns true, if succeeded to write it to the DB (can be false, due to 
	// lack of amount in stock)
	public static boolean writeNewInvoiceToDB(Connection con, Invoice inv) {
		CallableStatement cstmt1 = null;
		CallableStatement cstmt2 = null;
		try {
			// To maintain the integrity of business processes, set AutoCommit
			// to false
			con.setAutoCommit(false);
			// calling the stored procedure "add_new_invoice_header" that is in
			// the DB
			String sql = "{call add_new_invoice_header (?, ?)}";
			cstmt1 = con.prepareCall(sql);
			// Bind IN parameters
			cstmt1.setInt(1, inv.getOrder_id()); // Setting the order_id as 1st param
			cstmt1.registerOutParameter(2, java.sql.Types.NUMERIC);
			// Executing the stored procedure
			cstmt1.execute();
			inv.setInvoice_id(cstmt1.getInt(2));

			// insert all the items from the cart into the INVOICE_LINE table in
			// the DB
			sql = "{call add_new_invoice_line (?, ?, ?, ?)}";
			cstmt2 = con.prepareCall(sql);
			for (int count = 0; count < inv.getCart().cartSize(); count++) {
				// Bind IN parameters
				cstmt2.setInt(1, inv.getInvoice_id()); // setting invoice_id
				cstmt2.setInt(2, count + 1); // setting the relevant line number
				cstmt2.setInt(3, inv.getCart().getItemInCartAtIndex(count).getItem()
						.getId()); // item_id
				cstmt2.setInt(4, inv.getCart().getItemInCartAtIndex(count).getAmount()); // amount
				// Executing the stored procedure
				cstmt2.execute();
			}
			con.commit();
			con.setAutoCommit(true);
			cstmt1.close();
			cstmt2.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				// rollback the changes in the DB if an error occured
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	} // writeNewInvoiceToDB

	// this function calls DB-embedded function that returns amount of
	// open/closed orders in the DB
	// input to the function is open/closed
	public static Invoice[] getInvoicesFromDB(Connection con) {
		Invoice[] inv = null;
		Statement st1 = null;
		Statement st2 = null;
		try {
			st1 = con.createStatement();
			st2 = con.createStatement();
			// calling DB-embedded function num_of_invoices
			String sql1 = "select num_of_invoices" + " from dual";
			String sql2 = "select invoice_id, order_id, order_date, customer_id"
					   + " from invoice_header";
			ResultSet rs1 = st1.executeQuery(sql1);
			ResultSet rs2 = st2.executeQuery(sql2);
			rs1.next();
			int num_of_inv = rs1.getInt(1);
			inv = new Invoice[num_of_inv];
			int i = 0;
			while (rs2.next()) {
				int invoice_id_in = rs2.getInt("invoice_id");
				int order_id = rs2.getInt("order_id");
				String order_date = rs2.getString("order_date");
				int customer_id = rs2.getInt("customer_id");
				inv[i++] = new Invoice(invoice_id_in, order_id, order_date,
						customer_id);
			}
			rs1.close();
			rs2.close();
			st1.close();
			st2.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return inv;
	} // getInvoicesFromDB

	// this function invokes stored-procedure in the DB in order to cancel an open order(input)
	// when an invoice is cancelled, the amount is back to the stock balance
	// since the items are back!
	public static void cancelInvoice(Connection con, Invoice inv) {
		CallableStatement cstmt = null;
		try {
			String sql = "{call cancel_invoice (?)}";
			cstmt = con.prepareCall(sql);
			cstmt.setInt(1, inv.getInvoice_id()); // Setting the invoice_id as the IN parameter
			cstmt.execute();
			cstmt.close();
		} catch (SQLException se) {
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	} // deleteInvoiceFromDB
}
