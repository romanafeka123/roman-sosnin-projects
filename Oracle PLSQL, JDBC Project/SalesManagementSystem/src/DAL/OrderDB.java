package DAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import BL.ItemInCart;
import BL.Main;
import BL.Order;
import BL.OrderUpdateType;
import BL.Status;

public class OrderDB {

	// Write the submitted order to the DB (ORDER_HEADER and ORDER_LINE tables are updated)
	// Using store-procedures calling
	public static int writeNewOrderToDB(Connection con, Order order) {
		CallableStatement cstmt1 = null;
		CallableStatement cstmt2 = null;
		Statement st = null;
		int total_num_of_open_orders = -1;
		try {
			// To maintain the integrity of business processes, set AutoCommit
			// to false
			con.setAutoCommit(false);
			// calling the stored procedure "add_new_order_header" that is in
			// the DB
			String sql = "{call add_new_order_header (?, ?)}";
			cstmt1 = con.prepareCall(sql);
			// Bind IN parameter
			cstmt1.setInt(1, order.getCart().getCust().getId()); // Setting the cust_id as IN param
			// Because second parameter is OUT so register it
			cstmt1.registerOutParameter(2, java.sql.Types.NUMERIC);
			// Executing the stored procedure
			cstmt1.execute();
			// Retrieve the total number of open orders with getXXX method
			total_num_of_open_orders = cstmt1.getInt(2);
			st = con.createStatement();
			// get the new order_id from the DB(generated automatically in the
			// DB by sequence)
			String sql2 = "SELECT order_id FROM ORDER_HEADER"
					   + " ORDER BY order_id DESC";
			ResultSet rs = st.executeQuery(sql2);
			rs.next();
			order.setOrder_id(rs.getInt("order_id"));

			// insert all the items from the cart into the ORDER_LINE table in
			// the DB
			sql = "{call add_new_order_line (?, ?, ?, ?)}";
			cstmt2 = con.prepareCall(sql);
			for (int count = 0; count < order.getCart().cartSize(); count++) {
				// Bind IN parameters
				cstmt2.setInt(1, order.getOrder_id()); // setting order_id
				cstmt2.setInt(2, count + 1); // setting the relevant line number
				cstmt2.setInt(3, order.getCart().getItemInCartAtIndex(count)
						.getItem().getId()); // item_id
				cstmt2.setInt(4, order.getCart().getItemInCartAtIndex(count).getAmount()); // amount
				// Executing the stored procedure
				cstmt2.execute();
			}
			con.commit();
			con.setAutoCommit(true);
			rs.close();
			cstmt1.close();
			cstmt2.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				// rollback the changes in the DB if an error occured
				con.rollback();
				// con.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return total_num_of_open_orders;
	} // writeNewOrderToDB

	// this function calls DB-embedded function that returns amount of
	// open/closed orders in the DB
	// input to the function is open/closed
	public static Order[] getOrdersFromDB(Connection con, Status s) {
		Order[] orders = null;
		Statement st1 = null;
		Statement st2 = null;
		Statement st3 = null;
		String status;
		if (s == Status.open)
			status = "open";
		else
			status = "closed";
		try {
			st1 = con.createStatement();
			st2 = con.createStatement();
			st3 = con.createStatement();
			// calling DB-embedded function num_of_orders
			String sql1 = "select num_of_orders('" + status + "')"
					   + " from dual";
			String sql2 = "select order_id, order_date, customer_id"
					   + " from order_header" + " where status = '" + status + "'";
			String sql3 = "select order_id, item_id, amount"
					   + " from order_line";
			ResultSet rs1 = st1.executeQuery(sql1);
			ResultSet rs2 = st2.executeQuery(sql2);
			ResultSet rs3 = st3.executeQuery(sql3);
			rs1.next();
			int num_of_orders = rs1.getInt(1);
			orders = new Order[num_of_orders];
			int i = 0;
			while (rs2.next()) {
				int order_id = rs2.getInt("order_id");
				String order_date = rs2.getString("order_date");
				int customer_id = rs2.getInt("customer_id");
				orders[i++] = new Order(order_id, order_date, customer_id);
			}
			while (rs3.next()) {
				int ord_id = rs3.getInt("order_id");
				int itm_id = rs3.getInt("item_id");
				int amnt = rs3.getInt("amount");
				for (i = 0; i < num_of_orders; i++) {
					if (orders[i].getOrder_id() == ord_id) {
						for (int j = 0; j < Main.items.length; j++) {
							if (Main.items[j].getId() == itm_id)
								orders[i].getCart().addToCart(Main.items[j], amnt);
						}
					}
				}
			}
			rs1.close();
			rs2.close();
			rs3.close();
			st1.close();
			st2.close();
			st3.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return null;
		}
		return orders;
	} // getOpenOrdersFromDB

	// there can be 3 different types of updates. Update amount/status/add new
	// item to the order
	// all three updates are made by calling the DB's stored procedures
	public static int updateOpenOrderInDB(Connection con,
			OrderUpdateType updType, ItemInCart newItemToAdd,
			int amountToChange, int item_id, Order order) {
		CallableStatement cstmt = null;
		int total_num_of_items = 0;
		try {
			String sql = "";
			if (updType == OrderUpdateType.deleteItem) {
				sql = "{call DELETE_ITEM_FROM_OPEN_ORDER (?, ?)}";
				cstmt = con.prepareCall(sql);
				cstmt.setInt(1, order.getOrder_id()); // Setting the order_id as
														// the IN parameter
				cstmt.setInt(2, item_id); // Setting the item_id as the IN
											// parameter
				cstmt.execute();
			} else if (updType == OrderUpdateType.upd_amount) {
				sql = "{call UPDATE_ITEM_AMOUNT_OPEN_ORDER (?, ?, ?)}";
				cstmt = con.prepareCall(sql);
				cstmt.setInt(1, order.getOrder_id()); // Setting the order_id as
														// the IN parameter
				cstmt.setInt(2, item_id); // Setting the item_id as the IN
											// parameter
				cstmt.setInt(3, amountToChange); // Setting the amountToChange
													// as the IN parameter
				cstmt.execute();
			} else if (updType == OrderUpdateType.upd_addItem) {
				sql = "{call add_item_to_open_order (?, ?, ?, ?)}";
				cstmt = con.prepareCall(sql);
				cstmt.setInt(1, order.getOrder_id()); // similar comments
				cstmt.setInt(2, newItemToAdd.getItem().getId());
				cstmt.setInt(3, newItemToAdd.getAmount());
				cstmt.registerOutParameter(4, java.sql.Types.NUMERIC);
				cstmt.execute();
				total_num_of_items = cstmt.getInt(4);
			} else {
				sql = "{call close_order (?)}";
				cstmt = con.prepareCall(sql);
				cstmt.setInt(1, order.getOrder_id()); // similar comments
				cstmt.execute();
			}
			cstmt.close();
		} catch (SQLException se) {
			try {
				cstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return -1;
		}
		return total_num_of_items;
	} // updateOpenOrderInDB

	// this function invokes stored-procedure in the DB in order to cancel an
	// open order(input)
	public static void deleteOpenOrderFromDB(Connection con, Order order) {
		CallableStatement cstmt = null;
		try {
			String sql = "{call cancel_order (?)}";
			cstmt = con.prepareCall(sql);
			cstmt.setInt(1, order.getOrder_id()); // Setting the order_id as the
													// IN parameter
			cstmt.execute();
			cstmt.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
	} // deleteOpenOrderFromDB

	public static ResultSet getOrdersByDatesFromDB(Connection con, Status s, String d1, String d2) {
		ResultSet rs = null;
		Statement st = null;
		String sql = null;
		if (s == Status.open)
			sql = "select * from open_orders_by_dates_view" +
				 " where order_date between " + d1 + " and " + d2;
		else if ((s == Status.closed))
			sql = "select * from closed_orders_by_dates_view" +
			     " where order_date between " + d1 + " and " + d2;
		try {
			st = con.createStatement();	
			rs = st.executeQuery(sql);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return rs;
	} // getOrdersByDatesFromDB

	public static ResultSet getOrdersByIDsFromDB(Connection con, int order_id) {
		ResultSet rs = null;
		Statement st = null;
		try {
			st = con.createStatement();
			// SUBQUERY that shows order details according to input dates
			// (between date1 and date2)
			String sql = "select * from sold_items_report where order_id = " + order_id;
			rs = st.executeQuery(sql);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return rs;
	} // getOrdersByDatesFromDB
}