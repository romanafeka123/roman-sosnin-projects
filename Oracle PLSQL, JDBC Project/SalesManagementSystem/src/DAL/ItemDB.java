package DAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import BL.Item;

public class ItemDB {
	
	public static Item[] getItemsFromDB(Connection con) {
		Item[] items = null;
		Statement st = null;
		try {
			st = con.createStatement();
			String sql = "select item_id, description, price from item";
			ResultSet rs = st.executeQuery(sql);
			ArrayList<Item> arr = new ArrayList<Item>();
			while (rs.next()) {
				int item_id = rs.getInt("item_id");
				String descr = rs.getString("description");
				int pr = rs.getInt("price");
				arr.add(new Item(item_id, descr, pr));
			}
			items = new Item[arr.size()];
			for (int i = 0; i < arr.size(); i++) {
				items[i] = arr.get(i);
			}
			rs.close();
			st.close();		
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		} 
		return items;
	}  // getItemsFromDB
	
	// this function adds a new item to the DB, returns 1 if succeeded, -1 if failed.
	public static int addNewItemToDB(Connection con, String descr, int price) {
		CallableStatement st = null;
		try {
			// calling the stored procedure "create_new_item" in the DB
			String sql = "{call create_new_item (?, ?)}";
			st = con.prepareCall(sql);
			// Bind IN parameters
			st.setString(1, descr);
			st.setInt(2, price);
			// Executing the stored procedure
			st.execute();
			st.close();
		} catch (SQLException se) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return -1;
		}
		return 1;
	} // addNewItemToDB

	/*  this func is for receiving a cursor from DB and working on it
	public static Item[] stamFunc(Connection con) {
		Item[] items = null;
		CallableStatement st = null;
		ResultSet rs = null;
		try {
			String sql = "begin ?:= GET_STORE_HOUSES_WITH_ITEM(?); end;";
			st = con.prepareCall(sql);
			st.registerOutParameter(1,-10);
			st.setInt(2, 3334);
			st.execute();
			rs = (ResultSet)st.getObject(1);
			
		   while(rs.next()) {
				int aa = rs.getInt("sh_id");
				int bb = rs.getInt("amount");
				System.out.println(aa + "   " + bb);
			}
			rs.close();
			st.close();		
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		} 
		return null;
	}  // getItemsFromDB
	*/
}
