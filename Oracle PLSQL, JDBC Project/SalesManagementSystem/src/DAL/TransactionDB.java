package DAL;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import BL.TransactionType;

public class TransactionDB {

	public static ResultSet getTransactionsByDatesFromDB(Connection con, String d1, String d2) {
		ResultSet rs = null;
		Statement st = null;
		try {
			st = con.createStatement();
			String sql = "select * from transactions_view where transaction_date"
					  + " between " + d1 + " AND " + d2;
			rs = st.executeQuery(sql);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return rs;
	} // getTransactionsByDatesFromDB

	public static ResultSet getTransactionsByTypeFromDB(Connection con,
			TransactionType type) {
		ResultSet rs = null;
		Statement st = null;
		try {
			st = con.createStatement();
			String sql = "select * from transactions_view where transaction_type = '" + type + "'";
			rs = st.executeQuery(sql);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return rs;
	} // getTransactionsByTypeFromDB

	public static ResultSet getTransactionsOnlyWithSupplierFromDB(Connection con) {
		ResultSet rs = null;
		Statement st = null;
		try {
			st = con.createStatement();
			String sql = "select * from transactions_view where customer_id is null";
			rs = st.executeQuery(sql);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return rs;
	} // getTransactionsByTypeFromDB

	public static ResultSet getTransactionsByCustId(Connection con, int id) {
		ResultSet rs = null;
		Statement st = null;
		try {
			st = con.createStatement();
			String sql = "select * from transactions_view where customer_id = " + id;
			rs = st.executeQuery(sql);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return rs;
	} // getTransactionsByCustId
}
