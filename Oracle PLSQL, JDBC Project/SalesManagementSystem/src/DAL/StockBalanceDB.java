package DAL;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class StockBalanceDB {

	public static ResultSet getStockBalanceFromDB(Connection con) {
		ResultSet rs = null;
		Statement st = null;
		try {
			st = con.createStatement();
			String sql = "select * from sb_total_amounts_view";
			rs = st.executeQuery(sql);
		} catch (SQLException se) {
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		return rs;
	} // getStockBalanceFromDB

	// type - whether to pull all existing storehouses details or only per
	// specified(input)
	public static ResultSet getStockBalanceForStorehousesFromDB(Connection con,
			String type, int id) {
		ResultSet rs = null;
		Statement st = null;
		String sql;
		try {
			st = con.createStatement();
			if (type.equals("showAll"))
				sql = "select * from storehouses_stock_balance_view";
			else
				sql = "select * from storehouses_stock_balance_view where storehouse_id = "
						+ id;
			rs = st.executeQuery(sql);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return rs;
	} // getStockBalanceForStorehousesFromDB

	// type - whether to pull all existing storehouses details or only per
	// specified(input)
	public static ResultSet getLackingItemsViewFromDB(Connection con) {
		ResultSet rs = null;
		Statement st = null;
		String sql;
		try {
			st = con.createStatement();
			sql = "select * from lacking_items_view";
			rs = st.executeQuery(sql);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return rs;
	} // getLackingItemsViewFromDB

	// type - whether to pull all existing storehouses details or only per
	// specified(input)
	public static ResultSet getLackingItemsByIDsViewFromDB(Connection con,
			int id) {
		ResultSet rs = null;
		Statement st = null;
		String sql;
		try {
			st = con.createStatement();
			sql = "select * from lacking_items_view where sh_id = " + id;
			rs = st.executeQuery(sql);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return rs;
	} // getLackingItemsViewFromDB

	// this func gets the item_id and returns the amount of this item in the
	// stock
	public static int getItemAmountInStock(Connection con, int item_id) {
		int amount = 0;
		ResultSet rs = null;
		Statement st = null;
		try {
			st = con.createStatement();
			// calling the DB-embedded Function num_of_days_in_date
			String sql = "select GET_ITEM_AMOUNT_IN_STOCK(" + item_id
					+ ") from dual";
			rs = st.executeQuery(sql);
			rs.next();
			amount = rs.getInt(1);
		} catch (SQLException se) {
			// Handle errors for JDBC
			try {
				con.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, se.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			// Handle errors for Class.forName
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
		return amount;
	} // getNumOfDays

}
