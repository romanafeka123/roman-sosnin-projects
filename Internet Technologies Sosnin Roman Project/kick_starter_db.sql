-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2015 at 02:12 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kick_starter_db`
--
CREATE DATABASE IF NOT EXISTS `kick_starter_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kick_starter_db`;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `donate_to_project`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `donate_to_project`(IN `title_in` VARCHAR(50), IN `amount_in` INT(6), IN `firstname_in` VARCHAR(30), IN `lastname_in` VARCHAR(30), IN `email_in` VARCHAR(30))
    NO SQL
begin
  declare current_amount_donated INT(6);
  declare current_donators_amount INT(6);
  declare project_proposer_username VARCHAR(30);

  select project_proposer into project_proposer_username 
  from project as p
  where p.project_title = title_in;
  
  select amount_donated into current_amount_donated 
  from project as p
  where p.project_title = title_in;
  
  select donators_amount into current_donators_amount 
  from project as p
  where p.project_title = title_in;

  update project
  set amount_donated = current_amount_donated + amount_in
  where project_title = title_in;
  
  update project
  set donators_amount = current_donators_amount + 1
  where project_title = title_in;

  insert into project_donations
  values (title_in, firstname_in, lastname_in, email_in, amount_in, project_proposer_username);
      
end$$

DROP PROCEDURE IF EXISTS `remove_donator_donation`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_donator_donation`(IN `firstname_in` VARCHAR(30), IN `lastname_in` VARCHAR(30), IN `email_in` VARCHAR(30), IN `project_title_in` VARCHAR(50))
    NO SQL
begin
  declare amount_donated_to_subtract INT(6);
  declare current_amount_donated INT(6);
  declare current_donators_amount INT(6);
  
  select amount_donated into amount_donated_to_subtract 
  from project_donations
  where firstname = firstname_in AND lastname = lastname_in  AND email = email_in AND project_title = project_title_in;
  
  select amount_donated into current_amount_donated 
  from project
  where project_title = project_title_in;
  
  select donators_amount into current_donators_amount 
  from project
  where project_title = project_title_in;

  delete from project_donations
  where firstname = firstname_in AND lastname = lastname_in  AND email = email_in AND project_title = project_title_in;

  update project
  set amount_donated = current_amount_donated - amount_donated_to_subtract
  where project_title = project_title_in;
  
  update project
  set donators_amount = current_donators_amount - 1
  where project_title = project_title_in;
  
end$$

DROP PROCEDURE IF EXISTS `remove_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_user`(IN `username_in` VARCHAR(30))
    NO SQL
begin
  declare the_category VARCHAR(30);
  
  select category into the_category 
  from user_pass
  where username = username_in;
  
end$$

DROP PROCEDURE IF EXISTS `update_donated_amount`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_donated_amount`(IN `title_in` VARCHAR(50), IN `amount_in` INT(6), IN `firstname_in` VARCHAR(30), IN `lastname_in` VARCHAR(30), IN `email_in` VARCHAR(30))
    NO SQL
begin
  declare current_total_amount_donated INT(6);
  declare current_donator_amount_donated INT(6);

  select amount_donated into current_total_amount_donated 
  from project as p
  where p.project_title = title_in;
  
  select amount_donated into current_donator_amount_donated 
  from project_donations as pd
  where pd.project_title = title_in AND pd.firstname = firstname_in AND pd.lastname = lastname_in AND pd.email = email_in;

  update project
  set amount_donated = current_total_amount_donated +        amount_in
  where project_title = title_in;
  
  update project_donations
  set amount_donated = current_donator_amount_donated + amount_in
  where project_title = title_in AND firstname = firstname_in AND lastname = lastname_in AND email = email_in;
      
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `donator`
--

DROP TABLE IF EXISTS `donator`;
CREATE TABLE IF NOT EXISTS `donator` (
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`firstname`,`lastname`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donator`
--

INSERT INTO `donator` (`firstname`, `lastname`, `email`) VALUES
('Emanuel', 'Emanuelovich', 'eman@gmail.com'),
('Judd', 'Trump', 'judd337@yahoo.com'),
('Robert', 'Milkins', 'roberto578@gmail.ru'),
('Yana', 'Makhney', 'yana441@yandex.ru');

-- --------------------------------------------------------

--
-- Table structure for table `img_sources`
--

DROP TABLE IF EXISTS `img_sources`;
CREATE TABLE IF NOT EXISTS `img_sources` (
  `project_title` varchar(50) NOT NULL,
  `image_source` varchar(100) NOT NULL,
  PRIMARY KEY (`image_source`),
  KEY `fk_img_sources_proj` (`project_title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `img_sources`
--

INSERT INTO `img_sources` (`project_title`, `image_source`) VALUES
('Best Workout Shirt', 'pictures/Best Workout Shirt/Shirt1.jpg'),
('Best Workout Shirt', 'pictures/Best Workout Shirt/Shirt2.jpg'),
('Best Workout Shirt', 'pictures/Best Workout Shirt/Shirt3.jpg'),
('Feminist Music', 'pictures/Feminist Music/Music1.jpg'),
('Feminist Music', 'pictures/Feminist Music/Music2.jpg'),
('Feminist Music', 'pictures/Feminist Music/Music3.jpg'),
('Hybrid Watches - Redux COURG', 'pictures/Hybrid Watches - Redux COURG/watch1.jpg'),
('Hybrid Watches - Redux COURG', 'pictures/Hybrid Watches - Redux COURG/watch2.jpg'),
('Hybrid Watches - Redux COURG', 'pictures/Hybrid Watches - Redux COURG/watch3.jpg'),
('SWAT-KATS REVOLUTION', 'pictures/SWAT-KATS REVOLUTION/SWATKATS1.jpg'),
('SWAT-KATS REVOLUTION', 'pictures/SWAT-KATS REVOLUTION/SWATKATS2.jpg'),
('SWAT-KATS REVOLUTION', 'pictures/SWAT-KATS REVOLUTION/SWATKATS3.jpg'),
('Thinnest and lightest solar charger', 'pictures/Thinnest and lightest solar charger/solar1.jpg'),
('Thinnest and lightest solar charger', 'pictures/Thinnest and lightest solar charger/solar2.jpg'),
('Thinnest and lightest solar charger', 'pictures/Thinnest and lightest solar charger/solar3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `project_title` varchar(50) NOT NULL,
  `project_description` varchar(500) NOT NULL,
  `poster_img_src` varchar(100) NOT NULL,
  `requested_amount` int(6) NOT NULL,
  `amount_donated` int(6) NOT NULL,
  `donators_amount` int(6) NOT NULL,
  `expiry_datetime` datetime NOT NULL,
  `project_proposer` varchar(30) NOT NULL,
  PRIMARY KEY (`project_title`),
  KEY `fk_project_proposer_username` (`project_proposer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_title`, `project_description`, `poster_img_src`, `requested_amount`, `amount_donated`, `donators_amount`, `expiry_datetime`, `project_proposer`) VALUES
('Best Workout Shirt', 'Performance apparel scientifically proven to outperform every major sportswear brand. Stay cooler & drier than ever before. Guaranteed. \r\nUsing revolutionary technology, our shirts draw moisture away from the skin?s surface, dispersing it so that it can evaporate rapidly, thus keeping you feeling cool and dry.', 'pictures/Best Workout Shirt/Shirt1.jpg', 500000, 163000, 2, '2015-10-29 02:30:00', 'roma901'),
('Feminist Music', 'Ms. Cacie''s fun, progressive music for children: songs about consent, ableism, anxiety, non-binary gender, dinosaurs, robots, and more! \r\nI write progressive, feminist children''s songs: it''s time to make an album.', 'pictures/Feminist Music/Music1.jpg', 222333, 102700, 4, '2015-10-15 17:11:00', 'roma901'),
('Hybrid Watches - Redux COURG', 'Watches inspired by mission critical pilot instruments, fused with diver tools. Titanium. Powered by your adventures. Ridiculous value. \r\nThis watch is for all those of you out there with adventure in your blood, and missions to tackle.', 'pictures/Hybrid Watches - Redux COURG/watch1.jpg', 2700000, 38000, 1, '2015-12-17 02:30:00', 'ronnie111'),
('SWAT-KATS REVOLUTION', 'Dear SWAT-KATS Fans, join us and be part in the Revolution to bring back the Radical Squadron.\r\nThis new development, which will be done from your pledges, will provide all the necessary material to bring on board key partners ( broadcasters, co-producers, financiers, etc.) for the production of the new television series.', 'pictures/SWAT-KATS REVOLUTION/SWATKATS1.jpg', 150000, 100900, 3, '2015-10-29 17:32:00', 'roma901'),
('Thinnest and lightest solar charger', 'World''s first solar charger that can be placed inside your note or planner. It is a paper thin and ultra light weight solar charger \r\nSolar paper can charge almost any device that recharges via USB, including, smartphones, tablets, walkie talkies, flashlights, portable game consoles, cameras, video cameras, and rechargeable batteries.', 'pictures/Thinnest and lightest solar charger/solar1.jpg', 3400000, 1016800, 3, '2015-12-03 02:30:00', 'ronnie111');

-- --------------------------------------------------------

--
-- Stand-in structure for view `project_and_proposer_details`
--
DROP VIEW IF EXISTS `project_and_proposer_details`;
CREATE TABLE IF NOT EXISTS `project_and_proposer_details` (
`title` varchar(50)
,`proposer_username` varchar(30)
,`proposer_firstname` varchar(30)
,`proposer_lastname` varchar(30)
,`proposer_email` varchar(30)
,`requested_amount` int(6)
,`amount_donated` int(6)
,`expiry_datetime` datetime
);
-- --------------------------------------------------------

--
-- Table structure for table `project_donations`
--

DROP TABLE IF EXISTS `project_donations`;
CREATE TABLE IF NOT EXISTS `project_donations` (
  `project_title` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `amount_donated` int(6) NOT NULL,
  `project_proposer_username` varchar(30) NOT NULL,
  PRIMARY KEY (`project_title`,`firstname`,`lastname`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_donations`
--

INSERT INTO `project_donations` (`project_title`, `firstname`, `lastname`, `email`, `amount_donated`, `project_proposer_username`) VALUES
('Best Workout Shirt', 'Emanuel', 'Emanuelovich', 'eman@gmail.com', 150000, 'roma901'),
('Best Workout Shirt', 'Karisha', 'Sosnin', 'karish901@yandex.ru', 13000, 'roma901'),
('Feminist Music', 'Judd', 'Trump', 'judd337@yahoo.com', 25000, 'roma901'),
('Feminist Music', 'Karisha', 'Sosnin', 'karish901@yandex.ru', 5000, 'roma901'),
('Feminist Music', 'Robert', 'Milkins', 'roberto578@gmail.ru', 68000, 'roma901'),
('Feminist Music', 'Yana', 'Makhney', 'yana441@yandex.ru', 4700, 'roma901'),
('Hybrid Watches - Redux COURG', 'Yana', 'Makhney', 'yana441@yandex.ru', 38000, 'ronnie111'),
('SWAT-KATS REVOLUTION', 'Judd', 'Trump', 'judd337@yahoo.com', 38900, 'roma901'),
('SWAT-KATS REVOLUTION', 'Karisha', 'Sosnin', 'karish901@yandex.ru', 7000, 'roma901'),
('SWAT-KATS REVOLUTION', 'Robert', 'Milkins', 'roberto578@gmail.ru', 55000, 'roma901'),
('Thinnest and lightest solar charger', 'Judd', 'Trump', 'judd337@yahoo.com', 1012200, 'ronnie111'),
('Thinnest and lightest solar charger', 'Karisha', 'Sosnin', 'karish901@yandex.ru', 3400, 'ronnie111'),
('Thinnest and lightest solar charger', 'Yana', 'Makhney', 'yana441@yandex.ru', 1200, 'ronnie111');

-- --------------------------------------------------------

--
-- Stand-in structure for view `proposers_and_donators`
--
DROP VIEW IF EXISTS `proposers_and_donators`;
CREATE TABLE IF NOT EXISTS `proposers_and_donators` (
`username` varchar(30)
,`password` varchar(32)
,`category` varchar(10)
,`firstname` varchar(30)
,`lastname` varchar(30)
,`email` varchar(30)
);
-- --------------------------------------------------------

--
-- Table structure for table `user_pass`
--

DROP TABLE IF EXISTS `user_pass`;
CREATE TABLE IF NOT EXISTS `user_pass` (
  `username` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `category` varchar(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_pass`
--

INSERT INTO `user_pass` (`username`, `password`, `category`, `firstname`, `lastname`, `email`) VALUES
('admin111', '2e9d8a120ef545b73c2a4bb8dc27660d', 'Admin', 'MyAdminFirst', 'MyAdminLast', 'admin901@gmail.com'),
('grisha111', '173a5fb90afecfb791a460d4365701de', 'Proposer', 'Grishka', 'Grishkin', 'grisha236@gmail.ru'),
('judd111', '6182443f6edb3a29f1811fdd9c555ea5', 'Donator', 'Judd', 'Trump', 'judd337@yahoo.com'),
('karish555', 'f7c3db049ab432fd268b0b1beec070ff', 'Donator', 'Karisha', 'Sosnin', 'karish901@yandex.ru'),
('robert111', 'ebbc9f1ff4fb82d846466d21c176380f', 'Donator', 'Robert', 'Milkins', 'roberto578@gmail.ru'),
('roma901', '01d2b461c0e54eab07fcf3ec2dee0058', 'Proposer', 'Roman', 'Sosnin', 'roma901@yandex.ru'),
('ronnie111', '46cc93ddcb5cb529edb07590895de3c7', 'Proposer', 'Ronnie', 'Osullivan', 'ronnie@gmail.com'),
('sara111', 'c0c26e565243b7cb9e71fa06ed24d557', 'Proposer', 'Sara', 'Brightman', 'sara134@gmail.com'),
('yana111', '21c1c38d18f9ba7c86c1e55946d0ff6e', 'Donator', 'Yana', 'Makhney', 'yana441@yandex.ru');

-- --------------------------------------------------------

--
-- Table structure for table `video_sources`
--

DROP TABLE IF EXISTS `video_sources`;
CREATE TABLE IF NOT EXISTS `video_sources` (
  `project_title` varchar(50) NOT NULL,
  `video_src` varchar(100) NOT NULL,
  PRIMARY KEY (`project_title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video_sources`
--

INSERT INTO `video_sources` (`project_title`, `video_src`) VALUES
('Best Workout Shirt', 'videos/Best Workout Shirt/best_workout.mp4'),
('Feminist Music', 'videos/Feminist Music/feminist_music.mp4'),
('Hybrid Watches - Redux COURG', 'videos/Hybrid Watches - Redux COURG/redux_watches.mp4'),
('SWAT-KATS REVOLUTION', 'videos/SWAT-KATS REVOLUTION/swat_kats.mp4'),
('Thinnest and lightest solar charger', 'videos/Thinnest and lightest solar charger/solar_paper.mp4');

-- --------------------------------------------------------

--
-- Structure for view `project_and_proposer_details`
--
DROP TABLE IF EXISTS `project_and_proposer_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `project_and_proposer_details` AS select `p`.`project_title` AS `title`,`up`.`username` AS `proposer_username`,`up`.`firstname` AS `proposer_firstname`,`up`.`lastname` AS `proposer_lastname`,`up`.`email` AS `proposer_email`,`p`.`requested_amount` AS `requested_amount`,`p`.`amount_donated` AS `amount_donated`,`p`.`expiry_datetime` AS `expiry_datetime` from (`project` `p` join `user_pass` `up`) where (`p`.`project_proposer` = `up`.`username`);

-- --------------------------------------------------------

--
-- Structure for view `proposers_and_donators`
--
DROP TABLE IF EXISTS `proposers_and_donators`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `proposers_and_donators` AS select `user_pass`.`username` AS `username`,`user_pass`.`password` AS `password`,`user_pass`.`category` AS `category`,`user_pass`.`firstname` AS `firstname`,`user_pass`.`lastname` AS `lastname`,`user_pass`.`email` AS `email` from `user_pass` where (`user_pass`.`category` <> 'Admin') order by `user_pass`.`username`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `img_sources`
--
ALTER TABLE `img_sources`
  ADD CONSTRAINT `fk_img_sources_proj` FOREIGN KEY (`project_title`) REFERENCES `project` (`project_title`);

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `fk_project_proposer_username` FOREIGN KEY (`project_proposer`) REFERENCES `user_pass` (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
