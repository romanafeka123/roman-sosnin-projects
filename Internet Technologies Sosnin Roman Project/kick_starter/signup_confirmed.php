<?php
@session_start();
include_once 'database/User.php';

$message='';
$url = "signup.php";

if (isset($_POST['registered_by_admin']) && !empty($_POST['registered_by_admin'])) {
	$url = "admin_page.php";
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	//echo $_POST['username'] . "<br>";
	//echo $_POST['password'] . "<br>";
	//echo $_POST['category'] . "<br>";
	$message = User::testSignUp();
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && empty($message)) {    
	$result = User::insertNewUser();
	$donator_insert_result = '';
	if ($_POST['category'] == 'Donator') {
		$donator_insert_result = User::insertNewDonator();
	}
	if($result && $donator_insert_result == '') {
		if (isset($_POST['registered_by_admin']) && !empty($_POST['registered_by_admin'])) {
			$message = "The user was registered successfully!";		
		}
		else {
			$message = "Registration was completed! Login Please";
			$url = "login.php";
		}
	}
	else {
		$message = "Error in signup!";	
	}
}
echo "<script> alert('$message'); window.location.href='$url';</script>";
?>
