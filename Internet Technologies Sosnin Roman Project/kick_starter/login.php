<?php
@session_start();
?>
<html>
  <head>
    <title>Login</title>
      <link rel="stylesheet" type="text/css" href="styles/login_signup_style1.css" />
	  <link rel="stylesheet" type="text/css" href="styles/login_signup_style2.css" />
	  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
  </head>
  <body align="center">
    <div class="container" ng-app="loginApp" ng-controller="loginController">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Please Sign In</h3>
          </div>
          <div class="panel-body">
            <form action="login_confirmed.php" method="post" role="form" name="login" novalidate>
              <fieldset>
                <div class="form-group">
                  <input class="form-control" placeholder="Username" name="username" type="text" autofocus ng-model="username" required>
                </div>
					
                <div class="form-group">
                  <input class="form-control" placeholder="Password" name="password" type="password" ng-model="password" required>
                </div>                          
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select align="center" type="select" name="category" class="btn btn-success"> 
                   <option value="Proposer" selected> Project proposer </option>
				   <option value="Donator"> Donator </option>
				   <option value="Admin" > Admin </option>                                   
                 </select>                                  	                                 
                 <br><br> 
                 <input type="button" class="btn btn-lg btn-success btn-block" ng-click="reset()" value="Reset"></input>
                 <input type="submit" class="btn btn-lg btn-success btn-block" value="Login" ng-disabled="login.username.$invalid || login.password.$invalid" ></input>
               </fieldset>
                            
                <center>
                  <span style="color:red" ng-show="login.username.$dirty && login.username.$invalid">
                    <br><span ng-show="login.username.$error.required">Please enter user name!</span>
   	              </span>

   	              <span style="color:red" ng-show="login.password.$dirty && login.password.$invalid">
                    <br><span ng-show="login.password.$error.required">Please enter password!</span>
   	              </span>
   	            </center>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <script>
		var loginApp = angular.module("loginApp", []);
		loginApp.controller('loginController', function($scope) {
  		 $scope.reset = function(){
				$scope.username = "";
				$scope.password = "";
   		}   
  		 $scope.reset();
		});
	</script>
    
    </body>
</html>