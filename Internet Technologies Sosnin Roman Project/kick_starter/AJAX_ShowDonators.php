<?php
@session_start();
include_once 'database/Donations.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$all_donators = Donations::getAllDonationsOfByProposerUsername($_SESSION['username']);

$response= "";
foreach($all_donators['data'] as $donator) {
	if($response!= "") {
	   $response.= ",";
	}
	$response.= '{"project_title":"'.  $donator['project_title'] . '",';
	$response.= '"firstname":"'.       $donator['firstname'] . '",';
	$response.= '"lastname":"'.        $donator['lastname'] . '",';
	$response.= '"email":"'.           $donator['email'] . '",';
	$response.= '"amount_donated":"'.  $donator['amount_donated'] . '"}';
}
$response='{"records":['.$response.']}';
echo($response);
?>