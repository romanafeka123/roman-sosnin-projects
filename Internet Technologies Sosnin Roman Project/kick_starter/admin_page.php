<?php
@session_start();
include_once 'database/User.php';

define('ALL_USERS_TO_SHOW', 1);
define('ONLY_PROPOSERS', 2);
define('ONLY_DONATORS', 3);

$users_to_show = ALL_USERS_TO_SHOW;

if (isset($_POST['user_index_to_delete']) && !empty($_POST['user_index_to_delete'])) {
	for ($index = 1; $index <= $_POST['user_index_to_delete']; $index++) {
		if (isset($_POST['num' . $index]) && !empty($_POST['num' . $index]) ) {
			User::removeUser($_POST['num' . $index]);
		}
	}	
	$message = "All checked users were deleted successfully!";
	echo "<script> alert('$message'); </script>";
}

if (isset($_POST['users_to_show']) && !empty($_POST['users_to_show'])) {
	if ($_POST['users_to_show'] == ONLY_PROPOSERS) {
		$users_to_show = ONLY_PROPOSERS;
	}
	else if ($_POST['users_to_show'] == ONLY_DONATORS) {
		$users_to_show = ONLY_DONATORS;
	}
}
?>

<!doctype html>
<html>
  <head>
    <title> Admin access </title>
    <link rel="stylesheet" type="text/css" href="styles/admin_style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
  </head>
  <body align="center" ng-app="adminApp">
    <h2 align="center"><u>Admin access:</u></h2>  
      <div class="container" ng-controller="registerController">
        <div class="row">
          <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
              <div class="panel-body">
                <h3 align="center">Register new user:</h3>
                <form action="signup_confirmed.php" method="post" role="form" name="registerForm" novalidate>
                    <div class="form-group">
                      <input class="form-control" placeholder="First name" name="firstname" type="text" ng-minlength="2" ng-maxlength="30" autofocus ng-model="firstname" required>
                    </div>
                    <div class="form-group">
                      <input class="form-control" placeholder="Last name" name="lastname" type="text" ng-minlength="2" ng-maxlength="30" ng-model="lastname" required>
                    </div>
                    <div class="form-group">
                      <input class="form-control" placeholder="Email" name="email" type="email" ng-model="email" required>
                    </div>
                    <div class="form-group">
                      <input class="form-control" placeholder="Username" name="username" type="text" ng-minlength="6" ng-maxlength="30" ng-model="username" required>
                    </div>
                    <div class="form-group">
                      <input class="form-control" placeholder="Password" name="password" type="password" ng-minlength="6" ng-maxlength="30" ng-model="password" required>
                    </div>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                  
                    <select align="center" type="select" name="category" class="btn btn-success"> 
                      <option value="Proposer" selected> Project proposer </option>
					  <option value="Donator"> Donator </option>                               
                    </select>  
                    <br><br>                
                    <center>                 
                      <input type="button" class="btn btn-lg btn-success btn-block" ng-click="reset()" value="Reset"></input><br>         	
                      <input type="submit" class="btn btn-lg btn-success btn-block" value="Go" ng-disabled="registerForm.firstname.$invalid || registerForm.lastname.$invalid || registerForm.email.$invalid || registerForm.username.$invalid || registerForm.password.$invalid"></input>                         
                      <input type="hidden" name="registered_by_admin" value="registered_by_admin"></input>         
                               
                      <span style="color:red" ng-show="(registerForm.firstname.$dirty && registerForm.firstname.$invalid) || (registerForm.lastname.$dirty && registerForm.lastname.$invalid) || (registerForm.email.$dirty && registerForm.email.$invalid) || (registerForm.username.$dirty && registerForm.username.$invalid) || (registerForm.password.$dirty && registerForm.password.$invalid)">
                         <br><span ng-show="registerForm.firstname.$error.required || registerForm.lastname.$error.required || registerForm.email.$error.required || registerForm.username.$error.required || registerForm.password.$error.required">Please fill all fields!</span>
                         <span ng-show="registerForm.email.$error.email"><br>Please enter valid email!</span>
                         <span ng-show="registerForm.firstname.$error.minlength || registerForm.lastname.$error.minlength"><br>FirstName & LastName should contain at least 2 letters!</span>
      			  		 <span ng-show="registerForm.firstname.$error.maxlength || registerForm.lastname.$error.maxlength"><br>FirstName & LastName should contain maximum 30 letters!</span>
      			  		 <span ng-show="registerForm.username.$error.minlength || registerForm.password.$error.minlength"><br>Username & Password should contain at least 6 letters!</span>
      			  		 <span ng-show="registerForm.username.$error.maxlength || registerForm.password.$error.maxlength"><br>Username & Password should contain maximum 30 letters!</span>
   	                  </span>
   	                  </center> 	                           	                        
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <hr>   
      <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="myform1" >
        <div align="center">
     	  <input type="submit" value="Show all users"/ class="btn btn-success btn-block"> 
     	  <input type="hidden" value="<?php echo ALL_USERS_TO_SHOW ?>" name="users_to_show"/>
     	</div>
   	  </form> <br>
   
   	  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="myform2" >
        <div align="center">
     	  <input type="submit" value="Show only donators"/ class="btn btn-success btn-block"> 
     	  <input type="hidden" value="<?php echo ONLY_DONATORS ?>" name="users_to_show"/>
     	</div>
   	  </form> <br>
   
   	   <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="myform3" >
         <div align="center">
     	   <input type="submit" value="Show only proposers"/ class="btn btn-success btn-block"> 
     	   <input type="hidden" value="<?php echo ONLY_PROPOSERS ?>" name="users_to_show"/> 
     	 </div>
   	   </form> <br>
   		
   <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="myform4" >
     <div align="center">
       <table border=1 cellpadding=2 cellspacing=2 >
         <tr>
	       <th width=5 align="center">	V	    </th>
	       <th width=100 align="center">	Category	</th>
   	       <th width=150 align="center">	User name   </th>
           <th width=150 align="center">	First name	</th>
           <th width=150 align="center">	Last name	</th>
           <th width=150 align="center">	Email		</th>
         </tr>

   <?php
      $rows = null;
   	  if ($users_to_show == ALL_USERS_TO_SHOW) {
   	  	$rows = User::getAllProposersAndDonators();
   	  }
   	  else if ($users_to_show == ONLY_PROPOSERS) {
   	  	$rows = User::getAllProposers();
   	  }
   	  else if ($users_to_show == ONLY_DONATORS) {
   	  	$rows = User::getAllDonators();
   	  }
   	  
   	  $user_index_to_delete = 1;
   	  // printing all the users:  	    
   	  
      foreach($rows['data'] as $row) {
      	 echo "<tr>";
      	 echo "<td align=\"left\"><b> <input type=\"checkbox\" name=\"num" . $user_index_to_delete . "\" value=\"" . $row["username"] .  "\"/></b></td>";
         echo "<td align=\"center\"><b>" .$row["category"]."  </b></td>";
         echo "<td align=\"center\"><b>" .$row["username"]."  </b></td>";
         echo "<td align=\"center\"><b>" .$row["firstname"]." </b></td>";
         echo "<td align=\"center\"><b>" .$row["lastname"]."  </b></td>";
         echo "<td align=\"center\"><b>" .$row["email"]."	  </b></td>";    
         echo "</tr>";
         $user_index_to_delete++;
      }     
   ?>
   
       </table>   
       <input type="hidden" value=" <?php echo $user_index_to_delete ?> " name="user_index_to_delete"/> <br>
       <input id="buttn" type="submit" value="Delete checked users"/ class="btn btn-lg btn-success btn-block"> <br> 
     </div>
  </form>


    <div align="center" ng-controller="projDetailsController">
      <h2> Enter project's title to see its details: </h2>
        <div class="col-md-4 col-md-offset-4">
	      <div class="form-group">
            <input class="form-control" placeholder="Enter project title" type="text" ng-model="projectTitle"/>
          </div>         
        </div>   
        <br><br><br>
        <button class="btn btn-lg btn-success btn-block" ng-click="findProject()">Search</button><br>

	    <table align="center" border=1 cellpadding=2 cellspacing=2 >
		  <tr>
	  	    <th width=150 align="center">	Project title     </th>
      	    <th width=150 align="center">	Proposer username </th>
      	    <th width=150 align="center">	First name	      </th>
      	    <th width=150 align="center">	Last name		  </th>
      	    <th width=150 align="center">	Email             </th>
      	    <th width=150 align="center">	Requested Amount  </th>
      	    <th width=150 align="center">	Amount donated    </th>
      	    <th width=150 align="center">	Expiry date       </th>
   	      </tr>
      	  <tr ng-repeat="x in result">
            <td align="center"><b> {{ x.title}}   </b></td>
            <td align="center"><b> {{ x.proposer_username}}       </b></td>
            <td align="center"><b> {{ x.proposer_firstname}}        </b></td>
            <td align="center"><b> {{ x.proposer_lastname}}           </b></td>
            <td align="center"><b> {{ x.proposer_email}}  </b></td>    
            <td align="center"><b> {{ x.requested_amount}}  </b></td>
            <td align="center"><b> {{ x.amount_donated}}  </b></td>
            <td align="center"><b> {{ x.expiry_datetime}}  </b></td>
          </tr>
        </table>     
	    <br><br><br>
      </div>

      <script>
	    var adminApp = angular.module("adminApp", []);
		
		adminApp.controller('registerController', function($scope) {
  		 $scope.reset = function(){
				$scope.firstname = "";
				$scope.lastname = "";
				$scope.email = "";
				$scope.username = "";
				$scope.password = "";
   		}   
  		 $scope.reset();
		});
					
		adminApp.controller('projDetailsController', function($scope, $http) {	
      	$scope.findProject = function() {
      		data = {
            	'projectTitle' : $scope.projectTitle
        	};
			$http.post("AJAX_AdminShowProject.php", data).success(function(response) {
				var theResponse = response.records;
				if (theResponse == "") {
					theResponse = "Project with such title does not exist!";
				}
				$scope.result = theResponse;				
			});
      	}
	   });	   
    </script>
  </body>
</html>