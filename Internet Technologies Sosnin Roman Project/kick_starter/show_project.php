<?php
@session_start();
include_once 'database/Project.php';

if (isset($_POST['project_title'])) {
	$_SESSION['project_title'] = $_POST['project_title'];
}
?>

<html>
  <head>
    <title> Project details </title> 
    <link rel="stylesheet" type="text/css" href="styles/project_propose_style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
  </head>
  <body> <br><br>
<?php
	if (isset($_POST['project_title'])) {
		$project_details = Project::getProjectByTitle($_POST['project_title']);
		$all_img_sources = Project::getAllImgSourcesOfProject($project_details['project_title']);
    	echo "<center>";
        echo "<div class=\"proj_show\">";
        echo "<h3>" . $project_details['project_title'] . "</h3>";
        echo "<h4>" . $project_details['project_description'] . "</h4> <br>";
        foreach($all_img_sources['data'] as $src) {
        	echo "<img src=\"" . $src['image_source'] . "\" width=250 height=200>";
        }
        // show the video
        $video_src = Project::getVideoSource($project_details['project_title']);
        foreach($video_src['data'] as $src) {
        	$video_src =  $src['video_src'];
        }
        echo "<video width=\"320\" height=\"240\" controls>";
        echo "<source src=\"" . $video_src . "\" type=\"video/mp4\">";
        echo "Your browser does not support video!";
        echo "</video>";
        
        echo "<br>Donators amount: " . $project_details['donators_amount'] . "<br>";
	    echo "Desired sum: " . $project_details['requested_amount'] . "$<br>";
		echo "Amount donated: " . $project_details['amount_donated'] . "$<br>";
		echo round((($project_details['amount_donated']/$project_details['requested_amount'])*100)*100)/100 ."%<br>";
		echo "Expiry date: " . $project_details['expiry_datetime'] . "<br>";
        echo "</div>"; 
        echo "</center>";
    }
?>
    <!-- here insert the angularJS DB -->
    <hr><h2 align="center"> The project's donators: </h2> 
	<div ng-app="myApp" ng-controller="donatorsCtrl">	  
	  <table align="center" border=1 cellpadding=2 cellspacing=2 >
		<tr>
      	  <th width=200 align="center">	First name	</th>
      	  <th width=200 align="center">	Last name	</th>
      	  <th width=200 align="center">	Email		</th>
      	  <th width=150 align="center">	Amount donated</th>
   	    </tr>
      	<tr ng-repeat="x in donators">
         <td align="center"><b> {{ x.donator_firstname}}       </b></td>
         <td align="center"><b> {{ x.donator_lastname}}        </b></td>
         <td align="center"><b> {{ x.donator_email}}           </b></td>
         <td align="center"><b> {{ x.amount_donated}}  </b></td>    
         </tr>
      </table>    
	</div>
	
	<script>
	var app = angular.module('myApp', []);
	app.controller('donatorsCtrl', function($scope, $http) {
		$http.get("AJAX_ShowDonationsOfProject.php").success(function(response) {
			$scope.donators= response.records;
		});
	});
	</script>
  </body>
</html>