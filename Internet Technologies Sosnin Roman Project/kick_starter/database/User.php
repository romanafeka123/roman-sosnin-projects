<?php
@session_start();
include_once 'database/Database.php';

define('MIN_FIRSTNAME_LEN', 2);
define('MIN_LASTNAME_LEN', 2);
define('MIN_USERNAME_LEN', 6);
define('MIN_PASSWORD_LEN', 6);

class User {

    /*
      function to check if form was submitted ok
     * return errors if found any
     */
    public static function checkSignIn() {
        $err = '';
        if ((!isset($_POST['username'])) || (!isset($_POST['password'])) || (!isset($_POST['category'])) || 
             (empty($_POST['username'])) || (empty($_POST['password'])) || (empty($_POST['category']))) {
            $err = "Please fill in all the fields!";
        } else {
            $user = $_POST['username'];
            $result = User::getUser($user);
            //if user was found in the database -> continue with checking the password, otherwise show an error
            if ($result) {
                $password = $_POST['password'];
                $category = $_POST['category'];
                // verify the password
                $access = User::checkUserPassword($user, $password, $category);      
                if (!$access) {
                    $err = "Password does not match";
                }
            } else {
                $err = "This username wasn't registered!";
            }
        }
        return $err;
    }

    /*
     * function to check if form was submitted ok
     * return errors if found any
     */
    public static function testSignUp() {
        $err = '';
        if ((!isset($_POST['firstname'])) || (!isset($_POST['lastname'])) || (!isset($_POST['email'])) || (!isset($_POST['username'])) || (!isset($_POST['password'])) || (!isset($_POST['category'])) ||
             (empty($_POST['firstname'])) || (empty($_POST['lastname'])) || (empty($_POST['email'])) ||  (empty($_POST['username'])) ||(empty($_POST['password'])) || (empty($_POST['category']))) {
            $err = "Please fill in all the fields!";
        } else {
            $string_exp = "/^[A-Za-z0-9 .'-]+$/";
            if (!preg_match($string_exp, $_POST['firstname'])) {
                $err = "The Username you entered does not appear to be valid.";
            }
            if (!preg_match($string_exp, $_POST['lastname'])) {
                $err = "The Password you entered does not appear to be valid.";
            }
            if (!preg_match($string_exp, $_POST['username'])) {
                $err = "The Password you entered does not appear to be valid.";
            }
            if (!preg_match($string_exp, $_POST['password'])) {
                $err = "The Password you entered does not appear to be valid.";
            }
            $user = $_POST['username'];
            $result = User::getUser($user);
            //if user was not found in database -> create new user
            if (!$result) {
                if (strlen($_POST['firstname']) < MIN_FIRSTNAME_LEN) {
                    $err = "First name must contain at least " . MIN_FIRSTNAME_LEN . " digits!";
                }
                if (strlen($_POST['lastname']) < MIN_LASTNAME_LEN) {
                    $err = "Last name must contain at least " . MIN_LASTNAME_LEN . " digits!";
                }
                if (strlen($_POST['username']) < MIN_USERNAME_LEN) {
                    $err = "User name must contain at least " . MIN_USERNAME_LEN . " digits!";
                }
                if (strlen($_POST['password']) < MIN_PASSWORD_LEN) {
                    $err = "Password must contain at least " . MIN_PASSWORD_LEN . " digits!";
                }
            } else {
                $err = "User is already registered...";
            }
        }
        return $err;
    }

    public static function insertNewUser() {
    	$db = new Database();
    	$firstname = $_POST['firstname'];
    	$lastname = $_POST['lastname'];
    	$email = $_POST['email'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $category = $_POST['category'];
        $password = User::calculatePassword($password);
        $rows = $db->insert("user_pass", array('username' => $username, 
											   'password'=> $password, 
											   'category' => $category, 
											   'firstname' => $firstname, 
											   'lastname' => $lastname, 
											   'email' => $email),
											    array('username', 'password', 'category', 'firstname', 'lastname', 'email'));
		$db->db = null;									    
        return $rows;
    }
    
    public static function insertNewDonator() {
    	$db = new Database();
    	$firstname = $_POST['firstname'];
    	$lastname = $_POST['lastname'];
    	$email = $_POST['email'];
    	$result = $db->insert("donator", array('firstname' => $firstname, 
											   'lastname' => $lastname, 
											   'email' => $email),
											    array('firstname', 'lastname', 'email'));
		$db->db = null;
    	if ($result["status"] == "success") {
    		return '';
    	}   	
    	return 'error';
    }
    
    /**
     * get user from database
     * return false if was not found
     * @param - $user - a user name to search for
     */
    public static function getUser($user) {
        $db = new Database();
        $rows = $db->select("user_pass", array('username'=>$user));
        $username = '';    
        foreach($rows['data'] as $row) {  $username = $row['username'] ; }
        $db->db = null;
        if (strlen($username) > 0) {
            return $username;
        } else {
            return 0;
        }
    }
    
	public static function calculatePassword($pass) {
   		$pass = $pass[0].$pass.$pass[0];  //adding the first letter of the password to the begining and ending of the password string. (For example: "1234" => "112341")
   		$pass = md5($pass);  //encrypts the password
   		return $pass;
	}

/* Checks the user and password for match. Returns a STRING of the password if authorized, FALSE if not allowed. */
	public static function checkUserPassword($user, $pass, $category) {
		$db = new Database();
		$with_calc = true;
		if ($user == "" || $pass == "") {  //if empty username or password, return false
			$db->db = null;
   			return false;
		}
		$encrypted_pass = ($with_calc == true) ? User::calculatePassword($pass) : $pass;  // encrypts the password if needed
   		$res = $db->select("user_pass", array('username'=>$user, 'password'=>$encrypted_pass, 'category'=>$category));
   		$username = '';    
   		$passw = '';
   		$cats = '';
        foreach($res['data'] as $row) {  
        	$username = $row['username'];
        	$passw = $row['password'];
        	$cats = $row['category'];
        }
        $db->db = null;
        if (strlen($username) > 0) {
            return $username;
        }
        return FALSE;
	}
	
	// using the DB view to get the data
	public static function getAllProposersAndDonators() {	
		$db = new Database();
		$sql = "select * from proposers_and_donators";
		$rows['data'] = $db->db->query($sql);
		$db->db = null;
		return $rows;
	}
	
	public static function getAllProposers() {	
		$db = new Database();
		$rows = $db->select("user_pass", array('category'=>'Proposer'));
		$db->db = null;
		return $rows;
	}
	
	public static function getAllDonators() {	
		$db = new Database();
		$rows = $db->select("user_pass", array('category'=>'Donator'));
		$db->db = null;
		return $rows;
	}
	
	public static function removeUser($username) {	
		$db = new Database();
		$the_user = $db->select("user_pass", array('username'=>$username));
		foreach($the_user['data'] as $row) { $the_user = $row; }
		if ($the_user['category'] == 'Donator') {
			//User::removeDonator()
		}
		$db->delete("user_pass", array('username'=>$username));	
		// if the user is additionally Donator, then delete the entry from Donator table also.
		$db->delete("donator", array('firstname'=>$the_user['firstname']));
		$db->db = null;
	}
	
	public static function removeDonatorDonation($donator_details, $project_title) {
    	$donator_details = explode("_", $donator_details);
    	$db = new Database();
    	$stmt = $db->db->prepare("CALL remove_donator_donation(?,?,?,?)");
    	$stmt->bindParam(1, $donator_details[0], PDO::PARAM_STR); 
		$stmt->bindParam(2, $donator_details[1], PDO::PARAM_STR);
		$stmt->bindParam(3, $donator_details[2], PDO::PARAM_STR);
		$stmt->bindParam(4, $project_title,      PDO::PARAM_STR);
		// call remove_donator stored procedure
		$stmt->execute();
    	$db->db = null;	
    }
}
