<?php
@session_start();

include_once 'database/Database.php';

class Project {
    
    public static function getAllActiveProjects() {
    	$db = new Database();
    	$sql = "select * from project where curdate() < expiry_datetime";
    	$rows['data'] = $db->db->query($sql);
    	$db->db = null;
    	return $rows;
    }
    
    public static function getProjectByTitle($project_title) {
    	$db = new Database();
    	$rows = $db->select("project", array('project_title'=>$project_title));
    	$project_details = '';
    	foreach($rows['data'] as $row) {  $project_details = $row; }
    	$db->db = null;
    	return $project_details;
    }
    
    public static function getProjectAndProposerDetailsByTitle($project_title) {
    	$db = new Database();
    	$rows = $db->select("project_and_proposer_details", array('title'=>$project_title));
    	$project_details = '';
    	foreach($rows['data'] as $row) {  $project_details = $row; }
    	$db->db = null;
    	return $project_details;
    }
    
    public static function getActiveProjectsAmount() {
    	$db = new Database();
    	$sql = "select count(*) as count from project where curdate() < expiry_datetime";
    	$rows['data'] = $db->db->query($sql);
    	$num_of_projects = -1;
    	foreach($rows['data'] as $row) {  $num_of_projects = $row['count'] ; }
    	$db->db = null;
        return $num_of_projects;
    }
    
    public static function getAllActiveProjectsByUsername($username) {
    	$db = new Database();
    	$sql = "select * from project where curdate() < expiry_datetime and project_proposer = '" . $username . "'";
    	$rows['data'] = $db->db->query($sql);
    	$db->db = null;
    	return $rows;
    }
    
    public static function getAllImgSourcesOfProject($project_title) {
    	$db = new Database();
    	$rows = $db->select("img_sources", array('project_title'=>$project_title));
    	$db->db = null;
    	return $rows;
    }
    
    public static function getVideoSource($project_title) {
    	$db = new Database();
    	$rows = $db->select("video_sources", array('project_title'=>$project_title));
    	$db->db = null;
    	return $rows;
    }

    public static function getPosterImgSourcesOfProject($project_title) {
    	$db = new Database();
    	$rows = $db->select("project", array('project_title'=>$project_title));
    	$db->db = null;
    	return $rows;
    }
    
	public static function insertNewProject($username) {
		$db = new Database();
        $project_title = $_POST['project_title'];
    	$project_description = $_POST['project_description'];
    	$requested_amount = $_POST['requested_amount'];
    	$datetime = $_POST['datetime'];
        $rows = $db->insert("project", array('project_title' => $project_title, 
										     'project_description'=> $project_description,
                                             'requested_amount' => $requested_amount,
                                             'amount_donated'=> 0,
                                             'donators_amount' => 0, 
											 'project_proposer'=> $username, 
										     'expiry_datetime' => $datetime), 
                                             array('project_title', 'project_description', 'requested_amount', 'amount_donated', 
											 'donators_amount', 'project_proposer', 'expiry_datetime'));
		$db->db = null;
        return $rows;
     }	
     
     public static function insertImgSourcesOfProject($img_src) {
		$db = new Database();
        $project_title = $_POST['project_title'];
        $rows = $db->insert("img_sources", array('project_title' => $project_title, 'image_source'=> $img_src), 
                                           array('project_title', 'image_source'));
        $db->db = null;
        return $rows;
     }
     
     public static function insertVideoSrcOfProject($video_src) {
		$db = new Database();
        $project_title = $_POST['project_title'];
        $rows = $db->insert("video_sources", array('project_title' => $project_title, 'video_src'=> $video_src), 
                                           array('project_title', 'video_src'));
        $db->db = null;
        return $rows;
     }
     
     public static function updatePosterImgSourceOfProject($poster_img_src) {
		$db = new Database();
        $project_title = $_POST['project_title'];
        $rows = $db->update("project", array('poster_img_src'=>$poster_img_src), array('project_title'=>$project_title), array('poster_img_src'));
        $db->db = null;                                             
        return $rows;
     }
     
     public static function updateProjectDetails($project_title, $new_project_description, $new_requested_amount, $new_datetime) {
		$db = new Database();
        $rows = $db->update("project", array('project_description'=>$new_project_description,
                                             'requested_amount'=>$new_requested_amount,
                                             'expiry_datetime'=>$new_datetime), 
                                              array('project_title'=>$project_title), 
                                              array('project_description', 'requested_amount', 'expiry_datetime'));
        $db->db = null;                                                                                  
        return $rows;
     }
}
?>
