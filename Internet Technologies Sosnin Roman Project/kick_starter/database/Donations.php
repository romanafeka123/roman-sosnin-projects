<?php
@session_start();

include_once 'database/Database.php';

class Donations {
    
    public static function donateToProject($project_title, $amount_to_donate, $firstname, $lastname, $email) {
    	$db = new Database();
    	// check whether this donator already donated to this project or not:
    	$rows = $db->select("project_donations", array('project_title'=>$project_title, 'firstname'=>$firstname, 'lastname'=>$lastname, 'email'=>$email));
    	$entry_exists = '';
    	foreach($rows['data'] as $row) {  $entry_exists = $row['project_title']; }
    	// this donator has not donated to the project yet, therefore inserting a new row to the project_donations table
    	if ($entry_exists == '') {   	
			$stmt = $db->db->prepare("CALL donate_to_project(?,?,?,?,?)");
			$stmt->bindParam(1, $project_title,    PDO::PARAM_STR); 
			$stmt->bindParam(2, $amount_to_donate, PDO::PARAM_INT);
			$stmt->bindParam(3, $firstname, PDO::PARAM_STR);
			$stmt->bindParam(4, $lastname, PDO::PARAM_STR);
			$stmt->bindParam(5, $email, PDO::PARAM_STR);
			// call donate_to_project stored procedure
			$stmt->execute();
    	}
    	else { 	// this donator already donated to this project, therefore just updating the details (not inserting)	
    		$stmt = $db->db->prepare("CALL update_donated_amount(?,?,?,?,?)");
			$stmt->bindParam(1, $project_title,    PDO::PARAM_STR); 
			$stmt->bindParam(2, $amount_to_donate, PDO::PARAM_INT);
			$stmt->bindParam(3, $firstname, PDO::PARAM_STR);
			$stmt->bindParam(4, $lastname, PDO::PARAM_STR);
			$stmt->bindParam(5, $email, PDO::PARAM_STR);
			// call donate_to_project stored procedure
			$stmt->execute();
    	}
    	$db->db = null;
    }
    
    public static function getDonatorDetails($donator_username) {
    	$db = new Database();
    	$donator_details = '';
    	$rows = $db->select("user_pass", array('username'=>$donator_username));
    	foreach($rows['data'] as $row) {  $donator_details = $row; }
    	$db->db = null;
        return $donator_details;	
    }
    
    public static function amountOfProjectsUserDonated($firstname, $lastname, $email) {
    	$db = new Database();
    	$sql = "select count(*) as count from project_donations where firstname = '" . $firstname . "' and lastname = '" . $lastname . "' and email = '" . $email . "'";                  
    	$rows['data'] = $db->db->query($sql);
    	$num_of_projects_donated = -1;
    	foreach($rows['data'] as $row) {  $num_of_projects_donated = $row['count'] ; }
    	$db->db = null;
        return $num_of_projects_donated;	
    }
    
    public static function getAllProjectsUserDonatedTo($firstname, $lastname, $email) {
    	$db = new Database();
    	$projects_donated_to = $db->select("project_donations", array('firstname'=>$firstname, 'lastname'=>$lastname, 'email'=>$email));
    	$db->db = null;
    	return $projects_donated_to;	
    }
    
    public static function amountDonatedToProjectByUsername($project_title, $firstname, $lastname, $email) {
    	$db = new Database();
    	$rows = $db->select("project_donations", array('project_title'=>$project_title, 'firstname'=>$firstname, 'lastname'=>$lastname, 'email'=>$email));
    	$amount_donated = -1;
    	foreach($rows['data'] as $row) {  $amount_donated = $row['amount_donated'] ; }
    	$db->db = null;
    	return $amount_donated;	
    }
    
    public static function getAllDonatorsByProjectTitle($project_title) {
    	$db = new Database();
    	$rows = $db->select("project_donations", array('project_title'=>$project_title));
    	$db->db = null;
    	return $rows;	
    }
    
    public static function getDonatorDetailsByUsername($donator_username) {
    	$db = new Database();
    	$rows = $db->select("user_pass", array('username'=>$donator_username));
    	$db->db = null;
    	return $rows;	
    }

    public static function getAllDonationsOfByProposerUsername($proposer_username) {
    	$db = new Database();
    	$rows = $db->select("project_donations", array('project_proposer_username'=>$proposer_username));
    	$db->db = null;
    	return $rows;	
    }
    
    public static function getAllDonationsByProjectTitle($project_title) {
    	$db = new Database();
    	$rows = $db->select("project_donations", array('project_title'=>$project_title));
    	$db->db = null;
    	return $rows;	
    }
}
?>
