<?php
include_once 'database/Project.php';
?>

<html>
  <head>
    <title> Donate to a project </title> 
    <link rel="stylesheet" type="text/css" href="styles/project_propose_style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <link rel="stylesheet" type="text/css" href="styles/donator_style.css" />	
  </head>
  <body ng-app="donateApp" ng-controller="donateController">
  <?php
	if (isset($_POST['project_title']) && isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email'])) {
		$project_details = Project::getProjectByTitle($_POST['project_title']);
		$all_img_sources = Project::getAllImgSourcesOfProject($project_details['project_title']);
    	echo "<center>";
        echo "<div class=\"proj_show\">";
        echo "<h3>" . $project_details['project_title'] . "</h3>";
        echo "<h4>" . $project_details['project_description'] . "</h4> <br>";
        foreach($all_img_sources['data'] as $src) {
        	echo "<img src=\"" . $src['image_source'] . "\" width=250 height=200>";
        }
        
        // show the video
        $video_src = Project::getVideoSource($project_details['project_title']);
        foreach($video_src['data'] as $src) {
        	$video_src =  $src['video_src'];
        }
        echo "<video width=\"320\" height=\"240\" controls>";
        echo "<source src=\"" . $video_src . "\" type=\"video/mp4\">";
        echo "Your browser does not support video!";
        echo "</video>";
        
        echo "<br>Donators amount: " . $project_details['donators_amount'] . "<br>";
	    echo "Desired sum: " . $project_details['requested_amount'] . "$<br>";
		echo "Amount donated: " . $project_details['amount_donated'] . "$<br>";
		echo round((($project_details['amount_donated']/$project_details['requested_amount'])*100)*100)/100 ."%<br>";
		echo "Expiry date: " . $project_details['expiry_datetime'] . "<br>";
        echo "</div>"; 
        echo "</center>";
        echo "<hr>";
        ?>

        <form action="donator_page.php" method="post" enctype="multipart/form-data" name="donateForm" novalidate>
		  <table align="center" class="table_color">
		    <tr>
			  <td colspan="2">
			  <h2 align="center"> Amount to donate: {{amount_to_donate + "$"}}</h2> <hr>
			  </td>
			</tr>

			 <tr>			   
			   <td width="95%" align=center> 
			     <input type="text"  size="20" name="amount_to_donate" placeholder="Enter amount here" ng-pattern="/^[0-9]+$/" ng-model="amount_to_donate" required/><br>
			     <input type="hidden" name="project_title" value="<?php echo $_POST['project_title'] ?>"/>	
			     <input type="hidden" name="firstname"     value="<?php echo $_POST['firstname'] ?>"/>
			     <input type="hidden" name="lastname"      value="<?php echo $_POST['lastname'] ?>"/>
			     <input type="hidden" name="email"         value="<?php echo $_POST['email'] ?>"/>	
			     <span style="color:red" ng-show="donateForm.amount_to_donate.$dirty && donateForm.amount_to_donate.$invalid">
     			   <span ng-show="donateForm.amount_to_donate.$error.required">{{warning1}}</span>
				   <span ng-show="donateForm.amount_to_donate.$error.pattern">{{warning2}}</span>
   			     </span>
			   </td>
			 </tr>

			  <tr>
			    <td colspan="2">
			      <hr>
			      <p align="center">
			        <input id="buttn" type="submit" class="btn btn-default btn-lg" value="Donate" ng-disabled="donateForm.amount_to_donate.$invalid"/> 		      
			      </p>
			      <hr/>		   
			    </td>
			  </tr>
            </table>
    	  </form>
        <?php } ?>

    <script>
	    var app = angular.module('donateApp', []);
	      
	    app.controller('donateController', function($scope) {
		    $scope.warning1 = 'Please enter amount to donate!';
		    $scope.warning2 = 'Please enter only positive numbers!';
	    });	
    </script>	
  </body>
</html>