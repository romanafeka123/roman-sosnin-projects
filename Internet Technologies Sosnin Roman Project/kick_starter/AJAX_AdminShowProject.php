<?php
@session_start();
include_once 'database/Project.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// getting sent data(arguments) from the angularJS-Ajax request
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$projectTitle = $request->projectTitle;

$theProject = Project::getProjectAndProposerDetailsByTitle($projectTitle);

$response= "";
if (!empty($theProject['title'])) {
	$response.= '{"title":"'.    $theProject['title'] . '",';
	$response.= '"proposer_username":"'.  $theProject['proposer_username'] . '",';
	$response.= '"proposer_firstname":"'.  $theProject['proposer_firstname'] . '",';
	$response.= '"proposer_lastname":"'.    $theProject['proposer_lastname'] . '",';
	$response.= '"proposer_email":"'.    $theProject['proposer_email'] . '",';
	$response.= '"requested_amount":"'.    $theProject['requested_amount'] . '",';
	$response.= '"amount_donated":"'.    $theProject['amount_donated'] . '",';
	$response.= '"expiry_datetime":"'.   $theProject['expiry_datetime'] . '"}';	
}
else {
	$response.= '{"title":"not found",';
	$response.= '"proposer_username":"-",';
	$response.= '"proposer_firstname":"-",';
	$response.= '"proposer_lastname":"-",';
	$response.= '"proposer_email":"-",';
	$response.= '"requested_amount":"-",';
	$response.= '"amount_donated":"-",';
	$response.= '"expiry_datetime":"-"}';	
}
$response='{"records":['.$response.']}';
echo($response);
?>