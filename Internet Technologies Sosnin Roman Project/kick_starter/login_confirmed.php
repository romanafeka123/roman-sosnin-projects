<?php
@session_start();
include_once 'database/User.php';

$message='';
$url = "login.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$the_category = $_POST['category'];
	$message = User::checkSignIn();
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && empty($message)) {  
	$_SESSION['username'] = $_POST['username'];
	$message = "Login successful!";
	if ($the_category == 'Proposer')
		$url = "project_propose_page.php";
	else if ($the_category == 'Donator')
		$url = "donator_page.php";
	else if ($the_category == 'Admin')
		$url = "admin_page.php";
}
if ($message == "This username wasn't registered!") {
	include 'login.php';
	echo '<center> <span style="color:red">' . $message . '</span> </center>';
}
else {
	echo "<script> alert('$message'); window.location.href='$url';</script>";
}

?>