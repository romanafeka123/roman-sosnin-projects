<?php 

include_once 'database/Project.php';

define('NUM_OF_DIGITS_IN_TITLE', 40);
define('NUM_OF_DIGITS_IN_LINE_OF_TITLE', 25);
define('NUM_OF_DIGITS_IN_DESCRIPTION', 80);
define('NUM_OF_DIGITS_IN_LINE_OF_DESC', 27);

$project_number = 0; 
?>

<html>
  <head>
    <title>Project donating website</title>
    <link rel="stylesheet" type="text/css" href="styles/main_page_style.css" />
    <link rel="stylesheet" type="text/css" href="styles/donator_style.css" />
  </head>
<body id="myclass2" align="center">
	<?php 
		$num_of_projects = Project::getActiveProjectsAmount();
		$rows = Project::getAllActiveProjects();		
	?>		

	<h1 align="center"> Welcome to project donating website! </h1>
	<h2 align="center"> <?php echo "There are currently " . $num_of_projects . " live projects <br>"; ?> </h2>
	<span id="myclass1"> <button type="button" class="btn btn-primary btn-lg" onclick="location.href = 'signup.php';">Sign up</button> </span>
	<span id="myclass1"> <button type="button" class="btn btn-primary btn-lg" onclick="location.href = 'login.php';">Log in</button> </span>
	<div id="wrap">
	
	 <?php 				
	   foreach($rows['data'] as $row) { 	
	       $project_number++; 					
		   if ($project_number % 2 == 0) {
		       echo "<div class=\"green1\">";
		   }
		   else {
		       echo "<div class=\"green2\">";
		   }					
		   ?>											
		   <form action="show_project.php" method="post">		     		 		
	       <?php 			
		   $poster_img_src = Project::getPosterImgSourcesOfProject($row['project_title']);
		   echo "<center>";
		   foreach($poster_img_src['data'] as $src) { 
		       echo "<img src=\"" . $src['poster_img_src'] . "\" width=\"120\" height=\"120\"></img>";
           }
           echo "</center>";	
        			
           $title_len = strlen($row['project_title']);
           $title_partly = substr($row['project_title'], 0, NUM_OF_DIGITS_IN_TITLE);	      			
           if ($title_len > NUM_OF_DIGITS_IN_TITLE) {
               echo "<h4 align=\"center\">" . $row['project_title'] . "...</h4>" . "";
           }		
		   else {
		       echo "<h4 align=\"center\">" . $row['project_title'] . "</h4>" . "";
		   }
		   if ($title_len <= NUM_OF_DIGITS_IN_LINE_OF_TITLE) {
		       echo "<br>";
		   }
									
		   $description_partly = substr($row['project_description'], 0, NUM_OF_DIGITS_IN_DESCRIPTION);
		   echo "<p id=\"monospace\">" . $description_partly . "...</p>";
		   if (strlen($description_partly) < NUM_OF_DIGITS_IN_LINE_OF_DESC) {
		       echo "<br><br><br><br>";
		   }
		   else if (strlen($description_partly) >= NUM_OF_DIGITS_IN_LINE_OF_DESC && strlen($description_partly) < NUM_OF_DIGITS_IN_LINE_OF_DESC*2) {
			  echo "<br><br><br>";
		   }
		   else if (strlen($description_partly) >= NUM_OF_DIGITS_IN_LINE_OF_DESC*2 && strlen($description_partly) < NUM_OF_DIGITS_IN_LINE_OF_DESC*3){
		       echo "<br><br>";
		   }
		   else {
		       echo "<br>";
		   }
		   echo "<center>";
	       echo "Donators amount: " . $row['donators_amount'] . "<br>";
	       echo "Requested amount: " . $row['requested_amount'] . "$<br>";
		   echo "Donated amount: " . $row['amount_donated'] . "$<br>";
	       echo round((($row['amount_donated']/$row['requested_amount'])*100)*100)/100 ."%<br>";
	       echo "Expiry date: " . $row['expiry_datetime'] . "<br>";  
	       ?>
		   <input type="submit" class="btn btn-default btn-lg" value="Read more..."/>
		   <input type="hidden" name="project_title" value="<?php echo $row['project_title'] ?>"/>		
		 </center>
	   </form>
	 </div>
	<?php } ?>
  </body>
</html>