<?php
@session_start();
include_once 'database/Donations.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$all_donations = Donations::getAllDonationsByProjectTitle($_SESSION['project_title']);

$response= "";
foreach($all_donations['data'] as $donation) {
	if($response!= "") {
	   $response.= ",";
	}
	$response.= '{"donator_firstname":"'. $donation['firstname'] . '",';
	$response.= '"donator_lastname":"'.   $donation['lastname'] . '",';
	$response.= '"donator_email":"'.      $donation['email'] . '",';
	$response.= '"amount_donated":"'.     $donation['amount_donated'] . '"}';
}
$response='{"records":['.$response.']}';
echo($response);
?>