<?php
@session_start();
include_once 'database/Project.php';
include_once 'database/Donations.php';

define('NUM_OF_DIGITS_IN_TITLE', 40);
define('NUM_OF_DIGITS_IN_LINE_OF_TITLE', 25);
define('NUM_OF_DIGITS_IN_DESCRIPTION', 80);
define('NUM_OF_DIGITS_IN_LINE_OF_DESC', 27);

$username = $_SESSION['username'];

// Donating to a project
if (isset($_POST['amount_to_donate']) && isset($_POST['project_title']) && isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) &&
   !empty($_POST['amount_to_donate']) && !empty($_POST['project_title']) && !empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['email'])) {
    	Donations::donateToProject($_POST['project_title'], $_POST['amount_to_donate'], $_POST['firstname'], $_POST['lastname'], $_POST['email']);    					
		$message = "Your donation was submitted successfully!";
		echo "<script> alert('$message'); </script>";
}
?>
<html>
  <head>
    <title>Project donator page</title>
      <link rel="stylesheet" type="text/css" href="styles/main_page_style.css" />
      <link rel="stylesheet" type="text/css" href="styles/donator_style.css" />	
  </head>
    <body id="myclass2">                              
      <h1 align="center"> <?php echo $username ?>, Welcome to your project donating page! </h1>
   
    <?php 
       $donator_details = Donations::getDonatorDetails($username);
       $num_of_projects_donated = Donations::amountOfProjectsUserDonated($donator_details['firstname'], $donator_details['lastname'], $donator_details['email']);
       if ($num_of_projects_donated > 0) {
   	       if ($num_of_projects_donated == 1) {
   		       echo "<h2 align=\"center\"> You donated to " . $num_of_projects_donated . " project: </h2>";
   	       }
   	       else {
   		       echo "<h2 align=\"center\"> You donated to " . $num_of_projects_donated . " projects: </h2>";
   	       }
   	
           $projects_donated_to = Donations::getAllProjectsUserDonatedTo($donator_details['firstname'], $donator_details['lastname'], $donator_details['email']);
           foreach($projects_donated_to['data'] as $row) { 		
               $all_img_sources = Project::getAllImgSourcesOfProject($row['project_title']);
               $project_details = Project::getProjectByTitle($row['project_title']);
               $donator_amount_donated = Donations::amountDonatedToProjectByUsername($row['project_title'], $donator_details['firstname'], $donator_details['lastname'], $donator_details['email']);
               echo "<center>";
               echo "<div class=\"proj_show\">";
               echo "<h3>" . $project_details['project_title'] . "</h3>";
               echo "<h4>" . $project_details['project_description'] . "</h4> <br>";
               foreach($all_img_sources['data'] as $src) {
        	        echo "<img src=\"" . $src['image_source'] . "\" width=250 height=200>";
               }
        	
               // show the video
               $video_src = Project::getVideoSource($project_details['project_title']);
               foreach($video_src['data'] as $src) {
                   $video_src =  $src['video_src'];
               }
               echo "<video width=\"320\" height=\"240\" controls>";
               echo "<source src=\"" . $video_src . "\" type=\"video/mp4\">";
               echo "Your browser does not support video!";
               echo "</video>";
        
               echo "<br>Donators amount: " . $project_details['donators_amount'] . "<br>";
		       echo "Requested amount: " . $project_details['requested_amount'] . "$<br>";
		       echo "Total donated amount: " . $project_details['amount_donated'] . "$<br>";
		       echo "You donated: " . $donator_amount_donated . "$<br>"; 
		       echo round((($project_details['amount_donated']/$project_details['requested_amount'])*100)*100)/100 ."%<br>";
		       echo "Expiry date: " . $project_details['expiry_datetime'] . "<br>";
               echo "</div>"; 
               echo "</center>";			
               echo "<br>";     
               echo "<hr size=5 noshade>";  		
           }
       }  
   ?>
   
    <h2 align="center"> Choose a project to donate to: </h2>
    <div id="wrap">
    <?php 		
       $project_number = 0;
       $num_of_projects = Project::getActiveProjectsAmount();
	   $rows = Project::getAllActiveProjects();			
		          
	   foreach($rows['data'] as $row) { 	     		 	
	       $project_number++; 					
		   if ($project_number % 2 == 0) {
		       echo "<div class=\"green1\">";
		   }
		   else {
		       echo "<div class=\"green2\">";
		   }	
    ?>											
	<form action="donate.php" method="post">     		 		
	  <?php 			
	     $poster_img_src = Project::getPosterImgSourcesOfProject($row['project_title']);
		 echo "<center>";
		 foreach($poster_img_src['data'] as $src) { 
		     echo "<img src=\"" . $src['poster_img_src'] . "\" width=\"120\" height=\"120\"></img>";
         }
         echo "</center>";					
					
		 $title_len = strlen($row['project_title']);
         $title_partly = substr($row['project_title'], 0, NUM_OF_DIGITS_IN_TITLE);	      			
         if ($title_len > NUM_OF_DIGITS_IN_TITLE) {
             echo "<h4 align=\"center\">" . $row['project_title'] . "...</h4>" . "";
         }		
		 else {
		     echo "<h4 align=\"center\">" . $row['project_title'] . "</h4>" . "";
		 }
		 if ($title_len <= NUM_OF_DIGITS_IN_LINE_OF_TITLE) {
		     echo "<br>";
		 }
					
		 $description_partly = substr($row['project_description'], 0, NUM_OF_DIGITS_IN_DESCRIPTION);
	     echo "<p id=\"monospace\">" . $description_partly . "...</p>";
		 if (strlen($description_partly) < NUM_OF_DIGITS_IN_LINE_OF_DESC) {
		     echo "<br><br><br><br>";
		 }
    	 else if (strlen($description_partly) >= NUM_OF_DIGITS_IN_LINE_OF_DESC && strlen($description_partly) < NUM_OF_DIGITS_IN_LINE_OF_DESC*2) {
	         echo "<br><br><br>";
	     }
	     else if (strlen($description_partly) >= NUM_OF_DIGITS_IN_LINE_OF_DESC*2 && strlen($description_partly) < NUM_OF_DIGITS_IN_LINE_OF_DESC*3){
		     echo "<br><br>";
		 }
	     else {
		     echo "<br>";
	     }
	     echo "<center>";
	     echo "Donators amount: " . $row['donators_amount'] . "<br>";
	     echo "Requested amount: " . $row['requested_amount'] . "$<br>";
	     echo "Donated amount: " . $row['amount_donated'] . "$<br>";
	     echo round((($row['amount_donated']/$row['requested_amount'])*100)*100)/100 ."%<br>";
         echo "Expiry date: " . $row['expiry_datetime'] . "<br>";  
	     ?>
	       <input type="submit" class="btn btn-default btn-lg" value="Donate"/>
		   <input type="hidden" name="project_title" value="<?php echo $row['project_title'] ?>"/>	
		   <input type="hidden" name="firstname"     value="<?php echo $donator_details['firstname'] ?>"/>
		   <input type="hidden" name="lastname"      value="<?php echo $donator_details['lastname'] ?>"/>
		   <input type="hidden" name="email"         value="<?php echo $donator_details['email'] ?>"/>
	     </center>
	   </form>
     </div>														
   <?php } ?>
   </div>
 </body>
</html>