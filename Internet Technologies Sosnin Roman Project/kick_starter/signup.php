<?php
@session_start();
?>
<html>
  <head>
    <title>Signup</title>
    <link rel="stylesheet" type="text/css" href="styles/login_signup_style1.css" />
	<link rel="stylesheet" type="text/css" href="styles/login_signup_style2.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
  </head>
  <body align="center">
    <div class="container" ng-app="signupApp" ng-controller="signupController">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="login-panel panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Please Sign Up</h3>
            </div>
            <div class="panel-body">
              <form action="signup_confirmed.php" method="post" role="register_form" name="signup" novalidate>
                <fieldset>
                  <div class="form-group">
                    <input class="form-control" placeholder="First name" name="firstname" type="text" autofocus ng-model="firstname" required>
                  </div>
                  <div class="form-group">
                    <input class="form-control" placeholder="Last name" name="lastname" type="text" ng-model="lastname" required>
                  </div>
                  <div class="form-group">
                    <input class="form-control" placeholder="Email" name="email" type="text" ng-model="email" required>
                  </div>
                  <div class="form-group">
                    <input class="form-control" placeholder="Username" name="username" type="text" ng-model="username" required>
                  </div>
                  <div class="form-group">
                    <input class="form-control" placeholder="Password" name="password" type="password" ng-model="password" required>
                  </div>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select align="center" type="select" name="category" class="btn btn-success"> 
                    <option value="Proposer" selected> Project proposer </option>
			        <option value="Donator"> Donator </option>
				    <option value="Admin" > Admin </option>                                   
                  </select>                                  	
                  <br><br>
                  <input type="button" class="btn btn-lg btn-success btn-block" ng-click="reset()" value="Reset"></input>
                  <input type="submit" class="btn btn-lg btn-success btn-block" value="Sign Up" ng-disabled="signup.firstname.$invalid || signup.lastname.$invalid || signup.email.$invalid || signup.username.$invalid || signup.password.$invalid"></input>
                </fieldset>
               
                <center>
                  <span style="color:red" ng-show="signup.firstname.$dirty && signup.firstname.$invalid">
                    <br><span ng-show="signup.firstname.$error.required">Please enter first name!</span>
   	              </span>

   	              <span style="color:red" ng-show="signup.lastname.$dirty && signup.lastname.$invalid">
                    <br><span ng-show="signup.lastname.$error.required">Please enter last name!</span>
   	              </span>
   	                        
   	              <span style="color:red" ng-show="signup.email.$dirty && signup.email.$invalid">
                    <br><span ng-show="signup.email.$error.required">Please enter email!</span>
   	              </span>
   	                        
   	              <span style="color:red" ng-show="signup.username.$dirty && signup.username.$invalid">
                    <br><span ng-show="signup.username.$error.required">Please enter user name!</span>
   	              </span>
   	                        
   	              <span style="color:red" ng-show="signup.password.$dirty && signup.password.$invalid">
                    <br><span ng-show="signup.password.$error.required">Please enter password!</span>
   	              </span>
   	            </center>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <script>
		var signupApp = angular.module("signupApp", []);
		signupApp.controller('signupController', function($scope) {
  		 $scope.reset = function(){
				$scope.firstname = "";
				$scope.lastname = "";
				$scope.email = "";
				$scope.username = "";
				$scope.password = "";
   		}   
  		 $scope.reset();
		});
	</script>
  </body>
</html>