<?php
@session_start();
include_once 'database/Project.php';
include_once 'database/Thumbs.php';
include_once 'database/Donations.php';
include_once 'database/User.php';

define('TEMP_PICS_SRC', 'temp_pics/');
define('FINAL_PICS_SRC', 'pictures/');
define('FINAL_VIDEO_SRC', 'videos/');
define('THUMBS_SIZE', 250);
$username = $_SESSION['username'];

// Submitting a new project
if (isset($_POST['project_title']) && isset($_POST['project_description']) && isset($_POST['requested_amount']) && isset($_POST['datetime']) && 
   !empty($_POST['project_title']) && !empty($_POST['project_description']) && !empty($_POST['requested_amount']) && !empty($_POST['datetime'])) {
   		
   		$error_occured = 0;
   		
   		if (isset($_FILES['files']['name'])) {
   			foreach ($_FILES['files']['name'] as $f => $name) {
   				if ($_FILES['files']['name'][$f] == "") {
   					$error_occured = 1;
   					$message = 'You must provide pictures and video clip for the project!';
					$url = "project_propose_page.php";
   					echo "<script> alert('$message'); window.location.href='$url';</script>";	
   				}
   			}
   		}
   		if (isset($_FILES['videofile']['name'])) {
			foreach ($_FILES['videofile']['name'] as $f => $name) {
				if ($_FILES['videofile']['error'][$f] > 0) {
					$error_occured = 1;
					$message = 'You must provide pictures and video clip for the project!';
					$url = "project_propose_page.php";
   					echo "<script> alert('$message'); window.location.href='$url';</script>";
				} 
			}
		}
   		
   		$check_if_project_title_already_exists = Project::getProjectByTitle($_POST['project_title']);
   		if (isset($check_if_project_title_already_exists['project_title'])) {
   			$message = 'Project with such title already exists!';
			$url = "project_propose_page.php";
   			echo "<script> alert('$message'); window.location.href='$url';</script>";
   		}
   			
   		else if ($error_occured == 0){   		
    		$result = Project::insertNewProject($username);
    		$mask_for_poster_img_src = 1;
    		$poster_img_src = '';
    		$final_src = '';
    		
    		// these settings are for max upload file - in order to let videos get uploaded
    		ini_set('upload_max_filesize', '200M');
			ini_set('post_max_size', '200M');
			ini_set('max_input_time', 3000);
			ini_set('max_execution_time', 3000);	
		
    		// uploading the pictures to the server
    		// making a new directory: pictures/project_title
			mkdir(FINAL_PICS_SRC . $_POST['project_title']);
			$final_src = FINAL_PICS_SRC . $_POST['project_title'] . "/";
			if (isset($_FILES['files']['name'])) {				
				foreach ($_FILES['files']['name'] as $f => $name) {
					if ($_FILES['files']['name'][$f] != "") {
						$allowedExts= array("gif", "jpeg", "jpg", "png");
						$temp = explode(".", $_FILES['files']['name'][$f]);				
						$extension = end($temp);
						// max picture size is 4mb. the picture must have one of the following extensions
						if (($_FILES['files']['size'][$f] < 4000000) && in_array($extension, $allowedExts)) {				
							if ($_FILES['files']['error'][$f] > 0) {
								echo "Error: " . $_FILES['files']['error'][$f] . "<br>";
							} 
							else {																						
								// moving to the temporary folder (and after that, making thumbs in the final folder)							
								@move_uploaded_file($_FILES['files']['tmp_name'][$f], TEMP_PICS_SRC . $_FILES['files']['name'][$f]);					
								Project::insertImgSourcesOfProject($final_src . $_FILES['files']['name'][$f]);		
								// this condition is for making the poster_img_src as the first uploaded picture		
								if ($mask_for_poster_img_src == 1) {
								$poster_img_src = $final_src . $_FILES['files']['name'][$f];
									$mask_for_poster_img_src++;						
								}			
							}
						}
					}
				}
				Thumbs::createThumbs(TEMP_PICS_SRC, $final_src, THUMBS_SIZE);
			
				// deleting all pictures from the temp directory
				$files = glob(TEMP_PICS_SRC . "*"); // get all file names
				foreach($files as $file) { // iterate files
  					if(is_file($file))
    					unlink($file); // delete file
				}
			}		
		
			// uploading the video to the server
			// making a new directory: videos/project_title
			mkdir(FINAL_VIDEO_SRC . $_POST['project_title']);
			$final_video_src = FINAL_VIDEO_SRC . $_POST['project_title'] . "/";
			if (isset($_FILES['videofile']['name'])) {
				foreach ($_FILES['videofile']['name'] as $f => $name) {
					if ($_FILES['videofile']['error'][$f] > 0) {
						echo "Error: " . $_FILES['videofile']['error'][$f] . "<br>";
					} else {																						
						// moving to the video src package							
						move_uploaded_file($_FILES['videofile']['tmp_name'][$f], $final_video_src . $_FILES['videofile']['name'][$f]);					
						Project::insertVideoSrcOfProject($final_video_src . $_FILES['videofile']['name'][$f]);					
					}
				}
			}
		
			$message = '';
			if($result) {
				Project::updatePosterImgSourceOfProject($poster_img_src);
				$message = "Your project was submitted successfully!";
			} else {
				$message = "Error in submitting a new project!";	
			}
			echo "<script> alert('$message'); </script>";
   		} // closing the first else
}

// Updating an existing project
else if (isset($_POST['new_project_description']) && isset($_POST['new_requested_amount']) && isset($_POST['new_datetime']) && 
        !empty($_POST['new_project_description']) && !empty($_POST['new_requested_amount']) && !empty($_POST['new_datetime'])) {
    	$rows = Project::updateProjectDetails($_POST['project_title'],  
    								  		  $_POST['new_project_description'], 
    								  		  $_POST['new_requested_amount'], 
    								  		  $_POST['new_datetime']);
    	$message = '';
    	if ($rows) {
    		$message = "The project was updated successfully!";
    	}
    	else {
    		$message = "Error in updating the project!";;
    	}
    	echo "<script> alert('$message'); </script>";						  			
}

// Removing an existing donator from the donators list
else if (isset($_POST['donators']) && isset($_POST['project_title']) 
     && !empty($_POST['donators']) && !empty($_POST['project_title'])){
	$rows = User::removeDonatorDonation($_POST['donators'] ,$_POST['project_title']);
    $message = "The donator was removed successfully!";
    echo "<script> alert('$message'); </script>";
}

// Adding a new donator for the chosen project
if (isset($_POST['amount_to_donate']) && isset($_POST['project_title']) && isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) &&
   !empty($_POST['amount_to_donate']) && !empty($_POST['project_title']) && !empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['email'])) {
   		User::insertNewDonator();
    	Donations::donateToProject($_POST['project_title'], $_POST['amount_to_donate'], $_POST['firstname'], $_POST['lastname'], $_POST['email']);    					
		$message = "The donation was submitted successfully!";
		echo "<script> alert('$message'); </script>";
}
?>
<html>
  <head>
    <title> Project proposer page </title>
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="calendar_jquery/jquery.simple-dtpicker.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <link type="text/css" href="calendar_jquery/jquery.simple-dtpicker.css" rel="stylesheet" /> 
    <link rel="stylesheet" type="text/css" href="styles/project_propose_style.css" />
    <link rel="stylesheet" type="text/css" href="styles/admin_style.css" />
  
    <script type="text/javascript">
		$(function(){
			$('*[id=datetime]').appendDtpicker();
		});
    </script>
  </head>
    <body ng-app="proposerApp">          
      <div ng-controller="newProjectController">                 
        <h1 align="center"> <?php echo $username ?>, Welcome to your project proposing page! </h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" name="newProject" novalidate>
	      <table align="center" class="table_color" >

	        <tr>
	          <td colspan="2">
	          <h2 align="center"> Propose a new project </h2> <hr>
	          </td>
	        </tr>

	        <tr>	
	          <td width="75%" align="center"> 
	            <div class="col-md-4 col-md-offset-4">
	              <input class="form-control" type="text"  size="34" name="project_title" placeholder="Enter title here" ng-minlength="5" ng-maxlength="50" ng-model="project_title" required/>
	              <textarea cols="31" rows="7"  name="project_description" 	 placeholder="Enter description here" ng-minlength="20" ng-maxlength="500" ng-model="project_description" required></textarea><br>
	              <input class="form-control"type="text"  size="34" name="requested_amount" placeholder="Enter sum here" ng-pattern="/^[0-9]+$/" ng-model="requested_amount" required/>	
	              <input class="form-control"type="text"  size="34" name="datetime" id="datetime"  placeholder="Enter date and time here"  ng-model="datetime" required/><br>
	              &nbsp; &nbsp Upload Image #1: <input name="files[]" type="file" class="btn btn-default btn-lg"> </input> <br>	
  	              &nbsp; &nbsp Upload Image #2: <input name="files[]" type="file" class="btn btn-default btn-lg"> </input> <br>
  	              &nbsp; &nbsp Upload Image #3: <input name="files[]" type="file" class="btn btn-default btn-lg"> </input> <br>
  	              &nbsp; &nbsp Upload VideoClip:<input name="videofile[]" type="file" class="btn btn-default btn-lg"> </input> <br>
  	            </div>
	            </td>
	          </tr>

	          <tr>
	            <td colspan="2">
	            <hr>
	            <p align="center">
	            <input class="btn btn-default btn-lg" id="buttn" type="submit" value="Submit" ng-disabled="newProject.project_title.$invalid || newProject.project_description.$invalid || newProject.requested_amount.$invalid || newProject.datetime.$invalid" /> 
	            <input class="btn btn-default btn-lg" type="button" ng-click="reset()" value="Reset"></input>
	
                <span style="color:red" ng-show="newProject.project_title.$dirty && newProject.project_title.$invalid">
                  <br><span ng-show="newProject.project_title.$error.required">Please enter title!</span>
                  <span ng-show="newProject.project_title.$error.minlength">Title should contain at least 5 letters!</span>
                  <span ng-show="newProject.project_title.$error.maxlength">Title should contain maximum 50 letters!</span>
   	            </span>

   	            <span style="color:red" ng-show="newProject.project_description.$dirty && newProject.project_description.$invalid">
                  <br><span ng-show="newProject.project_description.$error.required">Please enter description!</span>
                  <span ng-show="newProject.project_description.$error.minlength">Description should contain at least 20 letters!</span>
                  <span ng-show="newProject.project_description.$error.maxlength">Description should contain maximum 500 letters!</span>
   	            </span>
   	
   	            <span style="color:red" ng-show="newProject.requested_amount.$dirty && newProject.requested_amount.$invalid">
                  <br><span ng-show="newProject.requested_amount.$error.required">Please enter amount!</span>
                  <span ng-show="newProject.requested_amount.$error.pattern">{{amount_warning}}</span>
   	            </span>
   	
   	            <span style="color:red" ng-show="newProject.datetime.$dirty && newProject.datetime.$invalid">
                  <br><span ng-show="newProject.datetime.$error.required">Please enter date & time!</span>
   	            </span> 
   	          <div>
	          <hr/>
	        </p>
	      </td>
	    </tr>
      </table>
    </form>
    <br><br>
    
    <!-- angularJS DB access: -->
    <h2 align="center"> Your donators: </h2> <hr>
   
	<div ng-controller="donatorsCtrl">	  
	  <table align="center" border=1 cellpadding=2 cellspacing=2 >
		<tr>
	  	  <th width=250 align="center">	Project title</th>
      	  <th width=150 align="center">	First name	</th>
      	  <th width=150 align="center">	Last name	</th>
      	  <th width=200 align="center">	Email		</th>
      	  <th width=150 align="center">	Amount donated</th>
   	    </tr>
      	<tr ng-repeat="x in donators">
         <td align="center"><b> {{ x.project_title}}   </b></td>
         <td align="center"><b> {{ x.firstname}}       </b></td>
         <td align="center"><b> {{ x.lastname}}        </b></td>
         <td align="center"><b> {{ x.email}}           </b></td>
         <td align="center"><b> {{ x.amount_donated}}  </b></td>    
         </tr>
      </table>    
	</div>

    <?php
    	$username = $_SESSION['username'];	
        $all_active_projects = Project::getAllActiveProjectsByUsername($username);
        if (!empty($all_active_projects['data'])) {
        	echo "<h2 align=\"center\"> Your existing active projects: </h2> <hr>";	
        	foreach($all_active_projects['data'] as $row) { 		
        		// show an existing project:
        		$all_img_sources = Project::getAllImgSourcesOfProject($row['project_title']);
        		echo "<center>";
        		echo "<div class=\"proj_show_at_proposer\">";
        		echo "<h3>" . $row['project_title'] . "</h3>";
        		echo "<h4>" . $row['project_description'] . "</h4> <br>";
        		// show all the pics
        		foreach($all_img_sources['data'] as $src) {
        			echo "<img src=\"" . $src['image_source'] . "\" width=250 height=200>";
        		}
        		echo "<br><br>";
        		// show the video
        		$video_src = Project::getVideoSource($row['project_title']);
        		foreach($video_src['data'] as $src) {
        			$video_src =  $src['video_src'];
        		}
        		echo "<video width=\"320\" height=\"240\" controls>";
                echo "<source src=\"" . $video_src . "\" type=\"video/mp4\">";
                echo "Your browser does not support video!";
                echo "</video>";
                
        		echo "<br>Donators amount: " . $row['donators_amount'] . "<br>";
			   	echo "Requested amount: " . $row['requested_amount'] . "$<br>";
			   	echo "Donated amount : " . $row['amount_donated'] . "$<br>";
			    echo round((($row['amount_donated']/$row['requested_amount'])*100)*100)/100 ."%<br>";
			    echo "Expiry date: " . $row['expiry_datetime'] . "<br>";
			    // if there is at least 1 donation for this project, enter this
			    if ($row['amount_donated'] != 0) {
			    	echo "<form action=" . $_SERVER['PHP_SELF'] . " method=\"post\" name=\"myform4\">";
			    	echo "All donators:<br>";
			    	echo "<select align=\"center\" type=\"select\" name=\"donators\">";
			    	$all_donators = Donations::getAllDonatorsByProjectTitle($row['project_title']);
			    	foreach($all_donators['data'] as $donator) {  
			    		echo "<option value=" . $donator['firstname'] . "_" . $donator['lastname'] . "_" . $donator['email'] . ">" . $donator['firstname'] . " " . $donator['lastname'] . " " . $donator['email'] . "</option>";
			    	}
			    	echo "</select>&nbsp;&nbsp;";		   
			    	echo "<input type=\"hidden\" name=\"project_title\" value=\"" . $row['project_title'] . "\"></input>";
			  
			    	echo "<input id=\"buttn3\" type=\"submit\" value=\"Remove donator\" />"; 
			    	echo "</form>";
			    }	
			    else {
			    	echo "There were no donations done for this project yet.<br>";
			    }	    
			    ?>
			    
				<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="addDonatorForm" ng-controller="addDonatorController" novalidate>
				  <hr>
				  <h3>Enter new donator details for this project:</h3>
				  <div class="col-md-4 col-md-offset-4">
				    <input class="form-control" type="text" size="33" name="firstname"       placeholder="Enter first name here" ng-minlength="2" ng-maxlength="30" ng-model="firstname" required/>	
				    <input class="form-control" type="text" size="33" name="lastname"        placeholder="Enter last name here"  ng-minlength="2" ng-maxlength="30" ng-model="lastname" required/>
				    <input class="form-control" type="email" size="33" name="email"           placeholder="Enter email here"     ng-minlength="10" ng-maxlength="30" ng-model="email" required/>
				    <input class="form-control" type="text" size="33" name="amount_to_donate"placeholder="Enter amount here"     ng-pattern="/^[0-9]+$/" ng-model="amount_to_donate" required/>
				    <input type="hidden"         name="project_title"   value="<?php echo $row['project_title'] ?>"/>
				    <input class="btn btn-default btn-lg" id="buttn3" type="submit" value="Submit donate" ng-disabled="addDonatorForm.firstname.$invalid || addDonatorForm.lastname.$invalid || addDonatorForm.email.$invalid || addDonatorForm.amount_to_donate.$invalid" /> 
				    <input class="btn btn-default btn-lg" type="button" ng-click="reset()" value="Reset"></input>
				  </div>
				  <span style="color:red" ng-show="(addDonatorForm.firstname.$dirty && addDonatorForm.firstname.$invalid) || (addDonatorForm.lastname.$dirty && addDonatorForm.lastname.$invalid) || (addDonatorForm.email.$dirty && addDonatorForm.email.$invalid) || (addDonatorForm.amount_to_donate.$dirty && addDonatorForm.amount_to_donate.$invalid)">
                    <br><br><br><br><br><br><br><br><br>
                    <span ng-show="addDonatorForm.firstname.$error.required || addDonatorForm.lastname.$error.required || addDonatorForm.email.$error.required || addDonatorForm.amount_to_donate.$error.required">Please fill all fields!</span>
                    <span ng-show="addDonatorForm.amount_to_donate.$error.pattern">{{amount_warning}}</span>
                    <span ng-show="addDonatorForm.email.$error.email">Please enter valid email!</span>
                    <span ng-show="addDonatorForm.firstname.$error.minlength || addDonatorForm.lastname.$error.minlength">FirstName & LastName should contain at least 2 letters!</span>
                    <span ng-show="addDonatorForm.email.$error.minlength">Email should contain at least 10 letters!</span>
      			    <span ng-show="addDonatorForm.firstname.$error.maxlength || addDonatorForm.lastname.$error.maxlength || addDonatorForm.email.$error.maxlength">FirstName & LastName & Email should contain maximum 30 letters!</span>
   	              </span> 	                        
				</form>
				
				<?php
        		echo "</div>"; 
        		echo "</center>";
        		echo "<br>";
        		?>
        		
        		<!--  Editing an existing project: -->
        		<center>
        		  <div class="proj_edit">
        		    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="editProjectForm" ng-controller="editProjectController" novalidate>     		
        		      <table align="center">
				        <tr>
				          <td colspan="2">
				            <h2 align="center"> Edit the project: <?php echo $row['project_title'] ?> </h2> <hr>
				          </td>
				        </tr>
				        <tr>   		
        		          <td width="75%" align=center> 
        		            <div class="col-md-4 col-md-offset-4">
				              <textarea cols="30" rows="7"  name="new_project_description" placeholder="Enter new description here" ng-minlength="20" ng-maxlength="500" ng-model="new_project_description" required></textarea><br>
				              <input class="form-control" type="text"  size="120" name="new_requested_amount" placeholder="Enter new sum here" ng-pattern="/^[0-9]+$/" ng-model="new_requested_amount" required/>	
				              <input class="form-control" type="text"  size="120" name="new_datetime"	placeholder="Enter date and time here" id="datetime" ng-model="new_datetime" required/>
				              <input type="hidden" name="project_title" value="<?php echo $row['project_title'] ?>"/>
				            </div>
				          </td>				
				        </tr>				
				        <tr>
				          <td colspan="2">
				          <hr>
				          <p align="center">
				            <input class="btn btn-default btn-lg" id="buttn2" type="submit" value="Submit" ng-disabled="editProjectForm.new_project_description.$invalid || editProjectForm.new_requested_amount.$invalid || editProjectForm.new_datetime.$invalid" /> 
				            <input class="btn btn-default btn-lg" type="button" ng-click="reset()" value="Reset"></input>
				
				            <span style="color:red" ng-show="(editProjectForm.new_project_description.$dirty && editProjectForm.new_project_description.$invalid) || (editProjectForm.new_requested_amount.$dirty && editProjectForm.new_requested_amount.$invalid) || (editProjectForm.new_datetime.$dirty && editProjectForm.new_datetime.$invalid)">
                              <br><span ng-show="editProjectForm.new_project_description.$error.required || editProjectForm.new_requested_amount.$error.required || editProjectForm.new_datetime.$error.required">Please fill all fields!</span>
                              <span ng-show="editProjectForm.new_requested_amount.$error.pattern">{{amount_warning}}</span>
                              <span ng-show="editProjectForm.new_project_description.$error.minlength">Description should contain at least 20 letters!</span>
     			              <span ng-show="editProjectForm.new_project_description.$error.maxlength">Description should contain maximum 500 letters!</span>
   	                        </span>  	            
				          <hr/>
				          </p>
				        </td>
				       </tr>
    			     </table>
        		   </form>
        		  <?php
        		  echo "</div>"; 
        		  echo "</center>";
        		  echo "<hr size=5 noshade>";
        	    }
           }
        ?>
        
    <script>
	var app = angular.module('proposerApp', []);
	app.controller('donatorsCtrl', function($scope, $http) {	
		$http.get("AJAX_ShowDonators.php").success(function(response) {
			$scope.donators= response.records;
		});
	});
	
	app.controller('newProjectController', function($scope) {
		 $scope.amount_warning = 'Amount should contain only positive numbers!';
		
  		 $scope.reset = function(){
				$scope.project_title = "";
				$scope.project_description = "";
				$scope.requested_amount = "";
				$scope.datetime = "";
   		}   
  		 $scope.reset();
	});	
	
	app.controller('addDonatorController', function($scope) {
  		 $scope.reset = function(){
				$scope.firstname = "";
				$scope.lastname = "";
				$scope.email = "";
				$scope.amount_to_donate = "";
   		}   
  		 $scope.reset();
	});
	
	app.controller('editProjectController', function($scope) {
  		 $scope.reset = function(){
				$scope.new_project_description = "";
				$scope.new_requested_amount = "";
				$scope.new_datetime = "";
   		}   
  		 $scope.reset();
	});    
	</script>
 </body>
</html>
	