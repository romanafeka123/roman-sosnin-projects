__author__ = 'Roman, Sosnin, ID:324731199'

#---------------------------------------------------Contact class------------------------------------------------------
class Contact:
    name = ""

    def __init__(self, olderContact = None):
        if olderContact is not None:
            if isinstance(olderContact, Contact):
                self.name = olderContact.name
                if hasattr(olderContact, 'cellPhone'):
                    self.cellPhone = olderContact.cellPhone

    def __str__(self):
        if hasattr(self, 'cellPhone'):
            return 'Name:' + self.name + ', Cell Phone:' + self.cellPhone
        else:
            return 'Name:' + self.name

    def __lt__(self, other):
        return self.name < other.name

    def ReadValues(self, commandType):
        while True: # reading the name
            try:
                if commandType == 'Command1':
                    inputName = str(input("Name:"))
                    if inputName == "":
                        raise Exception("")
                elif commandType == 'Command3':
                    inputName = str(input("Name(" + self.name + "):"))
                    if inputName == "x":
                        raise Exception("")
            except:
                if inputName == "x":
                    print ("Name cannot be deleted.")
                else:
                    print ("Name cannot be empty.")
            else:
                break

        while True: # reading the cellphone
            try:
                if commandType == 'Command1':
                    inputCellphone = str(input("Cell Phone:"))
                    if inputCellphone != "" and not inputCellphone.isdigit():
                        raise Exception("")

                elif commandType == 'Command3' and hasattr(self, 'cellPhone'):
                    inputCellphone = str(input("Cell Phone(" + self.cellPhone + "):"))
                    if inputCellphone != "" and not inputCellphone.isdigit() and inputCellphone != 'x':
                        raise Exception("")

                else:
                    inputCellphone = str(input("Cell Phone:"))
                    if inputCellphone != "" and not inputCellphone.isdigit() and inputCellphone != 'x':
                        raise Exception("")
            except:
                print("Cell phone can be digits only or empty.")
            else:
                break

        if inputName != "":
            self.name = inputName
        if inputCellphone != "":
            self.cellPhone = inputCellphone
        if inputCellphone == 'x':
            del self.cellPhone

    def Match(self, inputString):
        if inputString in self.name:
            return True
        if hasattr(self, 'cellPhone') and inputString in str(self.cellPhone):
            return True
        return False

#-----------------------------------------------FriendContact class-----------------------------------------------------
class FriendContact(Contact):

    def __init__(self, olderContact = None):
        if olderContact is not None:
            super().__init__(olderContact)
            if isinstance(olderContact, FriendContact):
                if hasattr(olderContact, 'homePhone'):
                    self.homePhone = olderContact.homePhone
                if hasattr(olderContact, 'personalEmail'):
                    self.personalEmail = olderContact.personalEmail

    def __str__(self):
        theString = ''
        if hasattr(self, 'homePhone'):
            theString = theString + ', Home Phone:' + self.homePhone
        if hasattr(self, 'personalEmail'):
            theString = theString + ', Personal Email:' + self.personalEmail
        return super().__str__() + theString

    def ReadValues(self, commandType):
        self.ReadContactValues1(commandType)
        self.ReadFriendContactValues(commandType)

    def ReadContactValues1(self,commandType):
        super().ReadValues(commandType)

    def ReadFriendContactValues(self, commandType):
        while True: # reading the home phone
            try:
                if commandType == 'Command1':
                    inputHomePhone = str(input("Home Phone:"))
                    if inputHomePhone != "" and not inputHomePhone.isdigit():
                        raise Exception("")

                elif commandType == 'Command3' and hasattr(self, 'homePhone'):
                    inputHomePhone = str(input("Home Phone(" + self.homePhone + "):"))
                    if inputHomePhone != "" and not inputHomePhone.isdigit() and inputHomePhone != 'x':
                        raise Exception("")

                else:
                    inputHomePhone = str(input("Home Phone:"))
                    if inputHomePhone != "" and not inputHomePhone.isdigit() and inputHomePhone != 'x':
                        raise Exception("")
            except:
                print("Home phone can be digits only or empty.")
            else:
                break

        while True: # reading the personal email
            try:
                if commandType == 'Command1':
                    inputPersEmail = str(input("Personal Email:"))
                    if inputPersEmail != "" and (not inputPersEmail .__contains__('@') or not inputPersEmail .__contains__('.')):
                        raise Exception("")

                elif commandType == 'Command3' and hasattr(self, 'personalEmail'):
                    inputPersEmail = str(input("Personal Email(" + self.personalEmail + "):"))
                    if inputPersEmail != "" and (not inputPersEmail .__contains__('@') or not inputPersEmail .__contains__('.')) and inputPersEmail != 'x':
                        raise Exception("")

                else:
                    inputPersEmail = str(input("Personal Email:"))
                    if inputPersEmail != "" and (not inputPersEmail .__contains__('@') or not inputPersEmail .__contains__('.')) and inputPersEmail != 'x':
                        raise Exception("")
            except:
                print("Personal Email should contain @ and . or to be empty.")
            else:
                break

        if inputHomePhone != "":
            self.homePhone = inputHomePhone
        if inputPersEmail != "":
            self.personalEmail = inputPersEmail
        if inputHomePhone == 'x':
            del self.homePhone
        if inputPersEmail == 'x':
            del self.personalEmail

    def Match(self, inputString):
        if super().Match(inputString):
            return True
        if hasattr(self, 'homePhone') and inputString in str(self.homePhone):
            return True
        if hasattr(self, 'personalEmail') and inputString in str(self.personalEmail):
            return True
        return False

#--------------------------------------------ProfessionalContact class--------------------------------------------------
class ProfessionalContact(Contact):

    def __init__(self, olderContact = None):
        if olderContact is not None:
            super().__init__(olderContact)
            if isinstance(olderContact, ProfessionalContact):
                if hasattr(olderContact, 'workPhone'):
                    self.workPhone = olderContact.workPhone
                if hasattr(olderContact, 'workEmail'):
                    self.workEmail = olderContact.workEmail

    def __str__(self):
        theString = ''
        if hasattr(self, 'workPhone'):
            theString = theString + ', Work Phone:' + self.workPhone
        if hasattr(self, 'workEmail'):
            theString = theString + ', Work Email:' + self.workEmail
        return super().__str__() + theString

    def ReadValues(self,commandType):
        self.ReadContactValues2(commandType)
        self.ReadProfessionalContactValues(commandType)

    def ReadContactValues2(self,commandType):
        super().ReadValues(commandType)

    def ReadProfessionalContactValues(self,commandType):
        while True: # reading the work phone
            try:
                if commandType == 'Command1':
                    inputWorkPhone = str(input("Work Phone:"))
                    if inputWorkPhone != "" and not inputWorkPhone.isdigit():
                        raise Exception("")

                elif commandType == 'Command3' and hasattr(self, 'workPhone'):
                    inputWorkPhone = str(input("Work Phone(" + self.workPhone + "):"))
                    if inputWorkPhone != "" and not inputWorkPhone.isdigit() and inputWorkPhone != 'x':
                        raise Exception("")

                else:
                    inputWorkPhone = str(input("Work Phone:"))
                    if inputWorkPhone != "" and not inputWorkPhone.isdigit() and inputWorkPhone != 'x':
                        raise Exception("")
            except:
                print("Work phone can be digits only or empty.")
            else:
                break

        while True: # reading the work email
            try:
                if commandType == 'Command1':
                    inputWorkEmail = str(input("Work Email:"))
                    if inputWorkEmail != "" and (not inputWorkEmail .__contains__('@') or not inputWorkEmail .__contains__('.')):
                        raise Exception("")

                elif commandType == 'Command3' and hasattr(self, 'workEmail'):
                    inputWorkEmail = str(input("Work Email(" + self.workEmail + "):"))
                    if inputWorkEmail != "" and (not inputWorkEmail .__contains__('@') or not inputWorkEmail .__contains__('.')) and inputWorkEmail != 'x':
                        raise Exception("")

                else:
                    inputWorkEmail = str(input("Work Email:"))
                    if inputWorkEmail != "" and (not inputWorkEmail .__contains__('@') or not inputWorkEmail .__contains__('.')) and inputWorkEmail != 'x':
                        raise Exception("")
            except:
                print("Work Email should contain @ and . or to be empty.")
            else:
                break

        if inputWorkPhone != "":
            self.workPhone = inputWorkPhone
        if inputWorkEmail != "":
            self.workEmail = inputWorkEmail
        if inputWorkPhone == 'x':
            del self.workPhone
        if inputWorkEmail == 'x':
            del self.workEmail

    def Match(self, inputString):
        if super().Match(inputString):
            return True
        if hasattr(self, 'workPhone') and inputString in str(self.workPhone):
            return True
        if hasattr(self, 'workEmail') and inputString in str(self.workEmail):
            return True
        return False

#-------------------------------------------ProfessionalFriendContact class---------------------------------------------
class ProfessionalFriendContact(ProfessionalContact, FriendContact):

    def __init__(self, olderContact = None):
        if olderContact is not None:
            super().__init__(olderContact)

    def __str__(self):
        return super().__str__()

    def ReadValues(self,commandType):
        super().ReadContactValues1(commandType)
        super().ReadFriendContactValues(commandType)
        super().ReadProfessionalContactValues(commandType)

    def Match(self, inputString):
        return super().Match(inputString)




































