__author__ = 'Roman, Sosnin, ID:324731199'
import contact

class PhoneBook:
    contactsList = []

    def Start(self):
        while True:
            self.printMenu()
            try:
                inputFromUser = int(input("-->"))
                if inputFromUser < 1 or inputFromUser > 6:
                    raise Exception()
            except:
                print("Please enter a number between 1 and 6!")
                print("")
            else:
                if inputFromUser == 1:
                    self.addNewContact()
                elif inputFromUser == 2:
                    self.showAllContacts()
                elif inputFromUser == 3:
                    self.editContact()
                elif inputFromUser == 4:
                    self.findContact()
                elif inputFromUser == 5:
                    self.deleteContact()
                elif inputFromUser == 6:
                    break

    def printMenu(self):
        print("\nWhat would you like to do?")
        print("1 - Add a new contact")
        print("2 - Show all contacts")
        print("3 - Edit a contact")
        print("4 - Find a contact")
        print("5 - Delete a contact")
        print("6 - Exit")

    # Command 1
    def addNewContact(self):
        inputFromUser = self.readFromUserTypeOfContact()

        # name and cellphone should be input in any case...
        inputContact = None
        if inputFromUser == 'S':
            inputContact = contact.Contact()
        elif inputFromUser == 'F':
            inputContact = contact.FriendContact()
        elif inputFromUser == 'P':
            inputContact = contact.ProfessionalContact()
        elif inputFromUser == 'B':
            inputContact = contact.ProfessionalFriendContact()

        inputContact.ReadValues('Command1')
        self.contactsList.append(inputContact)
        self.contactsList.sort()

     # Command 2
    def showAllContacts(self):
        for index in range(len(self.contactsList)):
            print("contact number #%d:" % (index+1), end = " ")
            print(self.contactsList[index])

     # Command 3
    def editContact(self):
        contactsListIndicesRange = [index+1 for index in range(len(self.contactsList))]
        while True:
            try:
                inputContactNumber = int(input("Enter a valid number of the contact you wish to edit:"))
                if inputContactNumber not in contactsListIndicesRange :
                    raise Exception("")
            except:
                print("Please enter a valid number of contact. Number of contacts is:",len(self.contactsList))
            else:
                break

        inputFromUser = self.readFromUserTypeOfContact()
        print("For the following fields click enter if there's no change, a new value if you want to replace the field, "
              "or x if you want to delete the field (the name field cannot be deleted).")
        inputContact = None
        if inputFromUser == 'S':
            inputContact = contact.Contact(self.contactsList[inputContactNumber-1])
        elif inputFromUser == 'F':
            inputContact = contact.FriendContact(self.contactsList[inputContactNumber-1])
        elif inputFromUser == 'P':
            inputContact = contact.ProfessionalContact(self.contactsList[inputContactNumber-1])
        elif inputFromUser == 'B':
            inputContact = contact.ProfessionalFriendContact(self.contactsList[inputContactNumber-1])

        inputContact.ReadValues('Command3')
        self.contactsList[inputContactNumber-1] = inputContact
        self.contactsList.sort()

     # Command 4
    def findContact(self):
        while True:
            inputFromUser = str(input("Type contact details (name, phone, email):"))
            if inputFromUser != "":
                break
        for index in range(len(self.contactsList)):
            if self.contactsList[index].Match(inputFromUser):
                print("contact number #%d:" % (index+1), end = " ")
                print(self.contactsList[index])

     # Command 5
    def deleteContact(self):
        if len(self.contactsList) == 0:
            print("Number of contacts in the phonebook is 0! There's nobody to delete!")
        else:
            while True:
                try:
                    indexToDelete = int(input("Enter a valid number of the contact you wish to delete:"))
                    if indexToDelete <= 0 or indexToDelete > len(self.contactsList):
                        raise Exception("")
                except:
                    print("Please enter a valid number of contact to delete. Number of contacts is:",len(self.contactsList))
                else:
                    break
            del self.contactsList[indexToDelete-1]

    def readFromUserTypeOfContact(self):
        acceptedInput = ['S', 'F', 'P', 'B']
        while True:
            try:
                inputFromUser = str(input("Should this contact be Simple (S), Friend (F), Professional (P) or Both (B)?"))
                if inputFromUser not in acceptedInput:
                    raise Exception("")
            except:
                print("Please enter one of the letters: 'S', 'F', 'P', 'B'")
            else:
                return inputFromUser

if __name__ == '__main__':
    myPhoneBook = PhoneBook()
    myPhoneBook.Start()
