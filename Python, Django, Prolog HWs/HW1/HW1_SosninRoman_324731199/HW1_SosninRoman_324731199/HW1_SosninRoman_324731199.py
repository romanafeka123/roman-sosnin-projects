__author__ = 'Roman Sosnin, ID: 324731199'

import functools
# Question 1
def IsPalindrome(theString):
    for index in range(len(theString)):
        if theString[index] != theString[len(theString) - index - 1]:
            return False
    return True

# Question 2
def char_freq(theString):
    d = {}
    for theChar in theString:
        if theChar not in d:
            d[theChar] = 1
        else:
            d[theChar] += 1
    return d

# Question 3
def Reverse(theList):
    for index in range(len(theList)//2):  # this for loop runs in O(n) complexity
        tempObj = theList[len(theList) - index - 1]
        theList[len(theList) - index - 1] = theList[index]
        theList[index] = tempObj

# Question 4
def CreateOccurancesList(theList):
    newList = [theList[index] for index in range(len(theList)) for numOfOccurances in range(0,index)]
    return newList

# Question 5
def SumOfOdds(list1, list2):
    res = functools.reduce(lambda x,y: x + y, list(filter(lambda x: x%2 != 0, [list2[index] for index in list1])))
    return res

if __name__ == '__main__':
    #1
    print('Question #1:')
    print('is radar palindrome?', IsPalindrome('radar'))
    print('is radmar palindrome?', IsPalindrome('radmar'))
    #2
    print('\nQuestion #2:')
    print(char_freq("abbabcbdbabdbdbabababcbcbab"))
    print(char_freq("abbcccdddd"))
    #3
    print('\nQuestion #3:')
    words = ['a', 'b', 'c', 'd', 'e']
    print('Original list:', words)
    Reverse(words)
    print('Reversed list:',words)
    #4
    print('\nQuestion #4:')
    newList = CreateOccurancesList([5, 4, 3, 2, 1])
    print(newList)
    newList = CreateOccurancesList(['a', 'b', 'c', 'd'])
    print(newList)
    #5
    print('\nQuestion #5:')
    print("the res is:", SumOfOdds([0, 2, 4],[6, 7, 11, 13, 17, 22]))














