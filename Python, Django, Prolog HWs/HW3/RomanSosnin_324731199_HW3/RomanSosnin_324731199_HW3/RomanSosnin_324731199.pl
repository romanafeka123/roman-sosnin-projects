% Sosnin Roman 324731199, Eliad Yalin 300081312

/* 1 */

word(astante, a, s, t, a, n, t, e).
word(astoria, a, s, t, o, r, i, a).
word(baratto, b, a, r, a, t, t, o).
word(cobalto, c, o, b, a, l, t, o).
word(pistola, p, i, s, t, o, l, a).
word(statale, s, t, a, t, a, l, e).

crossword(V1,V2,V3,H1,H2,H3) :-
 word(V1, _, V12, _, V14, _, V16, _),
 word(V2, _, V22, _, V24, _, V26, _),
 word(V3, _, V32, _, V34, _, V36, _),
 word(H1, _, V12, _, V22, _, V32, _),
 word(H2, _, V14, _, V24, _, V34, _),
 word(H3, _, V16, _, V26, _, V36, _),
 V1 \= H1.

/* 2 */

swap(leaf(XNode),leaf(XNode)).
swap(tree(XNode1,YNode1),tree(YNode2,XNode2)) :- swap(XNode1,XNode2), swap(YNode1,YNode2).

/* 3 */

swapFirstLast([Last|[]],[First],First,Last).

swapFirstLast([Match|Tail1],[Match|Tail2],First,Last):- swapFirstLast(Tail1,Tail2,First,Last).
swapFirstLast([Head1|Tail1],[Head2|Tail2]):- swapFirstLast(Tail1,Tail2,Head1,Head2).

/* 4 */

directTrain(ako,naharia).
directTrain(haifa,ako).
directTrain(jerusalem,haifa).
directTrain(tel_aviv,jerusalem).
directTrain(petah_tiqva,tel_aviv).
directTrain(modiin,petah_tiqva).
directTrain(natanya,modiin).

route(From, To) :- directTrain(From, To); directTrain(To, From).

travelBetween(From, To, [To]) :- route(From,To).
travelBetween(From, Thru, [To | R]) :- directTrain(From, To), travelBetween(To, Thru, R).

myRoute(From, To, [From | R]) :- travelBetween(From, To, R).
myRoute(From, To, [To | R]) :- travelBetween(To, From, R).

route(From, To, R) :- myRoute(From, To, RevR), reverse(RevR, R).
